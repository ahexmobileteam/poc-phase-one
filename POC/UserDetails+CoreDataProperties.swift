//
//  UserDetails+CoreDataProperties.swift
//  
//
//  Created by Ajeet N on 11/05/20.
//
//

import Foundation
import CoreData


extension UserDetails {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserDetails> {
        return NSFetchRequest<UserDetails>(entityName: "UserDetails")
    }

    @NSManaged public var baseURL: String?
    @NSManaged public var sessionID: String?
    @NSManaged public var empID: String?
    @NSManaged public var dcsID: String?

}
