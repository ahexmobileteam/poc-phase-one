//
//  APIs.swift
//  POC
//
//  Created by Ajeet N on 31/03/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//


import Foundation

struct API {
    
    static let ConfigURL = "http://poc.aquilasoftware.com/pocextacc-webservices/telephony/telephonyCheck"
    static let BASEURL = "http://poc.aquilasoftware.com/pocextacc-webservices_new/telephony/"
    static let BASEURLNEW = "http://poc.aquilasoftware.com/pocextacc-webservices_new/telephony/"
    static let BASEURLPHASE2 = "http://poc.aquilasoftware.com/pocextacc-webservices_phase2/telephony/"

    

    
    static let LoginURL = "authenticateTelephonyUser?empCode="
    static let DCSPIN = "&dcsPin="
    static let SessionID = "&sessionId="
    static let AuthenticationURL =  "authenticateDCS?empCode="
    static let CertificationsURL = "getCertifications?dcsId="
    static let profileAPI = "getProfile?dcsId="
    static let PreferencesAPI = "getPreferences?dcsId="
    static let ScheduleExceptionsAPI = "getExceptions?dcsId="
    static let SchedulesAPI = "getSchedulesForDCS?dcsId="
    static let searchSchedulesAPI = "getSchedules?dcsId="
    static let PSID = "&psId="
    static let StartDate = "&startDate="
    static let EndDate = "&endDate="
    //MARK:- Schedules
    static let GROUPSERVICES = "getArrivalGroupedServices?dcsId="
    static let CREATEREPORTEDVISIT = "createReportedVisitToSchedule?jsonObj="
    static let DEPARTUREDETAILS = "getDepartureDetails?officeId="
  
        
    static let SearchSchedule = "getSchedules?dcsId="
    static let getListOfPS = "getPSList?dcsId="
    static let getPSOfficesList = "getOfficesListForDCS?dcsId="
    static let getSpecialTransaction = "getSpecialTransactions?sessionId="
    static let getListOFPSListByOfficeID = "getPSListByOffice?officeId="
    static let SaveSpecialTransacation = "saveSpecialTransaction?imei=&dcsId="
    static let psTelecode = "&psTelCode="
    static let officeID = "&officeId="
    static let SpecialTransacationID = "&specialTransactionTypeId="
    static let dataPrompt = "&dataPrompt="
    static let latitude = "&latitude="
    static let longitude = "&longitude="
    static let address = "&address="
    static let ipAddress = "&ipAddress="
    static let ConsumedHours = "getConsumedUnitsForPS?visitDetailsId="
    static let dcsID = "&dcsId="
    
    
  static let GetTimeSheet = "getTimeSheet?dcsId="
  static let GetCallConfigURL = "getCallConfigDetails?officeId="
  static let GETPSOTPANDSERVICEDETAILS = "getPSOTPAndServiceDetails?psId="
    

   static let ValidateSingleTransacation = "validateSingleTransaction?imei=&dcsEmpCode="
   static let PSMRN = "&psMRN="
   static let TeleCode = "&telcode="
   static let Mileage = "&mileage="
   static let TravelTime = "&travelTime="
   static let TasksCode = "&taskCodes="
   static let TransactionType = "&transactionType="
   static let TransacationDate = "&transactionDate="
   static let IpAddress = "&ipAddress="
   static let offFiled = "&offieId="

    
    static let checkMultipleServcie = "getMultipleServicesCount?dcsId="
    static let ServiceId = "&serviceId="
    static let visitDetailID = "&visitDetailsId="
    static let punchTypeID = "&punchTypeFlag="

    
    static let conseutiveURL = "getConsecutiveServices?consecutiveVisitIds="
    static let saveConseutiveURL = "saveConsecutiveTransactions?imei="
    static let visitDetailArrry = "&visitDetailsArray="
    
    
    static let getTasksList = "getTaskList?serviceTelCode="
    static let taskName = "&taskName="
    static let singleTaskList = "getTaskList?officeId="
    static let serviceTeleCode =  "&serviceTelCode="
    static let saveScheduleProcessing = "saveScheduleProcessing?imei="
    
    static let arrDeptId = "&arrDeptId="
    static let ValidatDepartureDetails = "validateDepartureDetails?psMRN="

    
    static let CreateManualPunchForClockIn = "createManualPanch?jsonObj="
    
    static let SaveGroupedTransactions = "saveGroupedTransactions?imei="
    
    static let VisitDetailArrayWithJsonObj = "&visitDetailsArray="
    
    static let WeeklyTimeCardWithDCSID = "getPSListForWeeklyTimeCard?&dcsId="
    static let WeekStart = "&weekStart="
    
    static let GetWeeklyList = "getPSWeeklyTimeCard?&dcsId="
    static let SaveConseutive = "saveConsecutiveTransactionsNew?imei"
    
    static let  GetPsWeeklyTimeCardTasks  = "getPSWeeklyTimeCardTasks?jsonObj="
    
    static let  SavePushNotificationToken =  "saveTokenDetails?&deviceTypeId=2"
    static let DEVICEID = "&deviceId="
    static let TokenID = "&tokenId="
    
    static let updateDCSCalling = "updateDCSCallInFlag?dcsId="
    
    static let resetPin = "resetDCSPin?&dcsId="
    static let empCode = "&empCode="
    static let pinCode = "&dcsPin="
}
