//
//  Constants.swift
//  POC
//
//  Created by Ajeet N on 31/03/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import Foundation
import UIKit
import SwiftKeychainWrapper
import CoreData
let keychain  = KeychainWrapper.standard
let defaults = UserDefaults.standard
let deviceId = UIDevice.current.identifierForVendor?.uuidString


let kBaseURL = "SavedBaseURL"
let kSessionID = "sessionId"
let KdcsID =  "dcsId"
let kUserName = "UserName"
let kSession = "Session"
//MARK: Get User Saved Details
struct GetUserDetails {
   

    static let baseURL = keychain.string(forKey: kBaseURL)
    static let sessionID = keychain.string(forKey: kSessionID)
    static let dcsIDSaved = keychain.string(forKey: KdcsID)
    static let latitude = defaults.string(forKey: kLatitude)
    static let longitude = defaults.string(forKey: kLongitude)
}

let deviceID = UIDevice.current.identifierForVendor?.uuidString
let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

let kStartDate  = "AlertStartDate"
let kEndDate  = "AlertEndDate"
let kApplicationID = "SearchApplicarionID"
let kApplicationIDName = "SearchApplicarionIDName"
let kSearchBy = "SearchBy"

let kLatitude  = "Latitude"
let kLongitude = "Longitude"

