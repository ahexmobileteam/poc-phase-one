//
//  NetworkConnectivity.swift
//  POC
//
//  Created by Ajeet N on 18/03/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import Foundation
import Alamofire

struct Connectivity {
    static let sharedInstance = NetworkReachabilityManager()!
    static var isConnectedToInternet:Bool {
        return self.sharedInstance.isReachable
    }
}



