//
//  ServiceManager.swift
//  POC
//
//  Created by Ajeet N on 18/03/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class ServiceManager: NSObject {
    
    static let shared = ServiceManager()
    
    func request<T:Decodable>(type:T.Type, url:String, method: HTTPMethod, view:UIView, parameters: [String:Any], completion completionHandler:@escaping(T?) -> Void) {
         if Connectivity.isConnectedToInternet {
            view.isUserInteractionEnabled = false
            var activityIndicator = UIActivityIndicatorView()
            DispatchQueue.main.async {
                activityIndicator = self.showActivityIndicator(view: view)
            }
            guard let url = URL(string: url) else { return }
            let headers = ["Content-Type": "application/json","Accept":"application/json"]
            let configuration = URLSessionConfiguration.default
            configuration.timeoutIntervalForResource = TimeInterval(30)
            configuration.timeoutIntervalForRequest = TimeInterval(30)
            let sessionManager = Alamofire.SessionManager(configuration: configuration)
            sessionManager.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                switch (response.result) {
                    case .success:
                        guard let json = response.data else { return }
                        do{
                            let result = try JSONDecoder().decode(T.self, from: json)
                            DispatchQueue.main.async {
                                view.isUserInteractionEnabled = true
                                self.removeActivityindicator(indicator: activityIndicator)
                            }
                            completionHandler(result)
                        }catch{
                            print(error)
                            
                            DispatchQueue.main.async {
                                view.isUserInteractionEnabled = true
                                self.removeActivityindicator(indicator: activityIndicator)
                            }
                            self.toast(msgString: Errors.decode.rawValue, view: view)
                        }
                        break
                    case .failure( _):
                        DispatchQueue.main.async {
                            view.isUserInteractionEnabled = true
                            self.removeActivityindicator(indicator: activityIndicator)
                    
                        }
                         self.toast(msgString: "Failed to connect server, Please try again..!", view: view)
                        NotificationCenter.default.post(name: Notification.Name("SessionExpired"), object: nil)
                        
                        break
                        }
                }.session.finishTasksAndInvalidate()
                
       } else {
        self.toast(msgString: Errors.noInternet.rawValue, view: view)

       }
    }
    
    
    func request<T:Decodable>(type:T.Type, url: String, method: HTTPMethod, view:UIView, completion completionHandler:@escaping(T?) -> Void) {
         if Connectivity.isConnectedToInternet {
            var activityIndicator = UIActivityIndicatorView()
            print("The url in service manager is :\(String(describing: url))")
            DispatchQueue.main.async {
                view.isUserInteractionEnabled = false
                activityIndicator = self.showActivityIndicator(view: view)
            }
            guard let url = URL(string: url) else { return }
            let headers = ["Content-Type": "application/json","Accept":"application/json"]
            let configuration = URLSessionConfiguration.default
            configuration.timeoutIntervalForResource = TimeInterval(30)
            configuration.timeoutIntervalForRequest = TimeInterval(30)
            let sessionManager = Alamofire.SessionManager(configuration: configuration)
            sessionManager.request(url, method: method, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                switch (response.result) {
                    case .success:
                        guard let json = response.data else { return }
                        do{
                            let result = try JSONDecoder().decode(T.self, from: json)
                            DispatchQueue.main.async {
                                view.isUserInteractionEnabled = true
                                self.removeActivityindicator(indicator: activityIndicator)
                            }
                            completionHandler(result)
                        }catch{
                            print(error)
                            DispatchQueue.main.async {
                                view.isUserInteractionEnabled = true
                                self.removeActivityindicator(indicator: activityIndicator)
                            }
                            self.toast(msgString: Errors.decode.rawValue, view: view)
                        }
                        break
                    case .failure( _):
                        DispatchQueue.main.async {
                            self.removeActivityindicator(indicator: activityIndicator)
                        }
                        self.toast(msgString: "Failed to connect server, Please try again..!", view: view)
                        NotificationCenter.default.post(name: Notification.Name("SessionExpired"), object: nil)
                        break
                        }
                }.session.finishTasksAndInvalidate()
                
       } else {
            self.toast(msgString: Errors.noInternet.rawValue, view: view)
       }
    }
    
    func request(url: String, method: HTTPMethod, view:UIView, completion completionHandler:@escaping(Int) -> Void) {
         if Connectivity.isConnectedToInternet {
            var activityIndicator = UIActivityIndicatorView()
            print("The url in service manager is :\(String(describing: url))")
            DispatchQueue.main.async {
                view.isUserInteractionEnabled = false
                activityIndicator = self.showActivityIndicator(view: view)
            }
            guard let url = URL(string: url) else { return }
            let headers = ["Content-Type": "application/json","Accept":"application/json"]
            let configuration = URLSessionConfiguration.default
            configuration.timeoutIntervalForResource = TimeInterval(30)
            configuration.timeoutIntervalForRequest = TimeInterval(30)
            let sessionManager = Alamofire.SessionManager(configuration: configuration)
            sessionManager.request(url, method: method, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                switch (response.result) {
                    case .success:
                            DispatchQueue.main.async {
                                view.isUserInteractionEnabled = true
                                self.removeActivityindicator(indicator: activityIndicator)
                            }
                            if let statusCode = response.response?.statusCode{
                               completionHandler(statusCode)
                            }
                        break
                    case .failure( _):
                        DispatchQueue.main.async {
                            view.isUserInteractionEnabled = true
                            self.removeActivityindicator(indicator: activityIndicator)
                        }
                        completionHandler(404)
                        self.toast(msgString: "Failed to connect server, Please try again..!", view: view)
                        break
                        }
                }.session.finishTasksAndInvalidate()
                
       } else {
            self.toast(msgString: Errors.noInternet.rawValue, view: view)
       }
    }
}
