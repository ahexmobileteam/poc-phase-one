//
//  CloutOutTasksController.swift
//  POC
//
//  Created by Admin on 13/05/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

typealias ListOfTasksModel = [TasksModel
]
struct TasksModel: Decodable {
    let taskCode: String?
    let carePlanFlag: Int?
    let taskCodeAndName: String?
    let taskName: String?
    let taskId: Int?
}

class CloutOutTasksController: UIViewController {
    
    @IBOutlet weak var taskListTableView: UITableView!{
        didSet{
            self.taskListTableView.delegate = self
            self.taskListTableView.dataSource = self
        }
    }
    
    @IBOutlet weak var alertHeaderLabel: UILabel!
    @IBOutlet weak var transferentView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    internal var groupService: SchedulesResponseModel?
    private var tasksList: ListOfTasksModel?
    private var hadleSelection = [HandleSelection]()
    internal var receivedOtpFlag: String?
    internal var milageFlag: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        transferentView.isHidden = true
        navigationItem.title = "Tasks"
        getTasks()
    }
    private func getTasks(){
        let userDetails = self.getuserDetails()
        var url = String()
        url = userDetails.baseURL ?? ""
        url.append("getTaskList?officeId=\(groupService?.officeID ?? 0)")
        url.append("&sessionId=\(userDetails.sessionID ?? "")&taskName=&visitDetailsId=\(groupService?.visitDetailsID ?? 0)&psTelCode=\(groupService?.psID ?? 0)&serviceTelCode=\(groupService?.serviceCode ?? 0)")
        print(url)
        ServiceManager.shared.request(type: ListOfTasksModel.self, url: url, method: .get, view: self.view) { [weak self](response) in
            if response?.count ?? 0 !=  0{
                self?.tasksList = response
                for _ in 0..<response!.count {
                    self?.hadleSelection.append(HandleSelection(isTapped: false, isSelected: false))
                }
                DispatchQueue.main.async {
                    self?.taskListTableView.reloadData()
                }
            }
        }
    }
    
    @IBAction func save_action(_ sender: Any) {
        var taskCodes = [Int]()
        for index in 0..<hadleSelection.count {
            if hadleSelection[index].isTapped{
                if hadleSelection[index].isSelected{
                    taskCodes.append(self.tasksList?[index].taskId ?? 0)
                }
            }
        }
       // let selected = hadleSelection.filter{ $0.isTapped == true }.filter{ $0.isSelected == true }
        if taskCodes.count == 0{
            self.toast(msgString: "Select task to proceed", view: self.view)
            return
        }
        self.getAddressFromLatLons(pdblLatitude: GetUserDetails.latitude ?? "", withLongitude: GetUserDetails.longitude ?? "") { (address) in
            var url = String()
            let userDetails = self.getuserDetails()
            url.append(userDetails.baseURL ?? "")
            let latitude = GetUserDetails.latitude ?? ""
            let longitude = GetUserDetails.longitude ?? ""
            var jsonObject = [String: Any]()
            jsonObject["imei"] = deviceID ?? ""
            jsonObject["dcsId"] = userDetails.dcsID ?? ""
            jsonObject["psId"] = self.groupService?.psID ?? ""
            jsonObject["officeId"] = self.groupService?.officeID ?? 0
            jsonObject["serviceId"] = self.groupService?.serviceCode ?? 0
            if self.milageFlag != nil{
                jsonObject["mileage"] = self.milageFlag ?? ""
            }else{
               jsonObject["mileage"] = self.groupService?.mileageFlag ?? 0
            }
            jsonObject["travelTime"] = 0
            jsonObject["taskCodes"] = taskCodes.map { String($0) }
            .joined(separator: ", ")
            jsonObject["transactionType"] = "DEPARTURE"
            jsonObject["latitude"] = latitude
            jsonObject["longitude"] = longitude
            jsonObject["address"] = address
            jsonObject["visitDetailsId"] = self.groupService?.visitDetailsID ?? 0
            jsonObject["arrDeptId"] = self.groupService?.arrDeptID ?? 0
            jsonObject["ipAddress"] = "10.0.2.16"
            jsonObject["progressNotes"] = ""
            if self.receivedOtpFlag != nil{
                jsonObject["otp"] = self.receivedOtpFlag ?? ""
             }
          url.append("createReportedVisitToSchedule?jsonObj=\(jsonObject.jsonStringRepresentation ?? "")")
            url.append("&sessionId=\(userDetails.sessionID ?? "")")
            ServiceManager.shared.request(type: GroupServiceResponseModel.self, url: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "", method: .get, view: self.view) { [weak self](response) in
                if response?.result != nil{
                  if response?.validateFlag ?? "" == "departuretimecheck"{
                        DispatchQueue.main.async {
                            self?.showAlert(message: response?.result ?? "")
                        }
                    }else if response?.validateFlag ?? "" == "false" {
                        DispatchQueue.main.async {
                            self?.showAlert(message: response?.result ?? "")
                        }
                    }else{
                        DispatchQueue.main.async {
                        self?.transferentView.isHidden = false
                        if let result = response?.result{
                            let resultDateAndTime = result.suffix(19)
                            self?.dateLabel.text = String(resultDateAndTime.prefix(10))
                            self?.timeLabel.text = String(resultDateAndTime.suffix(8))
                            self?.alertHeaderLabel.text = (response?.result?.dropLast(19).string ?? "")
                        }
                    }
                    
                    }
                }
            }
        }
        
    }
    @IBAction func ok_action(_ sender: Any) {
        transferentView.isHidden = true
        guard let emptyStateVC = self.storyboard?.instantiateViewController(withIdentifier: "MoveToNextTransaction") as? MoveToNextTransaction else {return}
        self.navigationController?.pushViewController(emptyStateVC, animated: true)
        
    }
    
    @objc func selectAction(sender: UIButton){
        if self.hadleSelection[sender.tag].isTapped{
             if self.hadleSelection[sender.tag].isSelected == true{
                    self.self.hadleSelection[sender.tag].isTapped = false
             }else{
                    self.hadleSelection[sender.tag].isSelected = true
             }
        }else{
            self.hadleSelection[sender.tag].isTapped = true
            self.hadleSelection[sender.tag].isSelected = true
            
        }
        
        self.taskListTableView.reloadData()
    }
    @objc func deSelectAction(sender: UIButton){
        if self.hadleSelection[sender.tag].isTapped{
            
            if self.hadleSelection[sender.tag].isSelected == false{
                self.self.hadleSelection[sender.tag].isTapped = false
            }else{
                self.hadleSelection[sender.tag].isSelected = false
            }
        }else{
            self.hadleSelection[sender.tag].isTapped = true
            self.hadleSelection[sender.tag].isSelected = false
            
        }
        
        self.taskListTableView.reloadData()
    }

}

extension CloutOutTasksController: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasksList?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "taskListCell", for: indexPath) as? TaskListCell else { return UITableViewCell() }
        cell.selectionStyle = .none
        if hadleSelection[indexPath.row].isTapped{
            if hadleSelection[indexPath.row].isSelected{
                cell.selectAction.setImage(UIImage(named: "Completed_01")?.withRenderingMode(.alwaysOriginal), for: .normal)
                cell.deSelectAction.setImage(UIImage(named: "Close")?.withRenderingMode(.alwaysOriginal), for: .normal)
            }else{
                cell.selectAction.setImage(UIImage(named: "defaultCheck")?.withRenderingMode(.alwaysOriginal), for: .normal)
                cell.deSelectAction.setImage(UIImage(named: "Refused_01")?.withRenderingMode(.alwaysOriginal), for: .normal)
            }
        }else{
            cell.selectAction.setImage(UIImage(named: "defaultCheck")?.withRenderingMode(.alwaysOriginal), for: .normal)
            cell.deSelectAction.setImage(UIImage(named: "Close")?.withRenderingMode(.alwaysOriginal), for: .normal)
        }
        cell.taskTitleLabel.text = self.tasksList?[indexPath.row].taskName ?? ""
        cell.selectAction.tag = indexPath.row
        cell.selectAction.addTarget(self, action: #selector(selectAction(sender:)), for: .touchUpInside)
        cell.deSelectAction.tag = indexPath.row
        cell.deSelectAction.addTarget(self, action: #selector(deSelectAction(sender:)), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
}
