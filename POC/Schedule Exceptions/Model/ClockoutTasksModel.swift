//
//  ClockoutTasksModel.swift
//  POC
//
//  Created by Ajeet N on 30/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import Foundation


typealias ClockoutTaskslistModel = [ClockoutTasksModel]


// MARK: - WelcomeElement
struct ClockoutTasksModel: Codable {
    let taskCode, taskCodeAndName, taskName: String?
    let taskId: Int?

}

