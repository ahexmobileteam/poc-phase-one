//
//  ClockOutConseutiveModel.swift
//  POC
//
//  Created by Ajeet N on 30/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import Foundation


typealias ClockoutConseutiveModelData = [ClockOutConseutiveModel]

// MARK: - WelcomeElement
struct ClockOutConseutiveModel: Codable {
    
    let visitDetailsId, psCode, serviceCode, officeId: Int?
    let psName: String?
    let psId: Int?
    let serviceName, visitTimes: String?
}

