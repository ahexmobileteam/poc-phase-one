//
//  ClockOutGroupOrConseModel.swift
//  POC
//
//  Created by Ajeet N on 29/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import Foundation

struct  ClockOutGroupOrConseModel: Codable  {
    let groupedServiceCount : Int?
    let consecutiveServiceCount : Int?
    let consecutiveVisitIds : String?
}
