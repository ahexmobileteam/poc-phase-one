//
//  SchedulesModel.swift
//  POC
//
//  Created by Admin on 15/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import Foundation

typealias SchedulesModel = [SchedulesResponseModel]
struct SchedulesResponseModel: Decodable {
    let visitDetailsID, psCode: Int?
    let address: String?
    let serviceCode, arrDeptID, psID: Int?
    let serviceName, arrivalFlag, txnFlag: String?
    let officeID: Int?
    let phone, psName: String?
    let mileageFlag: Int?
    let absentFlag: String?
    let taskCapture: Int?
    let departureFlag: String?
    let otpFlag: Int?
    let specialFlag, visitTimes: String?

    enum CodingKeys: String, CodingKey {
        case visitDetailsID = "visitDetailsId"
        case psCode, address, serviceCode
        case arrDeptID = "arrDeptId"
        case psID = "psId"
        case serviceName, arrivalFlag, txnFlag
        case officeID = "officeId"
        case phone, psName
        case mileageFlag = "MileageFlag"
        case absentFlag, taskCapture, departureFlag
        case otpFlag = "OTPFlag"
        case specialFlag, visitTimes
    }
}

typealias ArrivedGroupServicesModel = [GroupServicesModel]
struct GroupServicesModel: Decodable {
    let visitDetailsId: Int?
    let officeId: Int?
    let psName: String?
    let arrDeptId: Int?
    let psId: Int?
    let serviceId: Int?
    let serviceName: String?
    let visitTimes: String?
}
