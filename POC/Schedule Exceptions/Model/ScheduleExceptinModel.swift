//
//  ScheduleExceptinModel.swift
//  POC
//
//  Created by Ajeet N on 08/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import Foundation


typealias ScheduleExceptionListModel = [ScheduleExceptinModel]


// MARK: - WelcomeElement
struct ScheduleExceptinModel: Decodable {
    let exceptionType, endDate, startDate, description: String?
    
}
