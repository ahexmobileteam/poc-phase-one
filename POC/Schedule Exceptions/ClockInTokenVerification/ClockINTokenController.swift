//
//  ClockINTokenController.swift
//  POC
//
//  Created by Admin on 27/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit
import  CoreLocation


class ClockINTokenController: UIViewController {

    private let locationManager = CLLocationManager()
    var addressString : String = ""

    @IBOutlet weak var alertHeaderLabel: UILabel!
    @IBOutlet weak var identifyLabel: UILabel!
    
    @IBOutlet weak var tokenCodeTF: CustomTField!
    var schedule: SchedulesResponseModel?
    
    @IBOutlet weak var transparentViewObj: UIView!
    @IBOutlet weak var cardViewObj: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    var isClockOut = false
    
    @IBAction func okButtonAction(_ sender: Any) {
        self.transparentViewObj.isHidden = true
        guard let emptyStatevC = self.storyboard?.instantiateViewController(withIdentifier: "MoveToNextTransaction") as? MoveToNextTransaction else {return}
        self.navigationController?.pushViewController(emptyStatevC, animated: true)
        
        
    }
    
    //MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        checkLocationServices()
        self.transparentViewObj.isHidden = true
        self.navigationItem.title = "POC Visit Verification"
        self.cardViewObj.layer.cornerRadius = 5.0
    }
    
    func checkLocationServices(){
           if CLLocationManager.locationServicesEnabled(){
                   self.locationManager.delegate = self
                   self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
                   self.checkLocationAuthorization()
           }else{
               self.alert(title: "Please enable the location permissions in order to submit token code", message: "", actionTitles: ["OK"], actionStyle: [.default], action: [
                   { ok in
                       
                   }
               ])
           }
       }
       
       func checkLocationAuthorization(){
           switch CLLocationManager.authorizationStatus() {
           case .authorizedWhenInUse:
               locationManager.startUpdatingLocation()
               break
           case .denied:
               self.alert(title: "Location services are denied,Please enable it in order to get items", message: "", actionTitles: ["OK"], actionStyle: [.default], action: [
                   { ok in
                       
                   }
               ])
               break
           case .notDetermined:
               locationManager.requestWhenInUseAuthorization()
           default:break
           }
       }

       
       //MARK: Converting User Latitude and Longitude into Physical Address
          func getAddressFromLatLons(pdblLatitude: String, withLongitude pdblLongitude: String) {
              
              var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
              let lat: Double = Double("\(pdblLatitude)") ?? 0.0
              let lon: Double = Double("\(pdblLongitude)") ?? 0.0
              let ceo: CLGeocoder = CLGeocoder()
              center.latitude = lat
              center.longitude = lon
              let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
              ceo.reverseGeocodeLocation(loc, completionHandler:
                  {(placemarks, error) in
                      if (error != nil)
                      {
                          print("reverse geodcode fail: \(error!.localizedDescription)")
                      }
                      let pm = placemarks! as [CLPlacemark]
                      
                      if pm.count > 0 {
                          let pm = placemarks![0]
                          if pm.subLocality != nil {
                              self.addressString = self.addressString + pm.subLocality! + ", "
                              print("The address string is :\(self.addressString)")
                          }
                          if pm.thoroughfare != nil {
                              self.addressString = self.addressString + pm.thoroughfare! + ", "
                          }
                          if pm.locality != nil {
                              self.addressString = self.addressString + pm.locality! + ", "
                          }
                          if pm.country != nil {
                              self.addressString = self.addressString + pm.country! + ", "
                          }
                          if pm.postalCode != nil {
                              self.addressString = self.addressString + pm.postalCode! + " "
                          }
                          DispatchQueue.main.async {
                              defaults.set(self.addressString, forKey: "CLOCKINUSERADDRESS")
                              
                          }
                      }
              })
          }
    
    
    @IBAction func save_token_action(_ sender: Any) {
        
        if tokenCodeTF.text!.isEmpty{
            self.toast(msgString: "Please enter token code", view: self.view)
        }else{
            
            let details = self.getuserDetails()

            var jsonObject = [String: Any]()
            jsonObject["imei"] = (deviceID ?? "")
            jsonObject["dcsId"] = (details.dcsID ?? "")
            jsonObject["psId"] = (self.schedule?.psID ?? 0)
            jsonObject["officeId"] = (self.schedule?.officeID ?? 0)
            jsonObject["serviceId"] = (self.schedule?.serviceCode ?? 0)
            jsonObject["mileage"] = 0
            jsonObject["travelTime"] = 0
            jsonObject["transactionType"] = "ARRIVAL"
            jsonObject["latitude"] = (GetUserDetails.latitude ?? "")
            jsonObject["longitude"] = (GetUserDetails.longitude ?? "")
            jsonObject["visitDetailsId"] = (self.schedule?.visitDetailsID ?? 0)
            jsonObject["arrDeptId"] = (self.schedule?.arrDeptID ?? 0)
            jsonObject["ipAddress"] = ""
            jsonObject["otp"] = tokenCodeTF.text!
            DispatchQueue.main.async {
                self.getAddressFromLatLons(pdblLatitude: (GetUserDetails.latitude ?? ""), withLongitude: (GetUserDetails.longitude ?? "")) { (address) in
                
                    jsonObject["address"] = address
                    
                    let details = self.getuserDetails()

                    let url = (details.baseURL ?? "") + API.CREATEREPORTEDVISIT + "\(jsonObject.jsonStringRepresentation ?? "")" + "&sessionId=\(details.sessionID ?? "")"
                    print("The url in clockin token screen is :\(String(describing: url))")
                    ServiceManager.shared.request(type: GroupServiceResponseModel.self, url: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "", method: .get, view: self.view) { [weak self](response) in
                        if response?.result != nil{
                                 if response?.validateFlag ?? "" == "departuretimecheck"{
                                         DispatchQueue.main.async {
                                             self?.showAlert(message: response?.result ?? "")
                                         }
                                    }else if response?.validateFlag ?? "" == "false" {
                                         DispatchQueue.main.async {
                                              self?.showAlert(message: response?.result ?? "")
                                    }
                                    }else{
                                         DispatchQueue.main.async {
                                              self?.transparentViewObj.isHidden = false
                                                if let result = response?.result{
                                                    let resultDateAndTime = result.suffix(19)
                                                    self?.dateLabel.text = String(resultDateAndTime.prefix(10))
                                                    self?.timeLabel.text = String(resultDateAndTime.suffix(8))
                                                    self?.alertHeaderLabel.text = (response?.result?.dropLast(19).string ?? "")
                                                       }
                                          }
                                    }
                        }else{
                                DispatchQueue.main.async {
                                    self?.toast(msgString: response?.result ?? "", view: self?.view ?? UIView())
                                }
                            }
                        }
                    }
                }
            }
        }
}


//MARK: Corelocation Delegate Methods
extension ClockINTokenController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        defaults.set(String(location.coordinate.latitude), forKey: kLatitude)
        defaults.set(String(location.coordinate.longitude), forKey: kLongitude)
        defaults.synchronize()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkLocationAuthorization()
    }
    
}

extension Dictionary {
    var jsonStringRepresentation: String? {
        guard let theJSONData = try? JSONSerialization.data(withJSONObject: self,
                                                            options: [.prettyPrinted]) else {
            return nil
        }
        return String(data: theJSONData, encoding: .ascii)
    }
}

