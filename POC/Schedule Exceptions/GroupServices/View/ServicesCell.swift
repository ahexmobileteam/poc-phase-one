//
//  ServicesCell.swift
//  POC
//
//  Created by Ajeet N on 05/05/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class ServicesCell: UITableViewCell {

    @IBOutlet weak var serviceNameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var isSelectAction: UIButton!
    var service: GroupServicesModel?{
        didSet{
            if let serviceName = self.service?.psName{
               self.serviceNameLabel.text = serviceName
            }
            if let time = self.service?.visitTimes{
                let desiredTime = time.split { $0.isNewline }
                self.timeLabel.text = String(describing: desiredTime[1])
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
