//
//  GroupedMultiTaskCell.swift
//  POC
//
//  Created by Ajeet N on 07/05/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class GroupedMultiTaskCell: UITableViewCell {

    @IBOutlet weak var taskLabel: UILabel!
    @IBOutlet weak var checkMarkOutlet: UIButton!
    @IBOutlet weak var crossMarkOutlet: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
