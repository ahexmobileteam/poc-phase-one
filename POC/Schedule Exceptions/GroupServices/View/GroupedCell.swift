//
//  GroupedCell.swift
//  POC
//
//  Created by Ajeet N on 07/05/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class GroupedCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var selecteStateOutlet: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
