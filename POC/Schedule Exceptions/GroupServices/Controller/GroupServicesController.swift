//
//  GroupServicesController.swift
//  POC
//
//  Created by Ajeet N on 05/05/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//


import UIKit
import  CoreLocation

struct GroupServiceResponseModel: Decodable {
    let result: String?
    let validateFlag: String?
}


class GroupServicesController: UIViewController {

    
    @IBOutlet weak var popupTransparentView: UIView!
    @IBOutlet weak var popUpCardView: UIView!
    @IBOutlet weak var popUpDateLabel: UILabel!
    @IBOutlet weak var popupTimeLabel: UILabel!
    
    
//MARK: Ok Button Action
    @IBAction func popUpOkButtonActn(_ sender: Any) {
        self.popupTransparentView.isHidden = true
        guard let emptyStateVC = self.storyboard?.instantiateViewController(withIdentifier: "MoveToNextTransaction") as? MoveToNextTransaction else {return}
        self.navigationController?.pushViewController(emptyStateVC, animated: true)
    }
    
    @IBOutlet weak var alertHeaderLabel: UILabel!
    private let locationManager = CLLocationManager()
    var addressString : String = ""

    @IBOutlet weak var servicesListTableView: UITableView!{
        didSet{
            self.servicesListTableView.delegate = self
            self.servicesListTableView.dataSource = self
        }
    }
    @IBOutlet weak var defaultSelecedServiceNane: UILabel!
    @IBOutlet weak var selectedServiceTime: UILabel!
    
    var groupServices: ArrivedGroupServicesModel?
    private var selectionHandler = [Bool]()
    var officeId: Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.popupTransparentView.isHidden = true
        
        checkLocationServices()
        navigationItem.title = "Clock In Group Services"
        for index in 0..<groupServices!.count{
            if index == 0{
                selectionHandler.append(true)
            }else{
                selectionHandler.append(false)
            }
        }
        if let serviceName = self.groupServices?[0].serviceName, let time = self.groupServices?[0].visitTimes{
            self.defaultSelecedServiceNane.text = serviceName
            self.selectedServiceTime.text = time
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(sessionExpired), name: Notification.Name("SessionExpired"), object: nil)
        
    }
    
    @objc func sessionExpired(){
           NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "SessionExpired"), object: nil)
           self.logout()
    }
    
    func checkLocationServices(){
        if CLLocationManager.locationServicesEnabled(){
                self.locationManager.delegate = self
                self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
                self.checkLocationAuthorization()
        }else{
            self.alert(title: "Please enable the location permissions in order to submit token code", message: "", actionTitles: ["OK"], actionStyle: [.default], action: [
                { ok in
                    
                }
            ])
        }
    }
    
    func checkLocationAuthorization(){
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
            break
        case .denied:
            self.alert(title: "Location services are denied,Please enable it in order to get items", message: "", actionTitles: ["OK"], actionStyle: [.default], action: [
                { ok in
                    
                }
            ])
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        default:break
        }
    }
    
    
    
@IBAction func save_selected_services(_ sender: Any) {
    var url = String()
    url.removeAll()
    
    let details = self.getuserDetails()

    url.append(details.baseURL ?? "")
    url.append(API.SaveGroupedTransactions + (deviceID ?? ""))
    url.append(API.dcsID + "\(details.dcsID ?? "")")
    url.append(API.officeID + "\(self.officeId ?? 0)")
    url.append(API.Mileage + "0")
    url.append(API.TravelTime + "0")
    url.append(API.TransactionType + "ARRIVAL")
    url.append(API.latitude + "\(GetUserDetails.latitude ?? "")")
    url.append(API.longitude + "\(GetUserDetails.longitude ?? "")")
    url.append(API.ipAddress + "10.0.2.16")
    url.append(API.SessionID + "\(details.sessionID ?? "")")
    self.getAddressFromLatLons(pdblLatitude: "\(GetUserDetails.latitude ?? "")", withLongitude: "\(GetUserDetails.longitude ?? "")") { (address) in
        url.append(API.address + "\(address)")
        let defaultService = ["psId":self.groupServices?[0].psId ?? 0,"serviceId":self.groupServices?[0].serviceId ?? 0,"visitDetailsId":self.groupServices?[0].visitDetailsId ?? 0,"arrivalDepartureId":self.groupServices?[0].arrDeptId ?? 0,"taskCodes":""] as [String: Any]
        var selectedServicesList = [NSMutableDictionary]()
        selectedServicesList.removeAll()
        var json = String()
        json.removeAll()
        for selected in 0..<self.selectionHandler.count{
            if self.selectionHandler[selected]{
                if selected == 0{
                    json.append(defaultService.jsonStringRepresentation ?? "")
                }else{
                    let sel = ["psId":self.groupServices?[selected].psId ?? 0,"serviceId":self.groupServices?[selected].serviceId ?? 0,"visitDetailsId":self.groupServices?[selected].visitDetailsId ?? 0,"arrivalDepartureId":self.groupServices?[selected].arrDeptId ?? 0,"taskCodes":""] as [String: Any]
                    let jsonObject = sel.jsonStringRepresentation ?? ""
                    json.append(",")
                    json.append(jsonObject)
                }
            }
        }
        url.append(API.VisitDetailArrayWithJsonObj + "[" + json + "]")
        print(url)
        DispatchQueue.main.async {
            ServiceManager.shared.request(type: GroupServiceResponseModel.self, url: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "", method: .get, view: self.view) { [weak self](response) in
                if response?.result != nil{
                  if response?.validateFlag ?? "" == "departuretimecheck"{
                        DispatchQueue.main.async {
                            self?.showAlert(message: response?.result ?? "")
                        }
                   }else if response?.validateFlag ?? "" == "false" {
                        DispatchQueue.main.async {
                             self?.showAlert(message: response?.result ?? "")
                   }
                   }else{
                        DispatchQueue.main.async {
                             self?.popupTransparentView.isHidden = false
                               if let result = response?.result{
                                   let resultDateAndTime = result.suffix(19)
                                   self?.popUpDateLabel.text = String(resultDateAndTime.prefix(10))
                                   self?.popupTimeLabel.text = String(resultDateAndTime.suffix(8))
                                   self?.alertHeaderLabel.text = (response?.result?.dropLast(19).string ?? "")
                                      }
                         }
                   }
                }else{
                    self?.toast(msgString: "Error occured", view: self?.view ?? UIView())
                }
            }
         
        }
     }
        
   }
    
    @objc func isSelected(sender: UIButton){
        selectionHandler[sender.tag] = !selectionHandler[sender.tag]
        servicesListTableView.reloadData()
    }
}


//MARK: UITableView Delegate and DataSource Methods
extension GroupServicesController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groupServices?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "servicesCell", for: indexPath) as? ServicesCell else { return UITableViewCell() }
        cell.selectionStyle = .none
        indexPath.row == 0 ? (cell.isSelectAction.setImage(UIImage(named: "Checked-2")?.withRenderingMode(.alwaysOriginal), for: .normal)) : (selectionHandler[indexPath.row] ? (cell.isSelectAction.setImage(UIImage(named: "Checked-2")?.withRenderingMode(.alwaysOriginal), for: .normal)) : (cell.isSelectAction.setImage(UIImage(named: "Checked_01-1")?.withRenderingMode(.alwaysOriginal), for: .normal)))
        indexPath.row == 0 ? (cell.isSelectAction.isUserInteractionEnabled = false) : (cell.isSelectAction.isUserInteractionEnabled = true)
        cell.service = self.groupServices?[indexPath.row]
        cell.isSelectAction.tag = indexPath.row
        cell.isSelectAction.addTarget(self, action: #selector(isSelected(sender:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 96
    }
    
}


//MARK: Corelocation Delegate Methods
extension GroupServicesController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        defaults.set(String(location.coordinate.latitude), forKey: kLatitude)
        defaults.set(String(location.coordinate.longitude), forKey: kLongitude)
        defaults.synchronize()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkLocationAuthorization()
    }
    
}
