//
//  SingleTaskCell.swift
//  POC
//
//  Created by Ajeet N on 30/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit


class SingleTaskCell: UITableViewCell {

    @IBOutlet weak var taskLabel: UILabel!
    @IBOutlet weak var checkOutlet: UIButton!
    @IBOutlet weak var crossOutlet: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    
    
    //MARK: Check Mark Action
    @IBAction func checkMarkAction(_ sender: Any) {
        
    }
    
    
    //MARK: Cross Mark Action
    @IBAction func crossMarkAction(_ sender: Any) {
        
    }

}
