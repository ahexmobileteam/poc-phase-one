//
//  ConsuetiveCell.swift
//  POC
//
//  Created by Ajeet N on 30/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class ConsuetiveCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
