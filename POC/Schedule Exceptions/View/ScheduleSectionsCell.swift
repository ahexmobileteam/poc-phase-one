//
//  ScheduleSectionsCell.swift
//  POC
//
//  Created by Admin on 14/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class ScheduleSectionsCell: UITableViewCell {

    @IBOutlet weak var scheduleNameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var mobileNumberLabel: UILabel!
    @IBOutlet weak var mapAction: UIButton!
    @IBOutlet weak var proceedAction: CustomButtonC!
    @IBOutlet weak var expandAction: UIButton!
    @IBOutlet weak var detailsHeight: NSLayoutConstraint!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var serviceAction: UIButton!
    
    @IBOutlet weak var heightOfSection: NSLayoutConstraint!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var sectionView: CustomView!
    
    @IBOutlet weak var clockInStack: UIStackView!
    
    @IBOutlet weak var clockOutStack: UIStackView!
    
    @IBOutlet weak var reportAbsentStack: UIStackView!
    
    @IBOutlet weak var proceedButton: CustomButtonC!
    
    @IBOutlet weak var clockInButton: UIButton!
    
    @IBOutlet weak var clockOutButton: UIButton!
    
    @IBOutlet weak var reportAbsentButton: UIButton!
    
    
    
    var schedule: SchedulesResponseModel?{
        didSet{
            self.scheduleNameLabel.text = self.schedule?.psName ?? "Not available"
            if let time = self.schedule?.visitTimes{
                let secondLine = time.components(separatedBy: CharacterSet.newlines)
                self.timeLabel.text = secondLine[1]
            }
            self.addressLabel.text = self.schedule?.address ?? "Not available"
            self.mobileNumberLabel.text = self.schedule?.phone ?? "Not available"
            self.serviceAction.setTitle(self.schedule?.serviceName ?? "Not available", for: .normal)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
