//
//  TaskListCell.swift
//  POC
//
//  Created by Admin on 13/05/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class TaskListCell: UITableViewCell {
    @IBOutlet weak var taskTitleLabel: UILabel!
    
    @IBOutlet weak var selectAction: UIButton!
    @IBOutlet weak var deSelectAction: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
