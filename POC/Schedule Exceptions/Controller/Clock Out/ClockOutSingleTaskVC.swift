//
//  ClockOutSingleTaskVC.swift
//  POC
//
//  Created by Ajeet N on 30/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//


import UIKit
import  CoreLocation


class ClockOutSingleTaskVC: UIViewController {
    
    @IBOutlet weak var alertTransparentView: UIView!
    @IBOutlet weak var alertCardViewObj: CustomView!
    @IBOutlet weak var alertFirstLabel: UILabel!
    @IBOutlet weak var alertDateLabel: UILabel!
    @IBOutlet weak var alertLocationLabel: UILabel!
    
    @IBOutlet weak var serviceNameLabel: UILabel!
    @IBOutlet weak var alertTimeLabel: UILabel!
    
    var isStringg:String?
    
    @IBOutlet weak var alertHeaderLabel: UILabel!
    
    @IBOutlet weak var saveButtonOutlet: CustomButtonC!
    private let locationManager = CLLocationManager()
    private var latitude: String? = nil
    private var longitude: String? = nil
    private var address: String? = nil
    private var groupServiceResponseModel: GroupServiceResponseModel?
    var selectionHandler = [HandleSelection]()
    var receivedMileagVal : String?
    var consutiveVales : String?
    var receieveOffId : Int?
    var receiveServiceTelCode : Int?
    var receoivePSID : Int?
    var receiveConseutivevisitedID : Int?
    var initialIndex = 0
    var ids = [String]()
    var selectedTaskIds = [Int]()
    var receiveArrDepID: Int?
    var selectedList: String? = nil
    var clockoutConsuetiveResponse :ClockoutConseutiveModelData?
    
    @IBOutlet weak var nameLabel: UILabel!
    private var clockoutTaksModelData : ClockoutTaskslistModel?
    @IBOutlet weak var taskstableViewObj: UITableView! {
        didSet {
            taskstableViewObj.delegate = self
            taskstableViewObj.dataSource = self
        }
    }
    

//MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alertTransparentView.isHidden = true
        
        if isStringg ?? "" == "Skip" {
            saveButtonOutlet.setTitle("Save", for: .normal)

           if consutiveVales != nil{
                       let idsList =  consutiveVales ?? ""
                       ids = idsList.split{$0 == ","}.map(String.init)
                       if ids.count != 0{
                           singleTaskAPICalling(visitedID: ids[0])
                       }
                   }
            
                        
        }
        else {
            
            if consutiveVales != nil{
                       let idsList =  consutiveVales ?? ""
                       ids = idsList.split{$0 == ","}.map(String.init)
                       if ids.count == 1{
                             saveButtonOutlet.setTitle("Save", for: .normal)
                           }else{
                             saveButtonOutlet.setTitle("Next", for: .normal)
                       }
                       if ids.count != 0{
                           singleTaskAPICalling(visitedID: ids[0])
                       }
                   }
            
            
        }
       
      navigationItem.title = "POC Visit Verification"
      let psName = defaults.value(forKey: "GLOBALPSNAME")
      nameLabel.text = String(describing: psName ?? "")
}
    
   

//MARK: Save Button Action
    @IBAction func saveButtonAction(_ sender: Any) {
        
/*http://poc.aquilasoftware.com/pocextacc-webservices_phase2/telephony/saveConsecutiveTransactionsNew?imei=7be47c44-8c7c-45d1-bbde-30857d2932b5&dcsId=14634&officeId=191&mileage=0.0&latitude=37.4219983&longitude=-122.084&address=1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA
&ipAddress=10.0.2.16&sessionId=196528_LUTWGNACV6MJR1F4&visitDetailsArray=[{"psId":20338,"serviceId":17,"visitDetailsId": ,"arrivalDepartureId":0,"taskCodes":"4|1|1|0,29|1|1|0,8|1|1|0,30|1|1|0","progressNotes":""},{"psId":20338,"serviceId":17,"visitDetailsId":4638159,"arrivalDepartureId":0,"taskCodes":"4|1|1|0,29|1|1|0,8|1|1|0,30|1|1|0","progressNotes":""}]
*/
       
        
        if isStringg ?? "" == "Skip" {
            
        }
        else {
            
            if initialIndex == ids.count {
                for index in 0..<selectionHandler.count{
                                   if selectionHandler[index].isTapped {
                                       if selectionHandler[index].isSelected {
                                           selectedTaskIds.append(clockoutTaksModelData?[index].taskId ?? 0)
                                       }
                                   }
                               }
                
            let list = ["psId":receoivePSID ?? 0,"serviceId":receiveServiceTelCode ?? 0,"visitDetailsId":clockoutConsuetiveResponse?[initialIndex - 1].visitDetailsId ?? 0,"arrivalDepartureId":receiveArrDepID ?? 0,"taskCodes":(selectedTaskIds.map {String($0)}.joined(separator: ","))] as [String : Any]
                
            //selectedList += (list.jsonStringRepresentation ?? "")
                if selectedList == nil{
                    selectedList = list.jsonStringRepresentation ?? ""

                }else{
                    selectedList! += ",\(list.jsonStringRepresentation ?? "")"

                }

                
                let totalIds = "[" + selectedList! + "]"
                let staticTaskID = totalIds
                        
                var url = String()
                url.removeAll()
                
                  let details = self.getuserDetails()
                self.getAddressFromLatLons(pdblLatitude: GetUserDetails.latitude ?? "", withLongitude: GetUserDetails.longitude ?? "") { (address) in
                    url.append((details.baseURL ?? "") + "saveConsecutiveTransactionsNew?imei=\(deviceID ?? "")&dcsId=\(details.dcsID ?? "")&officeId=\(self.receieveOffId ?? 0)&mileage=\(self.receivedMileagVal ?? "")&transactionType=DEPARTURE&travelTime=0&latitude=\(GetUserDetails.latitude ?? "")&longitude=\(GetUserDetails.longitude ?? "")&address=\(address)&ipAddress=&sessionId=\(details.sessionID ?? "")&visitDetailsArray=\(staticTaskID )")
                    print("The save conseutive url encoded is :\(url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")")
                    print("The save conseutive url encoded is :\(url)")
                    DispatchQueue.main.async {
                        ServiceManager.shared.request(type: GroupServiceResponseModel.self, url: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "", method: .get, view: self.view) { [weak self](response) in
                            if response?.result != nil{
                                self?.groupServiceResponseModel = response
                                if response?.validateFlag ?? "" == "departuretimecheck"{
                                    DispatchQueue.main.async {
                                        self?.showAlert(message: response?.result ?? "")
                                    }
                                }else if response?.validateFlag ?? "" == "false" {
                                    DispatchQueue.main.async {
                                        self?.showAlert(message: response?.result ?? "")
                                    }
                                }else{
                                    DispatchQueue.main.async {
                                    self?.alertTransparentView.isHidden = false
                                    if let result = response?.result{
                                        let resultDateAndTime = result.suffix(19)
                                        self?.alertDateLabel.text = String(resultDateAndTime.prefix(10))
                                        self?.alertTimeLabel.text = String(resultDateAndTime.suffix(8))
                                        self?.alertHeaderLabel.text = (response?.result?.dropLast(19).string ?? "")
                                    }
                                }
                                
                                }
                                                    
                            }
                        }
                    }
                        
                }
            }
                        else{
                        for index in 0..<selectionHandler.count{
                                if selectionHandler[index].isTapped {
                                    if selectionHandler[index].isSelected {
                                        selectedTaskIds.append(clockoutTaksModelData?[index].taskId ?? 0)
                                    }
                                }
                            }
                
                let list = ["psId":receoivePSID ?? 0,"serviceId":receiveServiceTelCode ?? 0,"visitDetailsId":clockoutConsuetiveResponse?[initialIndex - 1].visitDetailsId ?? 0,"arrivalDepartureId":receiveArrDepID ?? 0,"taskCodes":(selectedTaskIds.map {String($0)}.joined(separator: ","))] as [String : Any]
                       
                if selectedList == nil{
                    selectedList = list.jsonStringRepresentation ?? ""
                }else{
                    selectedList! += ",\(list.jsonStringRepresentation ?? "")"
                }

                            selectionHandler.removeAll()
                             selectedTaskIds.removeAll()
                            singleTaskAPICalling(visitedID: ids[initialIndex])
                            if initialIndex == (ids.count - 1){
                               saveButtonOutlet.setTitle("Save", for: .normal)
                        }else{
                               saveButtonOutlet.setTitle("Next", for: .normal)
                    }
                 }
        }
        
        
        

   }

    
    
    
//MARK: Single Task API Calling
    func singleTaskAPICalling(visitedID: String) {
        
// http://poc.aquilasoftware.com/pocextacc-webservices_new/telephony/getTaskList?officeId=191&sessionId=226145_UHCVHZ2EWXP6NREJ&taskName=&visitDetailsId=4636792&psTelCode=0&serviceTelCode=17
        
        let details = self.getuserDetails()
        
let singleTaskUrl = (details.baseURL ?? "") + API.singleTaskList + String(describing: receieveOffId ?? 0)
let sessionID = API.SessionID + (details.sessionID ?? "")
let taksName = API.taskName
let visitDetailsId = API.visitDetailID + visitedID
let psTele = API.psTelecode + String(describing: receoivePSID ?? 0 )
let serviceTeleCode = API.serviceTeleCode + String(describing: receiveServiceTelCode ?? 0)
        
let singleurl = singleTaskUrl + sessionID + taksName + visitDetailsId + psTele + serviceTeleCode
print("the single task url is :\(singleurl)")
    
ServiceManager.shared.request(type: ClockoutTaskslistModel.self , url: singleurl, method: .get, view: self.view) { (response) in
    print("The response of single taks url is :\(String(describing: response))")
    if response?.count ?? 0 != 0{
        for  _ in 0..<response!.count {
            self.selectionHandler.append(HandleSelection(isTapped: false, isSelected: false))
        }
        DispatchQueue.main.async {
            self.serviceNameLabel.text = self.clockoutConsuetiveResponse?[self.initialIndex].serviceName ?? ""
             self.clockoutTaksModelData = response
              self.initialIndex += 1
               self.taskstableViewObj.reloadData()
         }
    }
    }
}
    
    
//MARK: Check Mark Action
    @objc func checkMarkAction(sender: UIButton) {
    
         if self.selectionHandler[sender.tag].isTapped{
              if self.selectionHandler[sender.tag].isSelected == true{
                     self.selectionHandler[sender.tag].isTapped = false
              }else{
                     self.selectionHandler[sender.tag].isSelected = true
              }
         }else{
             self.selectionHandler[sender.tag].isTapped = true
             self.selectionHandler[sender.tag].isSelected = true
             
         }
         self.taskstableViewObj.reloadData()
}
    
    
    //MARK: Cross Mark Action
    @objc func crossMarkAction(sender: UIButton)  {
             
           if self.selectionHandler[sender.tag].isTapped{
               
               if self.selectionHandler[sender.tag].isSelected == false{
                   self.selectionHandler[sender.tag].isTapped = false
               }else{
                   self.selectionHandler[sender.tag].isSelected = false
               }
           }else{
               self.selectionHandler[sender.tag].isTapped = true
               self.selectionHandler[sender.tag].isSelected = false
               
           }
            self.taskstableViewObj.reloadData()
     
     }
    
    
    //MARK: Alert Ok Button Action
    @IBAction func alertOkButton(_ sender: Any) {
        if groupServiceResponseModel?.validateFlag ?? "" == "false"{
            alertTransparentView.isHidden = true
            return
        }
        alertTransparentView.isHidden = true
             guard let emptyStateVC = self.storyboard?.instantiateViewController(withIdentifier: "MoveToNextTransaction") as? MoveToNextTransaction else {return}
        self.navigationController?.pushViewController(emptyStateVC, animated: true)
        
        
    }
    
        
}


//MARK: UITableView Delegate and Datasource Methods
extension ClockOutSingleTaskVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isStringg ?? "" == "Skip" {
           return 0
        }
        else {
            return self.clockoutTaksModelData?.count ?? 0

        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
   
        if isStringg ?? "" == "Skip" {
            
            return UITableViewCell()
        }
        else {
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SingleTaskCell", for: indexPath) as? SingleTaskCell else { return UITableViewCell() }
                   cell.taskLabel.text =  self.clockoutTaksModelData?[indexPath.row].taskName ?? ""

            if selectionHandler[indexPath.row].isTapped {
                       if selectionHandler[indexPath.row].isSelected {
                           // Selected
                           cell.checkOutlet.setImage(UIImage(named: "selectedcheck"), for: .normal)
                           cell.crossOutlet.setImage(UIImage(named: "Close"), for: .normal)
                       }
                       else {
                           //
                           cell.checkOutlet.setImage(UIImage(named: "defaultCheck"), for: .normal)
                           cell.crossOutlet.setImage(UIImage(named: "selectedcross"), for: .normal)

                       }
                   }
                   else {
                       // Default image
                       cell.checkOutlet.setImage(UIImage(named: "defaultCheck"), for: .normal)
                       cell.crossOutlet.setImage(UIImage(named: "Close"), for: .normal)

                   }
                   
               cell.checkOutlet.tag = indexPath.row
               cell.crossOutlet.tag = indexPath.row
               cell.checkOutlet.addTarget(self, action: #selector(checkMarkAction), for: .touchUpInside)
               cell.crossOutlet.addTarget(self, action: #selector(crossMarkAction), for: .touchUpInside)
                   
               return cell
            
        }
    

        
    }
        
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
}



