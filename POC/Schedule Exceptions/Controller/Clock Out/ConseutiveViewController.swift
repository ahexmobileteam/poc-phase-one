//
//  ConseutiveViewController.swift
//  POC
//
//  Created by Ajeet N on 30/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit
import  CoreLocation


class ConseutiveViewController: UIViewController {


    @IBOutlet weak var alerttransparentView: UIView!
    @IBOutlet weak var alertFirstLabel: UILabel!
    @IBOutlet weak var alertDateLabel: UILabel!
    @IBOutlet weak var alertLocationLabel: UILabel!
    @IBOutlet weak var alertTimeLabel: UILabel!
    
    @IBOutlet weak var alertHeaderLabel: UILabel!
    
    @IBAction func alertOkAction(_ sender: Any) {
        if self.groupServiceResponseModel?.validateFlag ?? "" == "false"{
            alerttransparentView.isHidden = true
            return
        }
        alerttransparentView.isHidden = true
        guard let emptyStateVC = self.storyboard?.instantiateViewController(withIdentifier: "MoveToNextTransaction") as? MoveToNextTransaction else {return}
        self.navigationController?.pushViewController(emptyStateVC, animated: true)
        
    }
    
    @IBOutlet weak var heightOfMilageFlagTF: NSLayoutConstraint!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var buttonOutlet: CustomButtonC!
    @IBOutlet weak var mielageTextField: CustomTField!{
        didSet{
            self.mielageTextField.delegate = self
        }
    }
    
    @IBOutlet weak var consuetiveTableViewObj: UITableView! {
        didSet {
            consuetiveTableViewObj.delegate = self
            consuetiveTableViewObj.dataSource = self
        }
    }
    
    private let locationManager = CLLocationManager()
    private var latitude: String? = nil
    private var longitude: String? = nil
    private var address: String? = nil
    private var groupServiceResponseModel: GroupServiceResponseModel?
    var receivedMileage: Int?
    var receivedTaskCapture : Int?
    var receiveDetailID : String?
    var receiveConseutiveVisitIDs : Int?
    var receivePSName : String?
    var receiveVisitTime : String?
    var receivedOfficeID : Int?
    var receivePSID : Int?
    var receiveServiceCode : Int?
    var receiveArrDeptId: Int?
    
    private var ClockOutGroupedOrconseModelData : ClockOutGroupOrConseModel?
    private var clockOutConsuetiveModelResponse : ClockoutConseutiveModelData?
    private var clockoutTaksModelData : ClockoutTaskslistModel?
    
    
    var  conseutiveModel : ClockOutConseutiveModel?
    var schedule: SchedulesResponseModel?

    
    
//MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
            
        alerttransparentView.isHidden = true
        
            if receivePSName != nil {
            defaults.setValue(receivePSName ?? "", forKey: "GLOBALPSNAME")
            }
            
            self.nameLabel.text = (receivePSName ?? "")
            self.dateLabel.text = (receiveVisitTime ?? "")
            

        self.consuetiveAPICalling()

    let skipButton = UIBarButtonItem(title: "Skip", style: .plain, target: self, action: #selector(skipButtonAction));
     navigationItem.rightBarButtonItem = skipButton
            
            
    // Check Task Capture and desctiption
        if receivedTaskCapture == 0 {
            descriptionLabel.text = "Below are the Consecutive Services observed for the selected clock-out, tap on \"Save\" to create punched for all Consecutive Services , tap on \"Skip\" to proceed with selected Clock-Out option"
            buttonOutlet.setTitle("Save", for: .normal)
        print("Received taskcapture in conseutive view controller is 0")
    }
            
        else {
            
        descriptionLabel.text = "Below are the Consecutive Services observed for the selected clock-out, tap on \"Proceed\" to create punched for all Consecutive Services , tap on \"Skip\" to proceed with selected Clock-Out option"
            buttonOutlet.setTitle("Proceed", for: .normal)
            print("Received taskcapture in conseutive view controller is 1")

        }
            
            
    // Checking Mileage Flag
        if receivedMileage == 1 {
            self.mielageTextField.isHidden = false
        }
        else {
            self.mielageTextField.isHidden = true
            self.heightOfMilageFlagTF.constant = 0
        }
            
            
     // Saving Scehdules data
         if receivedOfficeID != nil {
                defaults.setValue(receivedOfficeID ?? "", forKey: "GLOBALOFFICEID")
            }
          
        if  receivePSID != nil {
                defaults.setValue(receivePSID ?? "", forKey: "GLOBALPSID")

        }
        
        if receiveDetailID != nil {
                defaults.setValue(receiveDetailID ?? "", forKey: "GLOBALVISITDETAILID")
        }
            
        if receiveServiceCode != nil {
                defaults.setValue(receiveServiceCode ?? "", forKey: "GLOBALSERVICEID")
         }
            
        if receiveArrDeptId != nil {
                defaults.set(receiveArrDeptId ?? "", forKey: "GLOBALARRDEPTID")
            }
        
        
}
    
    
//MARK: Skip Button Action
    @objc func skipButtonAction() {
        
        if receivedTaskCapture == 0 {
   self.getAddressFromLatLons(pdblLatitude: GetUserDetails.latitude ?? "", withLongitude: GetUserDetails.longitude ?? "", completion: { (address) in
                       var url = String()
                       let details = self.getuserDetails()

                       url.append((details.baseURL ?? "") + API.saveConseutiveURL + (deviceID ?? ""))
                       url.append("&dcsId=" + (details.dcsID ?? ""))
                       url.append(API.officeID + "\(self.receivedOfficeID ?? 0)")
                           if self.receivedMileage ?? 0 == 1{
                           url.append(API.Mileage + (self.mielageTextField.text ?? ""))
                       }else{
                           url.append(API.Mileage + ("0"))
                       }
                       url.append(API.latitude + (GetUserDetails.latitude ?? ""))
                       url.append(API.longitude + "\(GetUserDetails.longitude ?? "")")
                       url.append(API.address + address)
                       url.append(API.ipAddress + "10.2.0.1")
                       url.append(API.SessionID + (details.sessionID ?? ""))
                       var taskList: String?
                           if self.clockOutConsuetiveModelResponse?.count ?? 0 != 0{
                           for index in 0..<self.clockOutConsuetiveModelResponse!.count{
                               let list = ["psId": self.clockOutConsuetiveModelResponse?[index].psId ?? 0,"serviceId": self.clockOutConsuetiveModelResponse?[index].serviceCode ?? 0,"visitDetailsId": self.clockOutConsuetiveModelResponse?[index].visitDetailsId ?? 0,"arrivalDepartureId":self.receiveArrDeptId ?? 0,"taskCodes": ""] as [String : Any]
                               if taskList == nil{
                                   taskList = list.jsonStringRepresentation ?? ""
                               }else{
                                   taskList! += ",\(list.jsonStringRepresentation  ?? "")"
                               }
                           }
                           
                       }
                           url.append(API.visitDetailArrry + "[\(taskList ?? "")]")
                           print(url)
                           
                           ServiceManager.shared.request(type: GroupServiceResponseModel.self , url: url, method: .get, view: self.view) { [weak self](response) in
                               
                               if response?.result != nil {
                                   self?.groupServiceResponseModel = response
                                  if response?.validateFlag ?? "" == "departuretimecheck"{
                                       DispatchQueue.main.async {
                                           self?.showAlert(message: response?.result ?? "")
                                       }
                                   }else if response?.validateFlag ?? "" == "false" {
                                       DispatchQueue.main.async {
                                           self?.showAlert(message: response?.result ?? "")
                                       }
                                   }else{
                                       DispatchQueue.main.async {
                                       self?.alerttransparentView.isHidden = false
                                       if let result = response?.result{
                                           let resultDateAndTime = result.suffix(19)
                                           self?.alertDateLabel.text = String(resultDateAndTime.prefix(10))
                                           self?.alertTimeLabel.text = String(resultDateAndTime.suffix(8))
                                           self?.alertHeaderLabel.text = (response?.result?.dropLast(19).string ?? "")
                                       }
                                   }
                                   
                                   }
                               }
                           }
                           
                           
                       
                       })
                   }
        else {
            guard let cloutOutTasksController = self.storyboard?.instantiateViewController(withIdentifier: "cloutOutTasksController") as? CloutOutTasksController else { fatalError("not Found ClockINTokenController") }
            cloutOutTasksController.groupService = self.schedule
            self.navigationController?.pushViewController(cloutOutTasksController, animated: true)
            return
        }

    }
    
    
//MARK: Proceed or Save Button Action
    @IBAction func buttonAction(_ sender: Any) {

        if  receivedTaskCapture == 0 {
            
//http://poc.aquilasoftware.com/pocextacc-webservices_new/telephony/saveConsecutiveTransactions?imei=&dcsId=14619&officeId=191&mileage=36.25&latitude=37.4219983&longitude=-122.084&address=1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA&ipAddress=&sessionId=226145_OJ3BPA707WAVL0JE&visitDetailsArray=[{"psId":20334,"serviceId":99,"visitDetailsId":4636746,"arrivalDepartureId":0,"taskCodes":""},{"psId":20334,"serviceId":99,"visitDetailsId":4636762,"arrivalDepartureId":0,"taskCodes":""}]
       
            
        //http://poc.aquilasoftware.com/pocextacc-webservices_new/telephony/saveConsecutiveTransactions?imei=&dcsId=14568&officeId=0&mileage=&latitude=&longitude=&address=&ipAddress=&sessionId=194159_9GBNJU919LQ559XJ&visitDetailsArray=["visitDetailsId": 0, "serviceId": 0, "arrivalDepartureId": 0, "psId": 0]
            
            
            self.getAddressFromLatLons(pdblLatitude: GetUserDetails.latitude ?? "", withLongitude: GetUserDetails.longitude ?? "", completion: { (address) in
            var url = String()
            let details = self.getuserDetails()

            url.append((details.baseURL ?? "") + API.saveConseutiveURL + (deviceID ?? ""))
            url.append("&dcsId=" + (details.dcsID ?? ""))
            url.append(API.officeID + "\(self.receivedOfficeID ?? 0)")
                if self.receivedMileage ?? 0 == 1{
                url.append(API.Mileage + (self.mielageTextField.text ?? ""))
            }else{
                url.append(API.Mileage + ("0"))
            }
            url.append(API.latitude + (GetUserDetails.latitude ?? ""))
            url.append(API.longitude + "\(GetUserDetails.longitude ?? "")")
            url.append(API.address + address)
            url.append(API.ipAddress + "10.2.0.1")
            url.append(API.SessionID + (details.sessionID ?? ""))
            var taskList: String?
                if self.clockOutConsuetiveModelResponse?.count ?? 0 != 0{
                for index in 0..<self.clockOutConsuetiveModelResponse!.count{
                    let list = ["psId": self.clockOutConsuetiveModelResponse?[index].psId ?? 0,"serviceId": self.clockOutConsuetiveModelResponse?[index].serviceCode ?? 0,"visitDetailsId": self.clockOutConsuetiveModelResponse?[index].visitDetailsId ?? 0,"arrivalDepartureId":self.receiveArrDeptId ?? 0,"taskCodes": ""] as [String : Any]
                    if taskList == nil{
                        taskList = list.jsonStringRepresentation ?? ""
                    }else{
                        taskList! += ",\(list.jsonStringRepresentation  ?? "")"
                    }
                }
                
            }
                url.append(API.visitDetailArrry + "[\(taskList ?? "")]")
                print(url)
                
                ServiceManager.shared.request(type: GroupServiceResponseModel.self , url: url, method: .get, view: self.view) { (response) in
                    
                    if response?.result != nil {
                        self.groupServiceResponseModel = response
                        if response?.validateFlag ?? "" == "false"{
                             DispatchQueue.main.async {
                            self.alerttransparentView.isHidden = false

                            self.alertTimeLabel.text = response?.result ?? ""
                                 self.alertDateLabel.text = ""
                            }
                        }else{
                        print("The respone is :\(String(describing: response))")
                        DispatchQueue.main.async {
                            
                       self.alerttransparentView.isHidden = false
                      if let result = response?.result {
                    let resultDateAndTime = result.suffix(19)
                       self.alertDateLabel.text = String(resultDateAndTime.prefix(10))
                  self.alertTimeLabel.text = String(resultDateAndTime.suffix(8))
                                        }
                                        
                        }
                    }
                    }
                }
                
                
            
            })
        }
//    let url  = API.saveConseutiveURL + (GetUserDetails.dcsIDSaved ?? "")
//    let office = API.officeID + String(describing: self.conseutiveModel?.officeId ?? 0)
//    let mileage = API.Mileage + (self.mielageTextField.text ?? "")
//    let lat = API.latitude + (latitude ?? "")
//    let lon = API.longitude + (longitude ?? "")
//    let addres = API.address + (address ?? "")
//    let ip = API.ipAddress
//    let sesssionID = API.SessionID + (GetUserDetails.sessionID ?? "")
//    let visitDetail = API.visitDetailArrry
//
//    var jsonObject = [String: Any]()
//    jsonObject["psId"] = self.conseutiveModel?.psId ?? 0
//    jsonObject["serviceId"] = self.conseutiveModel?.serviceCode ?? 0
//    jsonObject["visitDetailsId"] = self.conseutiveModel?.visitDetailsId ?? 0
//    jsonObject["arrivalDepartureId"] = self.schedule?.arrDeptID ?? 0
//    jsonObject["taskCodes"] = self.conseutiveModel
//
//let saveUrl = url + office + mileage + lat + lon + addres + ip + sesssionID + visitDetail + "\(jsonObject)"
//    print("the save url is :\(String(describing: saveUrl))")
//
//    }

        else {
            if !(mielageTextField.text?.isEmpty ?? false){
                 if let milage = mielageTextField.text  {
                    if Double(milage) ?? 0.0 > 999.9{
                        toastWithDurationAndAlignment(msg: "Milage Value cannot be greater than 999.99", duration: 2.0, position: .bottom, view: self.view)
                                   return
                        }
                }
            }
            
        guard let singleTaskVC = self.storyboard?.instantiateViewController(withIdentifier: "ClockOutSingleTaskVC")  as? ClockOutSingleTaskVC else { return }
        singleTaskVC.receivedMileagVal = String(describing: self.mielageTextField?.text ?? "0")
        singleTaskVC.consutiveVales = self.receiveDetailID
        singleTaskVC.receieveOffId = self.receivedOfficeID
        singleTaskVC.clockoutConsuetiveResponse = clockOutConsuetiveModelResponse
        singleTaskVC.receiveConseutivevisitedID = receiveConseutiveVisitIDs
        singleTaskVC.receiveServiceTelCode = self.receiveServiceCode
        singleTaskVC.receoivePSID = self.receivePSID
            singleTaskVC.receiveArrDepID = self.receiveArrDeptId
        self.navigationController?.pushViewController(singleTaskVC, animated: true)
            
            
        }
        
                
}
    
    
//MARK: Consuetive API Calling
    func consuetiveAPICalling() {
        
    let details = self.getuserDetails()

    let Url = (details.baseURL ?? "")  +  API.conseutiveURL + (receiveDetailID ?? "0")
            
    let visistidID =  API.visitDetailID + "\(receiveConseutiveVisitIDs ?? 0)"
    let sessionID =  API.SessionID + (details.sessionID ?? "")
        let conseutiveUrl  = Url + visistidID + sessionID
        print("The conseutive url is :\(String(describing: conseutiveUrl))")
                                                   
    ServiceManager.shared.request(type: ClockoutConseutiveModelData.self, url: conseutiveUrl, method: .get, view: self.view) { (response) in
        print("The response of consecutive service apis is :\(String(describing: response))")
        
        DispatchQueue.main.async {
            self.clockOutConsuetiveModelResponse = response
            self.consuetiveTableViewObj.reloadData()
        }
                    
            }
        }

        
}


//MARK: UITableView Delegate and DataSource methods
extension ConseutiveViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return clockOutConsuetiveModelResponse?.count ?? 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
   
    guard let cell = tableView.dequeueReusableCell(withIdentifier: "ConsuetiveCell", for: indexPath) as? ConsuetiveCell else { return UITableViewCell() }

    cell.nameLabel.text = self.clockOutConsuetiveModelResponse?[indexPath.row].serviceName
        
    if let visted = self.clockOutConsuetiveModelResponse?[indexPath.row].visitTimes {
        let time = visted.split { $0.isNewline }
        cell.timeLabel.text = String(time[1])
    }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }

}

extension ConseutiveViewController: UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == mielageTextField{
            if textField.text?.count ?? 0 > 4{
                self.toast(msgString: "Limit reached, Milage should be below 6 characters.", view: self.view)
                textField.text?.removeLast()
                return false
            }else{
                return true
            }
        }
        return true
    }
}

