//
//  GroupedViewController.swift
//  POC
//
//  Created by Ajeet N on 07/05/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

typealias GroupedTaskModelResponse = [GroupedTaskModel]


struct GroupedTaskModel: Decodable {
    
    let visitDetailsId: Int?
    let psCode: Int?
    let serviceCode: Int?
    let officeId: Int?
    let psName: String?
    let arrDeptId: Int?
    let psId: Int?
    let serviceName: String?
    let visitTimes: String?
      
}


class GroupedViewController: UIViewController {
    
    var StringArrray = [String]()


    @IBOutlet weak var alertTransparentView: UIView!
    @IBOutlet weak var alertCardview: CustomView!
    @IBOutlet weak var alertFirstlabel: UILabel!
    @IBOutlet weak var alertDateLabel: UILabel!
    @IBOutlet weak var alertLocationLabel: UILabel!
    
    @IBOutlet weak var heightOfMilageTF: NSLayoutConstraint!
    @IBOutlet weak var alertTimeLabel: UILabel!
    @IBOutlet weak var milageStack: UIStackView!
    
    @IBOutlet weak var alertHeaderLabel: UILabel!
    
    var groupServiceListData : GroupedTaskModelResponse?
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var groupedTableView: UITableView!{
        didSet{
            self.groupedTableView.delegate = self
            self.groupedTableView.dataSource = self
        }
    }
    
    @IBOutlet weak var saveOrProccedButonOutlet: CustomButtonC!
    @IBOutlet weak var milegateTF: CustomTField!{
        didSet{
            self.milegateTF.delegate = self
        }
    }
    private var selectionHandler = [Bool]()
    
    var receivedServcieID : Int?
    var receivedVistDetailId : Int?
    var receivedtaskCapture : Int?
    var receivedOfficeId : Int?
    var receivedTitle: String?
    internal var milageFlag: Int?
    
//MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        if milageFlag ?? 0 == 1{
            milegateTF.isHidden = false
        }else{
            heightOfMilageTF.constant = 0
            milageStack.isHidden = true
            milegateTF.isHidden = true
        }
        navigationItem.title = "Clock Out For Grouped Services"
        groupedTableView.tableFooterView = UIView()
        self.alertTransparentView.isHidden = true
        
        getListofGrouopedServices()
        print("The recived task capture value in grouped view controller is :\(String(describing: receivedtaskCapture))")
        nameLabel.text = receivedTitle ?? ""
        if let task = receivedtaskCapture{
            if task == 1{
                saveOrProccedButonOutlet.setTitle("Proceed", for: .normal)
            }
            else {
                saveOrProccedButonOutlet.setTitle("Save", for: .normal)
            }
        }
    }
    
    
//MARK: Save or Proceed Button Action
    @IBAction func saveOrProceedButtonAction(_ sender: Any) {
        if let task = receivedtaskCapture{
            
           if task == 1{
            let defaultService = ["psId":self.groupServiceListData?[0].psId ?? 0,"serviceId":self.groupServiceListData?[0].serviceCode ?? 0,"visitDetailsId":self.groupServiceListData?[0].visitDetailsId ?? 0,"arrivalDepartureId":self.groupServiceListData?[0].arrDeptId ?? 0,"taskCodes":""] as [String: Any]
            var selectedServicesList = [[String: Any]]()
                      selectedServicesList.removeAll()
                      var json = String()
                      json.removeAll()
                      for selected in 0..<self.selectionHandler.count{
                          if self.selectionHandler[selected]{
                              if selected == 0{
                                selectedServicesList.append(defaultService)
                                StringArrray.append(self.groupServiceListData?[selected].psName ?? "")
                                

                              }else{
                                  let sel = ["psId":self.groupServiceListData?[selected].psId ?? 0,"serviceId":self.groupServiceListData?[selected].serviceCode ?? 0,"visitDetailsId":self.groupServiceListData?[selected].visitDetailsId ?? 0,"arrivalDepartureId":self.groupServiceListData?[selected].arrDeptId ?? 0,"taskCodes":""] as [String: Any]
                                selectedServicesList.append(sel)
                                StringArrray.append(self.groupServiceListData?[selected].psName ?? "")

                              }
                          }
                      }
            
              guard let groupedMultiTaskVC = self.storyboard?.instantiateViewController(withIdentifier: "GroupedMultiTaskClockoutVC") as? GroupedMultiTaskClockoutVC else {fatalError("not found GroupedMultiTaskClockoutVC") }
            groupedMultiTaskVC.selectedList = selectedServicesList
            groupedMultiTaskVC.StringArrray = self.StringArrray
            groupedMultiTaskVC.officeID = receivedOfficeId
            groupedMultiTaskVC.groupServiceListData = self.groupServiceListData

            if milageFlag ?? 0 == 1{
                if milegateTF.text?.isEmpty ?? false{
                     groupedMultiTaskVC.milageFlag = "0"
                }else{
                     groupedMultiTaskVC.milageFlag = self.milegateTF.text ?? ""
                }
            }else{
                groupedMultiTaskVC.milageFlag = "0"
            }
              self.navigationController?.pushViewController(groupedMultiTaskVC, animated: true)
            }
             else {
                saveGrouped()
            }
          }
    }
    

//MARK: Saved Grouped Services
    func saveGrouped() {
        
        var url = String()
        url.removeAll()
        
        let details = self.getuserDetails()
        
        url.append((details.baseURL ?? "")  + API.SaveGroupedTransactions + (deviceID ?? ""))
        url.append(API.dcsID + "\(details.dcsID ?? "")")
        url.append(API.officeID + "\(receivedOfficeId ?? 0)")
        if milageFlag ?? 0 == 1{
            url.append(API.Mileage + "\(self.milegateTF.text ?? "0")")
        }else{
            url.append(API.Mileage + "0")
        }
        url.append(API.TravelTime + "0")
        url.append(API.TransactionType + "DEPARTURE")
        url.append(API.latitude + "\(GetUserDetails.latitude ?? "")")
        url.append(API.longitude + "\(GetUserDetails.longitude ?? "")")
        url.append(API.ipAddress + "10.0.2.16")
        url.append(API.SessionID + "\(details.sessionID ?? "")")
        self.getAddressFromLatLons(pdblLatitude: "\(GetUserDetails.latitude ?? "")", withLongitude: "\(GetUserDetails.longitude ?? "")") { (address) in
            url.append(API.address + "\(address)")
            
            let defaultService = ["psId":self.groupServiceListData?[0].psId ?? 0,"serviceId":self.groupServiceListData?[0].serviceCode ?? 0,"visitDetailsId":self.groupServiceListData?[0].visitDetailsId ?? 0,"arrivalDepartureId":self.groupServiceListData?[0].arrDeptId ?? 0,"taskCodes":""] as [String: Any]
            var selectedServicesList = [NSMutableDictionary]()
            selectedServicesList.removeAll()
            var json = String()
            json.removeAll()
            for selected in 0..<self.selectionHandler.count{
                if self.selectionHandler[selected]{
                    if selected == 0{
                        json.append(defaultService.jsonStringRepresentation ?? "")
                    }else{
                        let sel = ["psId":self.groupServiceListData?[selected].psId ?? 0,"serviceId":self.groupServiceListData?[selected].serviceCode ?? 0,"visitDetailsId":self.groupServiceListData?[selected].visitDetailsId ?? 0,"arrivalDepartureId":self.groupServiceListData?[0].arrDeptId ?? 0,"taskCodes":""] as [String: Any]
                        let jsonObject = sel.jsonStringRepresentation ?? ""
                        json.append(",")
                        json.append(jsonObject)
                    }
                }
            }
            url.append(API.VisitDetailArrayWithJsonObj + "[" + json + "]")
            print(url)
            DispatchQueue.main.async {
                ServiceManager.shared.request(type: GroupServiceResponseModel.self, url: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "", method: .get, view: self.view) { [weak self](response) in
                    if response?.result != nil{
                        if response?.validateFlag ?? "" == "departuretimecheck"{
                            DispatchQueue.main.async {
                                self?.showAlert(message: response?.result ?? "")
                            }
                        }else if response?.validateFlag ?? "" == "false" {
                            DispatchQueue.main.async {
                                self?.showAlert(message: response?.result ?? "")
                            }
                        }else{
                            DispatchQueue.main.async {
                            self?.alertTransparentView.isHidden = false
                            if let result = response?.result{
                                let resultDateAndTime = result.suffix(19)
                                self?.alertDateLabel.text = String(resultDateAndTime.prefix(10))
                                self?.alertTimeLabel.text = String(resultDateAndTime.suffix(8))
                                self?.alertHeaderLabel.text = (response?.result?.dropLast(19).string ?? "")
                            }
                        }
                        
                        }

                    }else{
                        self?.toast(msgString: "Error occured", view: self?.view ?? UIView())
                    }
                }
             
            }
         }
    }
    
    
    //MARK: Is Selected Action
    @objc func isSelected(sender: UIButton){
           selectionHandler[sender.tag] = !selectionHandler[sender.tag]
           groupedTableView.reloadData()
       }
    
    
//MARK: Get List of Grouped Services
    func getListofGrouopedServices()  {
        
//http://poc.aquilasoftware.com/pocextacc-webservices_phase2/telephony/getDepartureGroupedServices?dcsId=14619&serviceId=17&visitDetailsId=4637007&sessionId=226145_QGX2N37JV0T9Q8XX
        
        let details = self.getuserDetails()
        
    let url = (details.baseURL ?? "") + "getDepartureGroupedServices?dcsId=\(details.dcsID ?? "")&serviceId=\(receivedServcieID ?? 0)&visitDetailsId=\(receivedVistDetailId ?? 0)&sessionId=\(details.sessionID ?? "")"
    
    ServiceManager.shared.request(type: GroupedTaskModelResponse.self, url: url, method: .get, view: self.view) { [weak self](response) in
        if response?.count ?? 0 != 0 {
            self?.groupServiceListData = response
            for indexValue in 0..<response!.count {
                if indexValue == 0 {
                    self?.selectionHandler.append(true)
                }
                else {
                    self?.selectionHandler.append(false)
                }
            }
            DispatchQueue.main.async {
                let visit = response?[0].visitTimes?.split { $0.isNewline }
                self?.dateLabel.text = String(visit?[0] ?? "")
                self?.groupedTableView.reloadData()
            }
        }
    }
    
}
    
    //MARK: ALert Okay Buttn Action
  @IBAction func alertOkbButtonAction(_ sender: Any) {
    
    alertTransparentView.isHidden = true
    guard let emptyStateVC = self.storyboard?.instantiateViewController(withIdentifier: "MoveToNextTransaction") as? MoveToNextTransaction else {return}
    self.navigationController?.pushViewController(emptyStateVC, animated: true)
    
    
    
    }
    
    
}


//MARK: UITableView Delegate and DataSource Methods
extension GroupedViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.groupServiceListData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "GroupedCell", for: indexPath) as? GroupedCell else { return UITableViewCell() }
        cell.selectionStyle = .none
        indexPath.row == 0 ? (cell.selecteStateOutlet.setImage(UIImage(named: "Checked")?.withRenderingMode(.alwaysOriginal), for: .normal)) : (selectionHandler[indexPath.row] ? (cell.selecteStateOutlet.setImage(UIImage(named: "Checked")?.withRenderingMode(.alwaysOriginal), for: .normal)) : (cell.selecteStateOutlet.setImage(UIImage(named: "Checked_01")?.withRenderingMode(.alwaysOriginal), for: .normal)))
        indexPath.row == 0 ? (cell.selecteStateOutlet.isUserInteractionEnabled = false) : (cell.selecteStateOutlet.isUserInteractionEnabled = true)
        cell.nameLabel.text = self.groupServiceListData?[indexPath.row].psName ?? ""
        if let visited = self.groupServiceListData?[indexPath.row].visitTimes{
            let time = visited.split { $0.isNewline }
            cell.timeLabel.text = String(time[1])
        }
        cell.selecteStateOutlet.tag = indexPath.row
        cell.selecteStateOutlet.addTarget(self, action: #selector(isSelected(sender:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
}
extension GroupedViewController: UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == milegateTF{
            if milegateTF.text?.count ?? 0 > 4{
                self.toast(msgString: "Limit reached, Milage should be below 6 characters.", view: self.view)
                textField.text?.removeLast()
                return false
            }else{
                return true
            }
        }
        return true
    }
}
