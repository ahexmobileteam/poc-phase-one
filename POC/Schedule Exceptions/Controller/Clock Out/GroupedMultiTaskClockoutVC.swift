//
//  GroupedMultiTaskClockoutVC.swift
//  POC
//
//  Created by Ajeet N on 07/05/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

typealias MultipleTasksModel = [TaskResponseModel]
struct TaskResponseModel: Decodable{
    let taskCode: String?
    let taskCodeAndName: String?
    let taskName: String?
    let taskId: Int?
}

class GroupedMultiTaskClockoutVC: UIViewController {
    @IBOutlet weak var serviceNameLabel: UILabel!
    
    @IBOutlet weak var alertTransparentView: UIView!
    @IBOutlet weak var alertCardView: CustomView!
    @IBOutlet weak var alertFirstLabel: UILabel!
    @IBOutlet weak var alertDateLabel: UILabel!
    @IBOutlet weak var alertLocationLabel: UILabel!
    @IBOutlet weak var alertTimeLabel: UILabel!
   
    @IBOutlet weak var alertHeaderLabel: UILabel!
    
    internal var StringArrray = [String]()

    private var  multiTaskModelData  : MultipleTasksModel?
    private var groupServiceResponseModel: GroupServiceResponseModel?
    var groupServiceListData : GroupedTaskModelResponse?

    @IBOutlet weak var saveButtonOutlet: CustomButtonC!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var tableViewObj: UITableView! {
        didSet {
            tableViewObj.delegate = self
            tableViewObj.dataSource = self
        }
    }
    
    var selectedList = [[String: Any]]()
    var selectionHandler = [HandleSelection]()
    var officeID: Int?
    
    private var selectedTasksList: String?
    internal var milageFlag: String?
    
    private var initialCount = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewObj.tableFooterView = UIView()
        alertTransparentView.isHidden = true
        navigationItem.title = "POC Visit Verification"
        if selectedList.count == 1{
            self.listOftasks(officeID: officeID ?? 0, visitedID: selectedList[0]["visitDetailsId"] as! Int, serviceTelCode: selectedList[0]["serviceId"] as! Int, psCode: selectedList[0]["psId"] as! Int)
            saveButtonOutlet.setTitle("Save", for: .normal)
            
        }else if selectedList.count > 1{
            self.listOftasks(officeID: officeID ?? 0, visitedID: selectedList[0]["visitDetailsId"] as! Int, serviceTelCode: selectedList[0]["serviceId"] as! Int, psCode: selectedList[0]["psId"] as! Int)
            saveButtonOutlet.setTitle("Next", for: .normal)
        }
    }
    
    func listOftasks(officeID: Int,visitedID: Int,serviceTelCode: Int,psCode: Int)  {
    //http://poc.aquilasoftware.com/pocextacc-webservices_new/telephony/getTaskList?officeId=191&sessionId=226145_UHCVHZ2EWXP6NREJ&taskName=&visitDetailsId=4636792&psTelCode=0&serviceTelCode=17
        var url = String()
        url.removeAll()
        
        let details = self.getuserDetails()
        url.append((details.baseURL ?? "") + "getTaskList?officeId=\(officeID)&sessionId=\(details.sessionID ?? "")&taskName=&visitDetailsId=\(visitedID)&psTelCode=\(psCode)&serviceTelCode=\(serviceTelCode)")
        ServiceManager.shared.request(type: MultipleTasksModel.self, url: url, method: .get, view: self.view) { [weak self](response) in
           
            if response?.count ?? 0 != 0 {
                self?.multiTaskModelData = response
                for  _ in 0..<response!.count {
                    self?.selectionHandler.append(HandleSelection(isTapped: false, isSelected: false))
                }
                DispatchQueue.main.async {
                    self?.nameLabel.text = self?.StringArrray[self?.initialCount ?? 0]
                    self?.serviceNameLabel.text = self?.groupServiceListData?[self?.initialCount ?? 0].serviceName ?? ""
                    self?.initialCount += 1
                    self?.tableViewObj.reloadData()
                }
            }
        }
    }
    
    
    
    func saveAll () {
        //http://poc.aquilasoftware.com/pocextacc-webservices_new/telephony/saveGropuedTransactions?imei=&dcsId=14619&officeId=191&mileage=36.25&latitude=37.4219983&longitude=-122.084&address=1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA&ipAddress=&sessionId=226145_OJ3BPA707WAVL0JE&visitDetailsArray=[{"psId":20334,"serviceId":99,"visitDetailsId":4636746,"arrivalDepartureId":0,"taskCodes":""},{"psId":20334,"serviceId":99,"visitDetailsId":4636762,"arrivalDepartureId":0,"taskCodes":""}]
        
        
        
    }
   
    @IBAction func saveButtonAction(_ sender: Any) {
        if initialCount == selectedList.count{
            var taskCodes = [String]()
            for index in 0..<selectionHandler.count{
                if selectionHandler[index].isTapped {
                    if selectionHandler[index].isSelected {
                        taskCodes.append(self.multiTaskModelData?[index].taskCode ?? "")
                    }
                }
            }
            let selected = ["psId":self.selectedList[initialCount - 1]["psId"] as! Int,"serviceId":self.selectedList[initialCount - 1]["serviceId"] as! Int,"visitDetailsId":self.selectedList[initialCount - 1]["visitDetailsId"] as! Int,"arrivalDepartureId":self.selectedList[initialCount - 1]["arrivalDepartureId"] as! Int,"taskCodes":taskCodes.map({ $0 }).joined(separator: ",")] as [String : Any]
            if selectedTasksList == nil{
                selectedTasksList = selected.jsonStringRepresentation ?? ""
            }else{
                selectedTasksList! += ",\(selected.jsonStringRepresentation ?? "")"

            }
            taskCodes.removeAll()
            var url = String()
            url.removeAll()
            let details = self.getuserDetails()
            self.getAddressFromLatLons(pdblLatitude:(GetUserDetails.latitude ?? ""), withLongitude: GetUserDetails.longitude ?? "") { (address) in
                url.append((details.baseURL ?? "") + "saveGroupedTransactions?imei=\(deviceID ?? "")&dcsId=\(details.dcsID ?? "")&officeId=\(self.officeID ?? 0)&mileage=\(self.milageFlag ?? "")&transactionType=DEPARTURE&travelTime=0&latitude=\(GetUserDetails.latitude ?? "")&longitude=\(GetUserDetails.longitude ?? "")&address=\(address)&ipAddress=100.0.10.2&sessionId=\(details.sessionID ?? "")&visitDetailsArray=[\(self.selectedTasksList ?? "")]")
                print(url)
                
                ServiceManager.shared.request(type: GroupServiceResponseModel.self, url: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "", method: .get, view: self.view) { [weak self](response) in
                    if response?.result != nil{
                        self?.groupServiceResponseModel = response
                       if response?.result != nil{
                           //It seems entered OTP is invalid, Clock - In created successfully at 05/05/2020 01:49 PM
                              if response?.validateFlag ?? "" == "departuretimecheck"{
                                                      DispatchQueue.main.async {
                                                          self?.showAlert(message: response?.result ?? "")
                                                      }
                                                  }else if response?.validateFlag ?? "" == "false" {
                                                      DispatchQueue.main.async {
                                                          self?.showAlert(message: response?.result ?? "")
                                                      }
                                                  }else{
                                                      DispatchQueue.main.async {
                                                      self?.alertTransparentView.isHidden = false
                                                      if let result = response?.result{
                                                          let resultDateAndTime = result.suffix(19)
                                                          self?.alertDateLabel.text = String(resultDateAndTime.prefix(10))
                                                          self?.alertTimeLabel.text = String(resultDateAndTime.suffix(8))
                                                          self?.alertHeaderLabel.text = (response?.result?.dropLast(19).string ?? "")
                                                      }
                                                  }
                                                  
                                                  }
                       }
                                                
                    }
                }
                
            }
            
            print(selectedTasksList)
            //save api
        }else{

            var taskCodes = [String]()
            for index in 0..<selectionHandler.count{
                if selectionHandler[index].isTapped {
                    if selectionHandler[index].isSelected {
                        taskCodes.append(self.multiTaskModelData?[index].taskCode ?? "")
                    }
                }
            }
            let selected = ["psId":self.selectedList[initialCount - 1]["psId"] as! Int,"serviceId":self.selectedList[initialCount - 1]["serviceId"] as! Int,"visitDetailsId":self.selectedList[initialCount - 1]["visitDetailsId"] as! Int,"arrivalDepartureId":self.selectedList[initialCount - 1]["arrivalDepartureId"] as! Int,"taskCodes":taskCodes.map({ $0 }).joined(separator: ",")] as [String : Any]
            if selectedTasksList == nil {
                selectedTasksList = selected.jsonStringRepresentation ?? ""
            }else{
                selectedTasksList! += ",\(selected.jsonStringRepresentation ?? "")"
            }
            selectionHandler.removeAll()
            taskCodes.removeAll()
            self.listOftasks(officeID: officeID ?? 0, visitedID: selectedList[initialCount]["visitDetailsId"] as! Int, serviceTelCode: selectedList[initialCount]["serviceId"] as! Int, psCode: selectedList[initialCount]["psId"] as! Int)
            if initialCount == (selectedList.count - 1){
                saveButtonOutlet.setTitle("Save", for: .normal)
            }else{
                saveButtonOutlet.setTitle("Next", for: .normal)
            }
        }
        
    }
    
    
    @objc func checkMarkAction(sender: UIButton) {
          
          self.selectionHandler[sender.tag].isTapped = true
          if self.selectionHandler[sender.tag].isSelected {
              self.selectionHandler[sender.tag].isSelected = false
          }
          else {
              self.selectionHandler[sender.tag].isSelected = true
          }
          self.tableViewObj.reloadData()
      }
      
      
     @objc func crossMarkAction(sender: UIButton)  {
          
      self.selectionHandler[sender.tag].isTapped = true
             if self.selectionHandler[sender.tag].isSelected {
                 self.selectionHandler[sender.tag].isSelected = false
             }
             else {
                 self.selectionHandler[sender.tag].isSelected = true
             }
             self.tableViewObj.reloadData()
      
      }

    
    @IBAction func alertOkButtonAction(_ sender: Any) {
        if groupServiceResponseModel?.validateFlag ?? "" == "false"{
              alertTransparentView.isHidden = true
            return
        }
        alertTransparentView.isHidden = true
              guard let emptyStateVC = self.storyboard?.instantiateViewController(withIdentifier: "MoveToNextTransaction") as? MoveToNextTransaction else {return}
            self.navigationController?.pushViewController(emptyStateVC, animated: true)
        
        
        
       }
    
}


//MARK: UITableView Delegate and Datasource Methods
extension GroupedMultiTaskClockoutVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.multiTaskModelData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
    guard let cell = tableView.dequeueReusableCell(withIdentifier: "GroupedMultiTaskCell", for: indexPath) as? GroupedMultiTaskCell else { return UITableViewCell() }
        cell.taskLabel.text =  self.multiTaskModelData?[indexPath.row].taskName ?? ""
        if selectionHandler[indexPath.row].isTapped {
            if selectionHandler[indexPath.row].isSelected {
                cell.checkMarkOutlet.setImage(UIImage(named: "selectedcheck"), for: .normal)
                cell.crossMarkOutlet.setImage(UIImage(named: "Close"), for: .normal)
            }
            else {
                cell.checkMarkOutlet.setImage(UIImage(named: "defaultCheck"), for: .normal)
                cell.crossMarkOutlet.setImage(UIImage(named: "selectedcross"), for: .normal)

            }
        }
        else {
            cell.checkMarkOutlet.setImage(UIImage(named: "defaultCheck"), for: .normal)
            cell.crossMarkOutlet.setImage(UIImage(named: "Close"), for: .normal)
        }
        cell.checkMarkOutlet.tag = indexPath.row
        cell.crossMarkOutlet.tag = indexPath.row
        cell.checkMarkOutlet.addTarget(self, action: #selector(checkMarkAction), for: .touchUpInside)
        cell.crossMarkOutlet.addTarget(self, action: #selector(crossMarkAction), for: .touchUpInside)
    return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
}
