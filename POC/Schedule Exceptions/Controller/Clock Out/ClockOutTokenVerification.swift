//
//  ClockOutTokenVerification.swift
//  POC
//
//  Created by Ajeet N on 29/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit
import Foundation
import  CoreLocation


class ClockOutTokenVerification: UIViewController{
    
    @IBOutlet weak var alertHeaderLabel: UILabel!
    @IBOutlet weak var alertTransparentView: UIView!
    @IBOutlet weak var alertCardViewObj: CustomView!
    @IBOutlet weak var alertFirstLabel: UILabel!
    @IBOutlet weak var alertDateLabel: UILabel!
    @IBOutlet weak var alertLocationLabel: UILabel!
    
    @IBOutlet weak var saveButton: CustomButtonC!
    @IBOutlet weak var alertTimeLabel: UILabel!
    internal var milageFlag: Int?
    @IBAction func alertOkButton(_ sender: Any) {
        
        alertTransparentView.isHidden = true
        guard let emptyStateVC = self.storyboard?.instantiateViewController(withIdentifier: "MoveToNextTransaction") as? MoveToNextTransaction else {return}
        self.navigationController?.pushViewController(emptyStateVC, animated: true)
    }
    
    private let locationManager = CLLocationManager()
    private var latitude: String? = nil
    private var longitude: String? = nil
    private var address: String? = nil
    
    var receivedOtpFlag : Int?
    var schedule: SchedulesResponseModel?
    var clockOutString: String?
    internal var taskCaptureFlag: Int?
    internal var groupService: SchedulesResponseModel?
    @IBOutlet weak var tokenTF: CustomTField!
    @IBOutlet weak var mileageTF: CustomTField!{
        didSet{
            self.mileageTF.delegate = self
        }
    }
    

//MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "POC Visit Verification"
        
        self.alertTransparentView.isHidden = true
        if milageFlag ?? 2 == 0{
            mileageTF.isHidden = true
            
        }else{
            mileageTF.isHidden = false
        }
        if taskCaptureFlag ?? 0 == 1{
            saveButton.setTitle("Next", for: .normal)
        }else{
            saveButton.setTitle("Save", for: .normal)

        }
        if receivedOtpFlag ?? 2 == 0{
            tokenTF.isHidden = true
        }else{
            tokenTF.isHidden = false
        }
        
}
    
    
    //MARK: Save Button Action
    @IBAction func saveButtonAction(_ sender: Any) {
        
        if receivedOtpFlag != 0{
            if tokenTF.text!.isEmpty {
                self.toastWithDurationAndAlignment(msg: "Please enter token code", duration: 2.0, position: .center, view: self.view)
                       return
                   }
        }
        if milageFlag ?? 2 == 0{
            
        }else{
            if mileageTF.text!.isEmpty {
//                self.toast(msgString: "Please enter Mileage Value", view: self.view)
//                return
            }
        }
        if taskCaptureFlag ?? 0 == 1{
            if let milage = mileageTF.text  {
                if Double(milage) ?? 0.0 > 999.9{
                    toastWithDurationAndAlignment(msg: "Milage Value cannot be greater than 999.99", duration: 2.0, position: .bottom, view: self.view)
                    return
                }
            }
            
            guard let cloutOutTasksController = self.storyboard?.instantiateViewController(withIdentifier: "cloutOutTasksController") as? CloutOutTasksController else { fatalError("not Found ClockINTokenController") }
            cloutOutTasksController.groupService = self.groupService
            if receivedOtpFlag ?? 2 != 0{
                cloutOutTasksController.receivedOtpFlag = tokenTF.text
            }
            if milageFlag ?? 2 != 0{
                cloutOutTasksController.milageFlag = mileageTF.text
            }
            self.navigationController?.pushViewController(cloutOutTasksController, animated: true)
            return
        }
        // If OTP Flag == 1 means we need to call one  API
    // http://poc.aquilasoftware.com/pocextacc-webservices_new/telephony/createReportedVisitToSchedule?jsonObj={"imei":"f47adbda-2810-47df-9c1f-a7724afa3208","dcsId":14619,"psId":20338,"officeId":191,"serviceId":17,"mileage":0,"travelTime":0,"taskCodes":"","transactionType":"DEPARTURE","latitude":"37.4219983","longitude":"-122.084","address":"1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA\n","visitDetailsId":4636011,"arrDeptId":0,"ipAddress":"10.0.2.16","otp":"123456"}&sessionId=226145_UJD006S6XBFIIWCE
      
    let StaticIPAddress = "103.59.154.211"

        let details = self.getuserDetails()

        var jsonObject = [String: Any]()
            jsonObject["imei"] = (deviceID ?? "")
            jsonObject["dcsId"] = (details.dcsID ?? "")
            jsonObject["psId"] = (self.schedule?.psID ?? 0)
            jsonObject["officeId"] = (self.schedule?.officeID ?? 0)
            jsonObject["serviceId"] = (self.schedule?.serviceCode ?? 0)
            if milageFlag ?? 2 == 0{
                jsonObject["mileage"] = "0"
            }else{
                jsonObject["mileage"] = mileageTF.text ?? ""
            }
            jsonObject["travelTime"] = 0
            jsonObject["transactionType"] = clockOutString ?? ""
            jsonObject["latitude"] = (GetUserDetails.latitude ?? "")
            jsonObject["longitude"] = (GetUserDetails.longitude ?? "")
            jsonObject["visitDetailsId"] = (self.schedule?.visitDetailsID ?? 0)
            jsonObject["arrDeptId"] = (self.schedule?.arrDeptID ?? 0)
            jsonObject["ipAddress"] = StaticIPAddress
            if receivedOtpFlag ?? 2 == 0{
               jsonObject["otp"] = ""
             }else{
                jsonObject["otp"] = tokenTF.text ?? ""
            }
        DispatchQueue.main.async {
            self.getAddressFromLatLons(pdblLatitude: (GetUserDetails.latitude ?? ""), withLongitude: (GetUserDetails.longitude ?? "")) { (address) in
            
                jsonObject["address"] = address
                
                let details = self.getuserDetails()

                
                let url = (details.baseURL ?? "") + API.CREATEREPORTEDVISIT + "\(jsonObject.jsonStringRepresentation ?? "")" + "&sessionId=\(details.sessionID ?? "")"
                print("The url in clockin token screen is :\(String(describing: url))")
                ServiceManager.shared.request(type: GroupServiceResponseModel.self, url: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "", method: .get, view: self.view) { [weak self](response) in
                    if response?.result != nil{
                         if response?.validateFlag ?? "" == "departuretimecheck"{
                                                   DispatchQueue.main.async {
                                                       self?.showAlert(message: response?.result ?? "")
                                                   }
                                               }else if response?.validateFlag ?? "" == "false" {
                                                   DispatchQueue.main.async {
                                                       self?.showAlert(message: response?.result ?? "")
                                                   }
                                               }else{
                                                   DispatchQueue.main.async {
                                                   self?.alertTransparentView.isHidden = false
                                                   if let result = response?.result{
                                                       let resultDateAndTime = result.suffix(19)
                                                       self?.alertDateLabel.text = String(resultDateAndTime.prefix(10))
                                                       self?.alertTimeLabel.text = String(resultDateAndTime.suffix(8))
                                                       self?.alertHeaderLabel.text = (response?.result?.dropLast(19).string ?? "")
                                                   }
                                               }
                                               
                                               }
                           
                    }
                    
                    else{
                            DispatchQueue.main.async {
                                self?.toast(msgString: response?.result ?? "", view: self?.view ?? UIView())
                        }
                        }
                    }
                }
            }
        }
    }



extension ClockOutTokenVerification: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == mileageTF{
            if textField.text?.count ?? 0 > 4{
                self.toast(msgString: "Limit reached, Milage should be below 6 characters.", view: self.view)
                textField.text?.removeLast()
                return false
            }else{
                return true
            }
        }
        return true
    }
}
