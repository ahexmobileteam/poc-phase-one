//
//  ScheduleExceptionsVC.swift
//  POC
//
//  Created by Ajeet N on 08/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit
import  CoreLocation
import Foundation


struct ServiceHandlerModel {
    var clockInSelected: Bool
    var clockOutSelected: Bool
    var reportAbsentSelected: Bool
}

struct MultipleServicesModel: Decodable {
    let groupedServiceCount: Int?
    let consecutiveServiceCount: Int?
}

class ScheduleExceptionsVC: UIViewController {
    
    @IBOutlet weak var popUpTransparentView: UIView!
    @IBOutlet weak var popUpCardViewObj: UIView!
    @IBOutlet weak var popUpDateLabel: UILabel!
    @IBOutlet weak var popUpTimeLabel: UILabel!
    
    @IBOutlet weak var alertHeaderTitleLabel: UILabel!
    var refreshControl = UIRefreshControl()

    var receivedCallInFlage: Int?
    
    @IBOutlet weak var emptyStatusLabel: UILabel!
    
    @IBAction func popupOkButtonAction(_ sender: Any) {
        self.popUpTransparentView.isHidden = true
        guard let emptyStateVC = self.storyboard?.instantiateViewController(withIdentifier: "MoveToNextTransaction") as? MoveToNextTransaction else {return}
        self.navigationController?.pushViewController(emptyStateVC, animated: true)
    }
    private var timer = Timer()
    var callInFlagValue : Int?
    private let locationManager = CLLocationManager()
    
    @IBOutlet weak var settingsTableViewheightConstrant: NSLayoutConstraint!
    @IBOutlet weak var heightOfSettingsTableView: NSLayoutConstraint!
    @IBOutlet weak var transparentView: UIView!
    @IBOutlet weak var settingsTableViewObj: UITableView! {
        didSet {
            self.settingsTableViewObj.delegate = self
            self.settingsTableViewObj.dataSource = self
            self.settingsTableViewObj.layer.cornerRadius = 5.0
        }
    }
    @IBOutlet weak var scheduleTableView: UITableView!{
        didSet{
            self.scheduleTableView.delegate = self
            self.scheduleTableView.dataSource = self
        }
    }
    private let transiton = SlideInTransition()
    private var schedules = [[SchedulesResponseModel]]()
    private var serviceHandler = [[ServiceHandlerModel]]()
    private var expand = [[Bool]]()
    let settingsArray = ["Today's Schedules","Search Schedules","Report UnScheduled Vist","Non-Client Shift","Time Sheet"]
    private var ClockOutGroupedOrconseModelData : ClockOutGroupOrConseModel?
    private var clockOutConsuetiveModelResponse : ClockoutConseutiveModelData?
    
    
    //MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkLocationServices()
        if let sessionTime = defaults.value(forKey: kSession) as? Int{
            timer = Timer.scheduledTimer(timeInterval: TimeInterval(sessionTime), target: self, selector: #selector(sessionTimeOut), userInfo: nil, repeats: true)
        }
        
        transparentView.isHidden = true
        popUpTransparentView.isHidden = true
        self.popUpCardViewObj.layer.cornerRadius = 5.0
        self.popUpCardViewObj.layer.masksToBounds = false
        
        
        self.configureNavigationBar(title: "Schedules")
        let logoutBarButtonItem = UIBarButtonItem(image: UIImage(named: "Right Menu"), style: .plain, target: self, action: #selector(logoutUser))
        self.navigationItem.rightBarButtonItem  = logoutBarButtonItem
        scheduleTableView.sectionHeaderHeight = 0
        scheduleTableView.sectionFooterHeight = 0
        
        settingsTableViewheightConstrant.constant = CGFloat(settingsArray.count * 50)
        
        scheduleTableView.tableHeaderView = UIView(frame: CGRect(origin: .zero, size:
            CGSize(width: 0, height: CGFloat.leastNormalMagnitude)))
        scheduleTableView.tableFooterView = UIView(frame: CGRect(origin: .zero, size:
            CGSize(width: 0, height: CGFloat.leastNormalMagnitude)))
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tapGesture.delegate = self
        transparentView.addGestureRecognizer(tapGesture)
        
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        scheduleTableView.addSubview(refreshControl) // not required when using UITableViewContr
        
        self.callInflagCheck()
        
        settingsTableViewObj.tableFooterView = UIView(frame: CGRect(origin: .zero, size:
            CGSize(width: 0, height: CGFloat.leastNormalMagnitude)))
        NotificationCenter.default.addObserver(self, selector: #selector(sessionExpired), name: Notification.Name("SessionExpired"), object: nil)
    }
    
    @objc func sessionTimeOut(){
        self.session()
    }
    
    func resetSession(){
        timer.invalidate()
        if let sessionTime = defaults.value(forKey: kSession) as? Int{
        timer = Timer.scheduledTimer(timeInterval: TimeInterval(sessionTime), target: self, selector: #selector(sessionTimeOut), userInfo: nil, repeats: true)
            
               }
    }
    override func viewWillDisappear(_ animated: Bool) {
        timer.invalidate()
        if let sessionTime = defaults.value(forKey: kSession) as? Int{
            timer = Timer.scheduledTimer(timeInterval: TimeInterval(sessionTime), target: self, selector: #selector(sessionTimeOut), userInfo: nil, repeats: true)
        }
    }
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        self.resetSession()
    }
    func callInflagCheck() {
        
        print("The received dcs flag is :\(String(describing: receivedCallInFlage ?? 0))")
        
        if receivedCallInFlage ?? 0 == 1 {
            self.alert(title: "NOTE: Your supervisor has requested that you call the office as soon as possible.", message: "", actionTitles: ["Remind Later", "Ignore"], actionStyle: [.default, .default], action: [
                           { Remind  in
                               
                               
                           }, { Ignore in
                  
//http://poc.aquilasoftware.com/pocextacc-webservices_phase2/telephony/updateDCSCallInFlag?dcsId=180837&sessionId=180837_7ZR5HYZ9SNQGC4MU

                let details = self.getuserDetails()
                let url = (details.baseURL ?? "") + API.updateDCSCalling + (details.dcsID ?? "")
                let sessinId = API.SessionID + (details.sessionID ?? "")

    let finalUrl = url + sessinId
    print("The final url is :\(String(describing: finalUrl ))")

      ServiceManager.shared.request(url: finalUrl, method: .get, view: self.view) { (statusCode) in
                     
                if  statusCode == 200 {
                    print("Sucess")
                }
                else {
                    print("Failure")
                }
            }
                }
            ])
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        gerSchdules()
    }
    
    @objc func refresh(_ sender: AnyObject) {
       // Code to refresh table view
        refreshControl.endRefreshing()
        self.schedules.removeAll()
        gerSchdules()

    }
    
    @objc func sessionExpired(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "SessionExpired"), object: nil)
        self.logout()
    }
    
    //MARK: Side Menu Button Action
    @IBAction func show_side_menu(_ sender: Any) {
        self.resetSession()
        guard let menuViewController = self.storyboard?.instantiateViewController(withIdentifier: "MenuTableViewController")  as? MenuTableViewController else { return }
        menuViewController.modalPresentationStyle = .overCurrentContext
        menuViewController.transitioningDelegate = self
        menuViewController.identifyController = .isSchedule
        self.present(menuViewController, animated: true)
        
    }
    
    func checkLocationServices(){
           if CLLocationManager.locationServicesEnabled(){
               self.locationManager.delegate = self
               self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
               self.checkLocationAuthorization()
           }else{
             
           }
       }
    
    
    func checkLocationAuthorization(){
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
            break
        case .denied:
            self.alert(title: "Location services are denied,Please enable it in order to get items", message: "", actionTitles: ["OK"], actionStyle: [.default], action: [
                { ok in
                    
                }
            ])
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        default:break
        }
    }
    
    
    //MARK: Get List of Today Schedules
    private func gerSchdules(){
        let details = self.getuserDetails()
        schedules.removeAll()
        expand.removeAll()
        serviceHandler.removeAll()
        self.scheduleTableView.reloadData()
        let url = (details.baseURL ?? "") + API.SchedulesAPI + (details.dcsID ?? "") + "&sessionId=\(details.sessionID ?? "")"
        ServiceManager.shared.request(type: SchedulesModel.self, url: url, method: .get, view: self.view) { [weak self](response) in
            if response?.count ?? 0 != 0{
                DispatchQueue.main.async {
                    self?.scheduleTableView.isHidden = false
                    self?.emptyStatusLabel.isHidden = true
                    print("The repsonse of get scheules is :\(String(describing: response))")
                    self?.customiseScheduleData(schedulesList: response!)
                }
            }else{
                DispatchQueue.main.async {
                    self?.emptyStatusLabel.text = "No Scheduels data found"
                    self?.scheduleTableView.isHidden = true
                    self?.emptyStatusLabel.isHidden = false
                }

            }
        }
        
    }
    
    private func customiseScheduleData(schedulesList: SchedulesModel){
        for value in schedulesList {
            if self.schedules.count == 0{
                self.schedules.append([value])
                self.expand.append([false])
                self.serviceHandler.append([.init(clockInSelected: false, clockOutSelected: false, reportAbsentSelected: false)])
            }else{
                if ((self.schedules[((self.schedules.count) - 1)][0].visitTimes ?? "").prefix(10) == (value.visitTimes ?? "").prefix(10)){
                    self.schedules[((self.schedules.count) - 1)].append(value)
                    self.expand[(self.expand.count) - 1].append(false)
                    self.serviceHandler[(self.serviceHandler.count) - 1].append(.init(clockInSelected: false, clockOutSelected: false, reportAbsentSelected: false))
                }else{
                    self.schedules.append([value])
                    self.expand.append([false])
                    self.serviceHandler.append([.init(clockInSelected: false, clockOutSelected: false, reportAbsentSelected: false)])
                }
            }
        }
        DispatchQueue.main.async {
             self.scheduleTableView.reloadData()
        }
       
    }
    
    
    //MARK: Show Settings Pop Up
    @objc func logoutUser() {
        print("Clicked")
        heightOfSettingsTableView.constant = 210
        transparentView.isHidden = false
        
    }
    
    //MARK: Tap Gesture Action
    @objc func handleTap(){
        self.resetSession()
        transparentView.isHidden = true
    }
    
    @objc func expandActions(sender: UITapGestureRecognizer){
        self.resetSession()
        if sender.state == .ended {
            let location = sender.location(in: self.scheduleTableView)
            let indexPath = self.scheduleTableView.indexPathForRow(at: location)
            if expand[indexPath!.section][indexPath!.row]{
                expand[indexPath!.section][indexPath!.row] = false
                self.scheduleTableView.reloadData()
            }else{
                for section in 0..<expand.count{
                    for row in 0..<expand[section].count{
                        expand[section][row] = false
                        serviceHandler[section][row].clockInSelected = false
                        serviceHandler[section][row].clockOutSelected = false
                        serviceHandler[section][row].reportAbsentSelected = false
                        
                    }
                }
                expand[indexPath!.section][indexPath!.row] = true
                self.scheduleTableView.reloadData()
            }
        }
    }
    
    @objc func proceedAction(sender: UIButton){
        self.resetSession()
        print("the button sender is :\(sender)")
        let buttonPosition = sender.convert(CGPoint.zero, to: scheduleTableView)
        if let indexPath: IndexPath = scheduleTableView.indexPathForRow(at: buttonPosition) {
            print(indexPath.row,indexPath.section)
            if !serviceHandler[indexPath.section][indexPath.row].clockInSelected && !serviceHandler[indexPath.section][indexPath.row].clockOutSelected && !serviceHandler[indexPath.section][indexPath.row].reportAbsentSelected{
                self.toast(msgString: "Choose any option to proceed", view: self.view)
                return
            }
            switch (schedules[indexPath.section][indexPath.row].txnFlag ?? "") {
            case "arrival":
                
             if serviceHandler[indexPath.section][indexPath.row].clockOutSelected {

                 let StaticDepartureValue = "DEPARTURE"
                 let details = self.getuserDetails()
                 let checkGroupOrConseutiveURL = (details.baseURL ?? "") + API.checkMultipleServcie + (details.dcsID ?? "") + "&serviceId=\(self.schedules[indexPath.section][indexPath.row].serviceCode ?? 0)&visitDetailsId=\(self.schedules[indexPath.section][indexPath.row].visitDetailsID ?? 0)&officeId=\(self.schedules[indexPath.section][indexPath.row].officeID ?? 0)&punchTypeFlag=\(String(describing: StaticDepartureValue))&sessionId=\(details.sessionID ?? "")"
                 print("The url of clock out formed is :\(String(describing: checkGroupOrConseutiveURL))")


                 ServiceManager.shared.request(type: ClockOutGroupOrConseModel.self, url: checkGroupOrConseutiveURL, method: .get, view: self.view) { (response) in
                 DispatchQueue.main.async {
                 print("The response of clockout: conseutive or grouped is :\(String(describing: self.ClockOutGroupedOrconseModelData))")

                     
                     if let otp = self.schedules[indexPath.section][indexPath.row].otpFlag {
                         
                         let StaticDepartureValue = "DEPARTURE"
                         
                         if otp == 1 {
                             
                             guard let tokenContoller = self.storyboard?.instantiateViewController(withIdentifier: "ClockOutTokenVerification") as? ClockOutTokenVerification else {
                                 fatalError("not Found ClockOutTokenVerification")
                             }
                             tokenContoller.schedule = self.schedules[indexPath.section][indexPath.row]
                             tokenContoller.receivedOtpFlag = otp
                            tokenContoller.groupService = self.schedules[indexPath.section][indexPath.row]
                            tokenContoller.taskCaptureFlag = self.schedules[indexPath.section][indexPath.row].taskCapture
                             tokenContoller.clockOutString = StaticDepartureValue
                            tokenContoller.milageFlag = self.schedules[indexPath.section][indexPath.row].mileageFlag
                             self.navigationController?.pushViewController(tokenContoller, animated: true)
                             return
                             
                         }else if (response?.groupedServiceCount ?? 0 > 1){
                                  print("0")
                             
                              guard let groupedVc = self.storyboard?.instantiateViewController(withIdentifier: "GroupedViewController") as? GroupedViewController else {
                                 fatalError("not found GroupedViewController")
                             }
                             groupedVc.receivedTitle = self.schedules[indexPath.section][indexPath.row].serviceName
                             groupedVc.milageFlag = self.schedules[indexPath.section][indexPath.row].mileageFlag
                             groupedVc.receivedServcieID =  self.schedules[indexPath.section][indexPath.row].serviceCode
                             groupedVc.receivedVistDetailId = self.schedules[indexPath.section][indexPath.row].visitDetailsID
                             groupedVc.receivedtaskCapture = self.schedules[indexPath.section][indexPath.row].taskCapture
                             groupedVc.receivedOfficeId = self.schedules[indexPath.section][indexPath.row].officeID
                             self.navigationController?.pushViewController(groupedVc, animated: true)
                             
                                         
                             return
                                         
                             } else if (response?.consecutiveServiceCount ?? 0  > 1) {
                                 
                             guard let conseuktiveVC = self.storyboard?.instantiateViewController(withIdentifier: "ConseutiveViewController") as? ConseutiveViewController else {
                                 fatalError("not Found ConseutiveViewController") }
                             conseuktiveVC.receivedMileage = self.schedules[indexPath.section][indexPath.row].mileageFlag ?? 0
                             conseuktiveVC.receivedTaskCapture = self.schedules[indexPath.section][indexPath.row].taskCapture ?? 0
                             conseuktiveVC.receiveDetailID = response?.consecutiveVisitIds
                             conseuktiveVC.receiveConseutiveVisitIDs = self.schedules[indexPath.section][indexPath.row].visitDetailsID
                             conseuktiveVC.receivePSName = self.schedules [indexPath.section][indexPath.row].psName ?? "Not Available"
                             conseuktiveVC.receivePSID = self.schedules [indexPath.section][indexPath.row].psID ?? 0
                             conseuktiveVC.receiveServiceCode = self.schedules [indexPath.section][indexPath.row].serviceCode ?? 0
                             conseuktiveVC.receiveVisitTime = self.schedules[indexPath.section][indexPath.row].visitTimes ?? "Not Available"
                             conseuktiveVC.receivedOfficeID = self.schedules[indexPath.section][indexPath.row].officeID ?? 0
                             conseuktiveVC.receiveArrDeptId = self.schedules[indexPath.section][indexPath.row].arrDeptID ?? 0
                             
                             self.navigationController?.pushViewController(conseuktiveVC, animated: true)
                             
                             return
                         }else {
                             
                             if self.schedules[indexPath.section][indexPath.row].mileageFlag  == 0 && self.schedules[indexPath.section][indexPath.row].taskCapture  == 0 {
              
                                 let details = self.getuserDetails()

                                 var jsonObject = [String: Any]()
                                 jsonObject["imei"] = (deviceID ?? "")
                                 jsonObject["dcsId"] = details.dcsID ?? ""
                                 jsonObject["psId"] = (self.schedules[indexPath.section][indexPath.row].psID ?? 0)
                                 jsonObject["officeId"] = (self.schedules[indexPath.section][indexPath.row].officeID ?? 0)
                                 jsonObject["serviceId"] = (self.schedules[indexPath.section][indexPath.row].serviceCode ?? 0)
                                 jsonObject["mileage"] = 0
                                 jsonObject["travelTime"] = 0
                                 jsonObject["transactionType"] = "DEPARTURE"
                                 jsonObject["latitude"] = (GetUserDetails.latitude ?? "")
                                 jsonObject["longitude"] = (GetUserDetails.longitude ?? "")
                                 jsonObject["visitDetailsId"] = (self.schedules[indexPath.section][indexPath.row].visitDetailsID ?? 0)
                                 jsonObject["arrDeptId"] = (self.schedules[indexPath.section][indexPath.row].arrDeptID ?? 0)
                                 jsonObject["ipAddress"] = ""
                                 jsonObject["otp"] = ""
                                 DispatchQueue.main.async {
                                     self.getAddressFromLatLons(pdblLatitude: (GetUserDetails.latitude ?? ""), withLongitude: (GetUserDetails.longitude ?? "")) { (address) in
                                     
                                         jsonObject["address"] = address
                                         let url = (details.baseURL ?? "") + API.CREATEREPORTEDVISIT + "\(jsonObject.jsonStringRepresentation ?? "")" + "&sessionId=\(details.sessionID ?? "")"
                                         print("The url in clockin token screen is :\(String(describing: url))")
                                         ServiceManager.shared.request(type: GroupServiceResponseModel.self, url: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "", method: .get, view: self.view) { [weak self](response) in
                                             if response?.result != nil{
                                                 //It seems entered OTP is invalid, Clock - In created successfully at 05/05/2020 01:49 PM
                                                      if response?.validateFlag ?? "" == "departuretimecheck"{
                                                                            DispatchQueue.main.async {
                                                                                self?.showAlert(message: response?.result ?? "")
                                                                            }                                                                        }else if response?.validateFlag ?? "" == "false" {
                                                                            DispatchQueue.main.async {
                                                                                self?.showAlert(message: response?.result ?? "")
                                                                            }
                                                                        }else{
                                                                            DispatchQueue.main.async {
                                                                            self?.popUpTransparentView.isHidden = false
                                                                            if let result = response?.result{
                                                                                let resultDateAndTime = result.suffix(19)
                                                                                self?.popUpDateLabel.text = String(resultDateAndTime.prefix(10))
                                                                                self?.popUpTimeLabel.text = String(resultDateAndTime.suffix(8))
                                                                                self?.alertHeaderTitleLabel.text = (response?.result?.dropLast(19).string ?? "")
                                                                            }
                                                                        }
                                                                        
                                                                        }
                                             }else{
                                                     DispatchQueue.main.async {
                                                         self?.toast(msgString: response?.result ?? "", view: self?.view ?? UIView())
                                                     }
                                                 }
                                             }
                                         }
                                     }

                                return
                             }
                             
                             else if (self.schedules[indexPath.section][indexPath.row].mileageFlag  == 0 && self.schedules[indexPath.section][indexPath.row].taskCapture  == 1) {
                                 
                                 guard let cloutOutTasksController = self.storyboard?.instantiateViewController(withIdentifier: "cloutOutTasksController") as? CloutOutTasksController else { fatalError("not Found ClockINTokenController") }
                                 cloutOutTasksController.groupService = self.schedules[indexPath.section][indexPath.row]
                                 self.navigationController?.pushViewController(cloutOutTasksController, animated: true)
                                 
                     
                                 return
                             }
                             
                             else {
                               // Clock OTP 1 --  Screen
                                 
                                 guard let tokenContoller = self.storyboard?.instantiateViewController(withIdentifier: "ClockOutTokenVerification") as? ClockOutTokenVerification else {
                                      fatalError("not Found ClockOutTokenVerification")
                                  }
                                  tokenContoller.schedule = self.schedules[indexPath.section][indexPath.row]
                                  tokenContoller.receivedOtpFlag = otp
                                 tokenContoller.groupService = self.schedules[indexPath.section][indexPath.row]
                                 tokenContoller.taskCaptureFlag = self.schedules[indexPath.section][indexPath.row].taskCapture
                                  tokenContoller.clockOutString = StaticDepartureValue
                                 tokenContoller.milageFlag = self.schedules[indexPath.section][indexPath.row].mileageFlag
                                  self.navigationController?.pushViewController(tokenContoller, animated: true)
                                 
                                 
                                 return
                             }
                         }
                                                     
                     }
                 //MARK: Grouped Service Count > 2
                     //Clock out ended
                     }
                     
                     
                     
                     }
                 return
                 }
                break
            case "departure":
                 if serviceHandler[indexPath.section][indexPath.row].clockInSelected {   // Clock in Started
                                   let details = self.getuserDetails()
                                   let groupServicesURL = (details.baseURL ?? "") + API.GROUPSERVICES + (details.dcsID ?? "")  + "&serviceId=\(self.schedules[indexPath.section][indexPath.row].serviceCode ?? 0)&visitDetailsId=\(self.schedules[indexPath.section][indexPath.row].visitDetailsID ?? 0)&officeId=\(self.schedules[indexPath.section][indexPath.row].officeID ?? 0)&sessionId=\(details.sessionID ?? "")"
                                   
                               var  url = String()
                               url.removeAll()
                               url.append((details.baseURL ?? "") + "getMultipleServicesCount?dcsId=\(details.dcsID ?? "")")
                               url.append("&serviceId=\(self.schedules[indexPath.section][indexPath.row].serviceCode ?? 0)")
                           url.append("&visitDetailsId=\(self.schedules[indexPath.section][indexPath.row].visitDetailsID ?? 0)")
                                   url.append("&officeId=\(self.schedules[indexPath.section][indexPath.row].officeID ?? 0)")
                                   url.append("&punchTypeFlag=ARRIVAL&sessionId=\(details.sessionID ?? "")")
                                   ServiceManager.shared.request(type: MultipleServicesModel.self, url: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "", method: .get, view: self.view) { (response) in
                                       if response?.groupedServiceCount ?? 2 == 1{
                                           if let otg = self.schedules[indexPath.section][indexPath.row].otpFlag{
                                               if otg == 1{
                                                   guard let tokenContoller = self.storyboard?.instantiateViewController(withIdentifier: "clockINTokenController") as? ClockINTokenController else { fatalError("not Found ClockINTokenController") }
                                                   tokenContoller.schedule = self.schedules[indexPath.section][indexPath.row]
                                                   self.navigationController?.pushViewController(tokenContoller, animated: true)
                                                   return
                                               }else{
                                                   DispatchQueue.main.async {
                                                    self.saveGroupService(officeId: String(describing: self.schedules[indexPath.section][indexPath.row].officeID ?? 0), visitedID: self.schedules[indexPath.section][indexPath.row].visitDetailsID ?? 0, serviceID: self.schedules[indexPath.section][indexPath.row].serviceCode ?? 0, psID: self.schedules[indexPath.section][indexPath.row].psID ?? 0, arraDepID: self.schedules[indexPath.section][indexPath.row].arrDeptID ?? 0)
                                                       return
                                                   }
                                               }
                                           }
                                       }else{
                                           ServiceManager.shared.request(type: ArrivedGroupServicesModel.self, url: groupServicesURL, method: .get, view: self.view) { (response) in
                                               if response?.count ?? 0 != 0{
                                                   DispatchQueue.main.async {
                                                       
                                                       guard let groupServicesController = self.storyboard?.instantiateViewController(withIdentifier: "groupServicesController") as? GroupServicesController else { fatalError("not Found GroupServicesController") }
                                                       groupServicesController.groupServices = response
                                                       groupServicesController.officeId = self.schedules[indexPath.section][indexPath.row].officeID
                                                       self.navigationController?.pushViewController(groupServicesController, animated: true)
                                                       return
                                                   }
                                               }
                                           }
                                       }
                                       
                                   }
                                   return
                               }
                break
            case "scheduled":
                if serviceHandler[indexPath.section][indexPath.row].clockInSelected {   // Clock in Started
                    let details = self.getuserDetails()
                    let groupServicesURL = (details.baseURL ?? "") + API.GROUPSERVICES + (details.dcsID ?? "")  + "&serviceId=\(self.schedules[indexPath.section][indexPath.row].serviceCode ?? 0)&visitDetailsId=\(self.schedules[indexPath.section][indexPath.row].visitDetailsID ?? 0)&officeId=\(self.schedules[indexPath.section][indexPath.row].officeID ?? 0)&sessionId=\(details.sessionID ?? "")"
                    
                var  url = String()
                url.removeAll()
                url.append((details.baseURL ?? "") + "getMultipleServicesCount?dcsId=\(details.dcsID ?? "")")
                url.append("&serviceId=\(self.schedules[indexPath.section][indexPath.row].serviceCode ?? 0)")
            url.append("&visitDetailsId=\(self.schedules[indexPath.section][indexPath.row].visitDetailsID ?? 0)")
                    url.append("&officeId=\(self.schedules[indexPath.section][indexPath.row].officeID ?? 0)")
                    url.append("&punchTypeFlag=ARRIVAL&sessionId=\(details.sessionID ?? "")")
                    ServiceManager.shared.request(type: MultipleServicesModel.self, url: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "", method: .get, view: self.view) { (response) in
                        if let otg = self.schedules[indexPath.section][indexPath.row].otpFlag{
                        if otg == 1{
                            guard let tokenContoller = self.storyboard?.instantiateViewController(withIdentifier: "clockINTokenController") as? ClockINTokenController else { fatalError("not Found ClockINTokenController") }
                            tokenContoller.schedule = self.schedules[indexPath.section][indexPath.row]
                            self.navigationController?.pushViewController(tokenContoller, animated: true)
                            return
                        }else if response?.groupedServiceCount ?? 2 > 1{
                            ServiceManager.shared.request(type: ArrivedGroupServicesModel.self, url: groupServicesURL, method: .get, view: self.view) { (response) in
                                
                                if response?.count ?? 0 != 0{
                                    DispatchQueue.main.async {
                                        
                                        guard let groupServicesController = self.storyboard?.instantiateViewController(withIdentifier: "groupServicesController") as? GroupServicesController else { fatalError("not Found GroupServicesController") }
                                        groupServicesController.groupServices = response
                                        groupServicesController.officeId = self.schedules[indexPath.section][indexPath.row].officeID
                                        self.navigationController?.pushViewController(groupServicesController, animated: true)
                                        return
                                    }
                                }
                            }
                        }else{
                            
                              DispatchQueue.main.async {
                                self.saveGroupService(officeId: String(describing: self.schedules[indexPath.section][indexPath.row].officeID ?? 0), visitedID: self.schedules[indexPath.section][indexPath.row].visitDetailsID ?? 0, serviceID: self.schedules[indexPath.section][indexPath.row].serviceCode ?? 0, psID: self.schedules[indexPath.section][indexPath.row].psID ?? 0, arraDepID: self.schedules[indexPath.section][indexPath.row].arrDeptID ?? 0)
                                return
                        }
                        
                        }
                    }
                    }
                    return
                }
               if serviceHandler[indexPath.section][indexPath.row].clockOutSelected {  // Clock Out Started

    let StaticDepartureValue = "DEPARTURE"
    let details = self.getuserDetails()
    let checkGroupOrConseutiveURL = (details.baseURL ?? "") + API.checkMultipleServcie + (details.dcsID ?? "") + "&serviceId=\(self.schedules[indexPath.section][indexPath.row].serviceCode ?? 0)&visitDetailsId=\(self.schedules[indexPath.section][indexPath.row].visitDetailsID ?? 0)&officeId=\(self.schedules[indexPath.section][indexPath.row].officeID ?? 0)&punchTypeFlag=\(String(describing: StaticDepartureValue))&sessionId=\(details.sessionID ?? "")"
    print("The url of clock out formed is :\(String(describing: checkGroupOrConseutiveURL))")


    ServiceManager.shared.request(type: ClockOutGroupOrConseModel.self, url: checkGroupOrConseutiveURL, method: .get, view: self.view) { (response) in
    DispatchQueue.main.async {
    print("The response of clockout: conseutive or grouped is :\(String(describing: self.ClockOutGroupedOrconseModelData))")

        
        if let otp = self.schedules[indexPath.section][indexPath.row].otpFlag {
            
            let StaticDepartureValue = "DEPARTURE"
            
            if otp == 1 {
                
                guard let tokenContoller = self.storyboard?.instantiateViewController(withIdentifier: "ClockOutTokenVerification") as? ClockOutTokenVerification else {
                    fatalError("not Found ClockOutTokenVerification")
                }
                tokenContoller.schedule = self.schedules[indexPath.section][indexPath.row]
                tokenContoller.receivedOtpFlag = 1
                tokenContoller.clockOutString = StaticDepartureValue
                self.navigationController?.pushViewController(tokenContoller, animated: true)
                return
                
            }else if (response?.groupedServiceCount ?? 0 > 1){
                     print("0")
                
                 guard let groupedVc = self.storyboard?.instantiateViewController(withIdentifier: "GroupedViewController") as? GroupedViewController else {
                    fatalError("not found GroupedViewController")
                }
                groupedVc.receivedTitle = self.schedules[indexPath.section][indexPath.row].serviceName
                groupedVc.milageFlag = self.schedules[indexPath.section][indexPath.row].mileageFlag
                groupedVc.receivedServcieID =  self.schedules[indexPath.section][indexPath.row].serviceCode
                groupedVc.receivedVistDetailId = self.schedules[indexPath.section][indexPath.row].visitDetailsID
                groupedVc.receivedtaskCapture = self.schedules[indexPath.section][indexPath.row].taskCapture
                groupedVc.receivedOfficeId = self.schedules[indexPath.section][indexPath.row].officeID
                self.navigationController?.pushViewController(groupedVc, animated: true)
                
                            
                return
                            
                } else if (response?.consecutiveServiceCount ?? 0  > 1) {
                    
                guard let conseuktiveVC = self.storyboard?.instantiateViewController(withIdentifier: "ConseutiveViewController") as? ConseutiveViewController else {
                    fatalError("not Found ConseutiveViewController") }
                conseuktiveVC.receivedMileage = self.schedules[indexPath.section][indexPath.row].mileageFlag ?? 0
                conseuktiveVC.receivedTaskCapture = self.schedules[indexPath.section][indexPath.row].taskCapture ?? 0
                conseuktiveVC.receiveDetailID = response?.consecutiveVisitIds
                conseuktiveVC.receiveConseutiveVisitIDs = self.schedules[indexPath.section][indexPath.row].visitDetailsID
                conseuktiveVC.receivePSName = self.schedules[indexPath.section][indexPath.row].psName ?? "Not Available"
                conseuktiveVC.schedule = self.schedules[indexPath.section][indexPath.row]
                conseuktiveVC.receivePSID = self.schedules[indexPath.section][indexPath.row].psID ?? 0
                conseuktiveVC.receiveServiceCode = self.schedules [indexPath.section][indexPath.row].serviceCode ?? 0
                conseuktiveVC.receiveVisitTime = self.schedules[indexPath.section][indexPath.row].visitTimes ?? "Not Available"
                conseuktiveVC.receivedOfficeID = self.schedules[indexPath.section][indexPath.row].officeID ?? 0
                conseuktiveVC.receiveArrDeptId = self.schedules[indexPath.section][indexPath.row].arrDeptID ?? 0
                
                self.navigationController?.pushViewController(conseuktiveVC, animated: true)
                
                return
            }else {
                
                if self.schedules[indexPath.section][indexPath.row].mileageFlag  == 0 && self.schedules[indexPath.section][indexPath.row].taskCapture  == 0 {
                    
                    //      Clock in- Save - otp = 0 same api
 
                    let details = self.getuserDetails()

                    var jsonObject = [String: Any]()
                    jsonObject["imei"] = (deviceID ?? "")
                    jsonObject["dcsId"] = details.dcsID ?? ""
                    jsonObject["psId"] = (self.schedules[indexPath.section][indexPath.row].psID ?? 0)
                    jsonObject["officeId"] = (self.schedules[indexPath.section][indexPath.row].officeID ?? 0)
                    jsonObject["serviceId"] = (self.schedules[indexPath.section][indexPath.row].serviceCode ?? 0)
                    jsonObject["mileage"] = 0
                    jsonObject["travelTime"] = 0
                    jsonObject["transactionType"] = "DEPARTURE"
                    jsonObject["latitude"] = (GetUserDetails.latitude ?? "")
                    jsonObject["longitude"] = (GetUserDetails.longitude ?? "")
                    jsonObject["visitDetailsId"] = (self.schedules[indexPath.section][indexPath.row].visitDetailsID ?? 0)
                    jsonObject["arrDeptId"] = (self.schedules[indexPath.section][indexPath.row].arrDeptID ?? 0)
                    jsonObject["ipAddress"] = ""
                    jsonObject["otp"] = ""
                    DispatchQueue.main.async {
                        self.getAddressFromLatLons(pdblLatitude: (GetUserDetails.latitude ?? ""), withLongitude: (GetUserDetails.longitude ?? "")) { (address) in
                        
                            jsonObject["address"] = address
                            let url = (details.baseURL ?? "") + API.CREATEREPORTEDVISIT + "\(jsonObject.jsonStringRepresentation ?? "")" + "&sessionId=\(details.sessionID ?? "")"
                            print("The url in clockin token screen is :\(String(describing: url))")
                            ServiceManager.shared.request(type: GroupServiceResponseModel.self, url: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "", method: .get, view: self.view) { [weak self](response) in
                                if response?.result != nil{
                                    //It seems entered OTP is invalid, Clock - In created successfully at 05/05/2020 01:49 PM
                                       if response?.validateFlag ?? "" == "departuretimecheck"{
                                                               DispatchQueue.main.async {
                                                                   self?.showAlert(message: response?.result ?? "")
                                                               }
                                                           }else if response?.validateFlag ?? "" == "false" {
                                                               DispatchQueue.main.async {
                                                                   self?.showAlert(message: response?.result ?? "")
                                                               }
                                                           }else{
                                                               DispatchQueue.main.async {
                                                               self?.popUpTransparentView.isHidden = false
                                                               if let result = response?.result{
                                                                   let resultDateAndTime = result.suffix(19)
                                                                   self?.popUpDateLabel.text = String(resultDateAndTime.prefix(10))
                                                                   self?.popUpTimeLabel.text = String(resultDateAndTime.suffix(8))
                                                                   self?.alertHeaderTitleLabel.text = (response?.result?.dropLast(19).string ?? "")
                                                               }
                                                           }
                                                           
                                                           }
                                }else{
                                        DispatchQueue.main.async {
                                            self?.toast(msgString: response?.result ?? "", view: self?.view ?? UIView())
                                        }
                                    }
                                }
                            }
                        }

                   return
                }
                
                else if (self.schedules[indexPath.section][indexPath.row].mileageFlag  == 0 && self.schedules[indexPath.section][indexPath.row].taskCapture  == 1) {
                    
                    guard let cloutOutTasksController = self.storyboard?.instantiateViewController(withIdentifier: "cloutOutTasksController") as? CloutOutTasksController else { fatalError("not Found ClockINTokenController") }
                    cloutOutTasksController.groupService = self.schedules[indexPath.section][indexPath.row]
                    self.navigationController?.pushViewController(cloutOutTasksController, animated: true)
                    
        
                    return
                }
                
                else {
                  // Clock OTP 1 --  Screen
                    
                    
                    guard let tokenContoller = self.storyboard?.instantiateViewController(withIdentifier: "ClockOutTokenVerification") as? ClockOutTokenVerification else {
                         fatalError("not Found ClockOutTokenVerification")
                     }
                     tokenContoller.schedule = self.schedules[indexPath.section][indexPath.row]
                     tokenContoller.receivedOtpFlag = otp
                    tokenContoller.groupService = self.schedules[indexPath.section][indexPath.row]
                    tokenContoller.taskCaptureFlag = self.schedules[indexPath.section][indexPath.row].taskCapture
                     tokenContoller.clockOutString = StaticDepartureValue
                    tokenContoller.milageFlag = self.schedules[indexPath.section][indexPath.row].mileageFlag
                     self.navigationController?.pushViewController(tokenContoller, animated: true)
                    
                    
                    return
                }
            }
                                        
        }
    //MARK: Grouped Service Count > 2
        //Clock out ended
        }
        
        
        
        }
    return
    }
               if serviceHandler[indexPath.section][indexPath.row].reportAbsentSelected {

var url = String()
url.removeAll()

    let details = self.getuserDetails()

self.getAddressFromLatLons(pdblLatitude: "\(GetUserDetails.latitude ?? "")", withLongitude: "\(GetUserDetails.longitude ?? "")") { (address) in
url.append((details.baseURL ?? "") + "saveScheduleProcessing?imei=\(deviceID ?? "")")
    url.append("&psId=\(String(describing: self.schedules[indexPath.section][indexPath.row].psID ?? 0))")
url.append("&officeId=\(String(describing: self.schedules[indexPath.section][indexPath.row].officeID ?? 0))")
url.append("&dcsId=\(details.dcsID ?? "")")
url.append("&serviceId=\(String(describing: self.schedules[indexPath.section][indexPath.row].serviceCode ?? 0))")
url.append("&mileage=0.0&travelTime=0&taskCodes=&transactionType=ABSENT")
url.append("&address=\(address)")
url.append("&visitDetailsId=\(String(describing: self.schedules[indexPath.section][indexPath.row].visitDetailsID ?? 0))&arrDeptId=0&ipAddress=10.0.2.16")
url.append("&latitude=\(GetUserDetails.latitude ?? "")")
url.append("&longitude=\(GetUserDetails.longitude ?? "")")
url.append("&sessionId=\(details.sessionID ?? "")")
print("The report absent url in schedules screens is  :\(url)")

ServiceManager.shared.request(type: GroupServiceResponseModel.self, url: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "", method: .get, view: self.view) { [weak self](response) in
    if response?.result != nil {
         if response?.validateFlag ?? "" == "departuretimecheck"{
                                DispatchQueue.main.async {
                                    self?.showAlert(message: response?.result ?? "")
                                }
                           }else if response?.validateFlag ?? "" == "false" {
                               DispatchQueue.main.async {
                                   self?.showAlert(message: response?.result ?? "")
                               }
                           }else{
                               DispatchQueue.main.async {
                               self?.popUpTransparentView.isHidden = false
                               if let result = response?.result{
                                   let resultDateAndTime = result.suffix(19)
                                   self?.popUpDateLabel.text = String(resultDateAndTime.prefix(10))
                                   self?.popUpTimeLabel.text = String(resultDateAndTime.suffix(8))
                                   self?.alertHeaderTitleLabel.text = (response?.result?.dropLast(19).string ?? "")
                               }
                           }
                           
                           }
        
    }else{
        DispatchQueue.main.async {
            self?.toast(msgString: response?.result ?? "", view: self?.view ?? UIView())
        }
    }
}
    
}

}
              default:break
               }

               return
              }

     }
    
    
    //MARK: ClockIn Button Action
    @objc func clockInAction(sender: UIButton){
        self.resetSession()
        let buttonPosition = sender.convert(CGPoint.zero, to: scheduleTableView)
        if let indexPath: IndexPath = scheduleTableView.indexPathForRow(at: buttonPosition) {
            print(indexPath.row,indexPath.section)
            self.serviceHandler[indexPath.section][indexPath.row].clockInSelected = true
            self.serviceHandler[indexPath.section][indexPath.row].clockOutSelected = false
            self.serviceHandler[indexPath.section][indexPath.row].reportAbsentSelected = false
            self.scheduleTableView.reloadData()
        }
    }
    
    //MARK: ClockOut Button Action
    @objc func clockOutAction(sender: UIButton){
        self.resetSession()
        let buttonPosition = sender.convert(CGPoint.zero, to: scheduleTableView)
        if let indexPath: IndexPath = scheduleTableView.indexPathForRow(at: buttonPosition) {
            print(indexPath.row,indexPath.section)
            self.serviceHandler[indexPath.section][indexPath.row].clockInSelected = false
            self.serviceHandler[indexPath.section][indexPath.row].clockOutSelected = true
            self.serviceHandler[indexPath.section][indexPath.row].reportAbsentSelected = false
            self.scheduleTableView.reloadData()
        }
    }
    
    //MARK: Report Absent Button Action
    @objc func reportAbsentAction(sender: UIButton){
        self.resetSession()
        let buttonPosition = sender.convert(CGPoint.zero, to: scheduleTableView)
        if let indexPath: IndexPath = scheduleTableView.indexPathForRow(at: buttonPosition) {
            print(indexPath.row,indexPath.section)
            self.serviceHandler[indexPath.section][indexPath.row].clockInSelected = false
            self.serviceHandler[indexPath.section][indexPath.row].clockOutSelected = false
            self.serviceHandler[indexPath.section][indexPath.row].reportAbsentSelected = true
            self.scheduleTableView.reloadData()
        }
    }
    
    func saveGroupService(officeId: String,visitedID :Int, serviceID: Int, psID: Int,arraDepID: Int){
        var url = String()
        url.removeAll()
        let details = self.getuserDetails()

        self.getAddressFromLatLons(pdblLatitude: "\(GetUserDetails.latitude ?? "")", withLongitude: "\(GetUserDetails.longitude ?? "")") { (address) in
            url.append((details.baseURL ?? "") + "saveScheduleProcessing?imei=\(deviceID ?? "")")
            url.append("&psId=\(psID)")
            url.append("&officeId=\(officeId)")
            url.append("&dcsId=\(details.dcsID ?? "")")
            url.append("&serviceId=\(serviceID)")
            url.append("&mileage=0.0&travelTime=0&taskCodes=&transactionType=ARRIVAL")
            url.append("&longitude=\(GetUserDetails.longitude ?? "")")
            url.append("&address=\(address)")
            url.append("&visitDetailsId=\(visitedID)&arrDeptId=\(arraDepID)&ipAddress=10.0.2.16")
            url.append("&latitude=\(GetUserDetails.latitude ?? "")")
            url.append("&sessionId=\(details.sessionID ?? "")")
            print(url)
            ServiceManager.shared.request(type: GroupServiceResponseModel.self, url: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "", method: .get, view: self.view) { [weak self](response) in
                if response?.result != nil{
                    //It seems entered OTP is invalid, Clock - In created successfully at 05/05/2020 01:49
                    if response?.validateFlag ?? "" == "departuretimecheck"{
                        DispatchQueue.main.async {
                            self?.showAlert(message: response?.result ?? "")
                        }
                    }else if response?.validateFlag ?? "" == "false" {
                        DispatchQueue.main.async {
                            self?.showAlert(message: response?.result ?? "")
                        }
                    }else{
                        DispatchQueue.main.async {
                        self?.popUpTransparentView.isHidden = false
                        if let result = response?.result{
                            let resultDateAndTime = result.suffix(19)
                            self?.popUpDateLabel.text = String(resultDateAndTime.prefix(10))
                            self?.popUpTimeLabel.text = String(resultDateAndTime.suffix(8))
                            self?.alertHeaderTitleLabel.text = (response?.result?.dropLast(19).string ?? "")
                        }
                    }
                    
                    }
                }else{
                    DispatchQueue.main.async {
                        self?.toast(msgString: response?.result ?? "", view: self?.view ?? UIView())
                    }
                }
            }
        }
    }
    
}


//MARK: UITableView Delegate and Datasource Methods
extension ScheduleExceptionsVC: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        switch tableView {
        case scheduleTableView:
            return schedules.count
        case settingsTableViewObj:
            return 1
        default:
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case scheduleTableView:
            return schedules[section].count
        case settingsTableViewObj:
            return settingsArray.count
        default:
            return 0
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch tableView {
            
        case scheduleTableView:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "scheduleSectionsCell", for: indexPath) as? ScheduleSectionsCell else {
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            cell.schedule = self.schedules[indexPath.section][indexPath.row]
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(expandActions(sender:)))
            cell.expandAction.addGestureRecognizer(tapGesture)
            
            expand[indexPath.section][indexPath.row] ? (cell.titleView.backgroundColor = UIColor.greenColor) : (cell.titleView.backgroundColor = .groupTableViewBackground)
            expand[indexPath.section][indexPath.row] ? (cell.sectionView.backgroundColor = UIColor.greenColor) : (cell.sectionView.backgroundColor = .groupTableViewBackground)
            expand[indexPath.section][indexPath.row] ? (cell.expandAction.setImage(UIImage(named: "ic_drop_up")?.withRenderingMode(.alwaysOriginal), for: .normal)) : (cell.expandAction.setImage(UIImage(named: "ic_drop")?.withRenderingMode(.alwaysOriginal), for: .normal))
            switch (schedules[indexPath.section][indexPath.row].txnFlag ?? "") {
            case "arrival":
                cell.clockInStack.isHidden = true
                cell.clockOutStack.isHidden = false
                cell.reportAbsentStack.isHidden = true
                if serviceHandler[indexPath.section][indexPath.row].clockInSelected{
                    cell.clockInButton.setImage(UIImage(named: "RadioButtonn"), for: .normal)
                    cell.clockOutButton.setImage(UIImage(named: "RadioButton"), for: .normal)
                    cell.reportAbsentButton.setImage(UIImage(named: "RadioButton"), for: .normal)
                }else if serviceHandler[indexPath.section][indexPath.row].clockOutSelected{
                    cell.clockInButton.setImage(UIImage(named: "RadioButton"), for: .normal)
                    cell.clockOutButton.setImage(UIImage(named: "RadioButtonn"), for: .normal)
                    cell.reportAbsentButton.setImage(UIImage(named: "RadioButton"), for: .normal)
                }else if serviceHandler[indexPath.section][indexPath.row].reportAbsentSelected{
                    cell.clockInButton.setImage(UIImage(named: "RadioButton"), for: .normal)
                    cell.clockOutButton.setImage(UIImage(named: "RadioButton"), for: .normal)
                    cell.reportAbsentButton.setImage(UIImage(named: "RadioButtonn"), for: .normal)
                }
                else {
                    
                cell.clockInButton.setImage(UIImage(named: "RadioButton"), for: .normal)
                cell.clockOutButton.setImage(UIImage(named: "RadioButton"), for: .normal)
                cell.reportAbsentButton.setImage(UIImage(named: "RadioButton"), for: .normal)
                    
                }
                expand[indexPath.section][indexPath.row] ? (cell.heightOfSection.constant = 220) : (cell.heightOfSection.constant = 0)
                cell.proceedAction.tag = indexPath.row
                cell.proceedAction.addTarget(self, action: #selector(proceedAction(sender:)), for: .touchUpInside)
                cell.clockOutButton.addTarget(self, action: #selector(clockOutAction(sender:)), for: .touchUpInside)
            case "departure":
                cell.clockInStack.isHidden = false
                cell.clockOutStack.isHidden = true
                cell.reportAbsentStack.isHidden = true
                cell.proceedAction.tag = indexPath.row
                cell.proceedAction.addTarget(self, action: #selector(proceedAction(sender:)), for: .touchUpInside)
                cell.clockInButton.addTarget(self, action: #selector(clockInAction(sender:)), for: .touchUpInside)
                if serviceHandler[indexPath.section][indexPath.row].clockInSelected{
                    cell.clockInButton.setImage(UIImage(named: "RadioButtonn"), for: .normal)
                    cell.clockOutButton.setImage(UIImage(named: "RadioButton"), for: .normal)
                    cell.reportAbsentButton.setImage(UIImage(named: "RadioButton"), for: .normal)
                }else if serviceHandler[indexPath.section][indexPath.row].clockOutSelected{
                    cell.clockInButton.setImage(UIImage(named: "RadioButton"), for: .normal)
                    cell.clockOutButton.setImage(UIImage(named: "RadioButtonn"), for: .normal)
                    cell.reportAbsentButton.setImage(UIImage(named: "RadioButton"), for: .normal)
                }else if serviceHandler[indexPath.section][indexPath.row].reportAbsentSelected{
                    cell.clockInButton.setImage(UIImage(named: "RadioButton"), for: .normal)
                    cell.clockOutButton.setImage(UIImage(named: "RadioButton"), for: .normal)
                    cell.reportAbsentButton.setImage(UIImage(named: "RadioButtonn"), for: .normal)
                }
                else {
                    
                cell.clockInButton.setImage(UIImage(named: "RadioButton"), for: .normal)
                cell.clockOutButton.setImage(UIImage(named: "RadioButton"), for: .normal)
                cell.reportAbsentButton.setImage(UIImage(named: "RadioButton"), for: .normal)
                    
                }
                expand[indexPath.section][indexPath.row] ? (cell.heightOfSection.constant = 220) : (cell.heightOfSection.constant = 0)
            case "scheduled":
                var height = 201
                if (schedules[indexPath.section][indexPath.row].departureFlag ?? "") == "1"{
                    cell.clockOutStack.isHidden = false
                    height += 30
                }else{
                    cell.clockOutStack.isHidden = true
                }
                if (schedules[indexPath.section][indexPath.row].arrivalFlag ?? "") == "1"{
                    cell.clockInStack.isHidden = false
                    height += 30
                }else{
                    cell.clockInStack.isHidden = true
                }
                if (schedules[indexPath.section][indexPath.row].absentFlag ?? "") == "1"{
                    cell.reportAbsentStack.isHidden = false
                    height += 30
                }else{
                    cell.reportAbsentStack.isHidden = true
                }
                if serviceHandler[indexPath.section][indexPath.row].clockInSelected{
                    cell.clockInButton.setImage(UIImage(named: "RadioButtonn"), for: .normal)
                    cell.clockOutButton.setImage(UIImage(named: "RadioButton"), for: .normal)
                    cell.reportAbsentButton.setImage(UIImage(named: "RadioButton"), for: .normal)
                }else if serviceHandler[indexPath.section][indexPath.row].clockOutSelected{
                    cell.clockInButton.setImage(UIImage(named: "RadioButton"), for: .normal)
                    cell.clockOutButton.setImage(UIImage(named: "RadioButtonn"), for: .normal)
                    cell.reportAbsentButton.setImage(UIImage(named: "RadioButton"), for: .normal)
                }else if serviceHandler[indexPath.section][indexPath.row].reportAbsentSelected{
                    cell.clockInButton.setImage(UIImage(named: "RadioButton"), for: .normal)
                    cell.clockOutButton.setImage(UIImage(named: "RadioButton"), for: .normal)
                    cell.reportAbsentButton.setImage(UIImage(named: "RadioButtonn"), for: .normal)
                }
                else {
                    
                cell.clockInButton.setImage(UIImage(named: "RadioButton"), for: .normal)
                cell.clockOutButton.setImage(UIImage(named: "RadioButton"), for: .normal)
                cell.reportAbsentButton.setImage(UIImage(named: "RadioButton"), for: .normal)
                    
                }
               cell.serviceAction.setTitle(self.schedules[indexPath.section][indexPath.row].serviceName ?? "", for: .normal)
                expand[indexPath.section][indexPath.row] ? (cell.heightOfSection.constant = CGFloat(height)) : (cell.heightOfSection.constant = 0)
                cell.proceedAction.tag = indexPath.row
                cell.proceedAction.addTarget(self, action: #selector(proceedAction(sender:)), for: .touchUpInside)
                cell.clockInButton.addTarget(self, action: #selector(clockInAction(sender:)), for: .touchUpInside)
                cell.clockOutButton.addTarget(self, action: #selector(clockOutAction(sender:)), for: .touchUpInside)
                cell.reportAbsentButton.addTarget(self, action: #selector(reportAbsentAction(sender:)), for: .touchUpInside)
            default:
                print("")
            }
            return cell
            
        case settingsTableViewObj:
            
            guard  let settingscell = tableView.dequeueReusableCell(withIdentifier: "SettingsCell", for: indexPath) as? SettingsCell else {
                return UITableViewCell() }
            settingscell.optionsLabel.text = settingsArray[indexPath.row]
            
            if indexPath.row == 0 {
                settingscell.optionsLabel.textColor = UIColor(displayP3Red: 116/255.0, green: 177/255.0, blue: 243/255.0, alpha: 1)
            }
            else {
                settingscell.optionsLabel.textColor = .black
            }
            
            settingscell.selectionStyle = .none
            return settingscell
            
        default:
            return UITableViewCell()
        }
    }
    
    
    //MARK: Did Select Row at Indexpath
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch tableView {
        case settingsTableViewObj:
            
            if indexPath.row == 0 {
                
            }
                
            else if (indexPath.row == 1) {
                
                DispatchQueue.main.async {
                    guard let searchSchedulesVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchSchdeulesVC") as? SearchSchdeulesVC else { return }
                    self.transparentView.isHidden = true
                    let navigation = UINavigationController(rootViewController: searchSchedulesVC)
                    navigation.modalPresentationStyle = .fullScreen
                    self.present(navigation, animated: false, completion: nil)
                }
                
            }
                
            else if (indexPath.row == 2) {
                
                 DispatchQueue.main.async {
                guard let ReportUnScheduledVC = self.storyboard?.instantiateViewController(withIdentifier: "ReportUnScheduledVC") as? ReportUnScheduledVC else { return }
                    self.transparentView.isHidden = true
                let navigation = UINavigationController(rootViewController: ReportUnScheduledVC)
                navigation.modalPresentationStyle = .fullScreen
                self.present(navigation, animated: false, completion: nil)
                }
            }
                
            else if (indexPath.row == 3) {
                 DispatchQueue.main.async {
                guard  let nonClientVC = self.storyboard?.instantiateViewController(withIdentifier: "NonClientVC") as? NonClientVC else { return }
                    self.transparentView.isHidden = true
                
                let navigation = UINavigationController(rootViewController: nonClientVC)
                navigation.modalPresentationStyle = .fullScreen
                self.present(navigation, animated: false, completion: nil)
                }
            }
                
            else if (indexPath.row == 4){
                 DispatchQueue.main.async {
                guard let timeSheet = self.storyboard?.instantiateViewController(withIdentifier: "TimeSheetVC") as? TimeSheetVC else { return }
                    self.transparentView.isHidden = true
                
              let navigation = UINavigationController(rootViewController: timeSheet)
            navigation.modalPresentationStyle = .fullScreen
            self.present(navigation, animated: false, completion: nil)
                }
            }
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch tableView {
        case scheduleTableView:
            return UITableView.automaticDimension
        case settingsTableViewObj:
            return 40
        default:
            return UITableView.automaticDimension
        }
        
        
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        switch tableView {
        case scheduleTableView:
            guard let header = tableView.dequeueReusableCell(withIdentifier: "scheduleHeadersCell") as? ScheduleHeadersCell else {
                return UIView()
            }
            header.dateLabel.text = self.schedules[section][0].visitTimes ?? ""
            header.selectionStyle = .none
            return header
            
        default:
            return UIView()
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        switch tableView {
        case scheduleTableView:
            return 35
        default:
            return 0
        }
        
        
    }
    
    
    
}

//MARK: UIView Controller Transistion Delegate
extension ScheduleExceptionsVC : UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transiton.isPresenting = true
        return transiton
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transiton.isPresenting = false
        return transiton
    }
}

//MARK: UIGesture Recognizer Delegate Methods
extension ScheduleExceptionsVC : UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view!.isDescendant(of: settingsTableViewObj) {
            return false
        }
        return true
    }
}

extension ScheduleExceptionsVC: CLLocationManagerDelegate
{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        defaults.set(String(location.coordinate.latitude), forKey: kLatitude)
        defaults.set(String(location.coordinate.longitude), forKey: kLongitude)
        defaults.synchronize()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkLocationAuthorization()
    }
}

extension Dictionary {
    mutating func merge(dict: [Key: Value]){
        for (k, v) in dict {
            updateValue(v, forKey: k)
        }
    }
}
