//
//  HomeVC.swift
//  POC
//
//  Created by Ajeet N on 11/03/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {

    let transiton = SlideInTransition()
    var topView: UIView?
    
    
//MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Home VC is caling")
        self.configureNavigationBar(title: "Home")
        
    self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 18)!,NSAttributedString.Key.foregroundColor : UIColor.white]

    }

    
    
//MARK: Side Menu Button Action
    @IBAction func didTapMenuButton(_ sender: UIBarButtonItem) {
    
        guard let menuViewController = self.storyboard?.instantiateViewController(withIdentifier: "MenuTableViewController")  as? MenuTableViewController else { return }
//       // menuViewController.didTapMenuType = { menuTypeSelected in
//            print(menuTypeSelected)
//            self.transistionToNew(_menuType: menuTypeSelected)
//        }
        menuViewController.modalPresentationStyle = .overCurrentContext
        menuViewController.transitioningDelegate = self
        present(menuViewController, animated: true)
        
    }
    
    

}


//MARK: UIView Controller Transistion Delegate
extension HomeVC : UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
           transiton.isPresenting = true
           return transiton
       }

       func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
           transiton.isPresenting = false
           return transiton
       }
}

//MARK: Navigation Bar Extension
extension UIViewController {
    
   func configureNavigationBar(title: String) {
    navigationController?.navigationBar.barTintColor = UIColor(displayP3Red: 116/255.0, green: 177/255.0, blue: 243/255.0, alpha: 1)
        navigationController?.navigationBar.tintColor =  .white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.isTranslucent = false
     self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 20)!,NSAttributedString.Key.foregroundColor : UIColor.white]
        navigationItem.title = title
   }
        
}
