//
//  CustomAlertController.swift
//  POC
//
//  Created by Admin on 14/05/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class CustomAlertController: UIViewController {
    @IBOutlet weak var alertMessage: UILabel!
    internal var message: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        alertMessage.text = message ?? ""
    }

    @IBAction func ok_action(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}
