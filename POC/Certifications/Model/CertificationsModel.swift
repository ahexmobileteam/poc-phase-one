//
//  CertificationsModel.swift
//  POC
//
//  Created by Ajeet N on 08/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import Foundation

// MARK: - Certification Model

typealias CertificationsListModel = [CertificationsModel]

struct CertificationsModel: Codable {
    let expiryDate: String?
    let certificationName: String?
    let expiredColorFlag: Int?
    let certificationStatus, aquiredDate: String?
}

