//
//  CertificationsVC.swift
//  POC
//
//  Created by Ajeet N on 11/03/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//


import UIKit

class CertificationsVC: UIViewController {
    
    let transiton = SlideInTransition()
     var topView: UIView?
    
    
    @IBOutlet weak var certificationsTableViewObj: UITableView!
    
        
    //MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureNavigationBar(title: "Certifications")
        print("Certifications controller called!")
    
    }
    
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.CertificationsAPICalling()

    }
    
    //MARK: Menu Bar Button Item
    @IBAction func menuBarButtonItem(_ sender: UIBarButtonItem) {
        
        guard let menuViewController = self.storyboard?.instantiateViewController(withIdentifier: "MenuTableViewController")  as? MenuTableViewController else { return }
//        menuViewController.didTapMenuType = { menuTypeSelected in
//            print(menuTypeSelected)
//            self.transistionToNew(_menuType: menuTypeSelected)
//        }
        menuViewController.modalPresentationStyle = .overCurrentContext
        menuViewController.transitioningDelegate = self
        present(menuViewController, animated: true)
        
    }
    
    //MARK:  Calling Certifications API
    func CertificationsAPICalling() {
        
        let details = self.getuserDetails()
        let url  = (details.baseURL ?? "") + API.CertificationsURL + (details.dcsID ?? "")
        let sessionIDURL = API.SessionID + (details.sessionID ?? "")
        
        let certificateUrl = url + sessionIDURL
        print("final url of certificaion is :\(certificateUrl)")
        
        ServiceManager.shared.request(type: CertificationsListModel.self, url: certificateUrl , method: .get, view: self.view) {  (response) in
            print("The certifications response is:\(String(describing: response))")
            
        }
    }

}


//MARK: UITableview Delegate and DataSource Methods
extension CertificationsVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CertificationsCell", for: indexPath) as! CertificationsCell
        cell.borderView.layer.borderWidth = 1.0
        cell.borderView.layer.borderColor = UIColor.lightGray.cgColor
        cell.borderView.layer.cornerRadius = 3.0
        cell.colorLabel.layer.cornerRadius = 2.0
        cell.colorLabel.layer.masksToBounds = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 135
    }
    
}

//MARK: UIView Controller Transistion Delegate
extension CertificationsVC : UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
           transiton.isPresenting = true
           return transiton
       }

       func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
           transiton.isPresenting = false
           return transiton
       }
    
}
