//
//  MenuTableViewController.swift
//  POC
//
//  Created by Ajeet N on 11/03/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit
struct MenuOptionsModel {
    var imageName: String
    let optionTitle: String
    var isSelected: Bool
}

enum IdentifyController
{
    case isAlerts
    case isSchedule
    case isWeeklyTimeCard
    case isMessages
    case isProfile
    case isLogout
}

class MenuTableViewController: UIViewController {

    @IBOutlet weak var menuTableViewObj: UITableView!
    
    private var menuOptions: [MenuOptionsModel] = [
        .init(imageName: "Alerts", optionTitle: "Alerts", isSelected: false),
        .init(imageName: "Schedules", optionTitle: "Schedules", isSelected: false),
         .init(imageName: "Weekly Time Card", optionTitle: "Weekely Time Card", isSelected: false),
         .init(imageName: "Messages", optionTitle: "Messages", isSelected: false),
         .init(imageName: "Profile", optionTitle: "Profile", isSelected: false),
         .init(imageName: "Logout", optionTitle: "Logout", isSelected: false)
    ]
    var identifyController : IdentifyController?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        menuTableViewObj.delegate = self
        menuTableViewObj.dataSource = self
        switch identifyController {
        case .isAlerts:
            menuOptions[0].isSelected = true
        case .isSchedule:
            menuOptions[1].isSelected = true

        case .isWeeklyTimeCard:
            menuOptions[2].isSelected = true

        case .isMessages:
            menuOptions[3].isSelected = true

        case .isProfile:
            menuOptions[4].isSelected = true

        case .isLogout:
            menuOptions[5].isSelected = true
        default:
            print("")
        }
        self.menuTableViewObj.reloadData()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(dismissSidMenu), name: NSNotification.Name(rawValue: "SideMenu"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "SideMenu"), object: nil)
    }
    

    @objc func dismissSidMenu() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "SideMenu"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    

    
    
}



extension MenuTableViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuOptions.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as? MenuCell else { return UITableViewCell() }
        cell.selectionStyle = .none
        let menuarrray = ["Alerts", "Schedules","Weekly Time Card", "Messages","Profile","Logout"]
        cell.menuImageView.image = UIImage(named: menuarrray[indexPath.row])
        cell.menuLabel.text = menuOptions[indexPath.row].optionTitle
        menuOptions[indexPath.row].isSelected ? (cell.cellBorderView.backgroundColor = UIColor.greenColor) : (cell.cellBorderView.backgroundColor = .white)
        menuOptions[indexPath.row].isSelected ? (cell.lineView.backgroundColor = UIColor.greenColor) : (cell.lineView.backgroundColor = .groupTableViewBackground)
        return cell
            
    }
           
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            guard let alertsController = self.storyboard?.instantiateViewController(withIdentifier: "alertsController") as? AlertsController else  { return }
                       let navigationControler = UINavigationController(rootViewController: alertsController)
                       navigationControler.modalPresentationStyle = .fullScreen
            self.present(navigationControler, animated: false, completion: nil)
            break
        case 1:
            guard let homeController = self.storyboard?.instantiateViewController(withIdentifier: "scheduleExceptionsVC") as? ScheduleExceptionsVC else  { return }
            let navigationControler = UINavigationController(rootViewController: homeController)
            navigationControler.modalPresentationStyle = .fullScreen
            self.present(navigationControler, animated: false, completion: nil)
            break
        case 2:
            guard let weeklyTimeSheetVC = self.storyboard?.instantiateViewController(withIdentifier: "WeeklyTimeSheetVC") as? WeeklyTimeSheetVC else  { return }
            let navigationControler = UINavigationController(rootViewController: weeklyTimeSheetVC)
            navigationControler.modalPresentationStyle = .fullScreen
            self.present(navigationControler, animated: false, completion: nil)
            
            break
        case 3:
            break
        case 4:
            guard let profileDetailsController = storyboard?.instantiateViewController(withIdentifier: "profileDetailsController") as? ProfileDetailsController else {
                fatalError("ProfileDetailsController not found")
            }
            let navigationController = UINavigationController(rootViewController: profileDetailsController)
            navigationController.modalPresentationStyle = .fullScreen
            self.present(navigationController, animated: false, completion: nil)
            break
        case 5:
            
            self.alert(title: "Are you sure you want to logout?", message: "", actionTitles: ["Cancel", "Ok"], actionStyle: [.destructive,.default], action: [
                {  Cancel in
                   
                    
                    
                }, { Ok in
                    
                    DispatchQueue.main.async {
                           
                           guard let loginController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController else {
                               fatalError("View Controller not found")
                           }
                           self.resetUserDefaults()
                           let navigationController = UINavigationController(rootViewController: loginController)
                           navigationController.modalPresentationStyle = .fullScreen
                           self.present(navigationController, animated: false, completion: nil)
                       }
                    
                }
            ])
            
    
        break
        default:break
        }
    }
    
    
}
extension UIViewController{
    func resetUserDefaults(){

        let dict = defaults.dictionaryRepresentation() as NSDictionary
        for key in dict.allKeys {
           defaults.removeObject(forKey: key as! String)
           defaults.removeObject(forKey: kSessionID)
            defaults.set("", forKey: kSessionID)
            print("removing user defaults method called")
        }
        defaults.synchronize()
    }
    
}

