//
//  TransactionsModel.swift
//  POC
//
//  Created by Ajeet N on 17/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//


import Foundation

typealias TransactionModelList = [TransactionsModel]


// MARK: - WelcomeElement
struct TransactionsModel: Codable {
    let displayName, typeName, formatName: String?
    let id, patientRequired, dataPrompt: Int?
}

