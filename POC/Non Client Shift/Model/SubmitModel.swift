//
//  SubmitModel.swift
//  POC
//
//  Created by Ajeet N on 19/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//


import Foundation

struct SubmitModel: Codable {
let result, specialTransactionId: String?
}
