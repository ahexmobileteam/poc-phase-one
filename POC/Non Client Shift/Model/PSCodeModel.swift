//
//  PSCodeModel.swift
//  POC
//
//  Created by Ajeet N on 17/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import Foundation


typealias PSCodeListModel = [PSCodeModel]


struct PSCodeModel: Codable {
let psCode, psName, psID: String?
}
