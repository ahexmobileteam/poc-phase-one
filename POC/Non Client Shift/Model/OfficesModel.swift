//
//  OfficesModel.swift
//  POC
//
//  Created by Ajeet N on 20/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import Foundation

typealias OfficesListModel = [OfficesModel]


// MARK: - Offices Model
struct OfficesModel: Codable {
    let code, name: String?
    let id: Int? 
}
