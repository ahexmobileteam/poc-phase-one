//
//  NonClientDetailVC.swift
//  POC
//
//  Created by Ajeet N on 17/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//


import UIKit
import CoreLocation


class NonClientDetailVC: UIViewController {
    

    @IBAction func psSearchButtonAction(_ sender: Any) {
    }
    
    @IBOutlet weak var psStackViewObj: UIStackView!
    
    var resSting : String?
    
    
    private var isSearching = false
    let cellReuseIdentifier = "cell"
    private var idString: String? = nil

    
    var tempPSID : String?
    @IBOutlet weak var tableViewTransparentView: UIView!
    @IBOutlet weak var tableViewCardView: UIView!
    
    @IBOutlet weak var tableViewSearchTF: CustomTField!
    
    
    @IBOutlet weak var psTableViewObj: UITableView!{
        didSet {
            psTableViewObj.delegate = self
            psTableViewObj.dataSource = self
        }
    }
    
    
    
    @IBOutlet weak var psSearchOutlet: UIButton!
    
    var receivedOfficeID  : String?
    
    @IBOutlet weak var alertTransparentView: UIView!
    @IBOutlet weak var alertCardViewObj: CustomView!
    @IBOutlet weak var alertFirstLabel: UILabel!
    @IBOutlet weak var alertDateLabel: UILabel!
    @IBOutlet weak var alertLocationLabel: UILabel!
    
    @IBOutlet weak var alertTimeLabel: UILabel!
    private var numberOfCharacters: String?
    private var selectedIndex = Int()
    private var transactionResponseModel : TransactionModelList?
    private var PsCodeResponeModel : PSCodeListModel?
    private var searchResults: PSCodeListModel?

    var addressString : String = ""

    private var isShowHOursTF = false
    
    @IBOutlet weak var transactionDropDown: DropDownTextField!
    @IBOutlet weak var psCodeDropDown: DropDownTextField!
    @IBOutlet weak var hourstextField: CustomTField!
    
    private let locationManager = CLLocationManager()

    
    
//MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
     navigationItem.title = "POC Visit Verification"
    hourstextField.delegate = self
    alertTransparentView.isHidden = true
        
        
        self.tableViewCardView.layer.cornerRadius = 5.0
        self.tableViewCardView.layer.masksToBounds = false
        
        tableViewTransparentView.isHidden = true


    hourstextField.isHidden = true
    psCodeDropDown.isHidden = true
    psSearchOutlet.isHidden = true
        
        DispatchQueue.main.async {
            self.getPSTransactions()

        }
        psTableViewObj.tableFooterView = UIView()

        
        
    // Select Transaction Drop Down
        transactionDropDown.dropDown.selectionAction = { (index, item) in
            self.selectedIndex = index
            self.transactionDropDown.text = item
            self.transactionDropDown.resignFirstResponder()
                    
        self.numberOfCharacters = self.transactionResponseModel?[index].formatName
            self.resSting = self.transactionResponseModel?[index].typeName ?? ""
    self.hourstextField.placeholder = "\(self.transactionResponseModel?[index].typeName ?? "")  (\((self.transactionResponseModel?[index].formatName ?? "").replacingOccurrences(of: "#", with: "#")))"
            
       DispatchQueue.main.async {
            defaults.set(self.transactionResponseModel?[index].displayName, forKey: "TRANSACTIONDISPLAYNAME")
            defaults.set(self.transactionResponseModel?[index].id, forKey: "TRANSACTIONID")
            defaults.set(self.transactionResponseModel?[index].patientRequired, forKey: "PATIENCEREQUIRED")
                
        if self.transactionResponseModel?[index].patientRequired == 0 {
                self.psCodeDropDown.isHidden = true
            self.psSearchOutlet.isHidden = true
            self.psStackViewObj.isHidden = true
            } else {
                self.psCodeDropDown.isHidden = false
                self.psSearchOutlet.isHidden = false
            self.psStackViewObj.isHidden = false
                self.getListOfPSCode()
        }
              
        if self.transactionResponseModel?[index].dataPrompt == 1 {
            self.hourstextField.isHidden = false
            self.isShowHOursTF = true
            
        } else {
            self.hourstextField.isHidden = true

        }
        
            }
    }
        
        psCodeDropDown.delegate = self
        
        //Select PSCode DropDown
//        psCodeDropDown.dropDown.selectionAction = { (index, item) in
//            self.selectedIndex = index
//            self.psCodeDropDown.text = item
//            self.psCodeDropDown.resignFirstResponder()
//            DispatchQueue.main.async {
//                defaults.set(self.PsCodeResponeModel?[index].psCode, forKey: "SELECTEDPSCODE")
//                defaults.set(self.PsCodeResponeModel?[index].psName, forKey: "SELECTEDPSNAME")
//                defaults.set(self.PsCodeResponeModel?[index].psID, forKey: "SELECTEDPSID")
//            }
//        }
        
    NotificationCenter.default.addObserver(self, selector: #selector(sessionExpired), name: Notification.Name("SessionExpired"), object: nil)
    }
    
    //MARK: Session Expired
         @objc func sessionExpired() {
                NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "SessionExpired"), object: nil)
                self.logout()
        }
    
//MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        checkLocationServices()
    }
    

//MARK: Get  User Current Location Services
    func checkLocationServices(){
          if CLLocationManager.locationServicesEnabled(){
                  self.locationManager.delegate = self
                  self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
                  self.checkLocationAuthorization()
                 self.locationManager.startUpdatingLocation()
          }else{
              self.alert(title: "Please enable the location permissions in order to get user current location", message: "", actionTitles: ["OK"], actionStyle: [.default], action: [
                  { ok in
                      
                  }
              ])
          }
      }
    
    
 //MARK: Location Authorization
func checkLocationAuthorization(){
           switch CLLocationManager.authorizationStatus() {
           case .authorizedWhenInUse:
               locationManager.startUpdatingLocation()
               break
           case .denied:
               self.alert(title: "Location services are denied, Please enable it in order to do further action", message: "", actionTitles: ["OK"], actionStyle: [.default], action: [
                   { ok in
                       
                   }
               ])
               break
           case .notDetermined:
               locationManager.requestWhenInUseAuthorization()
           default:break
           }
}
    
    
    //MARK: Get List of PS transactions
    func getPSTransactions()  {
        
        
        let details = self.getuserDetails()
        
        let psTransacationURL = (details.baseURL ?? "") + API.getSpecialTransaction + (details.sessionID ?? "")
        ServiceManager.shared.request(type: TransactionModelList.self, url: psTransacationURL, method: .get, view: self.view) { (response) in
            print("The transaction list is :\(String(describing: response))")
            if response?.count == 0 {
                self.alert(title: "Alert Message", message: "NO transactions were found")
            }
            else {
                self.transactionResponseModel = response
                DispatchQueue.main.async {
                    
            if let transactionid = self.transactionResponseModel?.map({ $0.displayName ?? ""}) {
                self.transactionDropDown.datasource = transactionid
                
                }
                    
                }
            }
        }
    }
    
    
    //MARK: Get List of PSCode
    func getListOfPSCode()  {
        
//        let savedOfficeID: Int = defaults.value(forKey: "PSOFFICEID") as? Int ?? 0
//        print("The reterived officeID from NonClientVC is :\(String(describing: savedOfficeID))")
        
            let details = self.getuserDetails()
        
        let psCodeUrl = (details.baseURL ?? "") + API.getListOFPSListByOfficeID + String(describing: receivedOfficeID ?? "")
        let sessionIDURL = API.SessionID + (details.sessionID ?? "")
        
        let psCodeByOfficeIDURL = psCodeUrl + sessionIDURL
        print("The pscode by office id url is :\(psCodeByOfficeIDURL)")
        
        ServiceManager.shared.request(type: PSCodeListModel.self, url: psCodeByOfficeIDURL, method: .get, view: self.view) { (response) in
            print("The response of pscode by office id is :\(String(describing: response))")
            
            if response?.count == 0 {
                self.alert(title: "Alert Message", message: "NO PSCode List Available")
            }
            else {
                self.PsCodeResponeModel = response
                DispatchQueue.main.async {
                    
            if let psId = self.PsCodeResponeModel?.map({ $0.psName ?? ""}) {
            self.psCodeDropDown.datasource =  psId
                self.psTableViewObj.reloadData()

                    }
                }
            }
        }
    }
    
    
    
//MARK: Save Button Action
    @IBAction func saveButtonAction(_ sender: Any) {
   
        
// http://poc.aquilasoftware.com/pocextacc-webservices_new/telephony/saveSpecialTransaction?imei=&dcsId=14635&psTelCode=126301&officeId=191&specialTransactionTypeId=3&dataPrompt=22&latitude=17.4486&longitude=78.3908&address=Rd%20Number%2010%2C%20Ayyappa%20Society%2C%20Mega%20Hills%2C%20Madhapur%2C%20Hyderabad%2C%20Telangana%20500081%2C%20India%0A&ipAddress=&sessionId=180837_ZOSDHN7XFABCRI88
        
        
        if transactionDropDown.text!.isEmpty {
            self.alert(title: "Alert Message", message: "Please Choose a transaction")
        }
        
        if isShowHOursTF == true {
            if hourstextField.text == "" {
                self.alert(title: "Alert Message", message: "Please enter format value")
                return
            }
           let value = hourstextField.text!.replacingMultipleOccurrences(using: (of: "0", with: "#"), (of: "1", with: "#"), (of: "2", with: "#"),(of: "3", with: "#"), (of: "4", with: "#"), (of: "5", with: "#"),(of: "6", with: "#"), (of: "7", with: "#"), (of: "8", with: "#"),(of: "9", with: "#"))
           print(value)
            
//            if (Int(hourstextField.text ?? "") ?? 0)  == 0 {
//                self.showAlert(message: "")
//               return
//            }
            
            
            if  (Double((numberOfCharacters?.replacingMultipleOccurrences(using: (of: "#", with: "9")) ?? "")) ?? 0.0) < (Double(hourstextField.text ?? "") ?? 0.0)  {
                self.alert(title: "Alert Message", message: "\(self.resSting ?? "") Should not be more than \(self.numberOfCharacters?.replacingOccurrences(of: "#", with: "9") ?? "") ")
                return
            }
        }
        
            
    let savedOfficeID: Int = defaults.value(forKey: "PSOFFICEID") as? Int ?? 0
        print("The reterived officeID from NonClientVC is :\(String(describing: savedOfficeID))")
      
        let specialtransacationID:Int = defaults.value(forKey: "TRANSACTIONID") as? Int ?? 0
        print("the selected id of transaction is :\(String(describing: specialtransacationID ))")
            
        let pscode = defaults.value(forKey: "SELECTEDPSCODE") ?? 0
        print ("The selected ps code values is :\(String(describing: pscode))")
            
        let staticDataPromptID:Int =  22
        print ("The static Data promot ID is :\(staticDataPromptID)")
            
        let lat = defaults.value(forKey: "kLatitude") ?? 0.0
        print("The user saved latitude is :\(String(describing: lat))")
            
        let lon = defaults.value(forKey: "kLongitude") ?? 0.0
        print("The user saved longitude is :\(String(describing: lon))")
            
        self.getAddressFromLatLons(pdblLatitude: String(describing: lat), withLongitude: String(describing: lon))
            
        let userAddress = defaults.value(forKey: "CONVERTEDUSERADDRESS") ?? ""
        print("The user address we have got is :\(String(describing: userAddress))")

         let details = self.getuserDetails()
        
        let saveTransaction = (details.baseURL ?? "") + API.SaveSpecialTransacation + (details.dcsID ?? "")
        let psTeleCode = API.psTelecode + String(describing: pscode )
        let OfficeID = API.officeID + String(describing: receivedOfficeID ?? "")
        let SpecialTransactionTypeID = API.SpecialTransacationID + String(describing: specialtransacationID)
        let DataPrompt = API.dataPrompt + String(describing:staticDataPromptID )
        let latitude = API.latitude +  String(describing: lat)
        let longitude = API.longitude + String(describing: lon)
            let address = API.address + String(describing: userAddress).addingPercentEncoding(withAllowedCharacters: NSCharacterSet.alphanumerics)!
        let iPAddress = API.ipAddress
        let SessonID = API.SessionID + (details.sessionID ?? "")
            
        
    let saveButtonUrl = saveTransaction + psTeleCode + OfficeID + SpecialTransactionTypeID + DataPrompt + latitude + longitude + address + iPAddress + SessonID
    print("The save button url is :\(saveButtonUrl)")
            
        ServiceManager.shared.request(type: SubmitModel.self, url: saveButtonUrl, method: .get, view: self.view) { (response) in
            print("The response of save button Action is :\(String(describing: response))")

            if response?.specialTransactionId  != nil {

                DispatchQueue.main.async {
                            self.alertTransparentView.isHidden = false
                             if let result = response?.result{
                                let resultDateAndTime = result.suffix(19)
                               self.alertFirstLabel.text = "Non Client Shift Created on"
                             self.alertDateLabel.text = String(resultDateAndTime.prefix(10))
                             self.alertTimeLabel.text = String(resultDateAndTime.suffix(8))
                                 }
                         }
                
            }
            else {
                   self.alert(title: "Alert Message", message: response?.result ?? "")
            }
            
        }

            
}
    
  
//MARK: Converting User Latitude and Longitude into Physical Address
func getAddressFromLatLons(pdblLatitude: String, withLongitude pdblLongitude: String) {

               var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
    let lat: Double = Double("\(pdblLatitude)") ?? 0.0
    let lon: Double = Double("\(pdblLongitude)") ?? 0.0
               let ceo: CLGeocoder = CLGeocoder()
               center.latitude = lat
               center.longitude = lon
               let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
               ceo.reverseGeocodeLocation(loc, completionHandler:
                   {(placemarks, error) in
                       if (error != nil)
                       {
                           print("reverse geodcode fail: \(error!.localizedDescription)")
                       }
                       let pm = placemarks! as [CLPlacemark]

                       if pm.count > 0 {
                           let pm = placemarks![0]
                           if pm.subLocality != nil {
                            self.addressString = self.addressString + pm.subLocality! + ", "
                            print("The address string is :\(self.addressString)")
                           }
                           if pm.thoroughfare != nil {
                            self.addressString = self.addressString + pm.thoroughfare! + ", "
                           }
                           if pm.locality != nil {
                            self.addressString = self.addressString + pm.locality! + ", "
                           }
                           if pm.country != nil {
                            self.addressString = self.addressString + pm.country! + ", "
                           }
                           if pm.postalCode != nil {
                            self.addressString = self.addressString + pm.postalCode! + " "
                           }
                           DispatchQueue.main.async {
                            defaults.set(self.addressString, forKey: "CONVERTEDUSERADDRESS")
                            
                           }
                     }
               })
    }
    

    //MARK: Alert Ok Button Action
    @IBAction func alertOkButtonAction(_ sender: Any) {
        
        alertTransparentView.isHidden = true
        guard let emptyStateVC = self.storyboard?.instantiateViewController(withIdentifier: "MoveToNextTransaction") as? MoveToNextTransaction else {return}
        self.navigationController?.pushViewController(emptyStateVC, animated: true)
        
    }
    
}


//MARK: Corelocation Delegate Methods
extension NonClientDetailVC: CLLocationManagerDelegate {
    
   func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        let lat = location.coordinate.latitude
        let lon = location.coordinate.longitude
       defaults.set(lat, forKey: "kLatitude")
       defaults.set(lon, forKey: "kLongitude")
       defaults.synchronize()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkLocationAuthorization()
    }
}

extension NonClientDetailVC : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == psCodeDropDown {
            self.tableViewTransparentView.isHidden = false
            return false
        }
        return true
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == hourstextField{
            if numberOfCharacters != nil {
                if (textField.text?.count ?? 0) <= (numberOfCharacters?.count ?? 0) {
                    return true
                }else{
//                    self.alert(title: "Alert Message", message: "Please Check your format data")
                    return true
                }
            }else{
                return true
            }
        }
        
       else  if textField == tableViewSearchTF {
            if tableViewSearchTF.text?.count ?? 0 > 1{
                self.isSearching = true
               let results = PsCodeResponeModel?.filter( { ($0.psName ?? "").localizedStandardContains(tableViewSearchTF.text ?? "")})
               self.searchResults = results
               self.psTableViewObj.reloadData()
            }else{
                self.isSearching = false
                searchResults = nil
                self.psTableViewObj.reloadData()
            }
        }
        
        
        
        return true
    }
}
public extension String {

    func replacingMultipleOccurrences<T: StringProtocol, U: StringProtocol>(using array: (of: T, with: U)...) -> String {
        var str = self
        for (a, b) in array {
            str = str.replacingOccurrences(of: a, with: b)
        }
        return str
    }

}

extension NonClientDetailVC: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
     return 1
        
     }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch tableView {

        case psTableViewObj:
            if isSearching {
                return searchResults?.count ?? 0
            }
            return PsCodeResponeModel?.count ?? 0
        default:
            return 0
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch tableView {
                   
        case psTableViewObj:
            
             guard let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier:cellReuseIdentifier) else {return UITableViewCell() }
            
             if isSearching{
           cell.textLabel?.text = "\(self.searchResults?[indexPath.row].psName ?? "" )  (\(self.searchResults?[indexPath.row].psCode ?? ""))"
             } else{
    cell.textLabel?.text = "\(self.PsCodeResponeModel?[indexPath.row].psName ?? "" )  (\(self.PsCodeResponeModel?[indexPath.row].psCode ?? ""))"

             }

    cell.textLabel?.textColor = UIColor.init(red: 20/255.0, green:81/255.0 , blue: 148/255.0, alpha: 1.0)
   cell.textLabel?.font = UIFont(name: "Avenir-Roman", size: 18.0)
             
    return cell
        default:
        return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch tableView {
        case psTableViewObj:
            
        if isSearching {
            idString =  String(self.searchResults?[indexPath.row].psID ?? "0" )
            
            psCodeDropDown.text = "\(self.searchResults?[indexPath.row].psName ?? "" )  (\(self.searchResults?[indexPath.row].psCode ?? ""))"
            tempPSID = self.searchResults?[indexPath.row].psID ?? "0"
                   }
        
        else{
              idString =  String(self.PsCodeResponeModel?[indexPath.row].psID ?? "0" )
              psCodeDropDown.text = "\(self.PsCodeResponeModel?[indexPath.row].psName ?? "" )  (\(self.PsCodeResponeModel?[indexPath.row].psCode ?? ""))"
                       tempPSID = self.PsCodeResponeModel?[indexPath.row].psID ?? "0"
                   }
            
            searchResults = nil
            isSearching = false
            
            DispatchQueue.main.async {
                       self.psTableViewObj.reloadData()
                       self.tableViewSearchTF.text = ""
                       self.tableViewTransparentView.isHidden = true
                   }
            
        default:
             break
}
        
        
 }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }

}


