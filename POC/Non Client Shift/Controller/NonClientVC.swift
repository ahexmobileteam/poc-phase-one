//
//  NonClientVC.swift
//  POC
//
//  Created by Ajeet N on 17/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//


import UIKit

class NonClientVC: UIViewController {
    
    private var timer = Timer()

     var tempPSID : Int?
    private let transiton = SlideInTransition()
    private var isSearching = false

    private var idString: String? = nil

    @IBOutlet weak var tableViewTransparentView: UIView!
    @IBOutlet weak var tableViewCardView: UIView!
    
    
    @IBOutlet weak var searchTextField: CustomTField! {
        didSet {
            self.searchTextField.delegate = self
        }
    }
    
    let cellReuseIdentifier = "cell"
    private var ReportFlagResponseModel: ReportFlagsModel?
    var tempOfficeID : String?
    
    
    @IBOutlet weak var officesTableViewObj: UITableView! {
        didSet {
            officesTableViewObj.delegate = self
            officesTableViewObj.dataSource = self
        }
    }
    
    private var selectedIndex = Int()
    private var officesResponseModel: OfficesListModel?
    private var searchOfficeResults: OfficesListModel?

    @IBOutlet weak var officesDropDown: DropDownTextField!
    @IBOutlet weak var settingsTransparentView: UIView!
    @IBOutlet weak var settingsTableView: UITableView! {
        didSet {
            settingsTableView.delegate = self
            settingsTableView.dataSource = self
            self.settingsTableView.layer.cornerRadius = 5.0
        }
    }
    
let settingsArray = ["Today's Schedules","Search Schedules","Report UnScheduled Vist","Non-Client Shift","Time Sheet"]
        
    
//MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureNavigationBar(title: "Non-Client Shift")
        settingsTransparentView.isHidden = true
        officesTableViewObj.tableFooterView = UIView()
        self.tableViewTransparentView.isHidden = true

        tableViewCardView.layer.cornerRadius = 5.0
        tableViewCardView.layer.masksToBounds = false
        

        if let sessionTime = defaults.value(forKey: kSession) as? Int{
                   timer = Timer.scheduledTimer(timeInterval: TimeInterval(sessionTime), target: self, selector: #selector(sessionTimeOut), userInfo: nil, repeats: true)
               }
        
        let settings = UIBarButtonItem(image: UIImage(named: "Right Menu"), style: .plain, target: self, action: #selector(settingsPopUP))
        self.navigationItem.rightBarButtonItems = [settings]
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tapGesture.delegate = self
        settingsTransparentView.addGestureRecognizer(tapGesture)
        
        
        
//        officesDropDown.dropDown.selectionAction = { (index, item) in
//            self.selectedIndex = index
//            self.officesDropDown.text = item
//            print("the selected offices drop down is :\(String(describing: self.officesDropDown.text))")
//            self.officesDropDown.resignFirstResponder()
//            DispatchQueue.main.async {
//                defaults.set(self.officesResponseModel?[index].code ?? 0, forKey: "PSOFFICECODE")
//                defaults.set(self.officesResponseModel?[index].name ?? "", forKey: "PSOFFICENAME")
//                defaults.set(self.officesResponseModel?[index].id ?? 0, forKey: "PSOFFICEID")
//            }
//        }
        
        DispatchQueue.main.async {
            self.getOfficesList()
        }
        
        officesDropDown.delegate = self

        
    let sidemenuButton = UIBarButtonItem(image: UIImage(named: "Menu"), style: .plain, target: self, action: #selector(openSideMenu))
            self.navigationItem.leftBarButtonItem = sidemenuButton
    
    NotificationCenter.default.addObserver(self, selector: #selector(sessionExpired), name: Notification.Name("SessionExpired"), object: nil)
        
    }
    
    
    @objc func sessionTimeOut(){
           self.session()
       }
       
       func resetSession(){
           timer.invalidate()
           if let sessionTime = defaults.value(forKey: kSession) as? Int{
           timer = Timer.scheduledTimer(timeInterval: TimeInterval(sessionTime), target: self, selector: #selector(sessionTimeOut), userInfo: nil, repeats: true)
               
                  }
       }
       override func viewWillDisappear(_ animated: Bool) {
           timer.invalidate()
           if let sessionTime = defaults.value(forKey: kSession) as? Int{
               timer = Timer.scheduledTimer(timeInterval: TimeInterval(sessionTime), target: self, selector: #selector(sessionTimeOut), userInfo: nil, repeats: true)
           }
       }
    
       func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
           self.resetSession()
       }
    
    

    
    //MARK: Tap Gesture Action
    @objc func handleTap(){
        settingsTransparentView.isHidden = true
    }
    
    @objc func settingsPopUP() {
           print("ddd")
           self.settingsTransparentView.isHidden = false
       }
    
    
    @objc func openSideMenu() {
        guard let menuViewController = self.storyboard?.instantiateViewController(withIdentifier: "MenuTableViewController")  as? MenuTableViewController else { return }
        menuViewController.modalPresentationStyle = .overCurrentContext
        menuViewController.transitioningDelegate = self
       // menuViewController.identifyController = .isSchedule
        self.present(menuViewController, animated: true)
        
    }
    
    
    
    //MARK: Session Expired
        @objc func sessionExpired() {
               NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "SessionExpired"), object: nil)
               self.logout()
}
    
    
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.officesDropDown.delegate = self
    }
    
    
    //MARK: Get List of Offices
    func getOfficesList() {
        
        let details = self.getuserDetails()
        let officeSlist = (details.baseURL ?? "") + API.getPSOfficesList + (details.dcsID ?? "")
        let sessionID = API.SessionID + (details.sessionID ?? "")
        let officeListUrl = officeSlist + sessionID
        print("The ps office list url is :\(officeListUrl)")
        ServiceManager.shared.request(type: OfficesListModel.self, url: officeListUrl, method: .get, view: self.view) { (response) in
            print("The PS list response is :\(String(describing: response))")
            if response?.count ?? 0 != 0 {
                DispatchQueue.main.async {
                    self.officesResponseModel = response
                    self.tableViewTransparentView.isHidden = true
                    self.tableViewTransparentView.layer.cornerRadius = 5.0
                    self.tableViewTransparentView.layer.masksToBounds = false
                    
                self.idString =  String(self.officesResponseModel?[0].id ?? 0 )
                self.tempPSID =  self.officesResponseModel?[0].id ?? 0
           
self.officesDropDown.text = "\(self.officesResponseModel?[0].name ?? "" )  (\(self.officesResponseModel?[0].code ?? ""))"
                    
                self.officesTableViewObj.reloadData()
                }
            }
            else {
               self.alert(title: "Alert Message", message: "No Offcies List were Found")
            }
        }
    }
        
    
    //MARK: Next Button Action
    @IBAction func nextButtonAction(_ sender: Any) {
                self.FlagsAPICalling()
    }
    
    
//MARK: Flags API Calling
    func FlagsAPICalling()  {

        let officeIDString = String(describing: tempOfficeID ?? "0")
        let details = self.getuserDetails()
        let url = (details.baseURL ?? "") + API.GetCallConfigURL + (officeIDString)
        let sessionID = API.SessionID + (details.sessionID ?? "")
        let flagsUrl = url + sessionID
        print("The flagsurl is :\(String(describing: flagsUrl))")
        ServiceManager.shared.request(type: ReportFlagsModel.self, url: flagsUrl, method: .get, view: self.view) { (response) in
        print("The response of flagsin non client vc is :\(String(describing: response))")
            if response == nil {
                self.alert(title: "Alert Message", message: "Please check your data")
            }
            else {
                self.ReportFlagResponseModel = response
                DispatchQueue.main.async {
                    if response?.specialFlag == 1 {
                        guard let nonClientDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "NonClientDetailVC") as? NonClientDetailVC  else { return }
                        nonClientDetailVC.receivedOfficeID = self.tempOfficeID
                        self.navigationController?.pushViewController(nonClientDetailVC, animated: true)
                        
                    }else {
                        self.alert(title: "Alert Message", message: "No Transaction are available")
                    }
                    
                }
                        
                }
            }
    }
    
    
}

//MARK: UIView Controller Transistion Delegate
extension NonClientVC : UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transiton.isPresenting = true
        return transiton
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transiton.isPresenting = false
        return transiton
    }
}

//MARK: UIGesture Recognizer Delegate Methods
extension NonClientVC : UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view!.isDescendant(of: settingsTableView) {
            return false
        }
        return true
    }
}


extension NonClientVC : UITextFieldDelegate {

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    if textField == officesDropDown{
        // self.getOfficesList()
        self.tableViewTransparentView.isHidden = false
        return false
    }
    return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == searchTextField{
            if searchTextField.text?.count ?? 0 > 1{
                self.isSearching = true
               let results = searchOfficeResults?.filter( { ($0.name ?? "").localizedStandardContains(searchTextField.text ?? "")})
               self.searchOfficeResults = results
               self.officesTableViewObj.reloadData()
            }else{
                self.isSearching = false
                searchOfficeResults = nil
                self.officesTableViewObj.reloadData()
            }
        }
        return true
    }
    
}


//MARK: UITableView Delegate and Datasource Methods
extension NonClientVC: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
     return 1
        
     }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                
        
        switch tableView {
        case settingsTableView:
             return settingsArray.count
        case officesTableViewObj:
            if isSearching {
                return searchOfficeResults?.count ?? 0
            }
            return  self.officesResponseModel?.count ?? 0
        default:
            break
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch tableView {
        case settingsTableView:
            
        guard  let settingscell = tableView.dequeueReusableCell(withIdentifier: "NonClientCell", for: indexPath) as? NonClientCell else { return UITableViewCell() }
            settingscell.nonClientNameLabel.text = settingsArray[indexPath.row]
            if indexPath.row == 3 {
                settingscell.nonClientNameLabel.textColor = UIColor(displayP3Red: 116/255.0, green: 177/255.0, blue: 243/255.0, alpha: 1)
            }
            else {
                settingscell.nonClientNameLabel.textColor = .black
            }
            settingscell.selectionStyle = .none
            return settingscell
            
        
        case officesTableViewObj:
            
            guard let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier:cellReuseIdentifier) else {return UITableViewCell() }
            
            if isSearching {
            cell.textLabel?.text = "\(self.searchOfficeResults?[indexPath.row].name ?? "" )  (\(self.searchOfficeResults?[indexPath.row].code ?? ""))"
            }else {
          cell.textLabel?.text = "\(self.officesResponseModel?[indexPath.row].name ?? "" )  (\(self.officesResponseModel?[indexPath.row].code ?? ""))"
                
            }
                        
            cell.textLabel?.textColor = UIColor.init(red: 20/255.0, green:81/255.0 , blue: 148/255.0, alpha: 1.0)
            cell.textLabel?.font = UIFont(name: "Avenir-Roman", size: 18.0)

            return cell
            
        default:
            return UITableViewCell()
        }
        
   }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch tableView {
            
        case settingsTableView:

            if indexPath.row == 0 {
               DispatchQueue.main.async {
                          guard let searchSchedulesVC = self.storyboard?.instantiateViewController(withIdentifier: "scheduleExceptionsVC") as? ScheduleExceptionsVC else { return }
                          self.settingsTransparentView.isHidden = true
                          let navigation = UINavigationController(rootViewController: searchSchedulesVC)
                          navigation.modalPresentationStyle = .fullScreen
                          self.present(navigation, animated: false, completion: nil)
                      }
                }
                                      
                else if (indexPath.row == 1) {
                         
                DispatchQueue.main.async {
                                  guard let searchSchedulesVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchSchdeulesVC") as? SearchSchdeulesVC else { return }
                                  self.settingsTransparentView.isHidden = true
                                  let navigation = UINavigationController(rootViewController: searchSchedulesVC)
                                  navigation.modalPresentationStyle = .fullScreen
                                  self.present(navigation, animated: false, completion: nil)
                    }
                                      
            }
                                      
                else if (indexPath.row == 2) {
                
                DispatchQueue.main.async {
                     guard let ReportUnScheduledVC = self.storyboard?.instantiateViewController(withIdentifier: "ReportUnScheduledVC") as? ReportUnScheduledVC else { return }
                         self.settingsTransparentView.isHidden = true
                     let navigation = UINavigationController(rootViewController: ReportUnScheduledVC)
                     navigation.modalPresentationStyle = .fullScreen
                     self.present(navigation, animated: false, completion: nil)
                     }
                                      
            }
                                      
              else if (indexPath.row == 3) {
                   
              }
                                      
          else if (indexPath.row == 4){
               DispatchQueue.main.async {
              guard let timeSheet = self.storyboard?.instantiateViewController(withIdentifier: "TimeSheetVC") as? TimeSheetVC else { return }
                  self.settingsTransparentView.isHidden = true
              
            let navigation = UINavigationController(rootViewController: timeSheet)
          navigation.modalPresentationStyle = .fullScreen
          self.present(navigation, animated: false, completion: nil)
              }
          }
            
        case officesTableViewObj:
            
            if isSearching {
                idString =  String(self.searchOfficeResults?[indexPath.row].id ?? 0 )
                officesDropDown.text = "\(self.searchOfficeResults?[indexPath.row].name ?? "" )  (\(self.searchOfficeResults?[indexPath.row].code ?? ""))"
                tempOfficeID = String(self.searchOfficeResults?[indexPath.row].id ?? 0)
                
            }
            else {
                idString =  String(self.officesResponseModel?[indexPath.row].id ?? 0 )
                officesDropDown.text = "\(self.officesResponseModel?[indexPath.row].name ?? "" )  (\(self.officesResponseModel?[indexPath.row].code ?? ""))"
                tempOfficeID = String(self.officesResponseModel?[indexPath.row].id ?? 0)
            }
                        
        searchOfficeResults = nil
        isSearching = false

            DispatchQueue.main.async {
                self.officesTableViewObj.reloadData()
                self.searchTextField.text = ""
                self.tableViewTransparentView.isHidden = true
            }
        default:
             break
}

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }

}



