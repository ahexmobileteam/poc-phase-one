//
//  WeeklyTimeSheetModel.swift
//  POC
//
//  Created by Ajeet N on 04/05/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//


import Foundation



// MARK: - Welcome
struct WeeklyTimeSheetModel: Codable {
    let signatureData: String?
    let jWeeklyTimeCardDetailsArray: [JWeeklyTimeCardDetailsArray]?
    let weekStart: String??
    let psName, weekEnd: String?
}

// MARK: - JWeeklyTimeCardDetailsArray
struct JWeeklyTimeCardDetailsArray: Codable {
    let reportedTimes: String?
    let taskNames: String?
    let arrivalInfoId, departureInfoId: Int?
    let procedureCode: String?
    let weekDay: String?
    let weekDate: String?
    let workedHours: Double?
    let taskIds: String?
}

// URLResponse.wekke[index.row].taksnames
