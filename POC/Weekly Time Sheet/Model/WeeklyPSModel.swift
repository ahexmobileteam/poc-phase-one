//
//  WeeklyPSModel.swift
//  POC
//
//  Created by Ajeet N on 04/05/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//


import Foundation

typealias WeeklyPSModelData = [WeeklyPSModel]

// MARK: - WelcomeElement
struct WeeklyPSModel: Codable {
    let psName, psId: String?
}
