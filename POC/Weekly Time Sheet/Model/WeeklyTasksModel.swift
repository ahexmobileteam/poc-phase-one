//
//  WeeklyTasksModel.swift
//  POC
//
//  Created by Ajeet N on 09/05/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import Foundation

typealias WeeklyTaskModelResponse = [WeeklyTasksModel]


// MARK: - WelcomeElement
struct WeeklyTasksModel: Codable {
    let taskName, taskAcceptFlag: String?
}

