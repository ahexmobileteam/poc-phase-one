//
//  WeeklyCell.swift
//  POC
//
//  Created by Ajeet N on 08/05/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class WeeklyCell: UITableViewCell {

    
    @IBOutlet weak var weekStart: UILabel!
    @IBOutlet weak var procedureCode: UILabel!
    @IBOutlet weak var weekDay: UILabel!
    @IBOutlet weak var reportedTimings: UILabel!
    @IBOutlet weak var reportedTimingDiff: UILabel!
    
    @IBOutlet weak var cellCardViewObj: UIView!
    @IBOutlet weak var tasksOutelt: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func tasksButtonAction(_ sender: Any) {
    }
    
}
