//
//  WeeklyTasksCell.swift
//  POC
//
//  Created by Ajeet N on 08/05/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//


import UIKit

class WeeklyTasksCell: UITableViewCell {

    @IBOutlet weak var cardViewObj: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageState: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
