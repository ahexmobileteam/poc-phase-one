//
//  WeeklyTimeSheetVC.swift
//  POC
//
//  Created by Ajeet N on 04/05/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//


import UIKit


class WeeklyTimeSheetVC: UIViewController {

    let cellReuseIdentifier = "cell"
    private var idString: String? = nil
    var tempPSID : String?

    
    private let transiton = SlideInTransition()
    var gesture = UITapGestureRecognizer()
    private var selectedIndex = Int()
    private var isSearching = false
    private var WeeklyPsModelResponseData : WeeklyPSModelData?
    private var searchResults : WeeklyPSModelData?

    private let datePicker = UIDatePicker()
    private var startWeek: String? = nil
    @IBOutlet weak var calendarObj: CustomTField!
    @IBOutlet weak var psDropDownTF: DropDownTextField!
    
    @IBOutlet weak var transperantView: UIView!
    @IBOutlet weak var cardViewObj: UIView!
    @IBOutlet weak var searchTextField: CustomTField!
    @IBOutlet weak var psTableViewObk: UITableView! {
        didSet{
            psTableViewObk.delegate = self
            psTableViewObk.dataSource = self
        }
    }
    
    
    
//MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        searchTextField.delegate = self
        self.configureNavigationBar(title: "Weekly Time Card")
        self.transperantView.isHidden = true
        psDropDownTF.delegate = self

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
             tapGesture.delegate = self
             transperantView.addGestureRecognizer(tapGesture)
        
        self.psTableViewObk.tableFooterView = UIView()

        
        NotificationCenter.default.addObserver(self, selector: #selector(sessionExpired), name: Notification.Name("SessionExpired"), object: nil)
        
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(datePickerValueChanged(_:)), for: .valueChanged)
        let startDateToolbar = UIToolbar()
          startDateToolbar.sizeToFit()
        let startDoneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(startDateDoneAction))
         let startSpaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
         let startCancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelAction))
         startDateToolbar.setItems([startCancelButton,startSpaceButton,startDoneButton], animated: false)
         calendarObj.inputAccessoryView = startDateToolbar
         calendarObj.inputView = datePicker
        
    }
    override func viewDidAppear(_ animated: Bool) {
        datePickerValueChanged(datePicker)
    }
    @objc func startDateDoneAction(){
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let selectedDate = formatter.string(from: datePicker.date)
        startWeek = selectedDate
        let nextDate = Calendar.current.date(byAdding: .day, value: 6, to: datePicker.date)
        let finalNextWeek = formatter.string(from: nextDate!)
        calendarObj.text = "\(selectedDate) - \(String(describing: finalNextWeek))"
        calendarObj.layer.borderColor = UIColor.greenColor.cgColor
        self.view.endEditing(true)
    }
    

    @objc func cancelAction(){
       self.view.endEditing(true)
     }
    
    @objc func datePickerValueChanged(_ sender: UIDatePicker) {
        let calender: Calendar = Calendar(identifier: .gregorian)
        let weekday = calender.component(.weekday, from: sender.date)
        if weekday == 2 {
                  datePicker.setDate(Date(timeInterval: 60*60*24*(-1), since: sender.date), animated: true)
        }else if weekday == 3 {
            datePicker.setDate(Date(timeInterval: 60*60*24*(-2), since: sender.date), animated: true)
        }else  if weekday == 4 {
              datePicker.setDate(Date(timeInterval: 60*60*24*(-3), since: sender.date), animated: true)
        }else if weekday == 5 {
              datePicker.setDate(Date(timeInterval: 60*60*24*(-4), since: sender.date), animated: true)
        }else if weekday == 6 {
              datePicker.setDate(Date(timeInterval: 60*60*24*(2), since: sender.date), animated: true)
        }else if weekday == 7 {
              datePicker.setDate(Date(timeInterval: 60*60*24*(1), since: sender.date), animated: true)
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let selectedDate = formatter.string(from: datePicker.date)
        startWeek = selectedDate
        let nextDate = Calendar.current.date(byAdding: .day, value: 6, to: datePicker.date)
        let finalNextWeek = formatter.string(from: nextDate!)
        calendarObj.text = "\(selectedDate) - \(String(describing: finalNextWeek))"
        calendarObj.layer.borderColor = UIColor.greenColor.cgColor
        
    }
    //MARK: Tap Gesture Action
       @objc func handleTap(){
           transperantView.isHidden = true
    }
    
    
//MARK: Session Expired
       @objc func sessionExpired() {
              NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "SessionExpired"), object: nil)
        self.logout()
    }
    
    
    
//MARK: Get list of PSLists
    func getListOfPsList()  {
        
//http://poc.aquilasoftware.com/pocextacc-webservices_new/telephony/getPSListForWeeklyTimeCard?&dcsId=14635&weekStart=04%2F26%2F2020&sessionId=180837_1435S08J3F9V0FM6
        
        let staticWeekDate = startWeek ?? ""
        //"04/26/2020"
        
        let details = self.getuserDetails()
        
        let baseurl = (details.baseURL ?? "")
        let url = API.WeeklyTimeCardWithDCSID + (details.dcsID ?? "")
        let weeStart = API.WeekStart + staticWeekDate
        let sessionId = API.SessionID + (details.sessionID ?? "")
        
        let Finalurl = baseurl +  url + weeStart + sessionId
        ServiceManager.shared.request(type: WeeklyPSModelData.self, url: Finalurl, method: .get, view: self.view) { (response) in
            print("The response of pslist is :\(String(describing: response))")
            if response?.count != nil {
                DispatchQueue.main.async {
                                   self.transperantView.isHidden = false
                                   self.transperantView.layer.cornerRadius = 5.0
                                   self.transperantView.layer.masksToBounds = true
                                   self.WeeklyPsModelResponseData  = response
                                   self.psTableViewObk.reloadData()
                        }
            }
            else {
                
                self.alert(title: "Alert Message", message: "No Offices Found")
                self.transperantView.isHidden = true
            }
        }
}
    
    //MARK: Side Menu Action
    @IBAction func sideMenuAction(_ sender: Any) {
        
guard let menuViewController = self.storyboard?.instantiateViewController(withIdentifier: "MenuTableViewController")  as? MenuTableViewController else { return }
              menuViewController.modalPresentationStyle = .overCurrentContext
              menuViewController.transitioningDelegate = self
              menuViewController.identifyController = .isWeeklyTimeCard
       self.present(menuViewController, animated: true)
        
    }
    
    
    //MARK: Go Button Action
    @IBAction func GoButtonAction(_ sender: Any) {
        if tempPSID == nil{
            self.toast(msgString: "Please choose PS", view: self.view)
            return
        }
        guard let WeeklySearchedListVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchWeeklyTimeSheet")  as? SearchWeeklyTimeSheet else { return }
        WeeklySearchedListVC.receivedPSID = tempPSID
        WeeklySearchedListVC.selectedWeek = calendarObj.text ?? ""
        WeeklySearchedListVC.startWeek = self.startWeek
          self.navigationController?.pushViewController(WeeklySearchedListVC, animated: true)
        
    }
    
}

//MARK: UIView Controller Transistion Delegate
extension WeeklyTimeSheetVC : UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
           transiton.isPresenting = true
           return transiton
       }

       func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
           transiton.isPresenting = false
           return transiton
       }
}


//MARK: UITableView Delegate and DataSource Methods
extension WeeklyTimeSheetVC : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  isSearching ? (searchResults?.count ?? 0) : WeeklyPsModelResponseData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier:cellReuseIdentifier) else {return UITableViewCell() }
        if isSearching{
            cell.textLabel?.text = self.searchResults?[indexPath.row].psName ?? ""

        }else{
        cell.textLabel?.text = self.WeeklyPsModelResponseData?[indexPath.row].psName ?? ""
        }
        cell.textLabel?.textColor = UIColor.init(red: 20/255.0, green:81/255.0 , blue: 148/255.0, alpha: 1.0)
        cell.textLabel?.font = UIFont(name: "Avenir-Roman", size: 18.0)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isSearching{
            idString =  String(self.searchResults?[indexPath.row].psId ?? "0" )
            psDropDownTF.text = self.searchResults?[indexPath.row].psName
            tempPSID = self.searchResults?[indexPath.row].psId ?? "0"
        }else{
            idString =  String(self.WeeklyPsModelResponseData?[indexPath.row].psId ?? "0" )
            psDropDownTF.text = self.WeeklyPsModelResponseData?[indexPath.row].psName
           tempPSID = self.WeeklyPsModelResponseData?[indexPath.row].psId ?? "0"
        }
        searchTextField.text = ""
        searchResults = nil
        isSearching = false
        self.transperantView.isHidden = true

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 50
    }
    
}


//MARK: UIGesture Recognizer Delegate Methods
extension WeeklyTimeSheetVC : UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view!.isDescendant(of: psTableViewObk) {
            return false
        }
        return true
    }
}


extension WeeklyTimeSheetVC : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == psDropDownTF{
            getListOfPsList()
        return false
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == searchTextField{
            if searchTextField.text?.count ?? 0 > 1{
                self.isSearching = true
               let results = WeeklyPsModelResponseData?.filter( { ($0.psName ?? "").localizedStandardContains(searchTextField.text ?? "")})
               self.searchResults = results
               self.psTableViewObk.reloadData()
            }else{
                self.isSearching = false
                searchResults = nil
                self.psTableViewObk.reloadData()
            }
        }
        return true
    }
}

