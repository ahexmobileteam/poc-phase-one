//
//  SearchWeeklyTimeSheet.swift
//  POC
//
//  Created by Ajeet N on 04/05/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//


import UIKit


class SearchWeeklyTimeSheet: UIViewController {

    private var weeklyTimeSheetResponseData: WeeklyTimeSheetModel?
    var receivedPSID : String?
    
    @IBOutlet weak var signatureImageView: UIImageView!
    @IBOutlet weak var transparentViewObj: UIView!
    @IBOutlet weak var cardViewObj: UIView!
    
    @IBOutlet weak var signatureActionsStack: UIStackView!
    
    @IBOutlet weak var signatureViewObjj: YPDrawSignatureView!
    
    @IBOutlet weak var signatureViewHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var headerStartDateandEndDate: UILabel!
    @IBOutlet weak var WeeklyTableViewObj: UITableView! {
        didSet{
            WeeklyTableViewObj.delegate = self
            WeeklyTableViewObj.dataSource = self
        }
    }
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var greenBorderView: UIView!
    @IBOutlet weak var totalHoursLabel: UILabel!
    @IBOutlet weak var totalhoursValueLabel: UILabel!
    internal var selectedWeek: String?
    internal var startWeek: String?
    internal var pdId: Int?
    private var signature = UIBarButtonItem()
    private var searchTimeSheet = UIBarButtonItem()
//MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getWeeklyListAPI()
        
        self.transparentViewObj.isHidden = true
       
        signatureViewObjj.delegate = self
        headerStartDateandEndDate.text = selectedWeek ?? ""
        signatureViewObjj.layer.cornerRadius = 3.0
        signatureViewObjj.layer.borderColor = UIColor.lightGray.cgColor
        signatureViewObjj.layer.borderWidth = 1.0
        

    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
    tapGesture.delegate = self
    transparentViewObj.addGestureRecognizer(tapGesture)
        
    print("The selected psID is :\(String(describing: receivedPSID))")
        
    self.greenBorderView.layer.borderWidth = 1.0
    self.greenBorderView.layer.borderColor = UIColor.init(red: 118/255.9, green: 189/255.0, blue: 67/255, alpha: 1.0).cgColor
    self.greenBorderView.layer.cornerRadius = 5.0
      
    signature = UIBarButtonItem(image: UIImage(named: "Signature_01"), style: .plain, target: self, action: #selector(SignatureScreen))
        
    searchTimeSheet = UIBarButtonItem(image: UIImage(named: "Search_01"), style: .plain, target: self, action: #selector(SearchScheduleScreen))
    
    navigationItem.rightBarButtonItems = [searchTimeSheet,signature]
        
}
    
//MARK: Tap Gesture Action
    @objc func handleTap(){
    transparentViewObj.isHidden = true
}
    
//MARK: Search Schedule Screen
    @objc func SearchScheduleScreen() {
    print("Search btton tapped!")
        self.navigationController?.popViewController(animated: true)
    }


//MARK: Signature Pop Up View
    @objc func SignatureScreen() {
        
       self.transparentViewObj.isHidden = false
        self.transparentViewObj.layer.masksToBounds = true
        self.cardViewObj.layer.cornerRadius = 5.0
        self.cardViewObj.layer.masksToBounds = false
    }
    
    
   //MARK: Clear Button Action
  @IBAction func clearButtonAction(_ sender: Any) {
    self.signatureViewObjj.clear()
    
    }
    
    
    //MARK: Cross Mark Button Action
    @IBAction func cardViewButtonAction(_ sender: Any) {
           self.transparentViewObj.isHidden = true
       }
    
    
    //MARK: Accept and Save Button Action
    @IBAction func acceptSaveAction(_ sender: Any) {
        var visit: String? = nil
        if (self.weeklyTimeSheetResponseData?.jWeeklyTimeCardDetailsArray?.count ?? 0) != 0{
            for index in 0..<self.weeklyTimeSheetResponseData!.jWeeklyTimeCardDetailsArray!.count {
                let reportTimes = self.weeklyTimeSheetResponseData?.jWeeklyTimeCardDetailsArray?[index].reportedTimes ?? ""
                let arrivalID = self.weeklyTimeSheetResponseData?.jWeeklyTimeCardDetailsArray?[index].arrivalInfoId ?? 0
                let departureID = self.weeklyTimeSheetResponseData?.jWeeklyTimeCardDetailsArray?[index].departureInfoId ?? 0
                let procedure = weeklyTimeSheetResponseData?.jWeeklyTimeCardDetailsArray?[index].procedureCode ?? ""
                let weekDay = self.weeklyTimeSheetResponseData?.jWeeklyTimeCardDetailsArray?[index].weekDay
                let weekDate = self.weeklyTimeSheetResponseData?.jWeeklyTimeCardDetailsArray?[index].weekDate ?? ""
                let workedHours = self.weeklyTimeSheetResponseData?.jWeeklyTimeCardDetailsArray?[index].workedHours ?? 0.0
                let visitDetails =  ["reportedTimes":reportTimes,"arrivalInfoId":arrivalID,"departureInfoId":departureID,"procedureCode":procedure,"weekDay":weekDay ?? "","weekDate":weekDate,"workedHours":workedHours] as [String : Any]
                if visit == nil{
                    visit = visitDetails.jsonStringRepresentation ?? ""
                }else{
                    visit!.append(",\(visitDetails.jsonStringRepresentation ?? "")")
                }
            }
        }
        
        
        let finalvisitDetials = "[\(visit ?? "")]"
        let userDetails = self.getuserDetails()
        var url = String()
        url.append(userDetails.baseURL ?? "")
        url.append("savePSWeeklyTimeCard?PSWeeklyTimeCard=")
        let startW = self.weeklyTimeSheetResponseData?.weekStart ?? ""
        let endW = self.weeklyTimeSheetResponseData?.weekEnd ?? ""
        let psID = receivedPSID ?? ""
        let parameters = ["dcsId":(userDetails.dcsID ?? ""),"psId":psID,"weekStartDate":startW ?? "","weekEndDate":endW,"jVisitDetailsArray":finalvisitDetials] as [String : Any]
        let finalparameters = parameters.jsonStringRepresentation ?? ""
        print(finalparameters)
        url.append(finalparameters)
        
        if let image = signatureViewObjj.getSignature() {
            let signature = image.toBase64()
            url.append("&signature=\(String(describing: signature ?? ""))")
            url.append("&sessionId=\(userDetails.sessionID ?? "")")
            self.transparentViewObj.isHidden = true
            ServiceManager.shared.request(type: SaveSignatureModel.self, url: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "", method: .get, view: self.view) { [weak self](response) in
                if response?.successFlag ?? "" == "SUCCESS"{
                    DispatchQueue.main.async {
                        self?.signatureViewObjj.clear()
                        self?.toast(msgString: "Time card details along with signature details submitted successfully.", view: self?.view ?? UIView())
                        self?.getWeeklyListAPI()
                    }
                }else{
                    DispatchQueue.main.async {
                        self?.toast(msgString: "Error occured", view: self?.view ?? UIView())
                    }
                }
            }
        }
        
            
    }
    
//MARK: Get Weekly List
    func getWeeklyListAPI()  {
        
//http://poc.aquilasoftware.com/pocextacc-webservices_phase2/telephony/getPSWeeklyTimeCard?&dcsId=14635&psId=14924&weekStart=05%2F03%2F2020&sessionId=180837_AL8VCIVLU2359ZE0
        
        let staticWeekDate = startWeek ?? ""

        let details = self.getuserDetails()
        
        let baseurl = (details.baseURL ?? "")
        let url = API.GetWeeklyList + (details.dcsID ?? "")
        let psID = API.PSID + String(describing: receivedPSID ?? "0")
        let weeklyStart = API.WeekStart + staticWeekDate
        let SessionID = API.SessionID + (details.sessionID ?? "")
        
        let finalUrl =  baseurl + url + psID + weeklyStart + SessionID
        print("The final url is :\(finalUrl)")
        
        
        ServiceManager.shared.request(type: WeeklyTimeSheetModel.self, url: finalUrl, method: .get, view: self.view) { (response) in
            print("The respnse of weekly list is :\(String(describing: response))")

            if response != nil {
                self.weeklyTimeSheetResponseData = response
                DispatchQueue.main.async {
                     self.WeeklyTableViewObj.reloadData()
                    let sum = self.weeklyTimeSheetResponseData?.jWeeklyTimeCardDetailsArray?.map({$0.workedHours ?? 0.0}).reduce(0, +)
                    self.totalHoursLabel.text = "\(sum ?? 0.0) hrs"
                    self.headerLabel.text = self.weeklyTimeSheetResponseData?.psName ?? ""
                    if (self.weeklyTimeSheetResponseData?.signatureData ?? "") == ""{
                        self.signature = UIBarButtonItem(image: UIImage(named: "Signature_01"), style: .plain, target: self, action: #selector(self.SignatureScreen))
                        self.navigationItem.rightBarButtonItems = [self.searchTimeSheet,self.signature]
                        self.signatureViewObjj.isHidden = false
                        self.signatureActionsStack.isHidden = false
                        self.signatureViewHeight.constant = 300
                        self.signatureImageView.isHidden = true
                    }else{
                        self.signature = UIBarButtonItem(image: UIImage(named: "Signature_01"), style: .plain, target: self, action: #selector(self.SignatureScreen))
                        self.navigationItem.rightBarButtonItems = [self.searchTimeSheet,self.signature]
                        self.signatureViewObjj.isHidden = true
                        self.signatureActionsStack.isHidden = true
                        self.signatureViewHeight.constant = 250
                        self.signatureImageView.isHidden = false
                        if let decodedData = Data(base64Encoded: self.weeklyTimeSheetResponseData?.signatureData ?? "", options: .ignoreUnknownCharacters) {
                            let image = UIImage(data: decodedData)
                            self.signatureImageView.image = image ?? UIImage()
                        }
                    }
                }
               
            }
    }
    
}
    
//MARK: Tasks Button Action
    @objc func taskButtonAction () {
        
        guard let WeeklytasksVC = self.storyboard?.instantiateViewController(withIdentifier: "WeeklyTasksViewController")  as? WeeklyTasksViewController else { return }
        self.navigationController?.pushViewController(WeeklytasksVC, animated: true)
        
    }

        

}


//MARK: UITableView Delegate and Datasource Methods
extension SearchWeeklyTimeSheet: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.weeklyTimeSheetResponseData?.jWeeklyTimeCardDetailsArray?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "WeeklyCell", for: indexPath) as? WeeklyCell else { return UITableViewCell() }
        cell.cellCardViewObj.layer.borderWidth = 1.0
        cell.cellCardViewObj.layer.cornerRadius = 5.0
        cell.cellCardViewObj.layer.borderColor = UIColor.lightGray.cgColor
        cell.tasksOutelt.addTarget(self, action: #selector(taskButtonAction), for: .touchUpInside)
        
        cell.reportedTimings.text = self.weeklyTimeSheetResponseData?.jWeeklyTimeCardDetailsArray?[indexPath.row].reportedTimes ?? ""
        cell.weekStart.text = self.weeklyTimeSheetResponseData?.jWeeklyTimeCardDetailsArray?[indexPath.row].weekDate ?? ""
        cell.procedureCode.text = self.weeklyTimeSheetResponseData?.jWeeklyTimeCardDetailsArray?[indexPath.row].procedureCode ?? ""
        cell.weekDay.text = self.weeklyTimeSheetResponseData?.jWeeklyTimeCardDetailsArray?[indexPath.row].weekDay ?? ""
        
//     let x : Int =  self.weeklyTimeSheetResponseData?.jWeeklyTimeCardDetailsArray[indexPath.row].workedHours ?? 0
        cell.reportedTimingDiff.text = "\(self.weeklyTimeSheetResponseData?.jWeeklyTimeCardDetailsArray?[indexPath.row].workedHours ?? 0) hrs"
        
    return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }
    
}

//MARK: UIGesture Recognizer Delegate Methods
extension SearchWeeklyTimeSheet : UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view!.isDescendant(of: WeeklyTableViewObj) {
            return false
        }
        return true
    }
}


//MARK: YPSignature Delegate Methods
extension SearchWeeklyTimeSheet : YPSignatureDelegate {
    
    func didStart(_ view: YPDrawSignatureView) {
        print("Started Drawing")
    }
    
    func didFinish(_ view: YPDrawSignatureView) {
        print("Finished Drawing")
    }
    
}

struct SaveSignatureModel: Decodable {
    let successFlag: String?
}
