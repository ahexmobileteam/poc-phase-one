//
//  WeeklyTasksViewController.swift
//  POC
//
//  Created by Ajeet N on 08/05/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class WeeklyTasksViewController: UIViewController {

    
    @IBOutlet weak var weeklyTaskTableViewObj: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getListofTaskAPI()
        
        weeklyTaskTableViewObj.delegate = self
        weeklyTaskTableViewObj.dataSource = self
        self.configureNavigationBar(title: "Tasks")
        
}
    
    
//MARK: Get List of Tasks API
    func getListofTaskAPI()  {
        
//http://poc.aquilasoftware.com/pocextacc-webservices_phase2/telephony/getPSWeeklyTimeCardTasks?jsonObj=%7B%22departureInfoId%22%3A1750877%7D&sessionId=180837_AL8VCIVLU2359ZE0
        

        
    }
    
}


//MARK: UITableView Delegate and DataSource Methods
extension WeeklyTasksViewController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "WeeklyTasksCell", for: indexPath) as? WeeklyTasksCell else { return UITableViewCell() }
        cell.nameLabel.text = "Washing Machine"
        cell.imageView?.image = UIImage(named: "checked")
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
}
 
