//
//  ProfileDetailsController.swift
//  POC
//
//  Created by Admin on 09/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class ProfileDetailsController: UIViewController {
    
    private var ProfileModelResponse: ProfileModel?
    private var ScheduleExceptionList: [ScheduleExceptinModel]?
    
    var profileNameLabel : UILabel?
    
    @IBOutlet weak var loggedInUserName: UIBarButtonItem!
    
    @IBOutlet weak var emptyStateLabel: UILabel!
    @IBOutlet weak var profileTabsCollectionView: UICollectionView!{
        didSet{
            self.profileTabsCollectionView.delegate = self
            self.profileTabsCollectionView.dataSource = self
        }
    }
    @IBOutlet weak var detailsButton: UIButton!
    @IBOutlet weak var availabilityButton: UIButton!
    @IBOutlet weak var certificationsButton: UIButton!
    @IBOutlet weak var preferencesButton: UIButton!
    @IBOutlet weak var scheduleExceptionsButton: UIButton!
    
    @IBAction func updatePrefNewAction(_ sender: Any) {
        self.toast(msgString: "Coming Soon", view: self.view)
    }
    
    
    private let transiton = SlideInTransition()
    
    @IBAction func show_side_menu(_ sender: Any) {
        guard let menuViewController = self.storyboard?.instantiateViewController(withIdentifier: "MenuTableViewController")  as? MenuTableViewController else { return }
               menuViewController.modalPresentationStyle = .overCurrentContext
               menuViewController.transitioningDelegate = self
               menuViewController.identifyController = .isProfile
        self.present(menuViewController, animated: true)
    }
    
//MARK:  View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureNavigationBar(title: "Profile")
        self.ProfileAPICalling()

self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 20)!,NSAttributedString.Key.foregroundColor : UIColor.white]
        
       NotificationCenter.default.addObserver(self, selector: #selector(sessionExpired), name: Notification.Name("SessionExpired"), object: nil)
            
        
    
}
    
//MARK: Session Expired
        @objc func sessionExpired() {
               NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "SessionExpired"), object: nil)
               self.logout()
        }
    
    
//MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
}
    
//MARK: Load Profile name on navigation bar
// func loadProfileName() {
//
//    if let navigationBar = self.navigationController?.navigationBar {
//        let firstFrame = CGRect(x: navigationBar.frame.width-160, y: 0, width: 250 , height: navigationBar.frame.height)
//    DispatchQueue.main.async {
//                 self.profileNameLabel = UILabel(frame: firstFrame)
//                 self.profileNameLabel?.font = UIFont.init(name: "Avenir-Medium", size: 15)
//                 self.profileNameLabel?.numberOfLines = 0
//                 self.profileNameLabel?.text = self.ProfileModelResponse?.name ?? "NA"
//                 print("the profile name is :\(String(describing: self.ProfileModelResponse))")
//                 self.profileNameLabel?.textColor = .white
//                 navigationBar.addSubview(self.profileNameLabel!)
//    }
//            }
//}

    
 //MARK: Detail Button Action
    @IBAction func details_action(_ sender: Any) {
        switch (sender as AnyObject).tag {
        case 0:
            detailsButton.setTitleColor(UIColor.greenColor, for: .normal)
            availabilityButton.setTitleColor(.darkGray, for: .normal)
            certificationsButton.setTitleColor(.darkGray, for: .normal)
            preferencesButton.setTitleColor(.darkGray, for: .normal)
            scheduleExceptionsButton.setTitleColor(.darkGray, for: .normal)
            scrollIndex(index: 0)
            break
        case 1:
            detailsButton.setTitleColor(.darkGray, for: .normal)
            availabilityButton.setTitleColor(UIColor.greenColor, for: .normal)
            certificationsButton.setTitleColor(.darkGray, for: .normal)
            preferencesButton.setTitleColor(.darkGray, for: .normal)
            scheduleExceptionsButton.setTitleColor(.darkGray, for: .normal)
            scrollIndex(index: 1)
            break
        case 2:
            detailsButton.setTitleColor(.darkGray, for: .normal)
            availabilityButton.setTitleColor(.darkGray, for: .normal)
            certificationsButton.setTitleColor(UIColor.greenColor, for: .normal)
            preferencesButton.setTitleColor(.darkGray, for: .normal)
            scheduleExceptionsButton.setTitleColor(.darkGray, for: .normal)
            scrollIndex(index: 2)
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "refreshCertifications"), object: nil)
            
            break
        case 3:
            detailsButton.setTitleColor(.darkGray, for: .normal)
            availabilityButton.setTitleColor(.darkGray, for: .normal)
            certificationsButton.setTitleColor(.darkGray, for: .normal)
            preferencesButton.setTitleColor(UIColor.greenColor, for: .normal)
            scheduleExceptionsButton.setTitleColor(.darkGray, for: .normal)
            scrollIndex(index: 3)
            break
        case 4:
            detailsButton.setTitleColor(.darkGray, for: .normal)
            availabilityButton.setTitleColor(.darkGray, for: .normal)
            certificationsButton.setTitleColor(.darkGray, for: .normal)
            preferencesButton.setTitleColor(.darkGray, for: .normal)
            scheduleExceptionsButton.setTitleColor(UIColor.greenColor, for: .normal)
            scrollIndex(index: 4)
            break
        default:
            break
        }
        
        self.profileTabsCollectionView.reloadData()
        
    }
    
    
//MARK: Scroll Index
    private func scrollIndex(index: Int){
        profileTabsCollectionView.scrollToItem(at: IndexPath(row: 0, section: index), at: .centeredHorizontally, animated: false)
    }
    
    
//MARK: Profile API Calling
      func ProfileAPICalling() {
        let details = self.getuserDetails()

        let profile = (details.baseURL ?? "") + API.profileAPI + (details.dcsID ?? "")
        let sessionIDURL = API.SessionID + (details.sessionID ?? "")
          let profileUrl = profile + sessionIDURL
          print("the profile url is :\(profileUrl)")
          ServiceManager.shared.request(type: ProfileModel.self, url: profileUrl, method: .get, view: self.view) { (response) in
           print("the profile Details api in profile detail view controller is  :\(String(describing: response))")
            
            DispatchQueue.main.async {
              self.ProfileModelResponse = response
           // self.loadProfileName()
            
            self.loggedInUserName.title = self.ProfileModelResponse?.name ?? ""
              self.profileTabsCollectionView.reloadData()
            }
         
           print("the saved repns is :\(String(describing: self.ProfileModelResponse))")
          }
        
}
    
    //MARK: Schedule Exceptions API
       func  ScheduleExceptionsAPI() {
           
        let details = self.getuserDetails()

        
        let url = (details.baseURL ?? "") + API.ScheduleExceptionsAPI + (details.dcsID ?? "")
        let sessionIDURL = API.SessionID + (details.sessionID ?? "")
           let scheduleExceptionsURL =  url + sessionIDURL
           print("the final schedule exception url is :\(scheduleExceptionsURL)")
           ServiceManager.shared.request(type: ScheduleExceptionListModel.self, url: scheduleExceptionsURL, method: .get, view: self.view) { (response) in
           print("The Schedule Exceptions api response is :\(String(describing: response))")
            
            DispatchQueue.main.async {
                self.ScheduleExceptionList = response
        
            }

        
           }
           
       }
    
}

//MARK: UICollectionView Delegate and Datasource Methods
extension ProfileDetailsController: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case 0:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "detailsTabCell", for: indexPath) as? DetailsTabCell else { return UICollectionViewCell() }
            
    let  addressString = "\(self.ProfileModelResponse?.street ?? ""), \(self.ProfileModelResponse?.city ?? ""), \(self.ProfileModelResponse?.state ?? ""), \(self.ProfileModelResponse?.zipCode ?? "")"
            
     cell.addressTextView.text = addressString
     cell.coordinatorId.text = self.ProfileModelResponse?.coordinator ?? ""
     cell.empIdLabel.text = self.ProfileModelResponse?.enterpriseId ?? ""
            
      
            
    let SplitPhoneThreeValue = self.ProfileModelResponse?.phone3 ?? ""
    let array = SplitPhoneThreeValue.components(separatedBy: ":")
    print("The splited phone 3 value :\(array)")
            
     cell.phoneTF.text = String(self.ProfileModelResponse?.phone3?.chopPrefix(8) ?? "")
     cell.workDetailsTF.text = String(self.ProfileModelResponse?.phone2?.chopPrefix(8) ?? "")
     cell.homeDetailsTF.text =  String(self.ProfileModelResponse?.phone1?.chopPrefix(5) ?? "")
     cell.emailTextfield.text = String(self.ProfileModelResponse?.email ?? "")
        cell.empIDViewObj.layer.cornerRadius = 5.0
        cell.empIDViewObj.layer.masksToBounds = false
        cell.addressTextView.layer.borderWidth = 1.0
        return cell
        case 1:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "availabilityTabCell", for: indexPath) as? AvailabilityTabCell else { return UICollectionViewCell() }
            return cell
        case 2:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "certificationsTabCell", for: indexPath) as? CertificationsTabCell else { return UICollectionViewCell() }
            return cell
        case 3:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "preferencesTabCell", for: indexPath) as? PreferencesTabCell else { return UICollectionViewCell() }
            return cell
        case 4:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "scheduleExceptionsTabCell", for: indexPath) as? ScheduleExceptionsTabCell else { return UICollectionViewCell() }
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: profileTabsCollectionView.frame.width, height: profileTabsCollectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

//MARK: UIView Controller Transistion Delegate
extension ProfileDetailsController : UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
           transiton.isPresenting = true
           return transiton
    }

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
           transiton.isPresenting = false
           return transiton
    }
}
