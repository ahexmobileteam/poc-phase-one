//
//  ScheduleListCell.swift
//  POC
//
//  Created by Admin on 09/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class ScheduleListCell: UITableViewCell {

    @IBOutlet weak var topLine: UIView!
    @IBOutlet weak var exceptionTypeLabel: UILabel!
    @IBOutlet weak var detailsStackView: UIStackView!
    @IBOutlet weak var expandAction: UIButton!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerDateLabel: UILabel!
    @IBOutlet weak var pinView: CustomView!
    @IBOutlet weak var titleView: CustomView!
    @IBOutlet weak var detailsView: UIView!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    var exceptions: ScheduleExceptinModel?{
        didSet{
            if let exception = self.exceptions?.exceptionType{
                self.exceptionTypeLabel.text = "\(exception)"
            }
            if let startDate = self.exceptions?.startDate{
                self.startDateLabel.text = startDate
                self.headerDateLabel.text = self.getConvertedDate(string: startDate)
            }
            if let endDate = self.exceptions?.endDate{
                self.endDateLabel.text = endDate
            }
            
            if let description = self.exceptions?.description{
                self.descriptionLabel.text = description
            }
            
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension UITableViewCell
{
    func getConvertedDate(string: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm a"
        let date = dateFormatter.date(from: string)
        dateFormatter.dateFormat = "d MMM YYYY"
        let convertedDate = dateFormatter.string(from: date!)
       return convertedDate
    }
}

extension NSObject
{
    func getConvertedDates(string: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm a"
        let date = dateFormatter.date(from: string)
        dateFormatter.dateFormat = "d MMM"
        let convertedDate = dateFormatter.string(from: date!)
       return convertedDate
    }
    
    
}
