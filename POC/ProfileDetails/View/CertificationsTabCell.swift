//
//  CertificationsTabCell.swift
//  POC
//
//  Created by Admin on 09/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit


class CertificationsTabCell: UICollectionViewCell {
    
    @IBOutlet weak var emptyStatusLabel: UILabel!
    private var certifications: CertificationsListModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.CertificationsAPICalling()
        self.certificationsTableView.tableFooterView = UIView()

    }
    
    @IBOutlet weak var cellCardViewObj: CustomView!
    @IBOutlet weak var certificationsTableView: UITableView!{
        didSet{
            self.certificationsTableView.delegate = self
            self.certificationsTableView.dataSource = self
        }
    }
    
    @objc func refreshCertifications() {
        self.CertificationsAPICalling()
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "refreshCertifications"), object: nil)
    }
    

    //MARK: Certifications API Calling
          func CertificationsAPICalling() {
              
            let details = self.getuserDetails()
              let url  =  (details.baseURL ?? "") + API.CertificationsURL + (details.dcsID ?? "")
              let sessionIDURL = API.SessionID + (details.sessionID ?? "")
              
              let certificateUrl = url + sessionIDURL
              print("final url of certificaion is :\(certificateUrl)")
              
            ServiceManager.shared.request(type: CertificationsListModel.self, url: certificateUrl , method: .get, view: self.contentView) {  (response) in
                  print("The certifications response is:\(String(describing: response))")
                self.certifications = response
    
                DispatchQueue.main.async {
                        
                    if self.certifications?.count ?? 0 == 0 {
                        self.emptyStatusLabel.isHidden = false
                        self.certificationsTableView.isHidden = true
                    }
                    else {
                        self.emptyStatusLabel.isHidden = true
                        self.certificationsTableView.isHidden = false

                    }
                    NotificationCenter.default.addObserver(self, selector: #selector(self.refreshCertifications), name: Notification.Name(rawValue: "refreshCertifications"), object: nil)
                    self.certificationsTableView.reloadData()
                }
              }
          }
}


extension CertificationsTabCell: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.certifications?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "certificationsListCell", for: indexPath) as? CertificationsListCell else {
            return UITableViewCell()
        }
        cell.certStatusLabel.text = self.certifications?[indexPath.row].certificationStatus
        cell.selectionStyle = .none
        
        if certifications?[indexPath.row].expiredColorFlag == 1 {
        cell.certificationNameLabel.text = self.certifications?[indexPath.row].certificationName
            cell.certificationNameLabel.textColor = .red
        }
        else  {
            cell.certificationNameLabel.textColor = .black
            cell.certificationNameLabel.text = "Expired"


        }
        
    cell.startDateAndEndDateLabel.text = "\(String(describing: self.certifications?[indexPath.row].aquiredDate ?? "")) - \(String(describing: self.certifications?[indexPath.row].expiryDate ?? ""))"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
