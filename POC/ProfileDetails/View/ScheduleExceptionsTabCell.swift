//
//  ScheduleExceptionsTabCell.swift
//  POC
//
//  Created by Admin on 09/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class ScheduleExceptionsTabCell: UICollectionViewCell {
    
    @IBOutlet weak var scheduleExceptionsTableView: UITableView!{
        didSet{
            self.scheduleExceptionsTableView.delegate = self
            self.scheduleExceptionsTableView.dataSource = self
            
        }
    }
    private var scheduleExcpetions: ScheduleExceptionListModel?
    private var expand = [Bool]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        scheduleExceptionsAPI()
    }
    func scheduleExceptionsAPI() {
        
        let details = self.getuserDetails()
        
        let url =  (details.baseURL ?? "") + API.ScheduleExceptionsAPI + (details.dcsID ?? "")
        let sessionIDURL = API.SessionID + (details.sessionID ?? "")
        let scheduleExceptionsURL =  url + sessionIDURL
        
        ServiceManager.shared.request(type: ScheduleExceptionListModel.self, url: scheduleExceptionsURL, method: .get, view: self.contentView) { [weak self](response) in
            if response?.count ?? 0 != 0 {
                self?.scheduleExcpetions = response
                for _ in response! {
                    self?.expand.append(false)
                }
                DispatchQueue.main.async {
                    self?.scheduleExceptionsTableView.reloadData()
                }
            }
        }
    }
    @objc func expandAction(sender: UIButton){
        expand[sender.tag] = !expand[sender.tag]
        self.scheduleExceptionsTableView.reloadData()
    }
    
}


//MARK: UITableView Delegate and Datasource Methods
extension ScheduleExceptionsTabCell: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scheduleExcpetions?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "scheduleListCell", for: indexPath) as? ScheduleListCell else {
            return UITableViewCell()
        }
        indexPath.row == 0 ? (cell.topLine.isHidden = true) : (cell.topLine.isHidden = false)
        cell.selectionStyle = .none
        cell.exceptions = self.scheduleExcpetions?[indexPath.row]
        let description = self.scheduleExcpetions?[indexPath.row].description ?? ""
        let height = description.height(withConstrainedWidth: cell.descriptionLabel.frame.width, font:  UIFont(name: "Avenir-Medium", size: 13.0)!)


        expand[indexPath.row] ? (cell.heightConstraint.constant = height + 70 ) : (cell.heightConstraint.constant = 0)
    
        cell.expandAction.tag = indexPath.row
        cell.expandAction.addTarget(self, action: #selector(expandAction(sender:)), for: .touchUpInside)
        if expand[indexPath.row]{
            cell.headerDateLabel.textColor = UIColor.greenColor
            cell.pinView.layer.borderColor = UIColor.greenColor.cgColor
            cell.titleView.backgroundColor = UIColor.greenColor
            cell.detailsView.backgroundColor = UIColor.greenColor
        }else{
            cell.headerDateLabel.textColor = .black
            cell.pinView.layer.borderColor = UIColor.lightGray.cgColor
            cell.titleView.backgroundColor = .groupTableViewBackground
            cell.detailsView.backgroundColor = .groupTableViewBackground
        }
        expand[indexPath.row] ? (cell.expandAction.setImage(UIImage(named: "ic_drop_up")?.withRenderingMode(.alwaysOriginal), for: .normal)) : (cell.expandAction.setImage(UIImage(named: "Dropdown")?.withRenderingMode(.alwaysOriginal), for: .normal))
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
