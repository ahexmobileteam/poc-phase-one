//
//  PreferencesTabCell.swift
//  POC
//
//  Created by Admin on 09/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class PreferencesTabCell: UICollectionViewCell {
    
    private var  PreferencesListResponse : PreferencesListModel?

    @IBOutlet weak var updatePreferencesAction: CustomButtonC!
    @IBOutlet weak var emptyStateLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.preferencesAPI()
    }
        
    @IBOutlet weak var preferencesTableView: UITableView!{
        didSet{
            self.preferencesTableView.delegate = self
            self.preferencesTableView.dataSource = self
            self.preferencesTableView.tableFooterView = UIView()
            self.preferencesTableView.layer.cornerRadius = 5
            self.preferencesTableView.layer.borderWidth = 1
            self.preferencesTableView.layer.borderColor = UIColor.lightGray.cgColor
            self.preferencesTableView.layer.masksToBounds = true
        }
    }
    
    
    //MARK: Preferences API Calling
    func preferencesAPI()  {
        
            let details = self.getuserDetails()
        
        let preferences =  (details.baseURL ?? "") + API.PreferencesAPI + (details.dcsID ?? "")
        let sessionIDURL = API.SessionID + (details.sessionID ?? "")
        let preferencesUrl = preferences + sessionIDURL
        print("The preferences url is :\(preferencesUrl)")
        
        ServiceManager.shared.request(type: PreferencesListModel.self, url: preferencesUrl, method: .get, view: self.contentView) { (response) in
        print("The prefernces response is :\(String(describing: response))")
        self.PreferencesListResponse = response
            
            if response?.count ?? 0 == 0 {
                DispatchQueue.main.async {
                    self.emptyStateLabel.isHidden = false
                    self.preferencesTableView.isHidden = true
                }
            }
            else {

                DispatchQueue.main.async {
                    self.emptyStateLabel.isHidden = true
                    self.preferencesTableView.isHidden = false
                    self.preferencesTableView.reloadData()
                }
            }
                      
        }
        
    }
    
}


//MARK: UITableView Delegate and Datasource Methods
extension PreferencesTabCell: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? self.PreferencesListResponse?.count ?? 0  : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "preferencesListCell", for: indexPath) as? PreferencesListCell else {
                return UITableViewCell()
            }
            cell.labelPreference.text = self.PreferencesListResponse?[indexPath.row].preference
            cell.selectionStyle = .none
            return cell
        }else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "updatePreferencesListCell", for: indexPath) as? UpdatePreferencesListCell else {
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0 ? UITableView.automaticDimension : 80
    }
}
