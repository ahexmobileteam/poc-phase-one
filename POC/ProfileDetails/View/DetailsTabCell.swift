
//
//  DetailsTabCell.swift
//  POC
//
//  Created by Admin on 09/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class DetailsTabCell: UICollectionViewCell {
    
    private var detailsResponse: ProfileModel?
    
    @IBOutlet weak var empIdLabel: UILabel!
    @IBOutlet weak var coordinatorId: UILabel!
    @IBOutlet weak var emailTextfield: CustomTField!
    @IBOutlet weak var homeDetailsTF: CustomTField!
    @IBOutlet weak var workDetailsTF: CustomTField!
    @IBOutlet weak var phoneTF: CustomTField!
    @IBOutlet weak var empIDViewObj: CustomView!
    
    
    @IBOutlet weak var addressTextView: UITextView!{
        didSet{
            self.addressTextView.delegate = self
            self.addressTextView.layer.borderWidth = 1
            self.addressTextView.layer.borderColor = UIColor.lightGray.cgColor
            self.addressTextView.layer.cornerRadius = 5.0
            self.addressTextView.layer.masksToBounds = false
        }
    }
    
    override func awakeFromNib() {
    super.awakeFromNib()
        
    }
    

    
}


//MARK: UITextView Delegate and Datasource Methods
extension DetailsTabCell: UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Enter address"{
            addressTextView.text = ""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == ""{
            addressTextView.text = "Enter address"
        }
    }
}

