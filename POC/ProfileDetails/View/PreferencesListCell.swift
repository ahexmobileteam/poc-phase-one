//
//  PreferencesListCell.swift
//  POC
//
//  Created by Admin on 09/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class PreferencesListCell: UITableViewCell {

    @IBOutlet weak var labelPreference: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
