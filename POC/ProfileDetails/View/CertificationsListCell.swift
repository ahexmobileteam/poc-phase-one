//
//  CertificationsListCell.swift
//  POC
//
//  Created by Admin on 09/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class CertificationsListCell: UITableViewCell {

    @IBOutlet weak var certificationNameLabel: UILabel!
    @IBOutlet weak var certStatusLabel: UILabel!
    @IBOutlet weak var startDateAndEndDateLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
