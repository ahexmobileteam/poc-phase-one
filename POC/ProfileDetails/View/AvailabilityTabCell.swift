//
//  AvailabilityTabCell.swift
//  POC
//
//  Created by Admin on 09/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class AvailabilityTabCell: UICollectionViewCell {
    @IBOutlet weak var availabilityListTableView: UITableView!{
        didSet{
            self.availabilityListTableView.delegate = self
            self.availabilityListTableView.dataSource = self
        }
    }
    
}

extension AvailabilityTabCell: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "availabilityCell", for: indexPath) as? AvailabilityCell else {
            return UITableViewCell()
        }
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
