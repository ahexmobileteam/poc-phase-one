//
//  UpdatePreferencesCell.swift
//  POC
//
//  Created by Ajeet N on 13/03/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class UpdatePreferencesCell: UITableViewCell {

    @IBOutlet weak var updatePreferencesButton: CustomButtonC!
    @IBOutlet weak var checkBoxOutlet: UIButton!
    @IBOutlet weak var updatePreferenceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
