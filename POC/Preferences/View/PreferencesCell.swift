//
//  PreferencesCell.swift
//  POC
//
//  Created by Ajeet N on 13/03/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class PreferencesCell: UITableViewCell {

    @IBOutlet weak var preferencesLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
