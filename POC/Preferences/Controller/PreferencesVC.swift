//
//  PreferencesVC.swift
//  POC
//
//  Created by Ajeet N on 13/03/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class PreferencesVC: UIViewController {
    
    @IBOutlet weak var preferencesTableViewObj: UITableView!
    var preferencesLabelArray  = ["Can operate Hoyer Lift", "Is CNA", "Is Allergic to Dogs", "Speaks English", "Speaks Spanish Language", "Works with Exotic Pets"]
        
    
    //MARK: View Did Load
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.configureNavigationBar(title: "Preferences")
        preferencesTableViewObj.layer.cornerRadius = 3.0
        preferencesTableViewObj.layer.borderColor = UIColor.lightGray.cgColor
        preferencesTableViewObj.layer.borderWidth = 1.0
        self.preferencesTableViewObj.tableFooterView = UIView()
        print("Preference view controller called !!")
    }
    
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.preferencesAPI()
    }
    
    //MARK: Preferences API Calling
    func preferencesAPI()  {
        
          let details = self.getuserDetails()
        
        let preferences = (details.baseURL ?? "") + API.PreferencesAPI + (details.dcsID ?? "")
        let sessionIDURL = API.SessionID + (details.sessionID ?? "")
        let preferencesUrl = preferences + sessionIDURL
        print("The preferences url is :\(preferencesUrl)")
        
        ServiceManager.shared.request(type: PreferencesListModel.self, url: preferencesUrl, method: .get, view: self.view) { (response) in
            print("The prefernces response is :\(String(describing: response))")
            
        }
        
    }
    
    //MARK: Side Menu Bar Button Item
    @IBAction func sidemenuBarButton(_ sender: UIBarButtonItem) {
    }
        
}

//MARK: UITableView Delegate and Datasource Methods
extension PreferencesVC : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? preferencesLabelArray.count : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PreferencesCell", for: indexPath) as! PreferencesCell
        cell.preferencesLabel.text = preferencesLabelArray[indexPath.row]
        return cell
        } else {
            let cell  = tableView.dequeueReusableCell(withIdentifier: "UpdatePreferencesCell", for: indexPath) as! UpdatePreferencesCell
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0 ? 65 : 95
    }
    
    
}
