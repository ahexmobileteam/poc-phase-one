//
//  UpdatePreferencesVC.swift
//  POC
//
//  Created by Ajeet N on 13/03/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class UpdatePreferencesVC: UIViewController {

    @IBOutlet weak var updatePreferenceTableViewObj: UITableView!
    
    var selectPreferenceArray = ["Can operate Hoyer lift", "Is CNA", "Is Allergic to Dogs", "Speaks English", "Speaks Spanish Language", "Works with exotic pets", "Speaks English"]
    
    
    //MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        updatePreferenceTableViewObj.tableFooterView = UIView()
    }

}

//MARK: UITableView Delegate and Datasource Methods
extension UpdatePreferencesVC : UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectPreferenceArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UpdatePreferencesCell", for: indexPath) as! UpdatePreferencesCell
        cell.updatePreferenceLabel.text  =  selectPreferenceArray[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Did Select row at indexpath")
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
           
           let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
           let label = UILabel()
           label.frame = CGRect.init(x: 15, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
           label.text = "Select Preferences"
           label.font = UIFont.init(name: "Poppins-SemiBold", size: 22)
           label.textColor = UIColor.black
           headerView.addSubview(label)
           return headerView
           
       }

    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
   
    
    
    
}
