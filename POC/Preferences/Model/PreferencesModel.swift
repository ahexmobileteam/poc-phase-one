//
//  PreferencesModel.swift
//  POC
//
//  Created by Ajeet N on 08/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import Foundation

import Foundation

// MARK: - WelcomeElement

typealias PreferencesListModel = [PreferencesModel]

struct PreferencesModel: Codable {
    let preference: String?
}
