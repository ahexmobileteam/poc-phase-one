//
//  AlertsController.swift
//  POC
//
//  Created by Admin on 17/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit
typealias UpdateAlertModel = [UpdateAlertResponseModel]
struct UpdateAlertResponseModel: Decodable {
    let errorCode: Int?
}

class AlertsController: UIViewController {
    
    var refreshControl = UIRefreshControl()

    @IBOutlet weak var emptyStateLabel: UILabel!
    @IBOutlet weak var alertsTableView: UITableView!{
        didSet{
            self.alertsTableView.delegate = self
            self.alertsTableView.dataSource = self
        }
    }
    
    private let transiton = SlideInTransition()
    private var alerts = [[AlertsResponseModel]]()
    private var getUserIDSessionModel: GetUserIDSessionModel?
    var gesture = UITapGestureRecognizer()
    
    
//MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        alertsTableView.sectionHeaderHeight = 0
        alertsTableView.sectionFooterHeight = 0
        alertsTableView.tableHeaderView = UIView(frame: CGRect(origin: .zero, size:
        CGSize(width: 0, height: CGFloat.leastNormalMagnitude)))
        alertsTableView.tableFooterView = UIView(frame: CGRect(origin: .zero, size:
        CGSize(width: 0, height: CGFloat.leastNormalMagnitude)))
        let searchLaerts = UIBarButtonItem(image: UIImage(named: "Search_01"), style: .plain, target: self, action: #selector(ssearchAlerts))
        self.navigationItem.rightBarButtonItem  = searchLaerts
        self.configureNavigationBar(title: "Alerts")
        getUserIDAndSessionID()
        
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        alertsTableView.addSubview(refreshControl) // not required when using UITableViewContr
        
    NotificationCenter.default.addObserver(self, selector: #selector(sessionExpired), name: Notification.Name("SessionExpired"), object: nil)
    }
    
    @objc func refresh(_ sender: AnyObject) {
          // Code to refresh table view
           refreshControl.endRefreshing()
        self.alerts.removeAll()
        self.getUserIDAndSessionID()
       }
    
    //MARK: Session Expired
    @objc func sessionExpired() {
           NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "SessionExpired"), object: nil)
           self.logout()
    }
    
    //MARK: Open Side Menu
    @IBAction func open_side_menu(_ sender: Any) {
        guard let menuViewController = self.storyboard?.instantiateViewController(withIdentifier: "MenuTableViewController")  as? MenuTableViewController else { return }
               menuViewController.modalPresentationStyle = .overCurrentContext
               menuViewController.transitioningDelegate = self
               menuViewController.identifyController = .isAlerts
        self.present(menuViewController, animated: true)
    }
    
    //MARK: Search Alerts
    @objc func ssearchAlerts(){
        guard let searchAlertsController = self.storyboard?.instantiateViewController(withIdentifier: "searchAlertsController")  as? SearchAlertsController else { return }
        searchAlertsController.delegate = self
        self.navigationController?.pushViewController(searchAlertsController, animated: true)
    }
    
//MARK: Get User Id and Session ID
    
    private func getUserIDAndSessionID(){
        let details = self.getuserDetails()
        let userName = defaults.value(forKey: kUserName) as? String
        let url = (details.baseURL ?? "") + "authenticateUserForDevices?&username=\(userName ?? "")&password=&deviceId=\(UIDevice.current.identifierForVendor?.uuidString ?? "")"
        ServiceManager.shared.request(type: GetUserIDSessionModel.self, url: url, method: .get, view: self.view) { [weak self](response) in
            if response?.count ?? 0 != 0{
                self?.getUserIDSessionModel = response
                if response?[0].userId != nil{
                    DispatchQueue.main.async {
                        self?.getAlerts(userID: response?[0].userId ?? 0, sessionID: response?[0].sessionId ?? "")
                    }
                }
            }
        }
    }
    private func updateAlert(flag: String,row: Int,section: Int){
        let userDetails = self.getuserDetails()
        var url = String()
        url.append(userDetails.baseURL ?? "")
        url.append("updateAlertStatus?&userId=\(getUserIDSessionModel?[0].userId ?? 0)")
        url.append("&ids=\(self.alerts[section][row].id ?? "")")
        url.append("&alertIds=\(self.alerts[section][row].alertId ?? "")")
        url.append("&aggregateAlertIds=\(self.alerts[section][row].aggregateAlertId ?? "")")
        url.append("&flag=\(flag)")
        url.append("&alertDefIds=\(self.alerts[section][row].alertDefId ?? 0)")
        url.append("&sessionId=\(getUserIDSessionModel?[0].sessionId ?? "")")
        ServiceManager.shared.request(type: UpdateAlertModel.self, url: url, method: .get, view: self.view) { [weak self](response) in
            if response?.count ?? 0 != 0{
                if response?[0].errorCode ?? 2 == 0{
                    DispatchQueue.main.async {
                        self?.alerts.removeAll()
                        self?.getUserIDAndSessionID()
                    }
                }else{
                    DispatchQueue.main.async {
                        self?.logout()
                    }
                }
            }
        }
        
    }
    
//MARK: Get Alerts
    private func getAlerts(userID: Int, sessionID: String){
        let date = Date()
        let start = Calendar.current.date(byAdding: .day, value: -3, to: date)
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let defaultStartDate = formatter.string(from: start!)
        let defaultEndDate = formatter.string(from: date)
        var url = String()
        let details = self.getuserDetails()
        url.append(details.baseURL ?? "")
        url.append("getAlertsForDevices?&userId=\(userID)")
        let startDate = defaults.value(forKey: kStartDate) as? String
        if startDate != nil{
            url.append("&startDate=\(startDate ?? "")")
        }else{
            url.append("&startDate=\(defaultStartDate)")
        }
        let endDate = defaults.value(forKey: kEndDate) as? String
        if endDate != nil{
            url.append("&endDate=\(endDate ?? "")")
        }else{
            url.append("&endDate=\(defaultEndDate)")
        }
        let searchBy = defaults.value(forKey: kSearchBy) as? String
        if searchBy != nil{
            url.append("&searchByKeyword=\(searchBy ?? "")")
        }else{
            url.append("&searchByKeyword=")
        }
        let applicationID = defaults.value(forKey: kApplicationID) as? Int
        if applicationID != nil{
            url.append("&viewByFlag=\(applicationID ?? 0)")
        }else{
            url.append("&viewByFlag=0")
        }
         url.append("&sessionId=\(sessionID)")
        print(url)
        ServiceManager.shared.request(type: AlertsModel.self, url: url, method: .get, view: self.view) { [weak self](response) in
            if response?.count ?? 0 != 0{
                DispatchQueue.main.async {
                    self?.sendPushNotoficationFCMToken()
                    self?.emptyStateLabel.isHidden = true
                    self?.alertsTableView.isHidden = false
                    self?.customiseAlerts(alertList: response!)
                }
            }
            else {
                DispatchQueue.main.async {
                    self?.emptyStateLabel.isHidden = false
                    self?.alertsTableView.isHidden = true
                }
            }
        }
      }
    private func customiseAlerts(alertList: AlertsModel){
          for value in alertList {
              if self.alerts.count == 0{
                  self.alerts.append([value])
              }else{
                if ((self.alerts[((self.alerts.count) - 1)][0].alertSendDate ?? "").prefix(10) == (value.alertSendDate ?? "").prefix(10)){
                      self.alerts[((self.alerts.count) - 1)].append(value)
                  }
                else{
                      self.alerts.append([value])
                  }
              }
          }
        self.alertsTableView.reloadData()
      }
    
    
    func sendPushNotoficationFCMToken() {
        
//http://poc.aquilasoftware.com/pocextacc-webservices_phase2/telephony/saveTokenDetails?&deviceTypeId=2&deviceId=43c7281b17850079&tokenId=dWobc2PUSlS7eQbIarnJ62%3AAPA91bEkRyaxHMf5y7zhpiQBRrHyqfFOfNseqkKJnL_Ie-3AmmCj4tlH0_dF55nKv0Ez1uSUUCKd40g3C-18uFNXQMvCAW-RaDSOG9Uq2LftUOT3xmShHg0WZUr-17hdDiKSliT5DNB2&sessionId=10947_JOP7JEZFV99MY1E1
        
        
        let details = self.getuserDetails()
        
        let fcmTokenObj = defaults.value(forKey: "FCMTOKENSAVED")
        
        let url = (details.baseURL ?? "") + API.SavePushNotificationToken
        let deviceId =  (API.DEVICEID) + (deviceID ?? "")
        let tokenId = API.TokenID + String(describing: fcmTokenObj ?? "")
        let sessionID = API.SessionID + (details.sessionID ?? "")

        let finalUrl = url + deviceId + tokenId + sessionID
        print("The final url of push notification is :\(String(describing: finalUrl))")
        
       // [{"returnStatus":1,"errorCode":0}]
        
        ServiceManager.shared.request(type: pushnotifyModelData.self, url: finalUrl, method: .get, view: self.view) { (response) in
            print("The reponse of push notification in alert vc is :\(String(describing: response))")
        
        }
        
    }
    
}


//MARK: UITableView Delegate and Datasource Methods
extension AlertsController: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return alerts.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return alerts[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "alertsSectionCell", for: indexPath) as? AlertsSectionCell else {
            return UITableViewCell()
        }
        cell.alert = self.alerts[indexPath.section][indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let header = tableView.dequeueReusableCell(withIdentifier: "alertsListHeaderCell") as? AlertsListHeaderCell else {
            return UIView()
        }
        header.selectionStyle = .none
        let id = defaults.value(forKey: kApplicationID) as? Int
        switch (id ?? 3) {
        case 1,2:
             header.headerTitleLabel.text = " \(self.alerts[section][0].groupBy ?? "") "
            break
        default:
            let date = self.alerts[section][0].alertSendDate ?? ""
            header.headerTitleLabel.text = " \(self.getConvertedDates(string: date)) "
        }
       
        
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
            return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        if ((self.alerts[indexPath.section][indexPath.row].alertDefId ?? 0) == 13) && ((self.alerts[indexPath.section][indexPath.row].messageTypeId ?? 0) == 1){
                let decline = UITableViewRowAction(style: .default, title: "Decline") { (action, indexPath) in
                    DispatchQueue.main.async {
                        self.alert(title: "Are you sure you want to decline selected alert?", message: "", actionTitles: ["NO","YES"], actionStyle: [.default,.default], action: [
                            { no in
                                
                            },{ yes in
                                DispatchQueue.main.async {
                                    self.updateAlert(flag: "1", row: indexPath.row, section: indexPath.section)
                                }
                            }
                        ])
                    }
                }
                decline.backgroundColor = .red
                let accept = UITableViewRowAction(style: .default, title: "Accept") { (action, indexPath) in
                    DispatchQueue.main.async {
                                         self.alert(title: "Are you sure you want to accept selected alert?", message: "", actionTitles: ["NO","YES"], actionStyle: [.default,.default], action: [
                                             { no in
                                                 
                                             },{ yes in
                                                 DispatchQueue.main.async {
                                                     self.updateAlert(flag: "2", row: indexPath.row, section: indexPath.section)
                                                 }
                                             }
                                         ])
                                     }
                }
                accept.backgroundColor = .blue
        return [decline,accept]
        }else{
            let dismiss = UITableViewRowAction(style: .default, title: "Dismiss") { (action, indexPath) in
                DispatchQueue.main.async {
                self.alert(title: "Are you sure you want to dismiss selected alert?", message: "", actionTitles: ["NO","YES"], actionStyle: [.default,.default], action: [
                                         { no in
                                             
                                         },{ yes in
                                             DispatchQueue.main.async {
                                                 self.updateAlert(flag: "1", row: indexPath.row, section: indexPath.section)
                                             }
                                         }
                                     ])
                                 }
            }
            dismiss.backgroundColor = .red
            return [dismiss]
        }
    }

}

//MARK: UIView Controller Transistion Delegate
extension AlertsController : UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
           transiton.isPresenting = true
           return transiton
       }

       func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
           transiton.isPresenting = false
           return transiton
       }
}

extension AlertsController: SearchAlertListDelegate{
    func searchAlert() {
        self.alerts.removeAll()
        self.getUserIDAndSessionID()
    }
}
