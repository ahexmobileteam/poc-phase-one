//
//  AlertsModel.swift
//  POC
//
//  Created by Admin on 17/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import Foundation

typealias AlertsModel = [AlertsResponseModel]

struct AlertsResponseModel: Decodable {
    let alertSendDate: String?
    let aggregateAlertId: String?
    let subject: String?
    let errorCode: Int?
    let id: String?
    let alertId: String?
    let alertDefId: Int?
    let groupBy: String?
    let applicationId: Int?
    let message: String?
    let messageTypeId: Int?
    let userId: Int?
    let status: Int?
}
