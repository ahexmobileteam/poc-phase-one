//
//  GetSessionAndUserIDModel.swift
//  POC
//
//  Created by Admin on 17/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import Foundation
typealias GetUserIDSessionModel = [GetUserIDResponseModel]

struct GetUserIDResponseModel: Decodable {
    let userTypeId: Int?
    let fname: String?
    let lname: String?
    let flag: Int?
    let active: Int?
    let id: Int?
    let sessionId: String?
    let userId: Int?
    let emailNotifFlag: Int?
    let applicationIds: String?
}
