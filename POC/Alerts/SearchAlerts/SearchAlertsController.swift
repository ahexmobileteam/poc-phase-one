//
//  SearchAlertsController.swift
//  POC
//
//  Created by Admin on 20/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit
struct ApplicationIDModel {
    let id: Int
    let title: String
}
protocol SearchAlertListDelegate {
    func searchAlert()
}

class SearchAlertsController: UIViewController {
    @IBOutlet weak var searchByTF: CustomTField!
    @IBOutlet weak var applicationIDTF: DropDownTextField!
    @IBOutlet weak var endDateTF: CustomTField!
    @IBOutlet weak var startDateTF: CustomTField!
    private var applicationIds: [ApplicationIDModel] = [
              .init(id: 0, title: "Select Alert Type"),
              .init(id: 1, title: "ALERT DEFINITION"),
              .init(id: 2, title: "APPLICATION")
    ]
    var delegate: SearchAlertListDelegate?
    
    private var applicationID: Int?
    private var applicationIDName: String?

    private let startDatePicker = UIDatePicker()
    private let endDatePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Search Alerts"
        loadStartAndEndDatePickers()
        
        
        let start = Calendar.current.date(byAdding: .day, value: -3, to: Date())
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        
       
        let defaultStartDate = formatter.string(from: start!)
        let defaultEndDate = formatter.string(from: Date())
        
    
    
        let startDate = defaults.value(forKey: kStartDate) as? String
        if startDate != nil {
            startDateTF.text = startDate
        }
        else {
            startDateTF.text = defaultStartDate
        }
        
        let endDate = defaults.value(forKey: kEndDate) as? String
        if endDate != nil {
            endDateTF.text = endDate
        }
        else {
            endDateTF.text = defaultEndDate
        }
        
        
        let searchKey = defaults.value(forKey: kSearchBy) as? String
        if searchKey != nil {
            searchByTF.text = searchKey
        }
        
        
        let id = defaults.value(forKey: kApplicationID) as? Int
        if id != nil {
            let name = defaults.value(forKey: kApplicationIDName) as? String
            applicationIDName = name ?? ""
            applicationIDTF.text = name ?? ""
            applicationID = id ?? 0
        }
        
        self.applicationIDTF.layer.borderColor = UIColor.greenColor.cgColor
        applicationIDTF.dropDown.dataSource = applicationIds.map({ $0.title })
        applicationIDTF.dropDown.direction = .any
        applicationIDTF.dropDown.selectionAction = { (index, item) in
            if index == 0{
                defaults.removeObject(forKey: kApplicationID)
                defaults.removeObject(forKey: kApplicationIDName)
                self.applicationID = nil
                self.applicationIDName = nil
                
            }else{
                self.applicationID = self.applicationIds[index].id
                self.applicationIDName = self.applicationIds[index].title
            }
            defaults.synchronize()
            self.applicationIDTF.text = item
            self.applicationIDTF.resignFirstResponder()
        }
        let reset = UIBarButtonItem(title: "Reset", style: .plain, target: self, action: #selector(resetAction))
        self.navigationItem.rightBarButtonItem = reset
    NotificationCenter.default.addObserver(self, selector: #selector(sessionExpired), name: Notification.Name("SessionExpired"), object: nil)
    }
    
    
    //MARK: Session Expired
      @objc func sessionExpired() {
             NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "SessionExpired"), object: nil)
             self.logout()
      }
    @objc func resetAction(){
        self.alert(title: "Are you sure you want to reset alerts search?", message: "", actionTitles: ["Ok","Yes"], actionStyle: [.default,.default], action: [
            { ok in
                
            },{ yes in
                defaults.removeObject(forKey: kApplicationID)
                defaults.removeObject(forKey: kApplicationIDName)
                defaults.removeObject(forKey: kSearchBy)
                defaults.removeObject(forKey: kStartDate)
                defaults.removeObject(forKey: kEndDate)
                defaults.synchronize()
                self.searchByTF.text = ""
                self.applicationIDTF.text = ""
//                self.startDateTF.text = ""
//                self.endDateTF.text = ""
                self.delegate?.searchAlert()
                self.toast(msgString: "Alerts search reset successfully.", view: self.view)
                
                let start = Calendar.current.date(byAdding: .day, value: -3, to: Date())
                       let formatter = DateFormatter()
                       formatter.dateFormat = "MM/dd/yyyy"
                       
                      
                       let defaultStartDate = formatter.string(from: start!)
                       let defaultEndDate = formatter.string(from: Date())
                       
                
                self.startDateTF.text = defaultStartDate
                self.endDateTF.text = defaultEndDate
                
                
                
                
            }
        ])
    }
    
    @IBAction func search_action(_ sender: Any) {
        if applicationID != nil{
            defaults.setValue(applicationID, forKey: kApplicationID)
            defaults.setValue(applicationIDName, forKey: kApplicationIDName)
        }
        if !searchByTF.text!.isEmpty{
            defaults.setValue(searchByTF.text ?? "" , forKey: kSearchBy)
        }
        if !startDateTF.text!.isEmpty{
            defaults.setValue(startDateTF.text ?? "", forKey: kStartDate)
        }
        if !endDateTF.text!.isEmpty{
            defaults.setValue(endDateTF.text ?? "", forKey: kEndDate)
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        guard let checkStartDate = formatter.date(from: startDateTF.text ?? "") else { return }
        guard let checkEndDate = formatter.date(from: endDateTF.text ?? "") else { return }
        switch checkStartDate.compare(checkEndDate) {
        case .orderedAscending:
                print("Start date is earlier than end date")
        case .orderedDescending:
            self.alert(title: "Alert Message", message: "Start date cannot be greater than end date")
            return
//        case .orderedSame:
//            print("Start date and end date are same")
//            self.alert(title: "Alert Message", message: "Start date and end date are same ")
//            return
        default:
            print("call API")

        }
        delegate?.searchAlert()
        self.navigationController?.popViewController(animated: true)
    }
    
    private func loadStartAndEndDatePickers(){
           let date = Date()
           let start = Calendar.current.date(byAdding: .day, value: -3, to: date)
           let formatter = DateFormatter()
           formatter.dateFormat = "MM/dd/yyyy"
           let defaultStartDate = formatter.string(from: start!)
           let defaultEndDate = formatter.string(from: date)
           startDatePicker.datePickerMode = .date
           let startDate = defaults.value(forKey: kStartDate)
           if startDate != nil{
               self.setCustomDate(datePicker: startDatePicker, date: String(describing: startDate ?? ""),textField: startDateTF)
           }else{
               self.setCustomDate(datePicker: startDatePicker, date: defaultStartDate, textField: startDateTF)
               defaults.set(defaultStartDate, forKey: kStartDate)
           }
           let startDateToolbar = UIToolbar()
             startDateToolbar.sizeToFit()
           let startDoneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(startDateDoneAction))
            let startSpaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            let startCancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelAction))
            startDateToolbar.setItems([startCancelButton,startSpaceButton,startDoneButton], animated: false)
            startDateTF.inputAccessoryView = startDateToolbar
            startDateTF.inputView = startDatePicker
           
           
            endDatePicker.datePickerMode = .date
           let endDate = defaults.value(forKey: kEndDate)
           if endDate != nil{
               self.setCustomDate(datePicker: endDatePicker, date: String(describing: endDate ?? ""), textField: endDateTF)
           }else{
               self.setCustomDate(datePicker: endDatePicker, date: defaultEndDate, textField: endDateTF)
               defaults.set(defaultEndDate, forKey: kEndDate)
           }
            let endDateToolbar = UIToolbar();
             endDateToolbar.sizeToFit()
            let endDoneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(endDateDoneAction));
            let endSpaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            let endCancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelAction));
            endDateToolbar.setItems([endCancelButton,endSpaceButton,endDoneButton], animated: false)
            endDateTF.inputAccessoryView = endDateToolbar
            endDateTF.inputView = endDatePicker
       }
       
       @objc func startDateDoneAction(){
           let formatter = DateFormatter()
           formatter.dateFormat = "MM/dd/yyyy"
           let selectedDate = formatter.string(from: startDatePicker.date)
           startDateTF.text = selectedDate
           defaults.set(selectedDate, forKey: kStartDate)
           startDateTF.layer.borderColor = UIColor.greenColor.cgColor
           self.view.endEditing(true)
       }
       
       @objc func endDateDoneAction(){
           let formatter = DateFormatter()
           formatter.dateFormat = "MM/dd/yyyy"
           let selectedDate = formatter.string(from: endDatePicker.date)
           endDateTF.text = selectedDate
           defaults.set(selectedDate, forKey: kEndDate)
           endDateTF.layer.borderColor = UIColor.greenColor.cgColor
           self.view.endEditing(true)
       }

       @objc func cancelAction(){
          self.view.endEditing(true)
        }

}
extension UIViewController
{
    func setCustomDate(datePicker: UIDatePicker,date: String,textField: CustomTField){
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let finalDate = formatter.date(from: String(describing: date))
        datePicker.setDate(finalDate!, animated: false)
        textField.text = formatter.string(from: finalDate!)
    }
}
