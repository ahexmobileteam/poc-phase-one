//
//  PushNotifyModel.swift
//  POC
//
//  Created by Ajeet N on 13/05/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import Foundation


typealias pushnotifyModelData = [PushNotifyModel]

struct PushNotifyModel : Codable {
    let returnStatus: Int?
    let errorCode : Int?
}

// [{"returnStatus":1,"errorCode":0}]
