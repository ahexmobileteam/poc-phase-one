//
//  AlertsListHeaderCell.swift
//  POC
//
//  Created by Admin on 17/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class AlertsListHeaderCell: UITableViewCell {
    @IBOutlet weak var headerTitleLabel: UILabel!{
        didSet{
            self.headerTitleLabel.layer.cornerRadius = 12
            self.headerTitleLabel.layer.masksToBounds = true
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
