//
//  AlertsSectionCell.swift
//  POC
//
//  Created by Admin on 17/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class AlertsSectionCell: UITableViewCell {

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var subjectLabel: UILabel!
    
    var alert: AlertsResponseModel?{
        didSet{
            if let time = self.alert?.alertSendDate{
                self.timeLabel.text = String(time.suffix(8))
            }
            if let subject = self.alert?.message{
                self.subjectLabel.text = subject
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
