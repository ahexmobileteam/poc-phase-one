//
//  MoveToNextTransaction.swift
//  POC
//
//  Created by Ajeet N on 02/05/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit


class MoveToNextTransaction: UIViewController {

    @IBOutlet weak var imageViewObj: UIImageView!
    
    @IBOutlet weak var descriptionLable: UILabel!
    private var isShowSchedule = true
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "POC Visit Verification"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.setHidesBackButton(true, animated: true);
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        if isShowSchedule{
            guard let schedule = storyboard?.instantiateViewController(withIdentifier: "scheduleExceptionsVC") as? ScheduleExceptionsVC else {
                       return
                   }
                   let navigation = UINavigationController(rootViewController: schedule)
                   navigation.modalPresentationStyle = .fullScreen
                   self.present(navigation, animated: false, completion: nil)
        }

    }
    
    @IBAction func nextButtonActoin(_ sender: Any) {
        isShowSchedule = false
        guard let schedule = storyboard?.instantiateViewController(withIdentifier: "scheduleExceptionsVC") as? ScheduleExceptionsVC else {
            return
        }
        let navigation = UINavigationController(rootViewController: schedule)
        navigation.modalPresentationStyle = .fullScreen
        self.present(navigation, animated: false, completion: nil)
    }
    

}
