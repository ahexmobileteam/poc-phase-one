//
//  ReportAbsentVC.swift
//  POC
//
//  Created by Ajeet N on 01/05/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//


import UIKit
import CoreLocation

enum ReportAbsentOptions {
    case pscode
    case serviceCode
}



class ReportAbsentVC: UIViewController {
    
    private var isSearching = false
    private var isPstextField = false

    private var checkValue : String?
    private var checkServiceValue: String?
    
    
    @IBOutlet weak var alertTransparentView: UIView!
    
    @IBOutlet weak var alertCardViewObj: CustomView!
    @IBOutlet weak var alertFirstLabel: UILabel!
    @IBOutlet weak var alertLocationLabel: UILabel!
    
    @IBOutlet weak var alertDateLabel: UILabel!
    @IBOutlet weak var alertTimeLabel: UILabel!
    
    var   receivedTempOfficesIdStoresss : Int?
   
    var tempPSID : String?
    var tempSeviceiD: Int?
    
    private var idString: String? = nil
    let cellReuseIdentifier = "cell"
    
    private var  reportAbsentModelData  : ReportAbsentModel?
    
    private var  reportOptionsObj : ReportAbsentOptions? = .pscode
    private var ReportOfficesResponseData : reportPSModelResponse?
    private var searchPSResults: reportPSModelResponse?

    
    private var ReportServiceCodeResponseData: ReportServiceCodeModel?
//    private var searchServiceResults: reportPSModelResponse?
    private var searchServiceResults: [PSAuthServiceDetail]?

    
    @IBOutlet weak var psSearchTextFieldObj: CustomTField!
    var reterivedOfficeIDFromOffcieScren: Int?

    
    var addressString : String = ""
    private let locationManager = CLLocationManager()

    @IBOutlet weak var transperentView: UIView!
    @IBOutlet weak var psCodetF: CustomTField!
    @IBOutlet weak var psSearchButton: UIButton!
    
    @IBOutlet weak var serviceCodeTF: CustomTField!
    @IBOutlet weak var serviceCodeSearchButton: UIButton!
    
    @IBOutlet weak var tokenTF: CustomTField!
    @IBOutlet weak var cardViewObj: UIView!
    
    @IBOutlet weak var psTableViewObj: UITableView!
    
    var seletctedOfficeID: Int?
    
    
//MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "POC Visit Verification"
        checkLocationServices()
        
        self.cardViewObj.layer.cornerRadius = 5.0
        self.cardViewObj.layer.masksToBounds = false
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 18)!,NSAttributedString.Key.foregroundColor : UIColor.white]
        
        psTableViewObj.delegate = self
        psTableViewObj.dataSource = self
        psTableViewObj.tableFooterView = UIView()

        psCodetF.delegate = self
        serviceCodeTF.delegate = self
        
        DispatchQueue.main.async {
            self.PSCodeAPI()
            }
        
        self.transperentView.isHidden = true
        self.alertTransparentView.isHidden = true

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tapGesture.delegate = self
        transperentView.addGestureRecognizer(tapGesture)
        tokenTF.isHidden = true
        
       seletctedOfficeID  = defaults.value(forKey: "REPORTOFFICEID") as? Int ?? 0
        print("The received office id in report absent screen is :\(String(describing: seletctedOfficeID))")

        NotificationCenter.default.addObserver(self, selector: #selector(sessionExpired), name: Notification.Name("SessionExpired"), object: nil)
   
}
    

//MARK: Session Expired
        @objc func sessionExpired() {
               NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "SessionExpired"), object: nil)
               self.logout()
        }
    
//MARK: Tap Gesture Action
       @objc func handleTap(){
           transperentView.isHidden = true
    }
    
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        
        self.transperentView.isHidden = true
        self.psSearchTextFieldObj.delegate = self

    }
    
    
    //MARK: PS Code API Calling
        func PSCodeAPI() {
            
        let details = self.getuserDetails()
            
        let pscode = (details.baseURL ?? "") + API.getListOFPSListByOfficeID + String(describing: receivedTempOfficesIdStoresss ?? 0 )
            let sessionID = API.SessionID + (details.sessionID ?? "")
            
            let psOfficesUrl = pscode + sessionID
            print("The ps office url is report details vc  :\(psOfficesUrl)")
            
        ServiceManager.shared.request(type: reportPSModelResponse.self, url: psOfficesUrl, method: .get, view: self.view) { (response) in
                print("The response of ps offices list is :\(String(describing: response))")
                
                if response?.count ?? 0 != 0 {
                    DispatchQueue.main.async {
                        self.ReportServiceCodeResponseData?.PSAuthServiceDetails?.removeAll()
                        self.ReportOfficesResponseData = response
                        self.alertTransparentView.layer.cornerRadius = 5.0
                        self.alertTransparentView.layer.masksToBounds = true
                        self.psTableViewObj.reloadData()
                    }
                    
                }
                else {
                    self.alert(title: "Alert Message", message: "No Records Found")
                    self.alertTransparentView.isHidden = true
                        
                    }
                }
            }

    
     //MARK: Service Code API Calling
        func ServiceCodeAPI()  {
            
            let details = self.getuserDetails()
            
            let staticPSId = idString ?? ""
            let url = (details.baseURL ?? "")  + API.GETPSOTPANDSERVICEDETAILS + staticPSId
            let officeId = API.offFiled + String(describing: receivedTempOfficesIdStoresss ?? 0)
            let sessionID = API.SessionID + (details.sessionID ?? "")
            let finalurl = url + officeId + sessionID
            
            print("the service code final url is :\(String(describing: finalurl))")
            
            ServiceManager.shared.request(type: ReportServiceCodeModel.self, url: finalurl, method: .get, view: self.view) { (response) in
                print("The response of Service code is :\(String(describing: response))")
                
                if response?.PSAuthServiceDetails?.count ?? 0 != 0 {

                DispatchQueue.main.async {
                      self.ReportServiceCodeResponseData?.PSAuthServiceDetails?.removeAll()
                      self.ReportServiceCodeResponseData = response
                      self.psTableViewObj.reloadData()
                      print("The response of Service code is main :\(String(describing: response))")
                      self.psTableViewObj.reloadData()

                    }
                
            }
            else {
                self.alert(title: "Alert Message", message: "No Data Found")
            }
        }
    }
    
    
    //MARK: PSCode API Calling
    @IBAction func psSearchButtonAction(_ sender: Any) {
        
         isSearching = false
        isPstextField = true
        transperentView.isHidden = false
        self.transperentView.layer.cornerRadius = 5.0
        self.transperentView.layer.masksToBounds = true
        
        reportOptionsObj = .pscode
        psTableViewObj.reloadData()
        self.PSCodeAPI()
        
    }
    
    
    //MARK: ServiceCode API Calling
    @IBAction func serviceCodeButtonAction(_ sender: Any) {
        
        if psCodetF.text!.isEmpty {
           self.toast(msgString: "Please select PS Code", view: self.view)
           return
        }
         isSearching = false
        isPstextField = false
        self.psSearchTextFieldObj.text = ""
        transperentView.isHidden = false
        self.transperentView.layer.cornerRadius = 5.0
        self.transperentView.layer.masksToBounds = true

        reportOptionsObj = .serviceCode
        psTableViewObj.reloadData()
        self.ServiceCodeAPI()
        
    }
    
    
    //MARK: Get  User Current Location Services
    func checkLocationServices(){
        
        if CLLocationManager.locationServicesEnabled(){
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            self.checkLocationAuthorization()
            self.locationManager.startUpdatingLocation()
        }else{
            self.alert(title: "Please enable the location permissions in order to get user current location", message: "", actionTitles: ["OK"], actionStyle: [.default], action: [
                { ok in
                    
                }
            ])
        }
    }
    
    
    //MARK: Location Authorization
    func checkLocationAuthorization(){
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
            break
        case .denied:
            self.alert(title: "Location services are denied, Please enable it in order to do further action", message: "", actionTitles: ["OK"], actionStyle: [.default], action: [
                { ok in
                    
                }
            ])
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        default:break
        }
    }
    
   
    
//MARK: Converting User Latitude and Longitude into Physical Address
    func getAddressFromLatLons(pdblLatitude: String, withLongitude pdblLongitude: String) {
        
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)") ?? 0.0
        let lon: Double = Double("\(pdblLongitude)") ?? 0.0
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    if pm.subLocality != nil {
                        self.addressString = self.addressString + pm.subLocality! + ", "
                        print("The address string is :\(self.addressString)")
                    }
                    if pm.thoroughfare != nil {
                        self.addressString = self.addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        self.addressString = self.addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        self.addressString = self.addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        self.addressString = self.addressString + pm.postalCode! + " "
                    }
                    DispatchQueue.main.async {
                        defaults.set(self.addressString, forKey: "REPORTAbsentUSERADDRESS")
                        
                    }
                }
        })
    }
    
    
    //MARK: Save Button Action
    @IBAction func saveButtonAction(_ sender: Any) {
        
//http://poc.aquilasoftware.com/pocextacc-webservices_new/telephony/validateSingleTransaction?imei=&dcsEmpCode=180837&psMRN=128634&officeId=191&telcode=36&mileage=0.0&travelTime=0&taskCodes=&transactionType=ARRIVAL&latitude=37.4219983&longitude=-122.084&address=1600%20Amphitheatre%20Pkwy%2C%20Mountain%20View%2C%20CA%2094043%2C%20USA%0A&transactionDate=&ipAddress=&sessionId=180837_J0L8U7Z10YOFWJVE
                
//    let pscode = defaults.value(forKey: "REPORTABSENTSELECTEDPSCODE")
//    print("the selected ps code in resport absent screen is :\(String(describing: pscode))")
        
//    let serviceCode = defaults.value(forKey: "REPORTABSENTSELECTEDSERVICECODE")
//    print("the selected service code in report absent screen is :\(String(describing: serviceCode))")
        
        if psCodetF.text!.isEmpty {
            self.alert(title: "Alert Message", message: "Please enter PS Code ")
            return
        }
                   
        if serviceCodeTF.text!.isEmpty  {
            self.alert(title: "Alert Message", message: " Please enter service code")
            return
        }
    
                
        let staticTransacationType = "ABSENT"
        
        let latitude = defaults.value(forKey: "ReportAbsentLatitude") ?? 0.0
                print("The latitude in report unscheduled is :\(String(describing: latitude))")
                
        let longitude = defaults.value(forKey: "ReportAbsentLongitude") ?? 0.0
                print("The longitude in report unscheduled is :\(String(describing: longitude))")
                
        self.getAddressFromLatLons(pdblLatitude: String(describing: latitude), withLongitude: String(describing: longitude))
                
        let userAddress = defaults.value(forKey: "REPORTAbsentUSERADDRESS") ?? ""
        print("The user address we have got in report absent screen is  :\(String(describing: userAddress))")
                
       // let dcsEmpCode = defaults.value(forKey: "GLOBALDCSEMPCODE")
        let Staticmileage  = 0.0
        let staticTravelTime = 1
        
        let details = self.getuserDetails()
        
        let validateUrl = (details.baseURL ?? "") + API.ValidateSingleTransacation + String(describing: details.empID ?? "")
                let psmrn = API.PSMRN + String(describing: tempPSID ?? "0")
                let officeId = API.officeID + String(describing: receivedTempOfficesIdStoresss ?? 0)
                let telecode = API.TeleCode + String(describing: tempSeviceiD ?? 0)
                let mileage = API.Mileage + String(describing: Staticmileage)
                let travelTime = API.TravelTime + String(describing: staticTravelTime)
                let tasksCode = API.TasksCode
        let transactionType = API.TransactionType + String(describing: staticTransacationType )
                let Reportlatitude = API.latitude + String(describing: latitude)
                let Reportlongitude = API.longitude + String(describing: longitude)
        let address = API.address + String (describing: userAddress).addingPercentEncoding(withAllowedCharacters: NSCharacterSet.alphanumerics)!
                let transactionDate = API.TransacationDate
                let ipAddress = API.ipAddress
                let sessionId = API.SessionID + (details.sessionID ?? "")
                
        let finalUrl = validateUrl + psmrn + officeId + telecode + mileage + travelTime + tasksCode + transactionType + Reportlatitude + Reportlongitude + address + transactionDate + ipAddress + sessionId
        print("The final url of save button is :\(finalUrl)")
        
    ServiceManager.shared.request(type: ReportAbsentModel.self, url: finalUrl, method: .get, view: self.view) { (response) in
            print("The response of report absent view controller is :\(String(describing: response))")
            
            if response?.validateFlag == nil {
                
                DispatchQueue.main.async {
                    self.reportAbsentModelData = response
                    self.alertTransparentView.isHidden = false
                    if let result = response?.result{
                    let resultDateAndTime = result.suffix(19)
                    self.alertFirstLabel.text = "Absent created successfully at"
                    self.alertDateLabel.text = String(resultDateAndTime.prefix(10))
                      self.alertTimeLabel.text = String(resultDateAndTime.suffix(8))
                                       }
                               }
            }
            else {
                self.alert(title: "Alert Message", message: response?.result ?? "")
            }
        }
    }
    
    
    //MARK: Alert Ok Button Action
    @IBAction func alertButtonAction(_ sender: Any) {
           self.alertTransparentView.isHidden = true
        guard let emptyStatevC = self.storyboard?.instantiateViewController(withIdentifier: "MoveToNextTransaction") as? MoveToNextTransaction else {return}
        self.navigationController?.pushViewController(emptyStatevC, animated: true)

       }
    
}

//MARK: UITableView Delegate and DataSource methods
extension ReportAbsentVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         
        switch reportOptionsObj {
        case .pscode:
            if isSearching {
                return searchPSResults?.count ?? 0
            }
          return self.ReportOfficesResponseData?.count ?? 0
            
            
            
            
        case .serviceCode:
            if isSearching {
                return searchServiceResults?.count ?? 0
            }
            return self.ReportServiceCodeResponseData?.PSAuthServiceDetails?.count ?? 0

        default:
        return 0
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier:cellReuseIdentifier) else {return UITableViewCell() }
        
        switch reportOptionsObj {
        case .pscode:
            cell.textLabel?.textColor = UIColor.init(red: 20/255.0, green:81/255.0 , blue: 148/255.0, alpha: 1.0)
            cell.textLabel?.font = UIFont(name: "Avenir-Roman", size: 18.0)
            
            if isSearching {
                 cell.textLabel?.text = self.searchPSResults?[indexPath.row].psName ?? ""
                     }
                         
            else {
                 cell.textLabel?.text = self.ReportOfficesResponseData?[indexPath.row].psName ?? ""
            }
            
            break
            
        case .serviceCode:
        cell.textLabel?.text = self.ReportServiceCodeResponseData?.PSAuthServiceDetails?[indexPath.row].serviceName ?? ""
        cell.textLabel?.textColor = UIColor.init(red: 20/255.0, green:81/255.0 , blue: 148/255.0, alpha: 1.0)
        cell.textLabel?.font = UIFont(name: "Avenir-Roman", size: 18.0)
        
             if isSearching {
        cell.textLabel?.text = self.searchServiceResults?[indexPath.row].serviceName ?? ""
                    
        }

        else {
        cell.textLabel?.text = self.ReportServiceCodeResponseData?.PSAuthServiceDetails?[indexPath.row].serviceName ?? ""
        }
                
            break
        default:
           return UITableViewCell()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch reportOptionsObj {
        case .pscode:
            
        if isSearching {
             idString =  String(self.searchPSResults?[indexPath.row].psId ?? "0" )
             psCodetF.text = self.searchPSResults?[indexPath.row].psName
             tempPSID = self.searchPSResults?[indexPath.row].psCode ?? "0"
             self.transperentView.isHidden = true
                     }
        else {
             idString =  String(self.ReportOfficesResponseData?[indexPath.row].psId ?? "0" )
             psCodetF.text = self.ReportOfficesResponseData?[indexPath.row].psName
             tempPSID = self.ReportOfficesResponseData?[indexPath.row].psCode ?? "0"
            self.transperentView.isHidden = true
            
            }
            
        break

        case .serviceCode:
            
            
            if isSearching {
                           
            serviceCodeTF.text = self.searchServiceResults?[indexPath.row].serviceName ?? ""
            tempSeviceiD = self.searchServiceResults?[indexPath.row].serviceCode ?? 0
            transperentView.isHidden = true
            }
            else {
                           
           serviceCodeTF.text = self.ReportServiceCodeResponseData?.PSAuthServiceDetails?[indexPath.row].serviceName ?? ""
           tempSeviceiD = self.ReportServiceCodeResponseData?.PSAuthServiceDetails?[indexPath.row].serviceCode ?? 0
           transperentView.isHidden = true
                           
                }

        break
        default:
        break
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 35
    }
    
}


//MARK: UIGesture Recognizer Delegate Methods
extension ReportAbsentVC : UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view!.isDescendant(of: psTableViewObj) {
            return false
        }
        return true
    }
}

//MARK: Corelocation Delegate Methods
extension ReportAbsentVC: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        let lat = location.coordinate.latitude
        let lon = location.coordinate.longitude
        defaults.set(lat, forKey: "ReportAbsentLatitude")
        defaults.set(lon, forKey: "ReportAbsentLongitude")
        defaults.synchronize()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkLocationAuthorization()
    }
    
}

extension ReportAbsentVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
           
              if textField == psSearchTextFieldObj {
                  if psSearchTextFieldObj.text?.count ?? 0 > 1 {
                      self.isSearching = true
                     if isPstextField {
                           let results = ReportOfficesResponseData?.filter( { ($0.psName ?? "").localizedStandardContains(psSearchTextFieldObj.text ?? "")})
                           self.searchPSResults = results
                           psTableViewObj.reloadData()
                      }
                     else {
               let results = ReportServiceCodeResponseData?.PSAuthServiceDetails?.filter( { ($0.serviceName ?? "").localizedStandardContains(psSearchTextFieldObj.text ?? "")})
               self.searchServiceResults = results
               self.psTableViewObj.reloadData()

                   }
                   
                  }else{
                      self.isSearching = false
                      searchPSResults = nil
                      self.psTableViewObj.reloadData()
                  }
              }
             return true
          }
    
    

}
