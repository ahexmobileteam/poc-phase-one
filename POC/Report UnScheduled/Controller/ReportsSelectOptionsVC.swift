//
//  ReportsSelectOptionsVC.swift
//  POC
//
//  Created by Ajeet N on 17/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

enum ReportSelectionOptions {
    case isClockIn
    case isClockOut
    case isReportAbsent
}
class ReportsSelectOptionsVC: UIViewController {

    var receivedtempPSID : Int?
    
    @IBOutlet weak var clockInButtonOutlet: UIButton!
    @IBOutlet weak var clockOutButtonOutlet: UIButton!
    @IBOutlet weak var reportAbsentButtonOutlet: UIButton!
    
    var reterivedOfficeID: Int?

    private var ReportFlagResponseModel: ReportFlagsModel?
    private var reportSelectionOptions: ReportSelectionOptions?
    
    
//MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "POC Visit Verification"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 18)!,NSAttributedString.Key.foregroundColor : UIColor.white]
        print("the reterived office id in flags screen is :\(String(describing: reterivedOfficeID))")
        
    NotificationCenter.default.addObserver(self, selector: #selector(sessionExpired), name: Notification.Name("SessionExpired"), object: nil)

        print("receoved ps id in optoins vc : \(receivedtempPSID ?? 0)")
}
    
//MARK: Session Expired
        @objc func sessionExpired() {
               NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "SessionExpired"), object: nil)
               self.logout()
}
    
//MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.FlagsAPICalling()
        self.loadUIButtons()
}
    
    
//MARK: Load UI Buttons
    func loadUIButtons() {
        
    clockInButtonOutlet.layer.borderColor = UIColor.init(red: 20/255, green: 81/255, blue: 148/255, alpha: 1.0).cgColor
    clockInButtonOutlet.layer.borderWidth = 1.0
    clockInButtonOutlet.layer.cornerRadius = 22.0
    clockInButtonOutlet.layer.masksToBounds = true
    clockInButtonOutlet.isHidden = true
               
    clockOutButtonOutlet.layer.borderColor = UIColor.init(red: 20/255, green: 81/255, blue: 148/255, alpha: 1.0).cgColor
    clockOutButtonOutlet.layer.borderWidth = 1.0
    clockOutButtonOutlet.layer.cornerRadius = 22.0
    clockOutButtonOutlet.layer.masksToBounds = true
    clockOutButtonOutlet.isHidden = true

    reportAbsentButtonOutlet.layer.borderColor = UIColor.init(red: 20/255, green: 81/255, blue: 148/255, alpha: 1.0).cgColor
    reportAbsentButtonOutlet.layer.borderWidth = 1.0
    reportAbsentButtonOutlet.layer.cornerRadius = 22.0
    reportAbsentButtonOutlet.layer.masksToBounds = true
    reportAbsentButtonOutlet.isHidden = true

}
    
    
  //MARK: Clock in Button Action
    @IBAction func clockInButtonAction(_ sender: Any) {
        let blueColor = UIColor.init(red: 20/255, green: 81/255, blue: 148/255, alpha: 1.0)
        switch (sender as AnyObject).tag {
        case 0:
            self.clockInButtonOutlet.backgroundColor = blueColor
            self.clockInButtonOutlet.setTitleColor(.white, for: .normal)
            self.clockOutButtonOutlet.setTitleColor(blueColor, for: .normal)
            self.reportAbsentButtonOutlet.setTitleColor(blueColor, for: .normal)
            self.clockOutButtonOutlet.backgroundColor = .clear
            self.reportAbsentButtonOutlet.backgroundColor = .clear
            self.clockInButtonOutlet.setImage(UIImage(named: "RadioButton_01"), for: .normal)
            self.clockOutButtonOutlet.setImage(UIImage(named: "RadioButton"), for: .normal)
            self.reportAbsentButtonOutlet.setImage(UIImage(named: "RadioButton"), for: .normal)
            self.reportSelectionOptions = .isClockIn
           
        break
            
        case 1:
          self.clockOutButtonOutlet.backgroundColor = blueColor
          self.clockOutButtonOutlet.setTitleColor(.white, for: .normal)
          self.clockInButtonOutlet.setTitleColor(blueColor, for: .normal)
          self.reportAbsentButtonOutlet.setTitleColor(blueColor, for: .normal)
          self.clockInButtonOutlet.backgroundColor = .clear
          self.reportAbsentButtonOutlet.backgroundColor = .clear
          self.clockOutButtonOutlet.setImage(UIImage(named: "RadioButton_01"), for: .normal)
          self.clockInButtonOutlet.setImage(UIImage(named: "RadioButton"), for: .normal)
          self.reportAbsentButtonOutlet.setImage(UIImage(named: "RadioButton"), for: .normal)
          self.reportSelectionOptions = .isClockOut
        break
        
        case 2:
           self.reportAbsentButtonOutlet.backgroundColor = blueColor
           self.reportAbsentButtonOutlet.setTitleColor(.white, for: .normal)
           self.clockOutButtonOutlet.setTitleColor(blueColor, for: .normal)
           self.clockInButtonOutlet.setTitleColor(blueColor, for: .normal)
           self.clockInButtonOutlet.backgroundColor = .clear
           self.clockOutButtonOutlet.backgroundColor = .clear
           self.reportAbsentButtonOutlet.setImage(UIImage(named: "RadioButton_01"), for: .normal)
           self.clockInButtonOutlet.setImage(UIImage(named: "RadioButton"), for: .normal)
           self.clockOutButtonOutlet.setImage(UIImage(named: "RadioButton"), for: .normal)
           self.reportSelectionOptions = .isReportAbsent
        break

        default:
        break
        
    }
}
    
    //MARK: Continue Button Action
    @IBAction func continueButtonAction(_ sender: Any) {
        switch reportSelectionOptions {
        case .isClockIn:
            // ReportsOptionsDetailsVC
            guard let reportsOptionsDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "ReportsOptionsDetailsVC") as? ReportsOptionsDetailsVC else { return }
//            reportsOptionsDetailsVC.reterivedOfficeIDFromOffcieScren = reterivedOfficeID
            reportsOptionsDetailsVC.receivedTempOfficesIdStores = receivedtempPSID
            reportsOptionsDetailsVC.reportOptionsIdentifier = .isClockIn
            self.navigationController?.pushViewController(reportsOptionsDetailsVC, animated: true)
            break
        case .isClockOut:
            // ReportsOptionsDetailsVC
            guard let reportsOptionsDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "ReportsOptionsDetailsVC") as? ReportsOptionsDetailsVC else { return }
//            reportsOptionsDetailsVC.reterivedOfficeIDFromOffcieScren = reterivedOfficeID
            reportsOptionsDetailsVC.receivedTempOfficesIdStores = receivedtempPSID

            reportsOptionsDetailsVC.reportOptionsIdentifier = .isClockOut
            self.navigationController?.pushViewController(reportsOptionsDetailsVC, animated: true)
            break
        case .isReportAbsent:
            guard let reportAbsentVC = self.storyboard?.instantiateViewController(withIdentifier: "ReportAbsentVC") as? ReportAbsentVC else { return }
//                reportAbsentVC.reterivedOfficeIDFromOffcieScren = reterivedOfficeID
            reportAbsentVC.receivedTempOfficesIdStoresss = receivedtempPSID
            self.navigationController?.pushViewController(reportAbsentVC, animated: true)
            break
        default:break
        }
    }
    
    
//MARK: Offices API Calling
    func FlagsAPICalling()  {

        
        let officeIDString = String(describing: reterivedOfficeID ?? 0)
        let officesID = officeIDString
        
        let details = self.getuserDetails()
        
        let url = (details.baseURL ?? "") + API.GetCallConfigURL + (String(describing: receivedtempPSID ?? 0))
        let sessionID = API.SessionID + (details.sessionID ?? "")

        let flagsUrl = url + sessionID
        print("The flagsurl is :\(String(describing: flagsUrl))")
        
        ServiceManager.shared.request(type: ReportFlagsModel.self, url: flagsUrl, method: .get, view: self.view) { (response) in
        print("The response of flagsin report select options vc is :\(String(describing: response))")
            
            if response == nil {
                self.alert(title: "Alert Message", message: "Not Found")
            }
            else {
                self.ReportFlagResponseModel = response
                DispatchQueue.main.async {
                    
                    if response?.absentFlag == 1 {
                        self.reportAbsentButtonOutlet.isHidden = false
                    }else {
                        self.reportAbsentButtonOutlet.isHidden = true
                    }
                    
                    
                    if response?.departureFlag == 1 {
                        self.clockOutButtonOutlet.isHidden = false
                    }else {
                        self.clockOutButtonOutlet.isHidden = true
                    }
                    
                    
                    if response?.arrivalFlag == 1 {
                        self.clockInButtonOutlet.isHidden = false
                    }else {
                        self.clockInButtonOutlet.isHidden = true
                    }
                    
                    
                    if response?.specialFlag == 1 {
                        
                    }else {
                        
                    }
                    
                }
                        
                }
            }
    }
}

