//
//  ReportClockoutTasksVC.swift
//  POC
//
//  Created by Ajeet N on 02/05/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit
import CoreLocation

struct HandleSelection {
    var isTapped: Bool
    var isSelected: Bool
}

class ReportClockoutTasksVC: UIViewController {

    var receivedOfficeIdvalue: Int?
    
    private var handleSelection = [HandleSelection]()
     
    @IBOutlet weak var alertTransparentViewObj: UIView!
    @IBOutlet weak var alertCardViewObj: CustomView!
    @IBOutlet weak var alertFirstLabel: UILabel!
    @IBOutlet weak var alertDateLabel: UILabel!
    
    @IBOutlet weak var alertLocationLabel: UILabel!
    
    @IBOutlet weak var alertTimeLabel: UILabel!
    @IBAction func alertOkButtonAction(_ sender: Any) {
        
        self.alertTransparentViewObj.isHidden = true
        guard let emptyStatevC = self.storyboard?.instantiateViewController(withIdentifier: "MoveToNextTransaction") as? MoveToNextTransaction else {return}
        self.navigationController?.pushViewController(emptyStatevC, animated: true)
        
    }
    
var reterivedOfficeIDFromClockinScreen : Int?
var receivedPSIDFromClcokinScreen : String?
var receivedServiceTeleCodeFromClockinScreen: Int?
var receivedPSNameFromClockInScreen : String?

    
    var userEnteredOTP : String?
    var ReceivedOTPFLag : Int?
    var userEnteredMilege : String?
    
    
private var  reportClockoutTasksResponseData: reportClockOutTaskModelData?
    private let locationManager = CLLocationManager()
    var addressString : String = ""
    
    
    @IBOutlet weak var headrNameLabel: UILabel!
    @IBOutlet weak var tasksTableViewObj: UITableView! {
        didSet {
            tasksTableViewObj.delegate = self
            tasksTableViewObj.dataSource = self
        }
    }
   
    
    //MARK: Save Button Action
    @IBAction func saveButtonAction(_ sender: Any) {
            
        if ReceivedOTPFLag == 1 {
            // Create Punch API
            self.CreatPunchAPI()
        }
        
        else {
            // Save API -- > Single Validation API
            self.SaveSingleValidationAPI()
        }

    }
    
    
//MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.getListofTaskAPICalling()
        
        
        self.headrNameLabel.text = String(receivedPSNameFromClockInScreen ?? "Not Available")
    print("The received otp flag from tasks clock/in/out screen is :\(String(describing: ReceivedOTPFLag))")
    print("The received otp code from tasks clock/in/out screen is :\(String(describing: userEnteredOTP))")
        
        self.checkLocationServices()
        
    alertTransparentViewObj.isHidden = true
        
      NotificationCenter.default.addObserver(self, selector: #selector(sessionExpired), name: Notification.Name("SessionExpired"), object: nil)
        
        
        
    }
    
    //MARK: Session Expired
         @objc func sessionExpired() {
                NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "SessionExpired"), object: nil)
                self.logout()
         }
    
    //MARK: Get  User Current Location Services
    func checkLocationServices(){
        
        if CLLocationManager.locationServicesEnabled(){
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            self.checkLocationAuthorization()
            self.locationManager.startUpdatingLocation()
        }else{
            self.alert(title: "Please enable the location permissions in order to get user current location", message: "", actionTitles: ["OK"], actionStyle: [.default], action: [
                { ok in
                    
                }
            ])
        }
    }
    
    
    //MARK: Location Authorization
    func checkLocationAuthorization(){
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
            break
        case .denied:
            self.alert(title: "Location services are denied, Please enable it in order to do further action", message: "", actionTitles: ["OK"], actionStyle: [.default], action: [
                { ok in
                    
                }
            ])
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        default:break
        }
    }
    
    
    //MARK: Converting User Latitude and Longitude into Physical Address
    func getAddressFromLatLons(pdblLatitude: String, withLongitude pdblLongitude: String) {
        
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)") ?? 0.0
        let lon: Double = Double("\(pdblLongitude)") ?? 0.0
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    if pm.subLocality != nil {
                        self.addressString = self.addressString + pm.subLocality! + ", "
                        print("The address string is :\(self.addressString)")
                    }
                    if pm.thoroughfare != nil {
                        self.addressString = self.addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        self.addressString = self.addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        self.addressString = self.addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        self.addressString = self.addressString + pm.postalCode! + " "
                    }
                    DispatchQueue.main.async {
                        defaults.set(self.addressString, forKey: "REPORTCLOCKOUTTASKUSERADDRESS")
                        
                    }
                }
        })
    }
    
    
    
//MARK: Get List of Tasks API Calling
    func getListofTaskAPICalling() {
        
//http://poc.aquilasoftware.com/pocextacc-webservices_phase2/telephony/getTaskList?officeId=191&sessionId=180837_R1V9MKXZSINVAKWY&taskName=&visitDetailsId=0&psTelCode=128595&serviceTelCode=1
        
        let staticVisitDetailID = "1"
        
        let details = self.getuserDetails()
        
        let taskAPI =  (details.baseURL ?? "") + API.singleTaskList + String(describing: (receivedOfficeIdvalue ?? 0))
        let sessionId = API.SessionID + (details.sessionID ?? "")
        let taskName = API.taskName
        let visitDetailId = API.visitDetailID + staticVisitDetailID
        let psTelecodeId = API.psTelecode + String(describing: receivedPSIDFromClcokinScreen ?? "0")
        let serviceCode = API.serviceTeleCode + String(describing: receivedServiceTeleCodeFromClockinScreen ?? 0)
        
    let finalUrl = taskAPI + sessionId + taskName + visitDetailId + psTelecodeId + serviceCode
        print("The final tasks url is :\(String(describing: finalUrl))")
        ServiceManager.shared.request(type: reportClockOutTaskModelData.self, url: finalUrl, method: .get, view: self.view) { (response) in
            print("The respnse of list of task api is :\(String(describing: response))")

            if response?.count == 0 {
                DispatchQueue.main.async {
                self.tasksTableViewObj.isHidden = true

                }
            }
            else {
                
                DispatchQueue.main.async {
                    if response?.count ?? 0 != 0 {
                        for _ in 0..<response!.count {
                            self.handleSelection.append(HandleSelection(isTapped: false, isSelected: false))
                        }
                        
                    }
                    

                    self.tasksTableViewObj.isHidden = false
                    self.reportClockoutTasksResponseData = response
                    self.tasksTableViewObj.reloadData()
                }
                
                
            }
        }
    }
    

//MARK: Create Punch API
    func CreatPunchAPI() {
       
//http://poc.aquilasoftware.com/pocextacc-webservices_phase2/telephony/createManualPanch?jsonObj={"psMRN":131095,"dcsEmpCode":"226145","telcode":17,"taskCodes":"","mileage":0,"travelTime":0,"transactionType":"ARRIVAL","transactionDate":"05\/02\/2020","officeId":191,"imei":"25ed4d91-d4c2-4c50-b072-d1e447513e9a","latitude":"37.4219983","longitude":"-122.084","address":"1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA\n","ipAddress":"10.0.2.16","otp":"123456"}&sessionId=226145_SN5HVVVGD7X6ISPN
             
        
        var ids = [String]()
        ids.removeAll()
        for index in 0..<self.handleSelection.count {
            
            if self.handleSelection[index].isTapped {
                if self.handleSelection[index].isSelected {
                    ids.append( self.reportClockoutTasksResponseData?[index].taskCode ?? "")
                
                }
            }
        }
        
        
        let details = self.getuserDetails()
        
        let Currentdate = Date().string(format: "MM-dd-yyyy")

        let dcsEmpCode = defaults.value(forKey: "GLOBALDCSEMPCODE")
        let staticTransactionType = "DEPARTURE"
        let statciDate = Currentdate
        let staticTravelTime = "0"
        let staticMileage = "0"
             
        let latitude = defaults.value(forKey: "REPORTCLOCKOUTTASKLatitude") ?? 0.0
        print("The latitude in report unscheduled is :\(String(describing: latitude))")
             
        let longitude = defaults.value(forKey: "REPORTCLOCKOUTTASKLongitude") ?? 0.0
        print("The longitude in report unscheduled is :\(String(describing: longitude))")
             
        self.getAddressFromLatLons(pdblLatitude: String(describing: latitude), withLongitude: String(describing: longitude))
             
        let userAddress = defaults.value(forKey: "REPORTCLOCKOUTTASKUSERADDRESS") ?? ""
       print("The user address we have got is :\(String(describing: userAddress))")
             
         var jsonObject = [String: Any]()
         jsonObject["psMRN"] = (receivedPSIDFromClcokinScreen  ?? 0)
        jsonObject["dcsEmpCode"] = (details.empID ?? "")
        jsonObject["telcode"] = String(describing: receivedServiceTeleCodeFromClockinScreen ?? 0)
        jsonObject["taskCodes"] = ids.map{String($0)}.joined(separator: ",")
        jsonObject["mileage"] = String(describing: userEnteredMilege ?? "")
        jsonObject["travelTime"] = staticTravelTime
        jsonObject["transactionType"] = staticTransactionType
        jsonObject["transactionDate"] = statciDate
        jsonObject["officeId"] = (receivedOfficeIdvalue ?? "")
        jsonObject["imei"] = ""
        jsonObject["latitude"] = (latitude )
        jsonObject["longitude"] = (longitude )
        jsonObject["address"] = "1600 Amphitheatre Pkwy, Mountain View, CA 94043"
        jsonObject["ipAddress"] = "10.0.2.16"
        jsonObject["otp"] = (userEnteredOTP ?? "")

    let finalurl =  (details.baseURL ?? "") + API.CreateManualPunchForClockIn + "\(jsonObject)" + "&sessionId=\(details.sessionID ?? "")"
    print("The final creat manual puch api :\(String(describing: finalurl))")
             
    ServiceManager.shared.request(type: ReportClockInModel.self, url: finalurl, method: .get, view: self.view) { (response) in
                 print("The response of create manual puch is taks vc is    :\(String(describing: response))")
                 
                 if response?.result == "" {
                     self.alert(title: "Alert Message", message: response?.result ?? "No Result")
                 }
                 else {
                     DispatchQueue.main.async {
                    self.alertTransparentViewObj.isHidden = false
                    if let result = response?.result{
                    let resultDateAndTime = result.suffix(19)
                    self.alertFirstLabel.text = "Departure created successfully at"
                    self.alertDateLabel.text = String(resultDateAndTime.prefix(10))
                    self.alertTimeLabel.text = String(resultDateAndTime.suffix(8))
                        
                                      }
                        
//                         self.alert(title: "Alert Message", message: response?.result ?? "")
                     }
                 }
             }
    }
    
    
//MARK: Single Validation API
    func SaveSingleValidationAPI()  {
        
    let details = self.getuserDetails()
        
        let Currentdate = Date().string(format: "MM-dd-yyyy")

    let dcsEmpCode = defaults.value(forKey: "GLOBALDCSEMPCODE")
    let staticTransactionType = "DEPARTURE"
    let statciDate = Currentdate
    let staticTravelTime = "0"
                   
    let latitude = defaults.value(forKey: "REPORTCLOCKOUTTASKLatitude") ?? 0.0
    print("The latitude in report unscheduled is :\(String(describing: latitude))")
                   
    let longitude = defaults.value(forKey: "REPORTCLOCKOUTTASKLongitude") ?? 0.0
    print("The longitude in report unscheduled is :\(String(describing: longitude))")
                   
    self.getAddressFromLatLons(pdblLatitude: String(describing: latitude), withLongitude: String(describing: longitude))
                   
    let userAddress = defaults.value(forKey: "REPORTCLOCKOUTTASKUSERADDRESS") ?? ""
            print("The user address we have got is :\(String(describing: userAddress))")
                   
            let validateUrl = (details.baseURL ?? "") + API.ValidateSingleTransacation + String(describing: details.empID ?? "")
            let psmrn = API.PSMRN + String(describing: self.receivedPSIDFromClcokinScreen ?? "0")
            let officeId = API.officeID + String(describing: receivedOfficeIdvalue ?? 0)
    let telecode = API.TeleCode + String(describing: self.receivedServiceTeleCodeFromClockinScreen ?? 0)
            let mileage = API.Mileage + String(describing: userEnteredMilege ?? "")
            let travelTime = API.TravelTime + staticTravelTime
            let tasksCode = API.TasksCode
            let transactionType = API.TransactionType + staticTransactionType
            let Reportlatitude = API.latitude + String(describing: latitude)
            let Reportlongitude = API.longitude + String(describing: longitude)
            let address = API.address + String (describing: userAddress).addingPercentEncoding(withAllowedCharacters: NSCharacterSet.alphanumerics)!
                let transactionDate = API.TransacationDate + statciDate
                let ipAddress = API.ipAddress
                let sessionId = API.SessionID + (details.sessionID ?? "")
                        
                let clockOutUrl = validateUrl + psmrn + officeId + telecode + mileage + travelTime + tasksCode + transactionType + Reportlatitude + Reportlongitude + address + transactionDate + ipAddress + sessionId
                print("The final url in tasks  clock out save is :\(clockOutUrl)")
            
            ServiceManager.shared.request(type: ReportClockInModel.self, url: clockOutUrl, method: .get, view: self.view) { (response) in
                if response != nil {
                    print("The response of clockout tasks is :\(String(describing: response))")
                    DispatchQueue.main.async {
                        
                        self.alertTransparentViewObj.isHidden = false
                        if let result = response?.result{
                        let resultDateAndTime = result.suffix(19)
                        self.alertFirstLabel.text = "Departure created successfully at"
                        self.alertDateLabel.text = String(resultDateAndTime.prefix(10))
                        self.alertTimeLabel.text = String(resultDateAndTime.suffix(8))
                                          }
                    }
                }
                else {
                    self.alert(title: "Alert Messager", message: response?.result ?? "")
                }
            }
        }
    
    
    @objc func checkMarkAction(sender: UIButton) {
        
        
        if self.handleSelection[sender.tag].isTapped  {
               if handleSelection[sender.tag].isSelected == false {
                   self.handleSelection[sender.tag].isSelected = true
               }
               else {
                   self.handleSelection[sender.tag].isTapped = false
               }
           }else {
               self.handleSelection[sender.tag].isTapped  = true
               self.handleSelection[sender.tag].isSelected = true
           }
        

        
//        self.handleSelection[sender.tag].isTapped = true
//        if self.handleSelection[sender.tag].isSelected {
//            self.handleSelection[sender.tag].isSelected = false
//        }
//        else {
//            self.handleSelection[sender.tag].isSelected = true
//        }
        self.tasksTableViewObj.reloadData()
    }
    
    
   @objc func crossMarkAction(sender: UIButton)  {
        
    if self.handleSelection[sender.tag].isTapped {
        if handleSelection[sender.tag].isSelected {
            self.handleSelection[sender.tag].isSelected = false
        }
        else {
            self.handleSelection[sender.tag].isTapped = false
        }
    }else {
        self.handleSelection[sender.tag].isTapped  = true
        self.handleSelection[sender.tag].isSelected = false
    }
    
//    self.handleSelection[sender.tag].isTapped = true
//           if self.handleSelection[sender.tag].isSelected {
//               self.handleSelection[sender.tag].isSelected = false
//           }
////           else {
////               self.handleSelection[sender.tag].isSelected = true
////           }
           self.tasksTableViewObj.reloadData()
    
    }
    
}
    
    
//MARK: UITableView Delegate and Datasource Methods
extension ReportClockoutTasksVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reportClockoutTasksResponseData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
    guard let cell = tableView.dequeueReusableCell(withIdentifier: "ReportsTasksClockoutCell", for: indexPath) as? ReportsTasksClockoutCell else { return UITableViewCell() }
    cell.namLabel.text =  self.reportClockoutTasksResponseData?[indexPath.row].taskName

//        cell.cardViewObj.layer.backgroundColor = UIColor.lightGray.cgColor
//        cell.cardViewObj.layer.masksToBounds = false
        
        if handleSelection[indexPath.row].isTapped {
            if handleSelection[indexPath.row].isSelected {
                // Selected
                cell.checkMarkOutlet.setImage(UIImage(named: "selectedcheck"), for: .normal)
                cell.crossMarkOutlet.setImage(UIImage(named: "close"), for: .normal)

            }
            else {
                //
                cell.checkMarkOutlet.setImage(UIImage(named: "defaultCheck"), for: .normal)
                cell.crossMarkOutlet.setImage(UIImage(named: "selectedcross"), for: .normal)

            }
        }
        else {
            // Default image
            cell.checkMarkOutlet.setImage(UIImage(named: "defaultCheck"), for: .normal)
            cell.crossMarkOutlet.setImage(UIImage(named: "close"), for: .normal)

        }
        
    cell.checkMarkOutlet.tag = indexPath.row
    cell.crossMarkOutlet.tag = indexPath.row
    cell.checkMarkOutlet.addTarget(self, action: #selector(checkMarkAction), for: .touchUpInside)
    cell.crossMarkOutlet.addTarget(self, action: #selector(crossMarkAction), for: .touchUpInside)
        
    return cell
        
    }
    
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        <#code#>
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}

//MARK: Corelocation Delegate Methods
extension ReportClockoutTasksVC: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        let lat = location.coordinate.latitude
        let lon = location.coordinate.longitude
        defaults.set(lat, forKey: "REPORTCLOCKOUTTASKLatitude")
        defaults.set(lon, forKey: "REPORTCLOCKOUTTASKLongitude")
        defaults.synchronize()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkLocationAuthorization()
    }
}
