//
//  ReportsTasksClockoutCell.swift
//  POC
//
//  Created by Ajeet N on 02/05/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class ReportsTasksClockoutCell: UITableViewCell {

    
    @IBOutlet weak var cardViewObj: UIView!
    @IBOutlet weak var namLabel: UILabel!
    @IBOutlet weak var checkMarkOutlet: UIButton!
    @IBOutlet weak var crossMarkOutlet: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func checkMarkButtonAction(_ sender: Any) {
    }
    
    @IBAction func crossMarkButtonAction(_ sender: Any) {
    }
    
}
