//
//  ReportsOptionsDetailsVC.swift
//  POC
//
//  Created by Ajeet N on 17/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//


import UIKit
import CoreLocation

enum ReportOptions {
    case pscode
    case serviceCode
}

enum ReportOptionsIdentifier {
    case isClockIn
    case isClockOut
}



struct ValidateClockOutModel: Codable {
        let result, validateFlag: String?
    }




struct DepartureDetialsModel: Decodable {
    let mileageFlag: Int?
    let taskCapture: Int?
}

class ReportsOptionsDetailsVC: UIViewController {
    
    var receivedTempOfficesIdStores: Int?
    
    private var isSearching = false
    let cellReuseIdentifier = "cell"
    
    private var isPstextField = false
    
    private var checkValue : String?
    private var checkServiceValue: String?
    
    @IBOutlet weak var alertTransparentView: UIView!
    @IBOutlet weak var alertCardview: CustomView!
    @IBOutlet weak var alertFirstLabel: UILabel!
    @IBOutlet weak var alertDateLabel: UILabel!
    @IBOutlet weak var alertTimeLabel: UILabel!
    @IBOutlet weak var alertLocationLabel: UILabel!
    
   
    @IBOutlet weak var transparentView: UIView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var psSearchTextField: CustomTField!
    @IBOutlet weak var psTableView: UITableView!
        
   
    
    @IBOutlet weak var transactionLabel: UILabel!
    @IBOutlet weak var psSearchButtonOutlet: UIButton!
    @IBOutlet weak var serviceCodeButtonOutlet: UIButton!
    
    @IBOutlet weak var reportOptionsIdentifierLabel: UILabel!
    @IBOutlet weak var tokenCodeTF: CustomTField!
    @IBOutlet weak var psCodeTF: CustomTField!
    @IBOutlet weak var serviceCodetF: CustomTField!
    
    @IBOutlet weak var milageFlagTF: CustomTField!
    private let locationManager = CLLocationManager()
    var addressString : String = ""
    
    var reterivedOfficeIDFromFirstScreen: Int?
    private var ReportOfficesResponseData : reportPSModelResponse?
    private var searchPSResults: reportPSModelResponse?

    private var ReportServiceCodeResponseData: ReportServiceCodeModel?
    private var searchServiceResults: [PSAuthServiceDetail]?

    
    private var   reportClockInSaveModelData  : ReportClockInModel?
    
    private var  DepartureDetailResponseData : DepartureDetialsModel?
    
    
    private var  reportOptionsObj : ReportOptions? = .pscode
    
    private var idString: String? = nil
    var reportOptionsIdentifier: ReportOptionsIdentifier?
    var reterivedOfficeIDFromOffcieScren: Int?
    
    var tempPSID: String?
    var tempSeviceiD: Int?
    var tempOTPFlag : Int?
    var tempOTPCode : String?
    
    
    @IBOutlet weak var saveAndNextButtonOutlet: CustomButtonC!
    
    //MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        tokenCodeTF.isHidden = true
        navigationItem.title = "POC Visit Verification"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 18)!,NSAttributedString.Key.foregroundColor : UIColor.white]
        
        self.transparentView.isHidden = true
        self.alertTransparentView.isHidden = true

        cardView.layer.cornerRadius = 5.0
        cardView.layer.masksToBounds = false
        
        psTableView.delegate = self
        psTableView.dataSource = self

        psTableView.tableFooterView = UIView()
        psCodeTF.delegate = self
        serviceCodetF.delegate = self
        
        DispatchQueue.main.async {
            self.PSCodeAPI()
            }
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tapGesture.delegate = self
        transparentView.addGestureRecognizer(tapGesture)
        
        tokenCodeTF.isHidden = true
        
        switch reportOptionsIdentifier {
        case .isClockIn:
            milageFlagTF.isHidden = true
            reportOptionsIdentifierLabel.text = "Clock-In"
            break
        case .isClockOut:
            reportOptionsIdentifierLabel.text = "Clock-Out"
            getDepartureDetails()
            break
        default:
            break
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(sessionExpired), name: Notification.Name("SessionExpired"), object: nil)

    }

    override func viewWillAppear(_ animated: Bool) {
        self.transparentView.isHidden = true
        self.psSearchTextField.delegate = self
        checkLocationServices()
    }

    
    //MARK: Session Expired
    @objc func sessionExpired() {
           NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "SessionExpired"), object: nil)
           self.logout()
    }
    
    //MARK: Tap Gesture Action
    @objc func handleTap(){
        transparentView.isHidden = true
    }
    
//    //MARK: View Will Appear
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(true)
//
//    }

    //MARK: Get Mileage Flag Value For ClockOut
    func getDepartureDetails(){
    //http://poc.aquilasoftware.com/pocextacc-webservices_phase2/telephony/getDepartureDetails?officeId=191&transactionType=DEPARTURE&sessionId=226145_SN5HVVVGD7X6ISPN
        
let details = self.getuserDetails()
let url = (details.baseURL ?? "") + API.DEPARTUREDETAILS + "\(String(describing: receivedTempOfficesIdStores ?? 0))" + "&transactionType=DEPARTURE"
        
        let sessionID = API.SessionID + (details.sessionID ?? "")

        let finalUrl = url + sessionID
        print(url)
        ServiceManager.shared.request(type: DepartureDetialsModel.self, url: finalUrl, method: .get, view: self.view) { [weak self](response) in
            print("the response to check taskcapture valueto change button status is :\(String(describing: response))")
            if response != nil {
                self?.DepartureDetailResponseData = response
            }
            
     // Here we are checkng the Mileage Flag
            if self?.DepartureDetailResponseData?.mileageFlag ?? 0 == 1{
                DispatchQueue.main.async {
                    self?.milageFlagTF.isHidden = false
                }
            }else{
                DispatchQueue.main.async {
                    self?.milageFlagTF.isHidden = true
                }
            }
            
    // Here we are checkng the taskcapture values
            if self?.DepartureDetailResponseData?.taskCapture == 1 {
                self?.saveAndNextButtonOutlet.setTitle("Next", for: .normal)
            }
            else {
                self?.saveAndNextButtonOutlet.setTitle("Save", for: .normal)
            }
        }
    }
    
    //MARK: Get  User Current Location Services
    func checkLocationServices(){
        
        if CLLocationManager.locationServicesEnabled(){
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            self.checkLocationAuthorization()
            self.locationManager.startUpdatingLocation()
        }else{
            self.alert(title: "Please enable the location permissions in order to get user current location", message: "", actionTitles: ["OK"], actionStyle: [.default], action: [
                { ok in
                    
                }
            ])
        }
    }
    
    
    //MARK: Location Authorization
    func checkLocationAuthorization(){
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
            break
        case .denied:
            self.alert(title: "Location services are denied, Please enable it in order to do further action", message: "", actionTitles: ["OK"], actionStyle: [.default], action: [
                { ok in
                    
                }
            ])
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        default:break
        }
    }
    
    
    //MARK: PS Code API Calling
    func PSCodeAPI() {
        
        let details = self.getuserDetails()
        
        let pscode = (details.baseURL ?? "") + API.getListOFPSListByOfficeID + String(describing: receivedTempOfficesIdStores ?? 0)
        let sessionID = API.SessionID + (details.sessionID ?? "")
        
        let psOfficesUrl = pscode + sessionID
        print("The ps office url is report details vc  :\(psOfficesUrl)")
        
        ServiceManager.shared.request(type: reportPSModelResponse.self, url: psOfficesUrl, method: .get, view: self.view) { (response) in
            print("The response of ps offices list is :\(String(describing: response))")
            
            if response?.count ?? 0 != 0 {
                DispatchQueue.main.async {
                    self.ReportServiceCodeResponseData?.PSAuthServiceDetails?.removeAll()
                    self.ReportOfficesResponseData = response
//                    self.transparentView.isHidden = false
                    self.transparentView.layer.cornerRadius = 5.0
                    self.transparentView.layer.masksToBounds = true
                    self.psTableView.reloadData()
//                    self.ServiceCodeAPI()
                    
                    }
            }
            else {
               self.alert(title: "Alert Message", message: "No Records Found")
                self.transparentView.isHidden = true

            }
        }
    }
    
    
    //MARK: Service Code API Calling
    func ServiceCodeAPI()  {
        
        if tempPSID == nil {
            tempPSID = psCodeTF.text ?? ""
            print("the temp ps id is :\(String(describing: tempPSID))")
        }
        let details = self.getuserDetails()
        
        let url =  (details.baseURL ?? "")  + API.GETPSOTPANDSERVICEDETAILS + (tempPSID ?? "")
        let officeId = API.offFiled + String(describing: receivedTempOfficesIdStores ?? 0)
        let sessionID = API.SessionID + (details.sessionID ?? "")
        let finalurl = url + officeId + sessionID
        
        print("the service code final url is :\(String(describing: finalurl))")
        
        ServiceManager.shared.request(type: ReportServiceCodeModel.self, url: finalurl, method: .get, view: self.view) { (response) in
            print("The response of Service code is :\(String(describing: response))")
            
            if response?.PSAuthServiceDetails?.count ?? 0 != 0 {
                
                DispatchQueue.main.async {
                                 self.ReportServiceCodeResponseData?.PSAuthServiceDetails?.removeAll()
                                 self.ReportServiceCodeResponseData = response
                                 print("The response of Service code is main :\(String(describing: response))")
                              self.psTableView.reloadData()

                                 self.tempOTPFlag = self.ReportServiceCodeResponseData?.OTPFlag
                                 if self.ReportServiceCodeResponseData?.OTPFlag == 1 {
                                     self.tokenCodeTF.isHidden = false
                                 }

                                 else {
                                     self.tokenCodeTF.isHidden = true
                                 }
                             }
            }
            else {
                 self.alert(title: "Alert Message", message: "No Data Found")

            }
        }
    }
    
    //MARK: PS Search Button Action
    @IBAction func psSearchButtonAction(_ sender: Any) {
        
         isSearching = false
        isPstextField = true
        transparentView.isHidden = false
        self.transparentView.layer.cornerRadius = 5.0
        self.transparentView.layer.masksToBounds = true
        
        reportOptionsObj = .pscode
        psTableView.reloadData()
        self.PSCodeAPI()
        
    }
    
    
    //MARK: Service Code Button Action
    @IBAction func serviceCodeButtonAction(_ sender: Any) {
        
        if psCodeTF.text!.isEmpty {
            self.toast(msgString: "Please select PS Code", view: self.view)
            return
        }
        isSearching = false
        isPstextField = false
        psSearchTextField.text = ""
        transparentView.isHidden = false
        self.transparentView.layer.cornerRadius = 5.0
        self.transparentView.layer.masksToBounds = true
        
        reportOptionsObj = .serviceCode
        self.ServiceCodeAPI()
        psTableView.reloadData()

    }
    
    //MARK: Save Button Action
    @IBAction func saveButtonAction(_ sender: Any) {
        
        switch reportOptionsIdentifier {

//MARK: ClockIn
        case .isClockIn:
            
            if psCodeTF.text!.isEmpty {
                self.alert(title: "Alert Message", message: "Please enter PS Code ")
                return
            }
            if serviceCodetF.text!.isEmpty  {
                self.alert(title: "Alert Message", message: " Please enter service code")
                return
            }
            
            self.clockInSaveAPICalling()
        break
            

//MARK: ClockOut
        case .isClockOut:
            
            if psCodeTF.text!.isEmpty {
                self.alert(title: "Alert Message", message: "Please enter PS Code ")
                return
            }
            
            if serviceCodetF.text!.isEmpty  {
                self.alert(title: "Alert Message", message: " Please enter service code")
                return
            }
            
// Here we are checking task capture value in order to change Save button title as Next or Save
            
            if self.DepartureDetailResponseData?.taskCapture ?? 0 == 1 {
                
                self.saveAndNextButtonOutlet.setTitle("Next", for: .normal)
              // Here we need to call validate API
                
//http://poc.aquilasoftware.com/pocextacc-webservices_phase2/telephony/validateDepartureDetails?psMRN=131095&officeId=191&telcode=17&sessionId=226145_SN5HVVVGD7X6ISPN
                
                let details = self.getuserDetails()
                
                if tempPSID == nil {
                tempPSID  =  psCodeTF.text ?? ""
                }
               
                
                if tempSeviceiD == nil {
                    tempSeviceiD = Int(serviceCodetF.text ?? "") ?? 0
                }
               
                
                let url = (details.baseURL ?? "") + API.ValidatDepartureDetails + String(describing: idString ?? "0")
                let officeID =  API.officeID +  String(describing: receivedTempOfficesIdStores ?? 0)
                let telecodeID = API.TeleCode + String(describing: tempSeviceiD ?? 0)
                let sessionID = API.SessionID + (details.sessionID ?? "")
                
                let validateurl = url + officeID + telecodeID + sessionID
                print("the validate url is :\(String(describing: validateurl))")
            
                
                
    ServiceManager.shared.request(type: ValidateClockOutModel.self, url: validateurl, method: .get, view: self.view) { (response) in
                    
            print("The response of validate urls is :\(String(describing: response))")
                    
        if ( Int(response?.validateFlag ?? "") ?? 0  >= 1 ){
            
            // show alert there only
            self.alert(title: "Alert Message", message: response?.result ?? "")

                }
                
                else {
            
            DispatchQueue.main.async {
      guard let reportTaskClockVC = self.storyboard?.instantiateViewController(withIdentifier: "ReportClockoutTasksVC")  as? ReportClockoutTasksVC else { return }
      //            reportTaskClockVC.reterivedOfficeIDFromClockinScreen = self.reterivedOfficeIDFromOffcieScren
                  reportTaskClockVC.receivedPSIDFromClcokinScreen = self.idString
                  reportTaskClockVC.receivedServiceTeleCodeFromClockinScreen = self.tempSeviceiD
                  reportTaskClockVC.receivedPSNameFromClockInScreen = self.psCodeTF.text
                  reportTaskClockVC.receivedOfficeIdvalue = self.receivedTempOfficesIdStores
                  reportTaskClockVC.ReceivedOTPFLag = self.tempOTPFlag
                  reportTaskClockVC.userEnteredOTP  = (self.tokenCodeTF.text ?? "")
                  reportTaskClockVC.userEnteredMilege = (self.milageFlagTF.text ?? "0")
                  self.navigationController?.pushViewController(reportTaskClockVC, animated: true)
                          }
                    }
                }
            } // If Closing
                
          else if ReportServiceCodeResponseData?.OTPFlag == 1 {
            self.saveAndNextButtonOutlet.setTitle("Save", for: .normal)
            //  Clock out  --  Title Save
            // Here We need to check otp = 1, Create manual puch api
            CreatManualPuch()
                
            }
                
        else {
                self.saveAndNextButtonOutlet.setTitle("Save", for: .normal)
                self.clockOutSaveAPICalling()
        } // Else Closing
            
        break
        default:
        break
                        
        } // Switch Closing
        
        
    }  //Save Button Closing
    
    
    //MARK: Converting User Latitude and Longitude into Physical Address
    func getAddressFromLatLons(pdblLatitude: String, withLongitude pdblLongitude: String) {
        
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)") ?? 0.0
        let lon: Double = Double("\(pdblLongitude)") ?? 0.0
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    if pm.subLocality != nil {
                        self.addressString = self.addressString + pm.subLocality! + ", "
                        print("The address string is :\(self.addressString)")
                    }
                    if pm.thoroughfare != nil {
                        self.addressString = self.addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        self.addressString = self.addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        self.addressString = self.addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        self.addressString = self.addressString + pm.postalCode! + " "
                    }
                    DispatchQueue.main.async {
                        defaults.set(self.addressString, forKey: "REPORTCLOCKINUSERADDRESS")
                        
                    }
                }
        })
    }
    
    
//MARK: Create Manual Puch

    func CreatManualPuch()  {
        
//http://poc.aquilasoftware.com/pocextacc-webservices_phase2/telephony/createManualPanch?jsonObj={"psMRN":131095,"dcsEmpCode":"226145","telcode":17,"taskCodes":"","mileage":0,"travelTime":0,"transactionType":"ARRIVAL","transactionDate":"05\/02\/2020","officeId":191,"imei":"25ed4d91-d4c2-4c50-b072-d1e447513e9a","latitude":"37.4219983","longitude":"-122.084","address":"1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA\n","ipAddress":"10.0.2.16","otp":"123456"}&sessionId=226145_SN5HVVVGD7X6ISPN
             
        let Currentdate = Date().string(format: "MM-dd-yyyy")

             let dcsEmpCode = defaults.value(forKey: "GLOBALDCSEMPCODE")
             let staticTransactionType = "DEPARTURE"
             let statciDate = Currentdate
             let staticMileageValue = "0"
             let staticTravelTime = "0"
             
             let latitude = defaults.value(forKey: "ReportCLOCKINLatitude") ?? 0.0
                     print("The latitude in report unscheduled is :\(String(describing: latitude))")
             
             let longitude = defaults.value(forKey: "ReportCLOCKINLongitude") ?? 0.0
                     print("The longitude in report unscheduled is :\(String(describing: longitude))")
             
             self.getAddressFromLatLons(pdblLatitude: String(describing: latitude), withLongitude: String(describing: longitude))
             
             let userAddress = defaults.value(forKey: "REPORTCLOCKINUSERADDRESS") ?? ""
                     print("The user address we have got is :\(String(describing: userAddress))")
             
        
        let details = self.getuserDetails()

        
             var jsonObject = [String: Any]()
             jsonObject["psMRN"] = (self.tempPSID  ?? 0)
             jsonObject["dcsEmpCode"] = (details.empID ?? "")
             jsonObject["telcode"] = (tempSeviceiD ?? 0)
             jsonObject["taskCodes"] = ""
             jsonObject["mileage"] = staticMileageValue
             jsonObject["travelTime"] = staticTravelTime
             jsonObject["transactionType"] = staticTransactionType
             jsonObject["transactionDate"] = statciDate
             jsonObject["officeId"] = (receivedTempOfficesIdStores ?? "")
             jsonObject["imei"] = ""
             jsonObject["latitude"] = (latitude )
             jsonObject["longitude"] = (longitude )
             jsonObject["address"] = (userAddress )
             jsonObject["ipAddress"] = ""
             jsonObject["otp"] = (self.tokenCodeTF.text ?? "")

          let finalurl = (details.baseURL ?? "")  + API.CreateManualPunchForClockIn + "\(jsonObject)" + "&sessionId=\(details.sessionID ?? "")"
             print("The final creat manual puch api :\(String(describing: finalurl))")
             
        ServiceManager.shared.request(type: ReportClockInModel.self, url: finalurl, method: .get, view: self.view) { (response) in
                 print("The response of create manual puch is   :\(String(describing: response))")
                 
                 if response?.result == "" {
                     self.alert(title: "Alert Message", message: "Please check your data")
                 }
                 else {
                     DispatchQueue.main.async {
                          self.reportClockInSaveModelData = response
                        
                        self.alertTransparentView.isHidden = false
                        if let result = response?.result{
                            let resultDateAndTime = result.suffix(19)
                         self.alertFirstLabel.text = "Clock In Completed In"
                         self.alertDateLabel.text = String(resultDateAndTime.prefix(10))
                         self.alertTimeLabel.text = String(resultDateAndTime.suffix(8))
                        }
                                                
//                        self.alert(title: "Alert Message", message: response?.result ?? "")
                     }
                 }
                 
             }
    }
    
        
//MARK: ClockIn Save API Calling
    func clockInSaveAPICalling() {
        
        
        if tempPSID == nil {
            tempPSID = psCodeTF.text
        }
       
        if tempSeviceiD == nil {
                   tempSeviceiD = Int(serviceCodetF.text ?? "") ?? 0
               }
        
        
        
        let Currentdate = Date().string(format: "MM-dd-yyyy")
        
        let dcsEmpCode = defaults.value(forKey: "GLOBALDCSEMPCODE")
        let staticTransactionType = "ARRIVAL"
        let statciDate = Currentdate
        let staticMileageValue = "0"
        let staticTravelTime = "0"
        
        let latitude = defaults.value(forKey: "ReportCLOCKINLatitude") ?? 0.0
                print("The latitude in report unscheduled is :\(String(describing: latitude))")
        
        let longitude = defaults.value(forKey: "ReportCLOCKINLongitude") ?? 0.0
                print("The longitude in report unscheduled is :\(String(describing: longitude))")
        
        self.getAddressFromLatLons(pdblLatitude: String(describing: latitude), withLongitude: String(describing: longitude))
        
        let userAddress = defaults.value(forKey: "REPORTCLOCKINUSERADDRESS") ?? ""
                print("The user address we have got is :\(String(describing: userAddress))")
        
         let details = self.getuserDetails()
        
        let validateUrl = (details.baseURL ?? "") + API.ValidateSingleTransacation + String(describing: details.empID ?? "")
             let psmrn = API.PSMRN + String(describing: self.idString ?? "0")
             let officeId = API.officeID + String(describing: receivedTempOfficesIdStores ?? 0)
             let telecode = API.TeleCode + String(describing: self.tempSeviceiD ?? 0)
             let mileage = API.Mileage + staticMileageValue
             let travelTime = API.TravelTime + staticTravelTime
             let tasksCode = API.TasksCode
             let transactionType = API.TransactionType + staticTransactionType
             let Reportlatitude = API.latitude + String(describing: latitude)
             let Reportlongitude = API.longitude + String(describing: longitude)
             let address = API.address + String (describing: userAddress).addingPercentEncoding(withAllowedCharacters: NSCharacterSet.alphanumerics)!
             let transactionDate = API.TransacationDate + statciDate
             let ipAddress = API.ipAddress
             let sessionId = API.SessionID + (details.sessionID ?? "")
             
             let finalUrl = validateUrl + psmrn + officeId + telecode + mileage + travelTime + tasksCode + transactionType + Reportlatitude + Reportlongitude + address + transactionDate + ipAddress + sessionId
             print("The final url of clock in save button action is  :\(finalUrl)")
        
        ServiceManager.shared.request(type: ReportClockInModel.self, url: finalUrl, method: .get, view: self.view) { (response) in
            print("The response of clock in save button is :\(String(describing: "response"))")
            if response?.validateFlag != nil  {
                
                DispatchQueue.main.async {
                    self.showAlert(message: response?.result ?? "")
                }
               
            }
            else {
//                self.alert(title: "Alert Message", message: response?.result ?? "")
                
            DispatchQueue.main.async {
                
              self.alertTransparentView.isHidden = false
               if let result = response?.result{
                   let resultDateAndTime = result.suffix(19)
                self.alertFirstLabel.text = "Clock In Completed In"
                self.alertDateLabel.text = String(resultDateAndTime.prefix(10))
                self.alertTimeLabel.text = String(resultDateAndTime.suffix(8))
               }
                    
                    
                }
                
            }
        }
        
}
    
    
//MARK: Clock out Save API Calling
    func clockOutSaveAPICalling()  {
        

         if tempPSID == nil {
             tempPSID = psCodeTF.text
         }
        
         if tempSeviceiD == nil {
            tempSeviceiD = Int(serviceCodetF.text ?? "") ?? 0
        }
        
        

        
        
        let Currentdate = Date().string(format: "MM-dd-yyyy")

        let details = self.getuserDetails()
        
        let dcsEmpCode = defaults.value(forKey: "GLOBALDCSEMPCODE")
        let staticTransactionType = "DEPARTURE"
        let statciDate = Currentdate
        let staticMileageValue = "0"
        let staticTravelTime = "0"
               
        let latitude = defaults.value(forKey: "ReportCLOCKINLatitude") ?? 0.0
        print("The latitude in report unscheduled is :\(String(describing: latitude))")
               
       let longitude = defaults.value(forKey: "ReportCLOCKINLongitude") ?? 0.0
        print("The longitude in report unscheduled is :\(String(describing: longitude))")
               
        self.getAddressFromLatLons(pdblLatitude: String(describing: latitude), withLongitude: String(describing: longitude))
               
        let userAddress = defaults.value(forKey: "REPORTCLOCKINUSERADDRESS") ?? ""
        print("The user address we have got is :\(String(describing: userAddress))")
               
        let validateUrl = (details.baseURL ?? "") + API.ValidateSingleTransacation + String(describing: details.empID ?? "")
                    let psmrn = API.PSMRN + String(describing: self.idString ?? "0")
                    let officeId = API.officeID + String(describing: receivedTempOfficesIdStores ?? 0)
                    let telecode = API.TeleCode + String(describing: self.tempSeviceiD ?? 0)
                    let mileage = API.Mileage + staticMileageValue
                    let travelTime = API.TravelTime + staticTravelTime
                    let tasksCode = API.TasksCode
                    let transactionType = API.TransactionType + staticTransactionType
                    let Reportlatitude = API.latitude + String(describing: latitude)
                    let Reportlongitude = API.longitude + String(describing: longitude)
                    let address = API.address + String (describing: userAddress).addingPercentEncoding(withAllowedCharacters: NSCharacterSet.alphanumerics)!
            let transactionDate = API.TransacationDate + statciDate
            let ipAddress = API.ipAddress
            let sessionId = API.SessionID + (details.sessionID ?? "")
                    
            let clockOutUrl = validateUrl + psmrn + officeId + telecode + mileage + travelTime + tasksCode + transactionType + Reportlatitude + Reportlongitude + address + transactionDate + ipAddress + sessionId
            print("The final url of clock out save is :\(clockOutUrl)")
        
        ServiceManager.shared.request(type: ReportClockInModel.self, url: clockOutUrl, method: .get, view: self.view) { (response) in
            if response?.validateFlag == "12" {
                self.alert(title: "Alert Message", message: "PS Code and Service Code are invalid for the Clock - Out transaction")
            }
            else {
                DispatchQueue.main.async {
                    self.alertTransparentView.isHidden = false
                     if let result = response?.result{
                         let resultDateAndTime = result.suffix(19)
                        self.alertFirstLabel.text = "Clock out Completed In"
                      self.alertDateLabel.text = String(resultDateAndTime.prefix(10))
                      self.alertTimeLabel.text = String(resultDateAndTime.suffix(8))
                     }
                }
                                
            }
        }
    }
    
    
    @IBAction func alertOkButtonAction(_ sender: Any) {
        
        self.alertTransparentView.isHidden = true
        guard let emptyStatevC = self.storyboard?.instantiateViewController(withIdentifier: "MoveToNextTransaction") as? MoveToNextTransaction else {return}
        self.navigationController?.pushViewController(emptyStatevC, animated: true)
        
    }
    
}



//MARK: UIGesture Recognizer Delegate Methods
extension ReportsOptionsDetailsVC : UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view!.isDescendant(of: psTableView) {
            return false
        }
        return true
    }
}

//MARK: UITableView Delegate and DataSource methods
extension ReportsOptionsDetailsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch reportOptionsObj {
        case .pscode:
            if isSearching {
                return searchPSResults?.count ?? 0
            }
            return self.ReportOfficesResponseData?.count ?? 0
            
        case .serviceCode:
            if isSearching {
                return searchServiceResults?.count ?? 0
            }
            return self.ReportServiceCodeResponseData?.PSAuthServiceDetails?.count ?? 0
            
        default:
            return 0
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier:cellReuseIdentifier) else {return UITableViewCell() }
        
        switch reportOptionsObj {
            
        case .pscode:
            
            cell.textLabel?.textColor = UIColor.init(red: 20/255.0, green:81/255.0 , blue: 148/255.0, alpha: 1.0)
            cell.textLabel?.font = UIFont(name: "Avenir-Roman", size: 18.0)
            
            if isSearching {
        cell.textLabel?.text = self.searchPSResults?[indexPath.row].psName ?? ""
            }
                
            else {
        cell.textLabel?.text = self.ReportOfficesResponseData?[indexPath.row].psName ?? ""
            }
    
            break
     
            
        case .serviceCode:
            cell.textLabel?.text = self.ReportServiceCodeResponseData?.PSAuthServiceDetails?[indexPath.row].serviceName ?? ""
            cell.textLabel?.textColor = UIColor.init(red: 20/255.0, green:81/255.0 , blue: 148/255.0, alpha: 1.0)
            cell.textLabel?.font = UIFont(name: "Avenir-Roman", size: 18.0)
            
            if isSearching {
    cell.textLabel?.text = self.searchServiceResults?[indexPath.row].serviceName ?? ""
                
                }

            else {
    cell.textLabel?.text = self.ReportServiceCodeResponseData?.PSAuthServiceDetails?[indexPath.row].serviceName ?? ""
                
            }
            
        default:
            return UITableViewCell()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch reportOptionsObj {
        case .pscode:
            
            if isSearching {
                idString =  String(self.searchPSResults?[indexPath.row].psCode ?? "0" )
                psCodeTF.text = self.searchPSResults?[indexPath.row].psName
                tempPSID = self.searchPSResults?[indexPath.row].psId ?? "0"
                self.ServiceCodeAPI()
                self.transparentView.isHidden = true
            }
            else {
                idString =  String(self.ReportOfficesResponseData?[indexPath.row].psCode ?? "0" )
                psCodeTF.text = self.ReportOfficesResponseData?[indexPath.row].psName
                tempPSID = self.ReportOfficesResponseData?[indexPath.row].psId ?? "0"
                 self.ServiceCodeAPI()
                transparentView.isHidden = true
               
            }

            break
            
        case .serviceCode:
            
            if isSearching {
                
        serviceCodetF.text = self.searchServiceResults?[indexPath.row].serviceName ?? ""
        tempSeviceiD = self.searchServiceResults?[indexPath.row].serviceCode ?? 0
                transparentView.isHidden = true
                
            }
            else{
                
            serviceCodetF.text = self.ReportServiceCodeResponseData?.PSAuthServiceDetails?[indexPath.row].serviceName ?? ""
            tempSeviceiD = self.ReportServiceCodeResponseData?.PSAuthServiceDetails?[indexPath.row].serviceCode ?? 0
            transparentView.isHidden = true
                
            }
            
            break
        default:
            break
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 35
    }
    
}

//MARK: Corelocation Delegate Methods
extension ReportsOptionsDetailsVC: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        let lat = location.coordinate.latitude
        let lon = location.coordinate.longitude
        defaults.set(lat, forKey: "ReportCLOCKINLatitude")
        defaults.set(lon, forKey: "ReportCLOCKINLongitude")
        defaults.synchronize()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkLocationAuthorization()
    }
}

extension ReportsOptionsDetailsVC : UITextFieldDelegate {
    
func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//    if textField == psCodeTF {
//        self.transparentView.isHidden = false
//        return false
//    }
    
    if textField == serviceCodetF {
        
        if psCodeTF.text?.isEmpty ?? false {
            self.toast(msgString: "Please enter PS Code", view: self.view)
            return false
        }
        else {
            DispatchQueue.main.async {
                self.ServiceCodeAPI()
            }
            return true
        }
    }
       return true

}
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
           if textField == psSearchTextField {
               if psSearchTextField.text?.count ?? 0 > 1 {
                   self.isSearching = true
                  if isPstextField {
                        let results = ReportOfficesResponseData?.filter( { ($0.psName ?? "").localizedStandardContains(psSearchTextField.text ?? "")})
                        self.searchPSResults = results
                        psTableView.reloadData()
                   }
                  else {
            let results = ReportServiceCodeResponseData?.PSAuthServiceDetails?.filter( { ($0.serviceName ?? "").localizedStandardContains(psSearchTextField.text ?? "")})
            self.searchServiceResults = results
            self.psTableView.reloadData()

                }
                
               }else{
                   self.isSearching = false
                   searchPSResults = nil
                   self.psTableView.reloadData()
               }
           }
          return true
       }
    
}
