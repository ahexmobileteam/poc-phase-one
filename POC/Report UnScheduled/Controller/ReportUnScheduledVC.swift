//
//  ReportUnScheduledVC.swift
//  POC
//
//  Created by Ajeet N on 17/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class ReportUnScheduledVC: UIViewController {
    
    private var timer = Timer()
    

    @IBOutlet weak var selectDropDownHeight: NSLayoutConstraint!
    
    private var isSearching = false
    let cellReuseIdentifier = "cell"
    var tempPSID : Int?
    private var idString: String? = nil

    
    
    private var selectedIndex = Int()
    private let transiton = SlideInTransition()

    @IBOutlet weak var tableViewTransparentView: UIView!
    @IBOutlet weak var tableViewCardView: UIView!
    
    
    @IBOutlet weak var searchTF: CustomTField!
       
   
    
    
    @IBOutlet weak var psTableViewObj: UITableView! {
        didSet {
            psTableViewObj.delegate = self
            psTableViewObj.dataSource = self
        }
    }
    
    
    
    private var ReportOfficesModelData: ReportResponseModel?
    private var searchResults: ReportResponseModel?

    
    @IBOutlet weak var selectDropDown: DropDownTextField!
    
    @IBOutlet weak var setttingsTransparentView: UIView!
    @IBOutlet weak var settingsView: UITableView! {
        didSet {
            settingsView.delegate = self
            settingsView.dataSource = self
            self.settingsView.layer.cornerRadius = 5.0
        }
    }
    
     let settingsArray = ["Today's Schedules","Search Schedules","Report UnScheduled Vist","Non-Client Shift","Time Sheet"]
    
    
    //MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableViewCardView.layer.cornerRadius = 5.0
        self.tableViewCardView.layer.masksToBounds = true
        
        if let sessionTime = defaults.value(forKey: kSession) as? Int{
            timer = Timer.scheduledTimer(timeInterval: TimeInterval(sessionTime), target: self, selector: #selector(sessionTimeOut), userInfo: nil, repeats: true)
        }
        
        let settings = UIBarButtonItem(image: UIImage(named: "Right Menu"), style: .plain, target: self, action: #selector(settingsPopUP))
        self.navigationItem.rightBarButtonItems = [settings]

        self.setttingsTransparentView.isHidden = true
        self.tableViewTransparentView.isHidden = true
        
        self.configureNavigationBar(title: "Report UnScheduled Visit")
    self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 20)!,NSAttributedString.Key.foregroundColor : UIColor.white]

        DispatchQueue.main.async {
             self.officesList()
        }
        
        
        psTableViewObj.tableFooterView = UIView()

        
//        selectDropDown.dropDown.selectionAction = { (index , item) in
//            self.selectedIndex = index
//            self.selectDropDown.text = item
//            self.selectDropDown.resignFirstResponder()
//            DispatchQueue.main.async {
//                defaults.set(self.ReportOfficesModelData?[index].id, forKey: "REPORTOFFICEID")
//                defaults.set(self.ReportOfficesModelData?[index].name, forKey: "REPORTOFFICENAME")
//            }
//        }
        
     let sidemenuButton = UIBarButtonItem(image: UIImage(named: "Menu"), style: .plain, target: self, action: #selector(openSideMenu))
     self.navigationItem.leftBarButtonItem = sidemenuButton
        
        selectDropDown.delegate = self
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tapGesture.delegate = self
        setttingsTransparentView.addGestureRecognizer(tapGesture)
        
    NotificationCenter.default.addObserver(self, selector: #selector(sessionExpired), name: Notification.Name("SessionExpired"), object: nil)
        
        
}
    
    
    @objc func sessionTimeOut(){
        self.session()
    }
    
    func resetSession(){
        timer.invalidate()
        if let sessionTime = defaults.value(forKey: kSession) as? Int{
        timer = Timer.scheduledTimer(timeInterval: TimeInterval(sessionTime), target: self, selector: #selector(sessionTimeOut), userInfo: nil, repeats: true)
            
               }
    }
    override func viewWillDisappear(_ animated: Bool) {
        timer.invalidate()
        if let sessionTime = defaults.value(forKey: kSession) as? Int{
            timer = Timer.scheduledTimer(timeInterval: TimeInterval(sessionTime), target: self, selector: #selector(sessionTimeOut), userInfo: nil, repeats: true)
        }
    }
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        self.resetSession()
    }

    
    @objc func settingsPopUP() {
        print("ddd")
        self.setttingsTransparentView.isHidden = false
        
    }
    
    //MARK: Tap Gesture Action
          @objc func handleTap(){
              setttingsTransparentView.isHidden = true
          }
    
  @objc func openSideMenu() {
        
        guard let menuViewController = self.storyboard?.instantiateViewController(withIdentifier: "MenuTableViewController")  as? MenuTableViewController else { return }
        menuViewController.modalPresentationStyle = .overCurrentContext
        menuViewController.transitioningDelegate = self
       // menuViewController.identifyController = .isSchedule
        self.present(menuViewController, animated: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.searchTF.delegate = self

    }
    
    
//MARK: Session Expired
        @objc func sessionExpired() {
               NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "SessionExpired"), object: nil)
               self.logout()
        }
    

//MARK: Get Report Offices List
    func officesList()  {
        
        let details = self.getuserDetails()
        let reportUrl = (details.baseURL ?? "") + API.getPSOfficesList + (details.dcsID ?? "")
        let sessionID = API.SessionID + (details.sessionID ?? "")
        let url = reportUrl + sessionID
        
        
    ServiceManager.shared.request(type: ReportResponseModel.self, url: url, method: .get, view: self.view) { (response) in
        print("the response of report offices list is :\(String(describing: response))")
        
        if response?.count ?? 0 != 0 {
                   DispatchQueue.main.async {
                       self.ReportOfficesModelData = response
                       self.tableViewTransparentView.isHidden = true
                       self.tableViewTransparentView.layer.cornerRadius = 5.0
                       self.tableViewTransparentView.layer.masksToBounds = true
                    
                    self.idString =  String(self.ReportOfficesModelData?[0].id ?? 0 )
                    self.tempPSID =  self.ReportOfficesModelData?[0].id ?? 0
                    
        self.selectDropDown.text = "\(self.ReportOfficesModelData?[0].name ?? "" )  (\(self.ReportOfficesModelData?[0].code ?? ""))"
                    
                       self.psTableViewObj.reloadData()
                      
                   }
               }
               else {
                   self.alert(title: "Alert Message", message: "No Records Found")
                   self.tableViewTransparentView.isHidden = true
               }
        
        
//        if response?.count == 0 {
//            self.alert(title: "Alert Message", message: "No Offices List Found")
//            }
//            else {
//            self.ReportOfficesModelData = response
//
//            DispatchQueue.main.async {
//                if let selectedName = self.ReportOfficesModelData?.map({$0.name ?? ""}) {
//
//                let nameString = response?[0].name ?? ""
//                let idString = response?[0].code ?? ""
//                self.selectDropDown.text = "\(nameString) \(idString)"
//                self.selectDropDown.datasource = selectedName
//                }
//            }
//        }
    }
}
    
    
//MARK: Next Button Action
    @IBAction func nextButtonAction(_ sender: Any) {

        guard let flagScreenVC = self.storyboard?.instantiateViewController(withIdentifier: "ReportsSelectOptionsVC") as? ReportsSelectOptionsVC else { return }
        let seletctedOfficeID: Int = defaults.value(forKey: "REPORTOFFICEID") as? Int ?? 0
        flagScreenVC.reterivedOfficeID = seletctedOfficeID
        flagScreenVC.receivedtempPSID  = self.tempPSID
        self.navigationController?.pushViewController(flagScreenVC, animated: true)
    }
    
}

//MARK: UIView Controller Transistion Delegate
extension ReportUnScheduledVC : UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transiton.isPresenting = true
        return transiton
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transiton.isPresenting = false
        return transiton
    }
}

//MARK: UIGesture Recognizer Delegate Methods
extension ReportUnScheduledVC : UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view!.isDescendant(of: settingsView) {
            return false
        }
        return true
    }
}

extension ReportUnScheduledVC: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
     return 1
        
     }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch tableView {
        case settingsView:
            return settingsArray.count

        case psTableViewObj:
            
            if isSearching {
                return searchResults?.count ?? 0
            }
            return ReportOfficesModelData?.count ?? 0
        default:
            return 0
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch tableView {
        case settingsView:
            
            guard  let settingscell = tableView.dequeueReusableCell(withIdentifier: "ReportSettingsCell", for: indexPath) as? ReportSettingsCell else {
                                return UITableViewCell() }
            settingscell.reportsNameLabel.text = settingsArray[indexPath.row]
                            
            if indexPath.row == 2 {
                settingscell.reportsNameLabel.textColor = UIColor(displayP3Red: 116/255.0, green: 177/255.0, blue: 243/255.0, alpha: 1)
            }
            else {
                settingscell.reportsNameLabel.textColor = .black
            }
            
            settingscell.selectionStyle = .none
            return settingscell
            
            
        case psTableViewObj:
            
             guard let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier:cellReuseIdentifier) else {return UITableViewCell() }
            
             if isSearching{
           cell.textLabel?.text = "\(self.searchResults?[indexPath.row].name ?? "" )  (\(self.searchResults?[indexPath.row].code ?? ""))"
                
             }else{
                                    
    cell.textLabel?.text = "\(self.ReportOfficesModelData?[indexPath.row].name ?? "" )  (\(self.ReportOfficesModelData?[indexPath.row].code ?? ""))"

             }

             cell.textLabel?.textColor = UIColor.init(red: 20/255.0, green:81/255.0 , blue: 148/255.0, alpha: 1.0)
             cell.textLabel?.font = UIFont(name: "Avenir-Roman", size: 18.0)
            return cell
        default:
        return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch tableView {
            
        case settingsView:
            
            if indexPath.row == 0 {
               DispatchQueue.main.async {
                          guard let searchSchedulesVC = self.storyboard?.instantiateViewController(withIdentifier: "scheduleExceptionsVC") as? ScheduleExceptionsVC else { return }
                          self.setttingsTransparentView.isHidden = true
                          let navigation = UINavigationController(rootViewController: searchSchedulesVC)
                          navigation.modalPresentationStyle = .fullScreen
                          self.present(navigation, animated: false, completion: nil)
                      }
                }
                                      
                else if (indexPath.row == 1) {
                         
                DispatchQueue.main.async {
                                  guard let searchSchedulesVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchSchdeulesVC") as? SearchSchdeulesVC else { return }
                                  self.setttingsTransparentView.isHidden = true
                                  let navigation = UINavigationController(rootViewController: searchSchedulesVC)
                                  navigation.modalPresentationStyle = .fullScreen
                                  self.present(navigation, animated: false, completion: nil)
                    }
                                      
            }
                                      
                else if (indexPath.row == 2) {
                                      
            }
                                      
              else if (indexPath.row == 3) {
                   DispatchQueue.main.async {
                  guard  let nonClientVC = self.storyboard?.instantiateViewController(withIdentifier: "NonClientVC") as? NonClientVC else { return }
                      self.setttingsTransparentView.isHidden = true
                  
                  let navigation = UINavigationController(rootViewController: nonClientVC)
                  navigation.modalPresentationStyle = .fullScreen
                  self.present(navigation, animated: false, completion: nil)
                  }
              }
                                      
          else if (indexPath.row == 4){
               DispatchQueue.main.async {
              guard let timeSheet = self.storyboard?.instantiateViewController(withIdentifier: "TimeSheetVC") as? TimeSheetVC else { return }
                  self.setttingsTransparentView.isHidden = true
              
            let navigation = UINavigationController(rootViewController: timeSheet)
          navigation.modalPresentationStyle = .fullScreen
          self.present(navigation, animated: false, completion: nil)
              }
          }
            
        case psTableViewObj:
            
            if isSearching{
                       idString =  String(self.searchResults?[indexPath.row].id ?? 0 )
                selectDropDown.text = "\(self.searchResults?[indexPath.row].name ?? "" )  (\(self.searchResults?[indexPath.row].code ?? ""))"
                
            let description = "\(self.searchResults?[indexPath.row].name ?? "" )  (\(self.searchResults?[indexPath.row].code ?? ""))"
//            let height = description.height(withConstrainedWidth: selectDropDown.frame.width, font:  UIFont(name: "Avenir-Medium", size: 13.0)!)
                
                
                tempPSID = self.searchResults?[indexPath.row].id ?? 0

                   }else{
              idString =  String(self.ReportOfficesModelData?[indexPath.row].id ?? 0 )
              selectDropDown.text = "\(self.ReportOfficesModelData?[indexPath.row].name ?? "" )  (\(self.ReportOfficesModelData?[indexPath.row].code ?? ""))"
                       tempPSID = self.ReportOfficesModelData?[indexPath.row].id ?? 0
                   }
            
            let description = "\(self.ReportOfficesModelData?[indexPath.row].name ?? "" )  (\(self.ReportOfficesModelData?[indexPath.row].code ?? ""))"
//            let height = description.height(withConstrainedWidth: selectDropDown.frame.width, font:  UIFont(name: "Avenir-Medium", size: 13.0)!)
            
            searchResults = nil
            isSearching = false
            
            DispatchQueue.main.async {
                       self.psTableViewObj.reloadData()
                       self.searchTF.text = ""
                       self.tableViewTransparentView.isHidden = true

                   }
            

        default:
             break
}
        
        
 }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }

}

extension ReportUnScheduledVC : UITextFieldDelegate {
    
func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    if textField == selectDropDown {
        self.tableViewTransparentView.isHidden = false
        return false
    }
    return true
    
}
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
           if textField == searchTF {
               if searchTF.text?.count ?? 0 > 1{
                   self.isSearching = true
                  let results = ReportOfficesModelData?.filter( { ($0.name ?? "").localizedStandardContains(searchTF.text ?? "")})
                  self.searchResults = results
                  self.psTableViewObj.reloadData()
               }else{
                   self.isSearching = false
                   searchResults = nil
                   self.psTableViewObj.reloadData()
               }
           }
           return true
       }
    
    
}

    
    

