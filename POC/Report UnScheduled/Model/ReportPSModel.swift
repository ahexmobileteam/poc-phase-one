//
//  ReportPSModel.swift
//  POC
//
//  Created by Ajeet N on 27/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import Foundation

typealias reportPSModelResponse = [ReportPSModel]


// MARK: - WelcomeElement
struct ReportPSModel: Codable {
    let psCode, psName, psId: String?
}
