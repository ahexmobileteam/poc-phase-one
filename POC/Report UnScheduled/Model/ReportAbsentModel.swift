//
//  ReportAbsentModel.swift
//  POC
//
//  Created by Ajeet N on 01/05/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//


import Foundation



// MARK: - Welcome
struct ReportAbsentModel: Codable {
    let result: String?
    let validateFlag : String?
}
