//
//  ReportsClockoutTaskModel.swift
//  POC
//
//  Created by Ajeet N on 02/05/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//


import Foundation


typealias reportClockOutTaskModelData = [ReportsClockoutTaskModel]


struct ReportsClockoutTaskModel: Codable {
    let taskCode, taskCodeAndName, taskName: String?
    let taskId: Int?

}
