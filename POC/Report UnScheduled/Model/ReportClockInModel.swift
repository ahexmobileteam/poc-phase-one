//
//  ReportClockInModel.swift
//  POC
//
//  Created by Ajeet N on 02/05/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import Foundation


// MARK: - Welcome
struct ReportClockInModel: Codable {
    let result: String?
    let validateFlag : String?
}
