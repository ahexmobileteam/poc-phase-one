//
//  ReportServiceCodeModel.swift
//  POC
//
//  Created by Ajeet N on 27/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import Foundation


struct ReportServiceCodeModel: Codable {
    
let OTPFlag: Int?
var PSAuthServiceDetails: [PSAuthServiceDetail]?
}

struct PSAuthServiceDetail: Codable {
    let serviceCode: Int?
    let serviceName: String?
}
