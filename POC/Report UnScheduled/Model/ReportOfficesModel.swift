//
//  ReportOfficesModel.swift
//  POC
//
//  Created by Ajeet N on 26/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import Foundation


typealias ReportResponseModel = [ReportOfficesModel]


// MARK: - WelcomeElement
struct ReportOfficesModel: Codable {
    let code, name: String?
    let id: Int?
}

