//
//  ReportFlagsModel.swift
//  POC
//
//  Created by Ajeet N on 26/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import Foundation


struct ReportFlagsModel: Codable {
    let absentFlag, departureFlag, arrivalFlag, specialFlag: Int?
}
