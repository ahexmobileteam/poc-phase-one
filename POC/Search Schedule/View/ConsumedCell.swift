//
//  ConsumedCell.swift
//  POC
//
//  Created by Ajeet N on 20/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class ConsumedCell: UITableViewCell {

    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var rightLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
