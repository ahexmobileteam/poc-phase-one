//
//  SearchScheduleCell.swift
//  POC
//
//  Created by Ajeet N on 15/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class SearchScheduleCell: UITableViewCell {
    
    @IBOutlet weak var backView: UIView!
    
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var secondView: UIView!
    
    @IBOutlet weak var firstImage: UIImageView!
    @IBOutlet weak var firstLabel: UILabel!
    
    @IBOutlet weak var procedureCodeOutlet: UIButton!
    
    @IBOutlet weak var psNameLabel: UILabel!
    @IBOutlet weak var procedureCodeButton: UIButton!
    @IBOutlet weak var secondImage: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    
    
    var SearchSchedule : SearchScheduleModel? {
        didSet {
            
//        self.procedureCodeButton.titleLabel?.text = SearchSchedule?.procedureCode ?? "Not Available"
//        self.psNameLabel.text = SearchSchedule?.psName ?? "Not Available"
//        self.timeLabel.text = SearchSchedule?.visitdetailstarttime ?? "Not Available"
//        self.firstLabel.text = SearchSchedule?.visitdetailstarttime ?? "Not Available"
            
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
