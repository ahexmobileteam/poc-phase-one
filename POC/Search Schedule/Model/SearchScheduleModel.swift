//
//  SearchScheduleModel.swift
//  POC
//
//  Created by Ajeet N on 15/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//


import Foundation


typealias searchScheduleListModel = [SearchScheduleModel]

struct SearchScheduleModel: Codable {
    let visitDetailsId: String?
    let procedureCode: String?
    let psName, visitdetailendtime, visitdetailstarttime: String?
}

