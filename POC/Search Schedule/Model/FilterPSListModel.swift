//
//  FilterPSListModel.swift
//  POC
//
//  Created by Ajeet N on 16/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import Foundation


typealias FilterPSListModel = [FilterPSModel]


struct FilterPSModel: Codable {
    let psName, psId: String?
}
