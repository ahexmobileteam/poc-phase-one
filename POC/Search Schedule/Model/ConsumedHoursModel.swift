//
//  ConsumedHoursModel.swift
//  POC
//
//  Created by Ajeet N on 20/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//


import Foundation


// MARK: - Welcome
struct ConsumedHoursModel: Codable {
    let period, authorizedHrs, servedHours, procedureCode: String?
    let myServedHours, remainingHrs: String?
}
