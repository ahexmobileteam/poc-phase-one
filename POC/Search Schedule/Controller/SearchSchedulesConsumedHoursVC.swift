//
//  SearchSchedulesConsumedHoursVC.swift
//  POC
//
//  Created by Ajeet N on 19/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class SearchSchedulesConsumedHoursVC: UIViewController {
    
    var visitedDetailsId: String?
    var psNamereceived: String?
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var authorizedLabel: UILabel!
    @IBOutlet weak var servedLabel: UILabel!
    @IBOutlet weak var remainingHoursLabel: UILabel!
    @IBOutlet weak var myServedHoursLabel: UILabel!
    private var ConsumedHoursModeData : ConsumedHoursModel?
    @IBOutlet weak var procedureLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    
//MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Consumed Hours"
        print("the visisted detail id in consumed view controller is :\(String(describing: visitedDetailsId))")
        print("the psName Received in the consumed view controller is :\(String(describing: psNamereceived))")
        
NotificationCenter.default.addObserver(self, selector: #selector(sessionExpired), name: Notification.Name("SessionExpired"), object: nil)
        
}
    
//MARK: Session Expired
    @objc func sessionExpired() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "SessionExpired"), object: nil)
        self.logout()
}
    
//MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.consumedAPICalling()
}
    
//MARK: Consumed API Calling
    func consumedAPICalling()  {
        
          let details = self.getuserDetails()
        
        let consumedhoursUrl = (details.baseURL ?? "") + API.ConsumedHours + (visitedDetailsId ?? "0")
        let dcsID = API.dcsID + (details.dcsID ?? "")
        let sessionID = API.SessionID + (details.sessionID ?? "")
        
        let finalUrl = consumedhoursUrl + dcsID + sessionID
        print("the consumed url is :\(finalUrl)")
        
        ServiceManager.shared.request(type:ConsumedHoursModel.self, url: finalUrl, method: .get, view: self.view) { (response) in
            print("The response of consumed hours is :\(String(describing: response))")
            
            if response == nil {
                
            }
            else {
                DispatchQueue.main.async {
                    self.ConsumedHoursModeData = response
                    self.positionLabel.text = response?.period ?? "Not Available"
                    self.authorizedLabel.text = response?.authorizedHrs ?? "Not Available"
                    self.servedLabel.text = response?.servedHours ?? "Not Available"
                    self.remainingHoursLabel.text = response?.remainingHrs ?? "Not Avaiable"
                    self.myServedHoursLabel.text = response?.myServedHours ?? "Not Available"
                    self.procedureLabel.text = response?.procedureCode ?? "Not Available"
                    self.nameLabel.text = (self.psNamereceived ?? "Not Available")
                }
            }
            
        }
        
    }
    
    
}
