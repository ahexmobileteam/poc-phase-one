//
//  FilterSearchScheduleVC.swift
//  POC
//
//  Created by Ajeet N on 15/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//


import UIKit

class FilterSearchScheduleVC: UIViewController {

    private var isSearching = false
    let cellReuseIdentifier = "cell"
    var tempPSID : String?
    private var idString: String? = nil

    
    private var selectedIndex = Int()
    private var FilterPSModelData: FilterPSListModel?
    private var searchResults: FilterPSListModel?
    @IBOutlet weak var psTableViewOBj: UITableView! {
        didSet{
            psTableViewOBj.delegate = self
            psTableViewOBj.dataSource = self
        }
    }
    @IBOutlet weak var cardViewObj: UIView!
    @IBOutlet weak var searchTextField: CustomTField!{
        didSet{
            self.searchTextField.delegate = self
        }
    }
    
    @IBOutlet weak var transparentViewObj: UIView!
    @IBOutlet weak var dropDownTF: DropDownTextField!
    @IBOutlet weak var startDateTF: CustomTField!
    @IBOutlet weak var endDateTF: CustomTField!
    
    private let startDatePicker = UIDatePicker()
    private let endDatePicker = UIDatePicker()
    internal var DefaultPSSate : String?
    
    
    
//MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // getListofPSlist()
        self.loadStartAndEndDatePickers()

        self.transparentViewObj.isHidden = true
        
        self.cardViewObj.layer.cornerRadius = 5.0
        self.cardViewObj.layer.masksToBounds = false
        
        psTableViewOBj.tableFooterView = UIView()

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tapGesture.delegate = self
        transparentViewObj.addGestureRecognizer(tapGesture)
        

        let psId = defaults.value(forKey: "FILTERPSID") as? String
        let psName =  defaults.value(forKey: "FILTERPSNAME") as? String
        
        if psName != nil {
            tempPSID = psId ?? ""
            dropDownTF.text = psName ?? ""
        }
        
        
        
//        dropDownTF.dropDown.selectionAction = {(index , item) in
//            if index == 0 {
//                self.DefaultPSSate = nil
//                defaults.removeObject(forKey: "FILTERPSID")
//                defaults.removeObject(forKey: "FILTERPSNAME")
//            }
//            else {
//                DispatchQueue.main.async {
//                defaults.set(self.FilterPSModelData?[index].psId ?? "0" , forKey: "FILTERPSID")
//                defaults.set(self.FilterPSModelData?[index].psName ?? 0, forKey: "FILTERPSNAME")
//                self.DefaultPSSate = (self.FilterPSModelData?[index].psId ?? "" )
//            print("The static default state is :\(String(describing: self.DefaultPSSate))")
//                          }
//            }
//
//            self.selectedIndex = index
//            self.dropDownTF.text = item
//            self.dropDownTF.resignFirstResponder()
//        }
        
        let alreadySelectedPScode = defaults.value(forKey: "FILTERPSNAME") as? String
        if alreadySelectedPScode != nil {
            dropDownTF.text = alreadySelectedPScode ?? ""
        }
        
        dropDownTF.delegate = self
        
     NotificationCenter.default.addObserver(self, selector: #selector(sessionExpired), name: Notification.Name("SessionExpired"), object: nil)
    
}
    
    
//MARK: Tap Gesture Action
       @objc func handleTap(){
           transparentViewObj.isHidden = true
    }
    
//MARK: Session Expired
    @objc func sessionExpired() {
        
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "SessionExpired"), object: nil)
    self.logout()
}
    
//MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
//MARK: Get List of PS
    func getListofPSlist()  {
        
          let details = self.getuserDetails()
        
        let PSname = (details.baseURL ?? "") + API.getListOfPS + (details.dcsID ?? "")
        let sessionID = API.SessionID + (details.sessionID ?? "")
        let GetListURL = PSname + sessionID
        print("Get list of PSLlist URL:\(GetListURL)")
        
    ServiceManager.shared.request(type: FilterPSListModel.self, url: GetListURL, method: .get, view: self.view) { (response) in
    print("The response of get Ps list is in Filter Search Schedule is :\(String(describing: response))")
        
        if response?.count ?? 0 != 0 {
            DispatchQueue.main.async {
                self.FilterPSModelData = response
                self.transparentViewObj.isHidden = false
                self.transparentViewObj.layer.cornerRadius = 5.0
                self.transparentViewObj.layer.masksToBounds = true
                self.psTableViewOBj.reloadData()
               
            }
        }
        else {
            self.alert(title: "Alert Message", message: "No Records Found")
            self.transparentViewObj.isHidden = true

        }
    }
}
    

//MARK: Load Start and End Date
private func loadStartAndEndDatePickers() {
      
    let date = Date()
           let start = Calendar.current.date(byAdding: .day, value: -3, to: date)
           let formatter = DateFormatter()
           formatter.dateFormat = "MM/dd/yyyy"
           let defaultStartDate = formatter.string(from: start!)
           let defaultEndDate = formatter.string(from: date)
           startDatePicker.datePickerMode = .date
           let startDate = defaults.value(forKey: "SEARCHSCHEDULESSTARTDATE")
           if startDate != nil{
               self.setCustomDate(datePicker: startDatePicker, date: String(describing: startDate ?? ""),textField: startDateTF)
           }else{
               self.setCustomDate(datePicker: startDatePicker, date: defaultStartDate, textField: startDateTF)
               defaults.set(defaultStartDate, forKey: "SEARCHSCHEDULESSTARTDATE")
           }
           
           let startDateToolbar = UIToolbar()
           startDateToolbar.sizeToFit()
           let startDoneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(startDateDoneAction))
           let startSpaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
           let startCancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelAction))
           startDateToolbar.setItems([startCancelButton,startSpaceButton,startDoneButton], animated: false)
           startDateTF.inputAccessoryView = startDateToolbar
           startDateTF.inputView = startDatePicker
           
           
           endDatePicker.datePickerMode = .date
           let endDate = defaults.value(forKey: "SEARCHSCHEDULESENDDATE")
           if endDate != nil{
               self.setCustomDate(datePicker: endDatePicker, date: String(describing: endDate ?? ""), textField: endDateTF)
           }else{
               self.setCustomDate(datePicker: endDatePicker, date: defaultEndDate, textField: endDateTF)
               defaults.set(defaultEndDate, forKey: "SEARCHSCHEDULESENDDATE")
           }
           let endDateToolbar = UIToolbar();
           endDateToolbar.sizeToFit()
           let endDoneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(endDateDoneAction));
           let endSpaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
           let endCancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelAction));
           endDateToolbar.setItems([endCancelButton,endSpaceButton,endDoneButton], animated: false)
           endDateTF.inputAccessoryView = endDateToolbar
           endDateTF.inputView = endDatePicker
    
}
    
//MARK: Start Date Done Button Action
    @objc func startDateDoneAction() {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let selectedDate = formatter.string(from: startDatePicker.date)
        startDateTF.text = selectedDate
        defaults.set(selectedDate, forKey: "SEARCHSCHEDULESSTARTDATE")
        startDateTF.layer.borderColor = UIColor.greenColor.cgColor
        self.view.endEditing(true)
    }
    
//MARK: End Date Done Button Action
    @objc func endDateDoneAction() {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let selectedDate = formatter.string(from: endDatePicker.date)
        endDateTF.text = selectedDate
        defaults.set(selectedDate, forKey: "SEARCHSCHEDULESENDDATE")
        endDateTF.layer.borderColor = UIColor.greenColor.cgColor
        self.view.endEditing(true)
    }
    
//MARK:Cancel Button Action
    @objc func cancelAction(){
        self.view.endEditing(true)
    }
    
//MARK: Go Button Action
    @IBAction func GoButtonAction(_ sender: Any) {
        
        
        if !startDateTF.text!.isEmpty{
           defaults.set(startDateTF.text ?? "", forKey: "SEARCHSCHEDULESSTARTDATE")
        }

        if !endDateTF.text!.isEmpty{
           defaults.set(endDateTF.text ?? "", forKey: "SEARCHSCHEDULESENDDATE")
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        
        guard let checkStartDate = formatter.date(from: startDateTF.text ?? "") else { return }
        guard let checkEndDate = formatter.date(from: endDateTF.text ?? "") else { return }
        switch checkStartDate.compare(checkEndDate) {
        case .orderedAscending:
                print("Start date is earlier than end date")
        case .orderedDescending:
            self.alert(title: "Alert Message", message: "Start date cannot be greater than end date")
            return
//        case .orderedSame:
//            print("Start date and end date are same")
//            self.alert(title: "Alert Message", message: "Start date and end date are same ")
//
//            return
        default:
            print("call API")

        }
        
        defaults.set(tempPSID ?? "0", forKey: "FILTERPSID")
        defaults.set(dropDownTF.text ?? "0", forKey: "FILTERPSNAME")
        defaults.synchronize()

        NotificationCenter.default.post(name: Notification.Name(rawValue:
            "UserChangedPSSearchValue"), object: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
}


extension FilterSearchScheduleVC : UITextFieldDelegate {

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    if textField == dropDownTF{
       self.getListofPSlist()
        return false
    }
    return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == searchTextField{
            if searchTextField.text?.count ?? 0 > 1{
                self.isSearching = true
               let results = FilterPSModelData?.filter( { ($0.psName ?? "").localizedStandardContains(searchTextField.text ?? "")})
               self.searchResults = results
               self.psTableViewOBj.reloadData()
            }else{
                self.isSearching = false
                searchResults = nil
                self.psTableViewOBj.reloadData()
            }
        }
        return true
    }
    
}

//MARK: UITableView Delegate and DataSource Methods
extension FilterSearchScheduleVC : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isSearching ? (searchResults?.count ?? 0) : ( 1 + (FilterPSModelData?.count ?? 0))
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier:cellReuseIdentifier) else {return UITableViewCell() }
        if isSearching{
            cell.textLabel?.text = self.searchResults?[indexPath.row].psName ?? ""
        }else{
            
            if indexPath.row == 0 {
                cell.textLabel?.text = "SELECT PS"
            }
            else {
                cell.textLabel?.text = self.FilterPSModelData?[indexPath.row].psName ?? ""
            }
            
        }
        cell.textLabel?.textColor = UIColor.init(red: 20/255.0, green:81/255.0 , blue: 148/255.0, alpha: 1.0)
        cell.textLabel?.font = UIFont(name: "Avenir-Roman", size: 18.0)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isSearching{
            idString =  String(self.searchResults?[indexPath.row].psId ?? "0" )
            dropDownTF.text = self.searchResults?[indexPath.row].psName
            tempPSID = self.searchResults?[indexPath.row].psId ?? "0"

        }else{
            
            if indexPath.row == 0 {
                idString =  "0"
                dropDownTF.text = "SELECT PS"
                tempPSID = "0"
            }
            else {
                
                idString =  String(self.FilterPSModelData?[indexPath.row].psId ?? "0" )
                dropDownTF.text = self.FilterPSModelData?[indexPath.row].psName
                tempPSID = self.FilterPSModelData?[indexPath.row].psId ?? "0"
            }
                        
            
        }
        
        searchResults = nil
        isSearching = false
        
        DispatchQueue.main.async {
            self.psTableViewOBj.reloadData()
            self.searchTextField.text = ""
            self.transparentViewObj.isHidden = true

        }
       

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 50
    }
    
}


//MARK: UIGesture Recognizer Delegate Methods
extension FilterSearchScheduleVC : UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view!.isDescendant(of: psTableViewOBj) {
            return false
        }
        return true
    }
}
