//
//  SearchSchdeulesVC.swift
//  POC
//
//  Created by Ajeet N on 13/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class SearchSchdeulesVC: UIViewController {

    
private var SearchScheduleResponse: searchScheduleListModel?

    private let transiton = SlideInTransition()

    @IBOutlet weak var settingsTableView: UITableView! {
        didSet {
            settingsTableView.delegate = self
            settingsTableView.dataSource = self
            self.settingsTableView.layer.cornerRadius = 5.0

        }
    }
    
    let settingsArray = ["Today's Schedules","Search Schedules","Report UnScheduled Vist","Non-Client Shift","Time Sheet"]
    
    @IBOutlet weak var settingstransparentView: UIView!
    @IBOutlet weak var emptyStatusLabel: UILabel!
    @IBOutlet weak var searchSchedulesTableView: UITableView! {
        didSet {
            searchSchedulesTableView.delegate = self
            searchSchedulesTableView.dataSource = self
        }
    }
    

//MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
    let searchTimeSheet = UIBarButtonItem(image: UIImage(named: "Search_01"), style: .plain, target: self, action: #selector(SearchScheduleScreen))
        
    let settings = UIBarButtonItem(image: UIImage(named: "Right Menu"), style: .plain, target: self, action: #selector(settingsPopUP))
    self.navigationItem.rightBarButtonItems = [settings,searchTimeSheet]
        
    let sidemenuButton = UIBarButtonItem(image: UIImage(named: "Menu"), style: .plain, target: self, action: #selector(openSideMenu))
    self.navigationItem.leftBarButtonItem = sidemenuButton
        
        
        settingsTableView.tableFooterView = UIView(frame: CGRect(origin: .zero, size:
        CGSize(width: 0, height: CGFloat.leastNormalMagnitude)))
    
        self.settingstransparentView.isHidden = true
        
    searchScheduleAPI()
    self.configureNavigationBar(title: "Search Schedules")
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tapGesture.delegate = self
        settingstransparentView.addGestureRecognizer(tapGesture)
        
        
        
    NotificationCenter.default.addObserver(self, selector: #selector(sessionExpired), name: Notification.Name("SessionExpired"), object: nil)

}
    
    
    //MARK: Tap Gesture Action
       @objc func handleTap(){
           settingstransparentView.isHidden = true
       }
    
//MARK: Search Schedule Screen
    @objc func SearchScheduleScreen() {
        
       // self.settingsTableView.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(receiveUserSearch), name: NSNotification.Name(rawValue: "UserChangedPSSearchValue"), object: nil)
    guard let filterScheduleScreen = self.storyboard?.instantiateViewController(withIdentifier: "FilterSearchScheduleVC") as? FilterSearchScheduleVC else { return }
    self.navigationController?.pushViewController(filterScheduleScreen, animated: true)
        
    }
    
//MARK: Session Expired
    @objc func sessionExpired() {
        
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "SessionExpired"), object: nil)
    self.logout()
    }
    
    
    @objc func openSideMenu() {
        
        guard let menuViewController = self.storyboard?.instantiateViewController(withIdentifier: "MenuTableViewController")  as? MenuTableViewController else { return }
        menuViewController.modalPresentationStyle = .overCurrentContext
        menuViewController.transitioningDelegate = self
       // menuViewController.identifyController = .isSchedule
        self.present(menuViewController, animated: true)
        
    }
    
    @objc func settingsPopUP() {
        print("ddd")
        self.settingstransparentView.isHidden = false
        
    }
    
    
//MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    @objc func receiveUserSearch()  {
        DispatchQueue.main.async {
            self.searchScheduleAPI()
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "UserChangedPSSearchValue"), object: nil)
        }
      
    }
    
    
//MARK: Search Schedule API
    func searchScheduleAPI()  {
        
        
        SearchScheduleResponse?.removeAll()
        searchSchedulesTableView.reloadData()
        
        
       let date = Date()
       let start = Calendar.current.date(byAdding: .day, value: -3, to: date)
       let formatter = DateFormatter()
       formatter.dateFormat = "MM/dd/yyyy"
       let defaultStartDate = formatter.string(from: start!)
       let defaultEndDate = formatter.string(from: date)
        
        
        
// Reterived Start Date from Filter Search Schedule Screen
        var receivedStardDate = defaults.value(forKey: "SEARCHSCHEDULESSTARTDATE") as? String
        if receivedStardDate != nil {
            
        }else {
            receivedStardDate = defaultStartDate
        }
        
        
// Reterived End Date from Filter Search Schedule Screen
        var receivedEndDate = defaults.value(forKey: "SEARCHSCHEDULESENDDATE") as? String
        if receivedEndDate != nil {
            
        }else{
            receivedEndDate = defaultEndDate
        }
        
        
// Reterived PSID from Filter Search Schedule Screen
        var receivedPSID = defaults.value(forKey: "FILTERPSID") as? String
        print("The received PSID in Search schedles vc is :\(String(describing: receivedPSID))")
        if receivedPSID != nil{
            
        }
        else {
            receivedPSID = "0"
        }
        
        let details = self.getuserDetails()

        let searchSchedule = (details.baseURL ?? "") + API.SearchSchedule + (details.dcsID ?? "")
        let sessionIDURL = API.SessionID + (details.sessionID ?? "")
        let psIDUrl = API.PSID + (receivedPSID ?? "0")
        let startDate = API.StartDate + (receivedStardDate ?? "")
        let endDate = API.EndDate + (receivedEndDate ?? "")
        
let searchScheduleURl =  searchSchedule + sessionIDURL + psIDUrl + startDate + endDate
print("The SearchSchedule API is :\(searchScheduleURl)")

        ServiceManager.shared.request(type: searchScheduleListModel.self, url: searchScheduleURl, method: .get, view: self.view) { (response) in
        print("the searchSchedule api response is :\(String(describing: response))")
            
            if response?.count == 0 {
                self.searchSchedulesTableView.isHidden = true
                self.emptyStatusLabel.isHidden = false
                self.emptyStatusLabel.text = "No Schedules found"
            }
            else {
            DispatchQueue.main.async {
                self.searchSchedulesTableView.isHidden = false
                self.emptyStatusLabel.isHidden = true
                self.SearchScheduleResponse = response
                self.searchSchedulesTableView.reloadData()
                print("the searchSchedule api response is passed is  :\(String(describing: response))")
                           }
            }
           
        }
}
    

//MARK: Consumed Button Action
    @objc func consumedHoursAction(_ sender:UIButton)  {
        print("the sender is :\(String(describing: sender.tag))")
        
    guard let consumedHoursVC = storyboard?.instantiateViewController(withIdentifier: "SearchSchedulesConsumedHoursVC") as? SearchSchedulesConsumedHoursVC else { return }
    consumedHoursVC.visitedDetailsId =  (self.SearchScheduleResponse?[sender.tag].visitDetailsId ?? "")
    consumedHoursVC.psNamereceived = (self.SearchScheduleResponse?[sender.tag].psName ?? "")
        print("The selecte ps name in searh schedule vc is :\(String(describing: (self.SearchScheduleResponse?[sender.tag].psName ?? "")))")
    self.navigationController?.pushViewController(consumedHoursVC, animated: true)
        
    }
    
}


//MARK: UITableView Delegate and Datasource Methods
extension SearchSchdeulesVC: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
         return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("The search shedules count is :\(self.SearchScheduleResponse?.count ?? 0)")
        
        switch tableView {
        case searchSchedulesTableView:
            return self.SearchScheduleResponse?.count ?? 0
        case settingsTableView:
            return settingsArray.count
        default:
            return 0
        }
        
    }
    
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
    
    switch tableView {
    case searchSchedulesTableView:
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SearchScheduleCell", for:indexPath) as? SearchScheduleCell else { return UITableViewCell() }
           cell.selectionStyle = .none
           cell.backView.layer.cornerRadius = 5.0
           cell.backView.layer.masksToBounds = true
           
           if let date = self.SearchScheduleResponse?[indexPath.row].visitdetailstarttime {
               cell.firstLabel.text = String(date.prefix(10))
           }
           
           if let Statrtime = self.SearchScheduleResponse?[indexPath.row].visitdetailstarttime , let endtime = self.SearchScheduleResponse?[indexPath.row].visitdetailendtime  {
               
               cell.timeLabel.text = "\(String(Statrtime.suffix(8))) - \(String(endtime.suffix(8)))"
           }
           
           cell.psNameLabel.text = self.SearchScheduleResponse?[indexPath.row].psName
           cell.procedureCodeOutlet.setTitle(self.SearchScheduleResponse?[indexPath.row].procedureCode, for: .normal)

           
           cell.procedureCodeOutlet.tag = indexPath.row
           cell.procedureCodeOutlet.addTarget(self, action: #selector(consumedHoursAction(_:)), for: .touchUpInside)
        
        return cell

    case settingsTableView:
        
        guard  let settingscell = tableView.dequeueReusableCell(withIdentifier: "settingsSearchCell", for: indexPath) as? settingsSearchCell else {
                       return UITableViewCell() }
            settingscell.searchNameLabel.text = settingsArray[indexPath.row]
                   
                   if indexPath.row == 1 {
                       settingscell.searchNameLabel.textColor = UIColor(displayP3Red: 116/255.0, green: 177/255.0, blue: 243/255.0, alpha: 1)
                   }
                   else {
                       settingscell.searchNameLabel.textColor = .black
                   }
                   
                   settingscell.selectionStyle = .none
                   return settingscell
        
    default:
        return UITableViewCell()
    }
    
   
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch tableView {
        case searchSchedulesTableView:
             return 140
            
        case settingsTableView:
            return 40
            
        default:
            return  UITableView.automaticDimension
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView {
        case settingsTableView:
            
         if indexPath.row == 0 {
                    DispatchQueue.main.async {
                               guard let searchSchedulesVC = self.storyboard?.instantiateViewController(withIdentifier: "scheduleExceptionsVC") as? ScheduleExceptionsVC else { return }
                               self.settingstransparentView.isHidden = true
                               let navigation = UINavigationController(rootViewController: searchSchedulesVC)
                               navigation.modalPresentationStyle = .fullScreen
                               self.present(navigation, animated: false, completion: nil)
                           }
                }
                           
                       else if (indexPath.row == 1) {
                           
                           
                       }
                           
                       else if (indexPath.row == 2) {
                           
                            DispatchQueue.main.async {
                           guard let ReportUnScheduledVC = self.storyboard?.instantiateViewController(withIdentifier: "ReportUnScheduledVC") as? ReportUnScheduledVC else { return }
                               self.settingstransparentView.isHidden = true
                           let navigation = UINavigationController(rootViewController: ReportUnScheduledVC)
                           navigation.modalPresentationStyle = .fullScreen
                           self.present(navigation, animated: false, completion: nil)
                           }
                       }
                           
                       else if (indexPath.row == 3) {
                            DispatchQueue.main.async {
                           guard  let nonClientVC = self.storyboard?.instantiateViewController(withIdentifier: "NonClientVC") as? NonClientVC else { return }
                               self.settingstransparentView.isHidden = true
                           
                           let navigation = UINavigationController(rootViewController: nonClientVC)
                           navigation.modalPresentationStyle = .fullScreen
                           self.present(navigation, animated: false, completion: nil)
                           }
                       }
                           
                       else if (indexPath.row == 4){
                            DispatchQueue.main.async {
                           guard let timeSheet = self.storyboard?.instantiateViewController(withIdentifier: "TimeSheetVC") as? TimeSheetVC else { return }
                               self.settingstransparentView.isHidden = true
                           
                         let navigation = UINavigationController(rootViewController: timeSheet)
                       navigation.modalPresentationStyle = .fullScreen
                       self.present(navigation, animated: false, completion: nil)
                           }
                       }
                   default:
                       break
                   }
    }
    
}


//MARK: UIView Controller Transistion Delegate
extension SearchSchdeulesVC : UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transiton.isPresenting = true
        return transiton
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transiton.isPresenting = false
        return transiton
    }
}

//MARK: UIGesture Recognizer Delegate Methods
extension SearchSchdeulesVC : UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view!.isDescendant(of: settingsTableView) {
            return false
        }
        return true
    }
}
