//
//  SessionExpiredController.swift
//  POC
//
//  Created by Admin on 14/05/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class SessionExpiredController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func ok_action(_ sender: Any) {
        self.logout()
    }
}
