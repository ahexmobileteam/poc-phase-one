//
//  AppDelegate.swift
//  POC
//
//  Created by Ajeet N on 03/03/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//


import UIKit
import IQKeyboardManagerSwift
import SwiftKeychainWrapper
import CoreData
import CoreLocation
import Firebase
import UserNotifications
import FirebaseMessaging

var deviceToken = String()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate{

    private let locationManager = CLLocationManager()

    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                guard let rootVC = storyboard.instantiateViewController(withIdentifier: "ViewController") as? ViewController else { return true }
//                let rootNC = UINavigationController(rootViewController: rootVC)
//                self.window?.rootViewController = rootNC
//                self.window?.makeKeyAndVisible()
        //    KeychainWrapper.standard.removeAllKeys()
        
//        FirebaseApp.configure()
//
//        let notificationTypes: UIUserNotificationType = [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound]
//        let pushNotificationSettings = UIUserNotificationSettings(types: notificationTypes, categories: nil)
//
//       UNUserNotificationCenter.current().delegate = self
//
//        application.registerUserNotificationSettings(pushNotificationSettings)
//        application.registerForRemoteNotifications()
        
        
        if #available(iOS 10.0, *) {
                   // For iOS 10 display notification (sent via APNS)
                   FirebaseApp.configure()

                   UNUserNotificationCenter.current().delegate = self
                   Messaging.messaging().delegate = self
                  
                    
                   let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
                   UNUserNotificationCenter.current().requestAuthorization(
                       options: authOptions,
                       completionHandler: {_, _ in })
               } else {
                   let settings: UIUserNotificationSettings =
                       UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
                   application.registerUserNotificationSettings(settings)
               }
        
           let token = Messaging.messaging().fcmToken
           print("FCM token in did fisnish launching is : \(token ?? "")")
           defaults.set(token ?? "" , forKey: "FCMTOKENSAVED")
           application.registerForRemoteNotifications()
        
        
        return true
    }

    private func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        print("DEVICE TOKEN = \(deviceToken)")
        
        let token = Messaging.messaging().fcmToken
        print("The token we have got from fcm is :\(String(describing: token))")
    
    }
    
    private func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print(error)
    }
    
   
    
    private func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        print("The received user info from firebase is :\(userInfo)")
        
        
        if application.applicationState == .active {
            let localNotification = UILocalNotification()
            localNotification.userInfo = userInfo
            localNotification.soundName = UILocalNotificationDefaultSoundName
//            localNotification.alertBody = userIn
            localNotification.fireDate = Date()
            UIApplication.shared.scheduleLocalNotification(localNotification)
        }
        
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        // do your stuff
     
        // this tells the system to show the notification to the user with sound
        completionHandler([.alert, .sound])
    }
    
   //MARK: This Method will be called when user taps on notification from foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                   didReceive response: UNNotificationResponse,
                                   withCompletionHandler completionHandler: @escaping () -> Void) {
           let userInfo = response.notification.request.content.userInfo
           
           
           // Print full message.
           print("tap on on forground app",userInfo)
           
           completionHandler()
       }
  
    
    
    // MARK: UISceneSession Lifecycle
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {

        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    
    }
    
    
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "POC")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }


}

extension Data {
    var hexString: String {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
}

//@available(iOS 10, *)
//extension AppDelegate : UNUserNotificationCenterDelegate {
//    
//    // Receive displayed notifications for iOS 10 devices.
//    func userNotificationCenter(_ center: UNUserNotificationCenter,
//                                willPresent notification: UNNotification,
//                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//        let userInfo = notification.request.content.userInfo
//        
//       
//        // Print full message.
//        print(userInfo)
//        
//        // Change this to your preferred presentation option
//        completionHandler(UNNotificationPresentationOptions.alert)
//    }
//}
