//
//  TimeSheetPSModel.swift
//  POC
//
//  Created by Ajeet N on 24/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import Foundation


typealias timeSheetModel = [TimeSheetPSModel]


// MARK: - WelcomeElement
struct TimeSheetPSModel: Codable {
    let psName, psId: String?
}
