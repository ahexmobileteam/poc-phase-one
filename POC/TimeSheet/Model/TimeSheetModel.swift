//
//  TimeSheetModel.swift
//  POC
//
//  Created by Ajeet N on 26/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import Foundation

typealias TimeSheetModelResponse = [TimeSheetModel]


// MARK: - TimeSheetModel
struct TimeSheetModel: Codable {
    let supervisorName: String?
    let arrivalMileage: Int?
    let reportedEnd: String?
    let travelTime: Int?
    let scheduleStart, procedureCode, psName, reportedStart: String?
    let reportedHours: Double?
    let scheduleEnd: String?
}
