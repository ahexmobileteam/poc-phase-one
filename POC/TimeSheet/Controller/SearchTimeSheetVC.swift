//
//  SearchTimeSheetVC.swift
//  POC
//
//  Created by Ajeet N on 24/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class SearchTimeSheetVC: UIViewController {
    
    private var selectedIndex = Int()
    private var  TimeSheetModelData : timeSheetModel?
    
    @IBOutlet weak var startDateTF: CustomTField!
    @IBOutlet weak var EndDateTF: CustomTField!
    @IBOutlet weak var dropDownTF: DropDownTextField!
    
    private let startDatePicker = UIDatePicker()
    private let endDatePicker = UIDatePicker()
    
    private  var selectedId: String? = nil
    private var selectedValue: String? = nil
    
    
//MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getPSListOfTimeSheet()
        self.loadStartAndEndDatePickers()
        navigationItem.title = "Search Time Sheets"
        
        dropDownTF.dropDown.selectionAction = {(index , item) in
            if index == 0{
                self.selectedId = nil
                self.selectedValue = nil
            }else{
                self.selectedId = self.TimeSheetModelData?[index - 1].psId ?? "0"
                self.selectedValue = self.TimeSheetModelData?[index - 1].psName ?? ""
                }
            self.selectedIndex = index
            self.dropDownTF.text = item
            self.dropDownTF.resignFirstResponder()
            
        }
        
        let alreadySelectedPSCode = defaults.value(forKey: "TIMESHEETNAME") as? String
//        TIMESHEETID
        if alreadySelectedPSCode != nil {
            dropDownTF.text = alreadySelectedPSCode ?? "Select PSCode "
            self.selectedValue = alreadySelectedPSCode ?? "Select PSCode "
        }
        
    NotificationCenter.default.addObserver(self, selector: #selector(sessionExpired), name: Notification.Name("SessionExpired"), object: nil)
        
    }
    
    //MARK: Session Expired
        @objc func sessionExpired() {
               NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "SessionExpired"), object: nil)
               self.logout()
    }
    
    
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    
    //MARK: Load Start and End Date
    private func loadStartAndEndDatePickers() {
        
        let date = Date()
        let start = Calendar.current.date(byAdding: .day, value: -3, to: date)
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let defaultStartDate = formatter.string(from: start!)
        let defaultEndDate = formatter.string(from: date)
        startDatePicker.datePickerMode = .date
        let startDate = defaults.value(forKey: "TIMESHEETSTARTDATE")
        if startDate != nil{
            self.setCustomDate(datePicker: startDatePicker, date: String(describing: startDate ?? ""),textField: startDateTF)
        }else{
            self.setCustomDate(datePicker: startDatePicker, date: defaultStartDate, textField: startDateTF)
            defaults.set(defaultStartDate, forKey: "TIMESHEETSTARTDATE")
        }
        
        let startDateToolbar = UIToolbar()
        startDateToolbar.sizeToFit()
        let startDoneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(startDateDoneAction))
        let startSpaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startCancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelAction))
        startDateToolbar.setItems([startCancelButton,startSpaceButton,startDoneButton], animated: false)
        startDateTF.inputAccessoryView = startDateToolbar
        startDateTF.inputView = startDatePicker
        
        
        endDatePicker.datePickerMode = .date
        let endDate = defaults.value(forKey: "TIMESHEETENDDATE")
        if endDate != nil{
            self.setCustomDate(datePicker: endDatePicker, date: String(describing: endDate ?? ""), textField: EndDateTF)
        }else{
            self.setCustomDate(datePicker: endDatePicker, date: defaultEndDate, textField: EndDateTF)
            defaults.set(defaultEndDate, forKey: "TIMESHEETENDDATE")
        }
        let endDateToolbar = UIToolbar();
        endDateToolbar.sizeToFit()
        let endDoneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(endDateDoneAction));
        let endSpaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let endCancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelAction));
        endDateToolbar.setItems([endCancelButton,endSpaceButton,endDoneButton], animated: false)
        EndDateTF.inputAccessoryView = endDateToolbar
        EndDateTF.inputView = endDatePicker
        
    }
    
    
    //MARK: Start Date Done Button Action
    @objc func startDateDoneAction(){
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let selectedDate = formatter.string(from: startDatePicker.date)
        startDateTF.text = selectedDate
        defaults.set(selectedDate, forKey: "TIMESHEETSTARTDATE")
        startDateTF.layer.borderColor = UIColor.greenColor.cgColor
        self.view.endEditing(true)
    }
    
    //MARK: End Date Done Button Action
    @objc func endDateDoneAction(){
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let selectedDate = formatter.string(from: endDatePicker.date)
        EndDateTF.text = selectedDate
        defaults.set(selectedDate, forKey: "TIMESHEETENDDATE")
        EndDateTF.layer.borderColor = UIColor.greenColor.cgColor
        self.view.endEditing(true)
    }
    
    
    //MARK:Cancel Button Action
    @objc func cancelAction(){
        self.view.endEditing(true)
    }
    
    //MARK: Go Button Action
    @IBAction func goButtonAction(_ sender: Any) {
        
        if !startDateTF.text!.isEmpty{
            defaults.set(startDateTF.text ?? "", forKey: "TIMESHEETSTARTDATE")
        }
        if !EndDateTF.text!.isEmpty{
            defaults.set(EndDateTF.text ?? "", forKey: "TIMESHEETENDDATE")
        }
        defaults.set(self.selectedId ?? "0", forKey: "TIMESHEETID")
        defaults.set(self.selectedValue ?? "",forKey: "TIMESHEETNAME")
        defaults.synchronize()
        
        
        let formatter = DateFormatter()
               formatter.dateFormat = "MM/dd/yyyy"
               
       guard let checkStartDate = formatter.date(from: startDateTF.text ?? "") else { return }
       guard let checkEndDate = formatter.date(from: EndDateTF.text ?? "") else { return }
       switch checkStartDate.compare(checkEndDate) {
       case .orderedAscending:
               print("Start date is earlier than end date")
       case .orderedDescending:
           self.alert(title: "Alert Message", message: "Start date cannot be greater than end date")
           return
//       case .orderedSame:
//           print("Start date and end date are same")
//           self.alert(title: "Alert Message", message: "Start date and end date are same ")

           return
       default:
           print("call API")

               }
        self.navigationController?.popViewController(animated: true)
    
}
    
    
    //MARK: Get PSList of TimeSheet
    func getPSListOfTimeSheet() {
        
        let details = self.getuserDetails()
        
        let PSname = (details.baseURL ?? "")  + API.getListOfPS + (details.dcsID ?? "")
        let sessionID = API.SessionID + (details.sessionID ?? "")
        let GetListURL = PSname + sessionID
        print("Get list of PSLlist URL:\(GetListURL)")
        
        ServiceManager.shared.request(type: timeSheetModel.self, url: GetListURL, method: .get, view: self.view) { (response) in
            print("The response of get Ps list in timesheet screen is  :\(String(describing: response))")
            
            if response?.count == 0 {
                self.alert(title: "Alert Message", message: "NO PSCode List Available")
            }
            else {
                self.TimeSheetModelData = response
                DispatchQueue.main.async {
                    
                    if let psID = self.TimeSheetModelData?.map({$0.psName ?? ""}) {
                        self.dropDownTF.datasource = ["Select PSCode"] + psID
                    }
                }
            }
        }
    }
    
}


////MARK: Set Custom Date Picker
//extension UIViewController {
//
//    func setCustomDate(datePicker: UIDatePicker,date: String,textField: CustomTField){
//        let formatter = DateFormatter()
//        formatter.dateFormat = "MM/dd/yyyy"
//        let finalDate = formatter.date(from: String(describing: date))
//        datePicker.setDate(finalDate!, animated: false)
//        textField.text = formatter.string(from: finalDate!)
//    }
//}
