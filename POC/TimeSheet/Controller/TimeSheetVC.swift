//
//  TimeSheetVC.swift
//  POC
//
//  Created by Ajeet N on 24/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//


import UIKit


class TimeSheetVC: UIViewController {

    
    @IBOutlet weak var settingsTransparentView: UIView!
    
    @IBOutlet weak var settingsTableView: UITableView! {
        didSet {
            settingsTableView.delegate = self
            settingsTableView.dataSource = self
            self.settingsTableView.layer.cornerRadius = 5.0
        }
    }
    
    let settingsArray = ["Today's Schedules","Search Schedules","Report UnScheduled Vist","Non-Client Shift","Time Sheet"]
    
    
    private let transiton = SlideInTransition()

    @IBOutlet weak var startDateEndDateLabel: UIView!
    @IBOutlet weak var totoalHrsLabel: UIView!
    
    @IBOutlet weak var startendlabelinside: UILabel!
    
    private var expand = [Bool]()
    private var TimeSheetModelData : [TimeSheetModel]?
    
    @IBOutlet weak var emptyStatusLabel: UILabel!
    
    @IBOutlet weak var totalhorsLabelinside: UILabel!
    @IBOutlet weak var timeSheetTableViewObj: UITableView! {
        didSet {
            timeSheetTableViewObj.delegate = self
            timeSheetTableViewObj.dataSource = self
        }
    }
     
    
//MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        settingsTransparentView.isHidden = true

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
               tapGesture.delegate = self
               settingsTransparentView.addGestureRecognizer(tapGesture)
        
        
        
    let searchTimeSheet = UIBarButtonItem(image: UIImage(named: "Search_01"), style: .plain, target: self, action: #selector(SearchTimeSheetScreen))
        
        
    let settings = UIBarButtonItem(image: UIImage(named: "Right Menu"), style: .plain, target: self, action: #selector(settingsPopUP))
        self.navigationItem.rightBarButtonItems = [settings,searchTimeSheet]
        
        
    self.configureNavigationBar(title: "TimeSheet")
    
self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 20)!,NSAttributedString.Key.foregroundColor : UIColor.white]
        
        
        startDateEndDateLabel.layer.cornerRadius = 25.0
        startDateEndDateLabel.layer.masksToBounds = false
        
        totoalHrsLabel.layer.cornerRadius = 25.0
        totoalHrsLabel.layer.masksToBounds = false
        
        let staticTotalHrs = "Total Hrs:"
        totalhorsLabelinside.text = "\(String(describing: staticTotalHrs))"
        

        let sidemenuButton = UIBarButtonItem(image: UIImage(named: "Menu"), style: .plain, target: self, action: #selector(openSideMenu))
                   self.navigationItem.leftBarButtonItem = sidemenuButton

        
        // TIMESHEETSTARTDATE
       // TIMESHEETENDDATE
        
       
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(sessionExpired), name: Notification.Name("SessionExpired"), object: nil)
}
    
    //MARK: Tap Gesture Action
       @objc func handleTap(){
           settingsTransparentView.isHidden = true
       }
    
    
    @objc func openSideMenu() {
           
           guard let menuViewController = self.storyboard?.instantiateViewController(withIdentifier: "MenuTableViewController")  as? MenuTableViewController else { return }
           menuViewController.modalPresentationStyle = .overCurrentContext
           menuViewController.transitioningDelegate = self
          // menuViewController.identifyController = .isSchedule
           self.present(menuViewController, animated: true)
           
       }
       
    
    
    @objc func settingsPopUP() {
          print("ddd")
          self.settingsTransparentView.isHidden = false
          
      }
    
    
//MARK: Session Expired
        @objc func sessionExpired() {
               NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "SessionExpired"), object: nil)
               self.logout()
        }
    
    
//MARK: Search Time Sheet Screen
     @objc func SearchTimeSheetScreen(){
        guard let SearchTimeSheetObj = self.storyboard?.instantiateViewController(withIdentifier: "SearchTimeSheetVC") as? SearchTimeSheetVC else { return }
        self.navigationController?.pushViewController(SearchTimeSheetObj, animated: true)
    }
    
    
//MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.timeSheetAPICalling()
        
        
        
        let date = Date()
               let start = Calendar.current.date(byAdding: .day, value: -3, to: date)
               let formatter = DateFormatter()
               formatter.dateFormat = "MM/dd/yyyy"
               let defaultStartDate = formatter.string(from: start!)
               let defaultEndDate = formatter.string(from: date)
               
              
               let timeSheetDate = defaults.value(forKey: "TIMESHEETSTARTDATE") as? String
               
               if timeSheetDate == nil {
//                   startendlabelinside.text = defaultStartDate
               }
               else {
//                   startendlabelinside.text = timeSheetDate ?? ""
               }
               
               let timeSheetEndDate = defaults.value(forKey: "TIMESHEETENDDATE") as? String
        
               if timeSheetEndDate == nil {
                startendlabelinside.text = "\(String(describing: defaultStartDate )) - \(String(describing: defaultEndDate ))"
//                   startendlabelinside.text = defaultEndDate
               }
               else {
                 startendlabelinside.text = "\(String(describing: timeSheetDate ?? "")) - \(String(describing: timeSheetEndDate ?? ""))"
//                   startendlabelinside.text = timeSheetEndDate ?? ""
               }
        
        
        
        
        
        
//        if self.TimeSheetModelData?.count == 0 {
//            DispatchQueue.main.async {
//                self.startDateEndDateLabel.isHidden = true
//                self.totoalHrsLabel.isHidden = true
//            }
//
//        }
//        else {
//
//            let receivedStartDate = defaults.value(forKey: "TIMESHEETSTARTDATE")
//            let receivedEndDate = defaults.value(forKey: "TIMESHEETENDDATE")
//
//        if receivedStartDate != nil && receivedEndDate != nil {
//                       startendlabelinside.text = "\(String(describing: receivedStartDate ?? "")) - \(String(describing: receivedEndDate ?? ""))"
//        }
//
//    }
        
        
}
    
    
    
//MARK: Time Sheet API Calling
    func timeSheetAPICalling() {
        
        let receivedPSId = defaults.value(forKey: "TIMESHEETID") as? String
        
        let staticPSID = String(describing: receivedPSId ?? "0")
        var receivedPSID = defaults.value(forKey: "TIMESHEETID") as? String
        print("Received psId in timesheet vc is :\(String(describing: receivedPSID))")
        
        
      let date = Date()
      let start = Calendar.current.date(byAdding: .day, value: -3, to: date)
      let formatter = DateFormatter()
      formatter.dateFormat = "MM/dd/yyyy"
      let defaultStartDate = formatter.string(from: start!)
      let defaultEndDate = formatter.string(from: date)
        
        
        
        if receivedPSID != nil {
        }
        else {
            receivedPSID = "0"
        }
        
        var startDateRecieved = defaults.value(forKey: "TIMESHEETSTARTDATE") as? String
        if startDateRecieved != nil {
            
        }
        else {
            startDateRecieved = defaultStartDate
        }
        
        var endDataReceived = defaults.value(forKey: "TIMESHEETENDDATE") as? String
        if endDataReceived != nil {
            
        }
        else {
            endDataReceived = defaultEndDate
        }
        
        let details = self.getuserDetails()
        
        let url = (details.baseURL ?? "") + API.GetTimeSheet + (details.dcsID ?? "")
        let sessionID = API.SessionID + (details.sessionID ?? "")
        let psID = API.PSID + (staticPSID )
        let startDate = API.StartDate + (startDateRecieved ?? "")
        let EndDate = API.EndDate + (endDataReceived ?? "")
        
    let timeSheeturl = url + sessionID + psID + startDate + EndDate
        print("The TimeSheet url is :\(String(describing: timeSheeturl))")
    ServiceManager.shared.request(type: TimeSheetModelResponse.self, url: timeSheeturl, method: .get, view: self.view) { (response) in
        print("the response of timeSheet is :\(String(describing: response))")
        
        if response?.count == 0 {
            print("No records Found")
            
            DispatchQueue.main.async {
                self.timeSheetTableViewObj.isHidden = true
                self.emptyStatusLabel.isHidden = false
                self.emptyStatusLabel.text = "No TimeSheet Data Found"
                self.startDateEndDateLabel.isHidden = true
                self.totoalHrsLabel.isHidden = true
                self.startDateEndDateLabel.isHidden = true
                self.totoalHrsLabel.isHidden = true
            }
            
        }
        else {
            DispatchQueue.main.async {
                if response?.count ?? 0 != 0{
                    let total = response?.map({ $0.reportedHours ?? 0.0 })
                    let totalHours = total?.sum()
                    self.totalhorsLabelinside.text = "Total Hrs: \((totalHours?.rounded() ?? 0.0))"
                    for _ in 0..<response!.count{
                        self.expand.append(false)
                    }
                }
                self.emptyStatusLabel.isHidden = true
                self.startDateEndDateLabel.isHidden = false
                self.totoalHrsLabel.isHidden = false
                self.timeSheetTableViewObj.isHidden = false
                self.TimeSheetModelData = response
                self.timeSheetTableViewObj.reloadData()
            }
            
        }
    }
}
    
    @objc func isExpand(sender: UIButton){
        
       
        self.expand[sender.tag] ? (self.expand[sender.tag] = false) : (self.expand[sender.tag] = true)
        self.timeSheetTableViewObj.reloadData()
    }
    
}

//MARK: UITableView Delegate and Datasource Methods
extension TimeSheetVC : UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch tableView {
            
        case timeSheetTableViewObj:
            return TimeSheetModelData?.count ?? 0

        case settingsTableView:
            return settingsArray.count
            
        default:
            return 0
        }
        
        
        
    }

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

    
    switch tableView {
    case timeSheetTableViewObj:
         
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "TimeSheetCell", for: indexPath) as? TimeSheetCell else { return UITableViewCell() }
            cell.selectionStyle = .none
            cell.mainName.text = self.TimeSheetModelData?[indexPath.row].psName ?? "Not Available"
            indexPath.row == 0 ? (cell.topView.isHidden = true) : (cell.topView.isHidden = false)
           
            if let startDate = self.TimeSheetModelData?[indexPath.row].reportedStart {
                cell.leftSideDateLabel.text = self.getConvertedDates(string: startDate)
               print(self.getConvertedDates(string: startDate))
            }
            
            if expand[indexPath.row] {
             //top details background  color green
                //details height 123
                // ic_drop
                // ic_drop_up
                cell.mainCardViewObj.backgroundColor = UIColor.greenColor
                cell.detailViewObj.backgroundColor = UIColor.greenColor
                cell.detailViewHeightConstrants.constant = 116
                cell.detailViewObj.isHidden = false
       // cell.expanAction.setImage(UIImage(named: "ic_drop_up")?.withRenderingMode(.alwaysOriginal))  //expandeed image
     cell.expanAction.setImage(UIImage(named: "ic_drop_up")?.withRenderingMode(.alwaysOriginal), for: .normal)
                
            }
            else {
                //top details background  color while
                //details  hehight 0
                cell.mainCardViewObj.backgroundColor = .groupTableViewBackground
                cell.detailViewObj.backgroundColor = UIColor.white
                cell.detailViewHeightConstrants.constant = 0
                cell.detailViewObj.isHidden = true
              //  cell.expanAction.setImage(UIImage(named: "ic_drop"), for: .normal)//normal image
       cell.expanAction.setImage(UIImage(named: "ic_drop")?.withRenderingMode(.alwaysOriginal), for: .normal)
                
            
            }
            cell.expanAction.tag = indexPath.row
            cell.expanAction.addTarget(self, action: #selector(isExpand(sender:)), for: .touchUpInside)
            if let reportStart = self.TimeSheetModelData?[indexPath.row].reportedStart {
               cell.reportStartButton.setTitle( "\(String(reportStart.suffix(8)))", for: .normal)
            }
                
            if let reportEnd = self.TimeSheetModelData?[indexPath.row].reportedEnd {
                cell.reportEndButton.setTitle("\(String(reportEnd.suffix(8)))", for: .normal)
            }
                
        cell.payableTravelTimeButton.setTitle(String(describing: self.TimeSheetModelData?[indexPath.row].travelTime ?? 0), for: .normal)
            cell.pinView.layer.borderColor = UIColor.greenColor.cgColor

        cell.expanAction.setImage(UIImage(named: "Dropdown")?.withRenderingMode(.alwaysOriginal), for: .normal)
        cell.serviceButton.setTitle(String(describing: self.TimeSheetModelData?[indexPath.row].procedureCode ?? "0"), for: .normal)
           

            
            return cell
                
    case settingsTableView:
        
        
        guard  let settingscell = tableView.dequeueReusableCell(withIdentifier: "TimeSheetSettingCell", for: indexPath) as? TimeSheetSettingCell else {
                   return UITableViewCell() }
        settingscell.timeSheetNameLabel.text = settingsArray[indexPath.row]
               
               if indexPath.row == 4 {
                   settingscell.timeSheetNameLabel.textColor = UIColor(displayP3Red: 116/255.0, green: 177/255.0, blue: 243/255.0, alpha: 1)
               }
               else {
                   settingscell.timeSheetNameLabel.textColor = .black
               }
               
               settingscell.selectionStyle = .none
               return settingscell
        
    default:
       return UITableViewCell()
    }
    
    
   
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch tableView {
        case settingsTableView:
            
            if indexPath.row == 0 {
                
               DispatchQueue.main.async {
                          guard let searchSchedulesVC = self.storyboard?.instantiateViewController(withIdentifier: "scheduleExceptionsVC") as? ScheduleExceptionsVC else { return }
                          self.settingsTransparentView.isHidden = true
                          let navigation = UINavigationController(rootViewController: searchSchedulesVC)
                          navigation.modalPresentationStyle = .fullScreen
                          self.present(navigation, animated: false, completion: nil)
                      }
            }
                                      
            else if (indexPath.row == 1) {
                           
                DispatchQueue.main.async {
                              guard let searchSchedulesVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchSchdeulesVC") as? SearchSchdeulesVC else { return }
                              self.settingsTransparentView.isHidden = true
                              let navigation = UINavigationController(rootViewController: searchSchedulesVC)
                              navigation.modalPresentationStyle = .fullScreen
                              self.present(navigation, animated: false, completion: nil)
                }
                
                                      
                }
                                      
            else if (indexPath.row == 2) {
                                      
               DispatchQueue.main.async {
              guard let ReportUnScheduledVC = self.storyboard?.instantiateViewController(withIdentifier: "ReportUnScheduledVC") as? ReportUnScheduledVC else { return }
                  self.settingsTransparentView.isHidden = true
              let navigation = UINavigationController(rootViewController: ReportUnScheduledVC)
              navigation.modalPresentationStyle = .fullScreen
              self.present(navigation, animated: false, completion: nil)
              }
                        }
                                      
            else if (indexPath.row == 3) {
                
               DispatchQueue.main.async {
              guard  let nonClientVC = self.storyboard?.instantiateViewController(withIdentifier: "NonClientVC") as? NonClientVC else { return }
                  self.settingsTransparentView.isHidden = true
              
              let navigation = UINavigationController(rootViewController: nonClientVC)
              navigation.modalPresentationStyle = .fullScreen
              self.present(navigation, animated: false, completion: nil)
              }
                
            }
                                      
            else if (indexPath.row == 4){
                
          
      }
  default:
      break
  }
        
               }
               
        
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch tableView {
        case timeSheetTableViewObj:
             return UITableView.automaticDimension
            
        case settingsTableView:
            return 40
            
        default:
            return UITableView.automaticDimension
        }
        

    }

}


extension Sequence where Element: AdditiveArithmetic {
    func sum() -> Element { reduce(.zero, +) }
}


//MARK: UIView Controller Transistion Delegate
extension TimeSheetVC : UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transiton.isPresenting = true
        return transiton
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transiton.isPresenting = false
        return transiton
    }
}

//MARK: UIGesture Recognizer Delegate Methods
extension TimeSheetVC : UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view!.isDescendant(of: settingsTableView) {
            return false
        }
        return true
    }
}
