//
//  TimeSheetSettingCell.swift
//  POC
//
//  Created by Ajeet N on 10/05/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class TimeSheetSettingCell: UITableViewCell {

    @IBOutlet weak var timeSheetNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
