//
//  TimeSheetCell.swift
//  POC
//
//  Created by Ajeet N on 26/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//


import UIKit

class TimeSheetCell: UITableViewCell {

    @IBOutlet weak var mainCardViewObj: CustomView!
    @IBOutlet weak var leftSideDateLabel: UILabel!
    @IBOutlet weak var pinView: CustomView!
    @IBOutlet weak var mainName: UILabel!
    @IBOutlet weak var mainHoursLabel: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var topCardViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var detailViewHeightConstrants: NSLayoutConstraint!
    
    @IBOutlet weak var topPinViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var reportStartButton: UIButton!
    @IBOutlet weak var reportEndButton: UIButton!
    @IBOutlet weak var payableTravelTimeButton: UIButton!
    @IBOutlet weak var serviceButton: UIButton!
    
    @IBOutlet weak var expanAction: UIButton!
    @IBOutlet weak var detailViewObj: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
