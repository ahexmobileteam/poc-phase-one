//
//  ViewController.swift
//  POC
//
//  Created by Ajeet N on 03/03/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var backgroundBlueView: UIView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var dcsCodeTextField: UITextField!
    
    var callInFlag: Int?
    
    private var details = [UserDetails]()
    //MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
       // self.pathCurvedForView(givenView: backgroundBlueView, curvedPercent: 0.9)
        
        NotificationCenter.default.addObserver(self, selector: #selector(sessionExpired), name: Notification.Name("SessionExpired"), object: nil)
        
 
        let logged = defaults.value(forKey: "logged")
        
        if logged == nil {

          let details = UserDetails(context: context)
          details.baseURL = "http://poc.aquilasoftware.com/pocextacc-webservices_phase2/telephony/"
          details.sessionID = ""
          details.dcsID = ""
          details.empID = ""
          self.saveContext()
              
        defaults.setValue(true, forKey: "logged")
        }
            
     
        
    }
    
    //MARK: Session Expired
         @objc func sessionExpired() {
                NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "SessionExpired"), object: nil)
                self.logout()
    }

    //MARK: Curve for Background View
    func pathCurvedForView(givenView: UIView, curvedPercent:CGFloat) ->UIBezierPath {
        let arrowPath = UIBezierPath()
        arrowPath.move(to: CGPoint(x:0, y:0))
        arrowPath.addLine(to: CGPoint(x:givenView.bounds.size.width, y:0))
        arrowPath.addLine(to: CGPoint(x:givenView.bounds.size.width, y:givenView.bounds.size.height - (givenView.bounds.size.height*curvedPercent)))
        arrowPath.addQuadCurve(to: CGPoint(x:0, y:givenView.bounds.size.height - (givenView.bounds.size.height*curvedPercent)), controlPoint: CGPoint(x:givenView.bounds.size.width/2, y:givenView.bounds.size.height))
        arrowPath.addLine(to: CGPoint(x:0, y:0))
        arrowPath.close()
        return arrowPath
        
    }
    
    //MARK: Continue Button Action
    @IBAction func continueButtonAction(_ sender: Any) {
        
        dcsCodeTextField.resignFirstResponder()
        if dcsCodeTextField.text!.isEmpty {
            self.alert(title: "Please review your information", message: "DCS Code can't be empty")
        }
        else {
      continueServiceCall()
        }
      
    }
       

    //MARK: Config Button Action Bottom Side
    @IBAction func configButtonAction(_ sender: Any) {
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute:  {
//            guard let configController = self.storyboard?.instantiateViewController(withIdentifier: "LoginConfigVC") as? LoginConfigVC else { return }
//            self.navigationController?.pushViewController(configController, animated: true)
//        })
         
}
    
    
   //MARK: Config Button Action Top Side
@IBAction func configTopButton(_ sender: Any) {
    
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute:  {
               guard let configController = self.storyboard?.instantiateViewController(withIdentifier: "LoginConfigVC") as? LoginConfigVC else { return }
               self.navigationController?.pushViewController(configController, animated: true)
    })
}

    
   //MARK: Continue Service Call
    func continueServiceCall()  {
        //Display Nation and Capital
        let request : NSFetchRequest<UserDetails> = UserDetails.fetchRequest()
               do
               {
                   details = try context.fetch(request)
                print(details)
                            
               }catch{
                  // print("While while fetching cards")
               }
        
        if details.count == 0 {
            self.toast(msgString: "Configure the url.", view: self.view)
            return
        }
        let dcsCode = self.dcsCodeTextField.text ?? ""
        let detials = self.getuserDetails()
        let authenticationURl = (detials.baseURL ?? "") +  API.AuthenticationURL + dcsCode
        print("The authentication url is :/", authenticationURl)
        
        ServiceManager.shared.request(type: AuthenticationModel.self, url: authenticationURl, method: .get, view: self.view) { (response) in
            print("The response of authetication url is :/", response!)
            
            if response?.dcsId ?? 0 > 0 {
                if self.details.count !=  0{
                    
                    let request : NSFetchRequest<UserDetails> = UserDetails.fetchRequest()
                    do
                    {
                        let dcsEmpIDString = response?.sessionId ?? ""
                        let dcsEmpCode = String(dcsEmpIDString.prefix(6))
                        self.callInFlag = response?.dcsCallInFlag ?? 0
                        let saveDetials = try context.fetch(request)
                        saveDetials[0].setValue(response?.sessionId ?? "", forKey: "sessionID")
                        saveDetials[0].setValue(dcsEmpCode, forKey: "empID")
                        saveDetials[0].setValue(String(response?.dcsId ?? 0), forKey: "dcsID")
                        defaults.set(response?.sessionTime ?? 0, forKey: kSession)
                        self.saveContext()
                        }catch{
                       // print("While while fetching cards")
                    }
                }
                
            if (response?.dcsPIN == nil) {

                let request : NSFetchRequest<UserDetails> = UserDetails.fetchRequest()
                                 do
                                 {
                                     let dcsEmpIDString = response?.sessionId ?? ""
                                     let dcsEmpCode = String(dcsEmpIDString.prefix(6))
                                     self.callInFlag = response?.dcsCallInFlag ?? 0
                                     let saveDetials = try context.fetch(request)
                                     saveDetials[0].setValue(response?.sessionId ?? "", forKey: "sessionID")
                                     saveDetials[0].setValue(dcsEmpCode, forKey: "empID")
                                     saveDetials[0].setValue(String(response?.dcsId ?? 0), forKey: "dcsID")
                                     defaults.set(response?.sessionTime ?? 0, forKey: kSession)
                                     self.saveContext()
                                     }catch{
                                    // print("While while fetching cards")
                                 }
                
            DispatchQueue.main.async {
                                
                guard let resetPinVc = self.storyboard?.instantiateViewController(withIdentifier: "ResetPinVC")  as? ResetPinVC  else { return }
                resetPinVc.receivedDcsId = response?.dcsId ?? 0
            let dcsEmpIDString = response?.sessionId ?? ""
            let dcsEmpCode = String(dcsEmpIDString.prefix(6))
                resetPinVc.receivedEmpCode = dcsEmpCode
                resetPinVc.receivedDcsPin = self.dcsCodeTextField.text ?? ""
                resetPinVc.receovedCallFlag = response?.dcsCallInFlag
                resetPinVc.receivedSessionId = response?.sessionId ?? ""
                self.navigationController?.pushViewController(resetPinVc, animated: true)
                        return
                    }
                }
                
//                keychain.set(response?.sessionId ?? "", forKey: kSessionID)
//                keychain.set(String(response?.dcsId ?? 0), forKey: KdcsID)
//
//            let dcsEmpIDString = response?.sessionId ?? ""
//            let dcsEmpCode = String(dcsEmpIDString.prefix(6))
//            print("The dcsEmpcode in view controller is  :\(String(describing: dcsEmpCode))")
//            defaults.set(dcsEmpCode, forKey: "GLOBALDCSEMPCODE")
                
            else {
                DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                                   guard let pinController = self.storyboard?.instantiateViewController(withIdentifier: "LoginWithPinVC")  as? LoginWithPinVC  else { return }
                                   pinController.dcsCodeValue = self.dcsCodeTextField.text
                                   pinController.receivedFlagValue = self.callInFlag
                                   self.navigationController?.pushViewController(pinController, animated: true)
                                           })
                }
                
            }
                
           
                
            else {
                 print("The continue service call api is:\(String(describing: response))")
                self.alert(title: "Alert Message", message: "Invalid DCS Code")
                
            }
        }
    }
    
    func saveContext()
    {
            do
            {
                try context.save()
            }catch{
               // print("Error while saving card")
            }
    }
}


