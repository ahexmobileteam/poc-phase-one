//
//  AuthenticationModel.swift
//  POC
//
//  Created by Ajeet N on 07/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//


import Foundation

// MARK: - Authentication Model

struct AuthenticationModel: Codable {
    
    let dcsId: Int?
    let dcsCallInFlag: Int?
    let sessionTime: Int?
    let sessionId : String?
    let dcsPIN: String?
    
    
}
