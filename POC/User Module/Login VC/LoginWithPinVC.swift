//
//  LoginWithPinVC.swift
//  POC
//
//  Created by Ajeet N on 06/03/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//


import UIKit


class LoginWithPinVC: UIViewController {
    
    @IBOutlet weak var dcsCodeTextfield: UITextField!
    @IBOutlet weak var firstTF: UITextField!
    @IBOutlet weak var secondTF: UITextField!
    @IBOutlet weak var thirdTF: UITextField!
    @IBOutlet weak var fourthTF: UITextField!
    @IBOutlet weak var fifthButtonTD: UIButton!
    
    @IBOutlet weak var fifithOutlet: UIButton!
    
    var dcsCodeValue : String?
    var receivedFlagValue : Int?
    
//MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dcsCodeTextfield.text = dcsCodeValue
        self.dcsCodeTextfield.isUserInteractionEnabled = false
        firstTF.delegate = self
        secondTF.delegate = self
        thirdTF.delegate = self
        fourthTF.delegate = self
        defaults.set(dcsCodeValue ?? "", forKey: kUserName)
        NotificationCenter.default.addObserver(self, selector: #selector(sessionExpired), name: Notification.Name("SessionExpired"), object: nil)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        firstTF.becomeFirstResponder()
    }
    
    //MARK: Session Expired
        @objc func sessionExpired() {
               NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "SessionExpired"), object: nil)
               self.logout()
    }
    
    //MARK: Login Button Action
    @IBAction func loginButtonAction(_ sender: Any) {
        
        fourthTF.resignFirstResponder()
        if (firstTF.text == "" || secondTF.text == "" || thirdTF.text == "" || fourthTF.text == "" ){
            self.alert(title: "Alert Message", message: "Please enter DCS PIN")

        }
        else {
            loginAPI()
        }
    }
    
    //MARK: Login API Calling
    func loginAPI()  {
        
        if self.firstTF.text == "" || self.secondTF.text == "" || self.thirdTF.text == "" || self.fourthTF.text == "" {
//            self.alert(title: "Alert Message", message: "Please enter DCS PIN")
            self.toast(msgString: "Please enter correct password", view: self.view)
            return
        }
        
        let passwordTF =  self.firstTF.text! + self.secondTF.text! + self.thirdTF.text! + self.fourthTF.text!
        let passwordValue = passwordTF
        
        let details = self.getuserDetails()
        let BaseURl = (details.baseURL ?? "") + API.LoginURL
        let ids = (dcsCodeValue ?? "") + (API.DCSPIN + passwordValue)
        let sessionIDURL = API.SessionID + (details.sessionID ?? "")

        
        let loginURL = BaseURl + ids + sessionIDURL
        print("The login with pin url is :\(loginURL)")

        
    ServiceManager.shared.request(type:LoginModel.self, url: loginURL, method: .get, view: self.view) { [weak self] (response) in
            print("The response of login with pin vc is :\(String(describing: response))")
            
            if response?.dcsId ?? 0 > 0 {
                self?.toast(msgString: "login successful", view: self?.view ?? UIView())
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
                    guard let homeController = self?.storyboard?.instantiateViewController(withIdentifier: "scheduleExceptionsVC") as? ScheduleExceptionsVC else  { return }
                    homeController.receivedCallInFlage = self?.receivedFlagValue
                    let navigationControler = UINavigationController(rootViewController: homeController)
                    navigationControler.modalPresentationStyle = .fullScreen
                    self?.present(navigationControler, animated: false, completion: nil)
                }
            }
                
            else if response?.dcsId == 0 {
                self?.alert(title: "Alert Message", message: "Please enter correct pin")
            }
                
            else  {
                self?.alert(title: "Alert Message", message: "Please Check your details")
            }
        }
    }
    
//MARK: Hide and Show Password
    @IBAction func fifthButtonAction(_ sender: Any) {
        
        if  firstTF.isSecureTextEntry {
            
            fifthButtonTD.setImage(UIImage(named: "Eye"), for: .normal)
            firstTF.isSecureTextEntry = false
            secondTF.isSecureTextEntry = false
            thirdTF.isSecureTextEntry = false
            fourthTF.isSecureTextEntry = false
        }
        else  {
            fifthButtonTD.setImage(UIImage(named: "Eye_01"), for: .normal)
            firstTF.isSecureTextEntry = true
            secondTF.isSecureTextEntry = true
            thirdTF.isSecureTextEntry = true
            fourthTF.isSecureTextEntry = true
        }
    }
    
    
//MARK: Config Button Action Top Side
    @IBAction func configTopButtonAction(_ sender: Any) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute:  {
            guard let configController = self.storyboard?.instantiateViewController(withIdentifier: "LoginConfigVC") as? LoginConfigVC else { return }
            self.navigationController?.pushViewController(configController, animated: true)
           })
    }
}


//MARK: UITextfield Delegate and Datasource Methods
extension LoginWithPinVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if ((textField.text?.count)! < 1 ) && (string.count > 0) {
            
            if textField == firstTF {
                firstTF.layer.borderColor = UIColor.greenColor.cgColor
                firstTF.layer.borderWidth = 1.0
                firstTF.layer.cornerRadius = 5.0
                firstTF.layer.masksToBounds = false
                secondTF.becomeFirstResponder()
            }
            
            if textField == secondTF {
                secondTF.layer.borderColor = UIColor.greenColor.cgColor
                secondTF.layer.borderWidth = 1.0
                secondTF.layer.cornerRadius = 5.0
                secondTF.layer.masksToBounds = false
               thirdTF.becomeFirstResponder()
            }
            
            if textField == thirdTF {
                thirdTF.layer.borderColor = UIColor.greenColor.cgColor
                thirdTF.layer.borderWidth = 1.0
                thirdTF.layer.cornerRadius = 5.0
                thirdTF.layer.masksToBounds = false
               fourthTF.becomeFirstResponder()
            }
            
            if textField == fourthTF {
                fourthTF.layer.borderColor = UIColor.greenColor.cgColor
                fourthTF.layer.borderWidth = 1.0
                fourthTF.layer.cornerRadius = 5.0
                fourthTF.layer.masksToBounds = false
                fourthTF.becomeFirstResponder()
            }
            
            textField.text = string
            return false
        }
        
        
        else if ((textField.text?.count)! >= 1) && (string.count == 0) {
            
            if textField == secondTF {
                secondTF.layer.borderColor = UIColor.lightGray.cgColor
                secondTF.layer.borderWidth = 1.0
                secondTF.layer.cornerRadius = 5.0
                secondTF.layer.masksToBounds = false
                firstTF.becomeFirstResponder()
            }
            if textField == thirdTF {
                thirdTF.layer.borderColor = UIColor.lightGray.cgColor
                thirdTF.layer.borderWidth = 1.0
                thirdTF.layer.cornerRadius = 5.0
                thirdTF.layer.masksToBounds = false
               secondTF.becomeFirstResponder()
            }
            if textField == fourthTF {
                firstTF.layer.borderColor = UIColor.lightGray.cgColor
                firstTF.layer.borderWidth = 1.0
                firstTF.layer.cornerRadius = 5.0
                firstTF.layer.masksToBounds = false
              thirdTF.becomeFirstResponder()
            }
            if textField == firstTF {
                firstTF.layer.borderColor = UIColor.lightGray.cgColor
                firstTF.layer.borderWidth = 1.0
                firstTF.layer.cornerRadius = 5.0
                firstTF.layer.masksToBounds = false
               firstTF.becomeFirstResponder()
            }
                      
            textField.text = ""
            return false
        }
        
        else if (textField.text?.count)! >= 1 {
            textField.text = string
            return false
        }
        
        return true
    }
    
}
