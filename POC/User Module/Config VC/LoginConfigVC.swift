//
//  LoginConfigVC.swift
//  POC
//
//  Created by Ajeet N on 06/03/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit
import CoreData
class LoginConfigVC: UIViewController {

   
    @IBOutlet weak var serverConfigTF: UITextField!
    private var userdetails = [UserDetails]()
    
    
    //MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        let request : NSFetchRequest<UserDetails> = UserDetails.fetchRequest()
        do{
             userdetails = try context.fetch(request)
        }catch{
            
        }
        if userdetails.count != 0{
            serverConfigTF.text = userdetails[0].baseURL ?? ""
        }else{
           self.serverConfigTF.text = "http://poc.aquilasoftware.com/pocextacc-webservices_phase2/telephony/"
        }
        
      }
    
    //MARK: Save and Next Button Action
    @IBAction func saveNextButtonAcgion(_ sender: Any) {
        
        if serverConfigTF.text!.isEmpty{
                   self.alert(title: "Web service url can't be empty", message: "")
               }else{
                   saveURL()
               }
    }
    
    private func saveURL(){
           if serverConfigTF.text!.contains("/telephonyCheck"){
               let url = serverConfigTF.text!.replacingOccurrences(of: "/telephonyCheck", with: "/")
               self.save(url: url)
           }else{
               save(url: serverConfigTF.text!)
           }
       }
       
    
    private func save(url: String){
        
              let urll = (self.serverConfigTF.text ?? "") + ("telephonyCheck")
          ServiceManager.shared.request(url: urll, method: .get, view: self.view) { (statusCode) in
                  
                  if statusCode == 200 {
                    
                    DispatchQueue.main.async {
                        
                        if self.userdetails.count != 0{
                                    let request : NSFetchRequest<UserDetails> = UserDetails.fetchRequest()
                                    do{
                                        let details = try context.fetch(request)
                                        details[0].setValue(url, forKey: "baseURL")
                                        details[0].setValue("", forKey: "dcsID")
                                        details[0].setValue("", forKey: "empID")
                                        details[0].setValue("", forKey: "sessionID")
                                        self.saveContext()
                                    }catch{
                                        
                                    }
                                }
                                else{
                                    let details = UserDetails(context: context)
                                    details.baseURL = url
                                    details.sessionID = ""
                                    details.dcsID = ""
                                    details.empID = ""
                                    self.saveContext()
                                }
                                  
                        self.navigationController?.popViewController(animated: true)
                        
        
                        
                    }
                   
                  }
                  else {
                      DispatchQueue.main.async {
                        self.toast(msgString: "configuration test failed. Try again", view: self.view)
                        return
                      }
                  }
              }
                          
       
                
       
    }
    //MARK:- Save Context
    func saveContext()
    {
            do
            {
                try context.save()
            }catch{
               // print("Error while saving card")
            }
    }
    

    //MARK: Test Button Action
    @IBAction func testButtonAction(_ sender: Any) {
        if serverConfigTF.text!.isEmpty {
            self.alert(title: "Web service url can't be empty", message: "")
        } else {
             testURL()
        }
        
    }
    
    //MARK: Test Config URL
    private func testURL() {
        
        let urll = (self.serverConfigTF.text ?? "") + ("telephonyCheck")
    ServiceManager.shared.request(url: urll, method: .get, view: self.view) { (statusCode) in
            
            if statusCode == 200 {
                DispatchQueue.main.async {
            self.toast(msgString: "Configuration test successfull", view: self.view)
            }
            }
            else {
                DispatchQueue.main.async {
                    self.toast(msgString: "configuration test failed.Try again", view: self.view)
                }
            }
        }
        
    }
            
    
}
