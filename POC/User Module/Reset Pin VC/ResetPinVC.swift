//
//  ResetPinVC.swift
//  POC
//
//  Created by Ajeet N on 06/03/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//


import UIKit


struct ResetPinModel: Codable {
    let dcsId : Int?
}

class ResetPinVC: UIViewController {

    @IBOutlet weak var dcsCodeTextField: UITextField!
    @IBOutlet weak var firstTF: UITextField!
    @IBOutlet weak var secondTF: UITextField!
    @IBOutlet weak var thirdTF: UITextField!
    @IBOutlet weak var fourthTF: UITextField!
    

    var receovedCallFlag : Int?
    
    var receivedDcsPin: String?
    @IBOutlet weak var eyeIconForpin: UIButton!
    @IBOutlet weak var eyeIconForConfirmPin: UIButton!
    
        
    @IBOutlet weak var fifthTF: UITextField!
    @IBOutlet weak var sixthTF: UITextField!
    @IBOutlet weak var seventhTF: UITextField!
    @IBOutlet weak var eighthTF: UITextField!
    
    
    var receivedDcsId: Int?
    var receivedEmpCode : String?
    var receivedSessionId : String?
    
    //MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dcsCodeTextField.text = receivedDcsPin ?? ""
        dcsCodeTextField.isUserInteractionEnabled = false
        
        firstTF.delegate = self
        secondTF.delegate = self
        thirdTF.delegate = self
        fourthTF.delegate = self
        fifthTF.delegate = self
        sixthTF.delegate = self
        seventhTF.delegate = self
        eighthTF.delegate = self
    }
    
//MARK: Confirm Button Action
    @IBAction func confirmButtonAction(_ sender: Any) {
        
        if self.firstTF.text == "" || self.secondTF.text == "" || self.thirdTF.text == "" || self.fourthTF.text == "" || self.fifthTF.text == "" || self.sixthTF.text == "" || self.seventhTF.text == "" || self.eighthTF.text == "" {
            
            self.alert(title: "Alert Message", message: "Please enter DCS code")
                   return
        }
        else {
            
        let userEnterPin =  self.firstTF.text! + self.secondTF.text! + self.thirdTF.text! + self.fourthTF.text!
        let enteredPinValue = userEnterPin
            
            
       let userConfirmPin = self.fifthTF.text! + self.sixthTF.text! + self.seventhTF.text! + self.eighthTF.text!
       let confirmPinValue = userConfirmPin
         
        if enteredPinValue == confirmPinValue {
    //http://poc.aquilasoftware.com/pocextacc-webservices_phase2/telephony/resetDCSPin?&dcsId=15744&empCode=123456&dcsPin=1111&sessionId=123456_W067MK2JH6ZXX5ZL (654ms)
                        
            let details = self.getuserDetails()
             
            let url = (details.baseURL ?? "")  +  API.resetPin + String(describing: receivedDcsId ?? 0)
                let empCode = API.empCode + (receivedEmpCode ?? "")
            let pin = API.pinCode + (enteredPinValue )
                let sessionId = API.SessionID + String(describing: receivedSessionId ?? "")
                        
                let finalUrl = url + empCode + pin + sessionId
                print("the reset pin url is :\(finalUrl)")
                        
            ServiceManager.shared.request(type: ResetPinModel.self, url: finalUrl, method: .get, view: self.view) { (response) in

                if (response?.dcsId ?? 0) >= 0 {
                    
                    self.toast(msgString: "Rest pin was succesfull", view: self.view)
                    DispatchQueue.main.asyncAfter(deadline: .now()+0.30, execute: {
//                        self.navigationController?.popViewController(animated: true)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
                            guard let homeController = self.storyboard?.instantiateViewController(withIdentifier: "scheduleExceptionsVC") as? ScheduleExceptionsVC else  { return }
                            homeController.receivedCallInFlage = self.receovedCallFlag
                        let navigationControler = UINavigationController(rootViewController: homeController)
                            navigationControler.modalPresentationStyle = .fullScreen
                            self.present(navigationControler, animated: false, completion: nil)
                                     }

                    })
                        
                    
                                        
                    }
                else {
                    self.toast(msgString: "Please check your rest and confirm password", view: self.view)
                }
                
                }
            
        }
        else {
            
    self.alert(title: "Alert Message", message: "Please check your rest and confirm password")
            
            
        }
            
                
        }
        
}
    
    
    //MARK: Eye Icon for Enter Pin
    @IBAction func eyeActionForPinAction(_ sender: Any) {
        
        if  firstTF.isSecureTextEntry {
            eyeIconForpin.setImage(UIImage(named: "Eye"), for: .normal)
            firstTF.isSecureTextEntry = false
            secondTF.isSecureTextEntry = false
            thirdTF.isSecureTextEntry = false
            fourthTF.isSecureTextEntry = false
        }
        else  {
            eyeIconForpin.setImage(UIImage(named: "Eye_01"), for: .normal)
            firstTF.isSecureTextEntry = true
            secondTF.isSecureTextEntry = true
            thirdTF.isSecureTextEntry = true
            fourthTF.isSecureTextEntry = true
        }
        
    }
    
    
    //MARK: Eye Icon for Confrim Pin Action
    @IBAction func eyeIconForConfirmPinAction(_ sender: Any) {
        
        if  fifthTF.isSecureTextEntry {
                   eyeIconForConfirmPin.setImage(UIImage(named: "Eye"), for: .normal)
                   fifthTF.isSecureTextEntry = false
                   sixthTF.isSecureTextEntry = false
                   seventhTF.isSecureTextEntry = false
                   eighthTF.isSecureTextEntry = false
               }
               else  {
                   eyeIconForConfirmPin.setImage(UIImage(named: "Eye_01"), for: .normal)
                   fifthTF.isSecureTextEntry = true
                   sixthTF.isSecureTextEntry = true
                   seventhTF.isSecureTextEntry = true
                   eighthTF.isSecureTextEntry = true
               }
    }
}

//MARK: UITextfield Delegate and Datasource Methods
extension ResetPinVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if ((textField.text?.count)! < 1 ) && (string.count > 0) {
            
            if textField == firstTF {
                secondTF.becomeFirstResponder()
            }
            
            if textField == secondTF {
               thirdTF.becomeFirstResponder()
            }
            
            if textField == thirdTF {
               fourthTF.becomeFirstResponder()
            }
            
            if textField == fourthTF {
                fourthTF.becomeFirstResponder()
            }
            
            if textField == fifthTF {
                sixthTF.becomeFirstResponder()
            }

            if textField == sixthTF{
                seventhTF.becomeFirstResponder()
            }

            if textField == seventhTF {
                eighthTF.becomeFirstResponder()
            }

            if textField == eighthTF{
                eighthTF.becomeFirstResponder()
            }

            textField.text = string
            return false
        }
        
        
        else if ((textField.text?.count)! >= 1) && (string.count == 0) {
            
            if textField == secondTF {
                firstTF.becomeFirstResponder()
            }
            if textField == thirdTF {
               secondTF.becomeFirstResponder()
            }
            if textField == fourthTF {
              thirdTF.becomeFirstResponder()
            }
            if textField == firstTF {
               firstTF.becomeFirstResponder()
            }
            
            if textField == sixthTF {
                fifthTF.becomeFirstResponder()
            }
            
            if textField == seventhTF {
               sixthTF.becomeFirstResponder()
            }
            
            if textField == eighthTF {
              seventhTF.becomeFirstResponder()
            }
            
            if textField == fifthTF {
                fifthTF.becomeFirstResponder()
            }
                      
            textField.text = ""
            return false
        }
        
        else if (textField.text?.count)! >= 1 {
            textField.text = string
            return false
        }
        
        return true
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if (textField == firstTF) {
            firstTF.layer.borderColor = UIColor.init(red: 118/255.0, green: 189/255.0, blue: 67/255.0, alpha: 1.0).cgColor
            firstTF.layer.borderWidth = 1.0
            firstTF.layer.cornerRadius = 5.0
            firstTF.layer.masksToBounds = false
        }
        else {
            firstTF.layer.borderColor = UIColor.init(red: 166/255.0, green: 185/255.0, blue: 197/255.0, alpha: 1.0).cgColor
        }
        
        
        if (textField == secondTF) {
            secondTF.layer.borderColor = UIColor.init(red: 118/255.0, green: 189/255.0, blue: 67/255.0, alpha: 1.0).cgColor
            secondTF.layer.borderWidth = 1.0
            secondTF.layer.cornerRadius = 5.0
            secondTF.layer.masksToBounds = false
        }
        else {
            secondTF.layer.borderColor = UIColor.init(red: 166/255.0, green: 185/255.0, blue: 197/255.0, alpha: 1.0).cgColor
        }
        
        
        if (textField == thirdTF) {
            thirdTF.layer.borderColor = UIColor.init(red: 118/255.0, green: 189/255.0, blue: 67/255.0, alpha: 1.0).cgColor
            thirdTF.layer.borderWidth = 1.0
            thirdTF.layer.cornerRadius = 5.0
            thirdTF.layer.masksToBounds = false
        }
        else {
            thirdTF.layer.borderColor = UIColor.init(red: 166/255.0, green: 185/255.0, blue: 197/255.0, alpha: 1.0).cgColor
        }
        
        
        if (textField == fourthTF) {
            fourthTF.layer.borderColor = UIColor.init(red: 118/255.0, green: 189/255.0, blue: 67/255.0, alpha: 1.0).cgColor
            fourthTF.layer.borderWidth = 1.0
            fourthTF.layer.cornerRadius = 5.0
            fourthTF.layer.masksToBounds = false
        }
        else {
            fourthTF.layer.borderColor = UIColor.init(red: 166/255.0, green: 185/255.0, blue: 197/255.0, alpha: 1.0).cgColor
        }
        
        
        if (textField == fifthTF) {
                   fifthTF.layer.borderColor = UIColor.init(red: 118/255.0, green: 189/255.0, blue: 67/255.0, alpha: 1.0).cgColor
                   fifthTF.layer.borderWidth = 1.0
                   fifthTF.layer.cornerRadius = 5.0
                   fifthTF.layer.masksToBounds = false
               }
               else {
                   fifthTF.layer.borderColor = UIColor.init(red: 166/255.0, green: 185/255.0, blue: 197/255.0, alpha: 1.0).cgColor
               }
        
        
        if (textField == sixthTF) {
                          sixthTF.layer.borderColor = UIColor.init(red: 118/255.0, green: 189/255.0, blue: 67/255.0, alpha: 1.0).cgColor
                          sixthTF.layer.borderWidth = 1.0
                          sixthTF.layer.cornerRadius = 5.0
                          sixthTF.layer.masksToBounds = false
                      }
                      else {
                          sixthTF.layer.borderColor = UIColor.init(red: 166/255.0, green: 185/255.0, blue: 197/255.0, alpha: 1.0).cgColor
                      }
        
        
        if (textField == seventhTF) {
                                 seventhTF.layer.borderColor = UIColor.init(red: 118/255.0, green: 189/255.0, blue: 67/255.0, alpha: 1.0).cgColor
                                 seventhTF.layer.borderWidth = 1.0
                                 seventhTF.layer.cornerRadius = 5.0
                                 seventhTF.layer.masksToBounds = false
                             }
                             else {
                                 seventhTF.layer.borderColor = UIColor.init(red: 166/255.0, green: 185/255.0, blue: 197/255.0, alpha: 1.0).cgColor
                             }
        
        
        
            if (textField == eighthTF) {
                                     eighthTF.layer.borderColor = UIColor.init(red: 118/255.0, green: 189/255.0, blue: 67/255.0, alpha: 1.0).cgColor
                                     eighthTF.layer.borderWidth = 1.0
                                     eighthTF.layer.cornerRadius = 5.0
                                     eighthTF.layer.masksToBounds = false
                                 }
                                 else {
                                     eighthTF.layer.borderColor = UIColor.init(red: 166/255.0, green: 185/255.0, blue: 197/255.0, alpha: 1.0).cgColor
                                 }
        
        
        
    }
    
}
