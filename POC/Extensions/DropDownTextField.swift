//
//  DropDownTextField.swift
//  POC
//
//  Created by Ajeet N on 16/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//


import Foundation
import IQKeyboardManagerSwift
import DropDown


protocol DropDownTextFieldDelegate {
    func onSelected(index: Int, item: String)
}

class DropDownTextField:UITextField,UITextFieldDelegate {
    
    public let shapeLayer = CAShapeLayer()

    public override func layoutSubviews() {
        super.layoutSubviews()
        self.borderStyle = .none
        setTopRightAndBottomLeftCornersForDropDown()
        self.shapeLayer.frame = self.bounds
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear {
           didSet {
               layer.borderColor = borderColor.cgColor
           }
       }
    
    let dropdowndelegate:DropDownTextFieldDelegate? = nil
    let dropDown:DropDown = DropDown()
    
    var datasource = [String]() {
        didSet {
            dropDown.dataSource = datasource
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        IQKeyboardManager.shared.enableAutoToolbar = false
        self.dropDown.show()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        IQKeyboardManager.shared.enableAutoToolbar = true
    }
    
    override func awakeFromNib() {
        dropDown.anchorView = self
        self.inputView = UIView()
        self.setLeftPaddingPoints(16)
        self.setRightPaddingPoints(16)
        self.setBoarderColor()
        self.rightImageView()
        self.delegate = self
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.resignFirstResponder()
            if let thedelegate = self.dropdowndelegate {
                thedelegate.onSelected(index: 0, item: item)
            }
        }
        
        dropDown.cancelAction = { () in
            self.resignFirstResponder()
        }
        
    }
}

extension UITextField {
    
    func setLeftPaddingPoints(_ amount:CGFloat){
           let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
           self.leftView = paddingView
           self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
           let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
           self.rightView = paddingView
           self.rightViewMode = .always
    }
    func setBoarderColor(){
        // self.layer.borderColor =  UIColor.lightGray.cgColor
        // self.layer.cornerRadius = 24.0
       // self.layer.borderWidth = 1.0
    }
    
    func rightImageView()
    {
        let view = UIView(frame: CGRect(x: bounds.size.width - 80, y: 0, width: 45 , height:40))
        let imageView = UIImageView(frame: CGRect(x: 0, y: 5, width: 30 , height:30))
        imageView.image = UIImage(named: "Dropdown_01")
        view.addSubview(imageView)
        self.rightView = view
        self.rightViewMode = .always
    }
    
        
}

extension  DropDownTextField  {
    
    func setTopRightAndBottomLeftCornersForDropDown(){
         let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topRight,.bottomLeft], cornerRadii: CGSize(width: 15, height: 15))
         shapeLayer.path = path.cgPath
         shapeLayer.frame = bounds
        shapeLayer.strokeColor = UIColor.init(red: 118/255.0, green: 189/255.0, blue: 67/255.0, alpha: 1.5).cgColor
        shapeLayer.cornerRadius = 1.0
         shapeLayer.fillColor = UIColor.white.cgColor
//        self.layer.masksToBounds = true
        self.layer.mask = shapeLayer
        self.layer.insertSublayer(shapeLayer, at: 0)
    }
}
