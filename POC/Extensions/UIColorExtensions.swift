//
//  UIColorExtensions.swift
//  POC
//
//  Created by Ajeet N on 04/03/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import Foundation
import UIKit


extension UIColor {
    static let gradientColor1 = UIColor(displayP3Red: 116/255.0, green: 177/255.0, blue: 243/255.0, alpha: 1)
    static let gradientColor2 = UIColor(displayP3Red: 20/255.0, green: 81/255.0, blue: 200/255.0, alpha: 1)
    static let greenColor = UIColor(displayP3Red: 118/255.0, green: 189/255.0, blue: 67/255.0, alpha: 0.5)
}
