//
//  UIButtonExtensions.swift
//  POC
//
//  Created by Ajeet N on 04/03/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//


import Foundation
import UIKit


@IBDesignable
public class CustomButton: UIButton {

    
@IBInspectable var borderColor: UIColor = UIColor.white {
        didSet{
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth : CGFloat = 2.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    let gradientLayer = CAGradientLayer()
    public override func layoutSubviews() {
        super.layoutSubviews()
         gradientLayer.frame = self.bounds
    }
    @IBInspectable var setGradient: Bool = false{
        didSet{
            if self.setGradient == true{
                if #available(iOS 10.0, *) {
                    gradientLayer.colors = [UIColor.gradientColor1.cgColor, UIColor.gradientColor2.cgColor]
                } else {
                    // Fallback on earlier versions
                }
                       gradientLayer.locations = [0.0, 1.0]
                gradientLayer.cornerRadius = 23
                layer.insertSublayer(gradientLayer, at: 0)
            }
        }
    }
}


//MARK: Class of Custom Green Button
@IBDesignable
public class CustomButtonC : UIButton {
public let shapeLayer = CAShapeLayer()

public override func layoutSubviews() {
    super.layoutSubviews()
    setTopRightAndBottomLeftCorners()
    self.shapeLayer.frame = self.bounds
}


    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }

    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
           didSet {
               layer.cornerRadius = cornerRadius
           }
       }
       @IBInspectable var paddingLeft: CGFloat = 0
       @IBInspectable var paddingRight: CGFloat = 0

}


//MARK: Class of Custom Blue Button
@IBDesignable
public class CustomButtonBlue : UIButton {
public let shapeLayer = CAShapeLayer()

public override func layoutSubviews() {
    super.layoutSubviews()
    setTopRightAndBottomLeftCornersNew()
    self.shapeLayer.frame = self.bounds
}


    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }

    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
           didSet {
               layer.cornerRadius = cornerRadius
           }
       }
       @IBInspectable var paddingLeft: CGFloat = 0
       @IBInspectable var paddingRight: CGFloat = 0

}



//MARK: Custom Green Button Extension
extension CustomButtonC {
    func setTopRightAndBottomLeftCorners(){
             let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topRight,.bottomLeft], cornerRadii: CGSize(width: 15, height: 15))
             shapeLayer.path = path.cgPath
             shapeLayer.frame = bounds
//        shapeLayer.backgroundColor = UIColor.init(red: 118/255.0, green: 189/255.0, blue: 67/255.0, alpha: 1.5).cgColor
            shapeLayer.strokeColor = UIColor.init(red: 118/255.0, green: 189/255.0, blue: 67/255.0, alpha: 1.5).cgColor
            shapeLayer.cornerRadius = 1.0
             shapeLayer.fillColor = UIColor.init(red: 118/255.0, green: 189/255.0, blue: 67/255.0, alpha: 1.5).cgColor
//            self.layer.masksToBounds = true
            self.layer.mask = shapeLayer
            self.layer.insertSublayer(shapeLayer, at: 0)
        }
    
}


// MARK: Custom Blue Button Extension
extension  CustomButtonBlue {
    
       func setTopRightAndBottomLeftCornersNew(){
                 let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topRight,.bottomLeft], cornerRadii: CGSize(width: 15, height: 15))
                 shapeLayer.path = path.cgPath
                 shapeLayer.frame = bounds
    //        shapeLayer.backgroundColor = UIColor.init(red: 118/255.0, green: 189/255.0, blue: 67/255.0, alpha: 1.5).cgColor
                shapeLayer.strokeColor = UIColor.init(red: 0/255.0, green: 173/255.0, blue: 240/255.0, alpha: 1.5).cgColor
                shapeLayer.cornerRadius = 1.0
                 shapeLayer.fillColor = UIColor.init(red: 0/255.0, green: 173/255.0, blue: 240/255.0, alpha: 1.5).cgColor
    //            self.layer.masksToBounds = true
                self.layer.mask = shapeLayer
                self.layer.insertSublayer(shapeLayer, at: 0)
            }
}
