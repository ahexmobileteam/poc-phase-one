//
//  ProfileViewController.swift
//  POC
//
//  Created by Ajeet N on 12/03/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    
    let transiton = SlideInTransition()
    var topView: UIView?
    var leftArray = ["Name", "Enterprise ID", "Office", "Coordinator", "Location", "Phone 1", "Phone 2", "Phone 3", "Email", "Street", "Suit", "City", "State", "Zip Code"]
    var rightArray = ["Adam, John", "123", "SHC-ROME", "Yes", "Home", "67875454", "54545453", "9090909090", "test@gmail.com", "763 Brock Road", "No", "Rockmart", "Georgia", "30153"]
    
    @IBOutlet weak var tableViewObj: UITableView!
    
    
    
    //MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureNavigationBar(title: "Profile")
        self.tableViewObj.layer.borderColor = UIColor.lightGray.cgColor
        self.tableViewObj.layer.borderWidth = 1.0
        self.tableViewObj.layer.cornerRadius = 3.0
        self.tableViewObj.tableFooterView = UIView()
        print("Profile controller called!")
        
    }
    
    
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.ProfileAPICalling()
        
    }
    
    //MARK: Profile API Calling
    func ProfileAPICalling() {
        
          let details = self.getuserDetails()
        
        let profile = (details.baseURL ?? "")  + API.profileAPI + (details.dcsID ?? "")
        let sessionIDURL = API.SessionID + (details.sessionID ?? "")
        let profileUrl = profile + sessionIDURL
        print("the profile url is :\(profileUrl)")
        ServiceManager.shared.request(type: ProfileModel.self, url: profileUrl, method: .get, view: self.view) { (response) in
            print("the profile api response is :\(String(describing: response))")
            
        }
        
    }
    
    
    //MARK: Menu Bar Button Item
    @IBAction func menuBarButtonItem(_ sender: UIBarButtonItem) {
        
            
    }
    

}

//MARK: UITableView Controller Delegate and Datasource Methods
extension ProfileViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath) as! ProfileCell
        cell.leftLabel.text = leftArray[indexPath.row]
        cell.rightLabel.text = rightArray[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 70
       }

}
