//
//  ProfileModel.swift
//  POC
//
//  Created by Ajeet N on 08/04/20.
//  Copyright © 2020 AhexTechnologies. All rights reserved.
//

import Foundation


// MARK: - Welcome
struct ProfileModel: Codable {
    let zipCode, city, street, name: String?
    let location, office, state, phone1, phone2, phone3: String?
    let coordinator : String?
    let enterpriseId, email: String?
}
